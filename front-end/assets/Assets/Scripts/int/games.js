﻿var GameEnum = {
    Vendor: { Slot: "slot", LiveCasino: "livecasino" },
    View: { Grid: "grid", List: "list" }
}

function _startGame(gameProvider, gameId, isDemo, isDownload, gameCode, gameType) {
    /* provider specific checking placed here */
    _openGame(gameProvider, gameId, isDemo, isDownload, gameCode, gameType);
}

function _openGame(gameProvider, gameId, isDemo, isDownload, gameCode, gameType) {
    var popupTitle = "Product-" + (Product._singlePopUpProvider.indexOf(gameProvider) !== -1
        ? gameProvider
        : gameId + isDemo === 1 ? "-demo" : "-real");
    var popupHeight = 630;
    var popupWidth = 1060;


    var launchGameUrl;

    if (document.location.protocol == "https:" && $.inArray(gameProvider.toUpperCase(), Helper.SSLVendorList) == -1) {
        launchGameUrl = "http://" + sslRedirectSubdomain + "." + document.location.hostname.match(/.*\.(.*\..*)/)[1] + "/Gamelobby/" + (isDownload === 1 ? "Download/" : "Desktop/") + gameProvider + "/" + gameId + "/" + (gameCode === 0 ? "0" : gameCode) + "/" + (gameType === 0 ? "0" : gameType) + "/" + isDemo;
    } else {
        launchGameUrl = "/Gamelobby/" + (isDownload === 1 ? "Download/" : "Desktop/") + gameProvider + "/" + gameId + "/" + (gameCode === 0 ? "0" : gameCode) + "/" + (gameType === 0 ? "0" : gameType) + "/" + isDemo;
    }


    Page.popupWindow(launchGameUrl , popupTitle, popupWidth, popupHeight);
}

function FilterCasinoVendor(vendor) {
    window.scrollTo(0, 0);
    $('input.chkBoxGameVendor[type="checkbox"][value="' + vendor + '"]').click();
    $('input.chkBoxGameProductGroup[type="checkbox"][value="' + vendor + '"]').click();
}
