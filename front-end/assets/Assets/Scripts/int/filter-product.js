﻿// To keep our code clean and modular, all custom functionality will be contained inside a single object literal called "multiFilter".

var multiFilter = {

    // Declare any variables we will need as properties of the object
    $filterGroups: null,
    $filterUi: null,
    $reset: null,
    groups: [],
    outputArray: [],
    outputString: '',
    sortString: '',
    minPayLine: 0,
    maxPayLine: 1024,

    // The "init" method will run on document ready and cache any jQuery objects we will need.

    init: function () {
        var self = this; // As a best practice, in each method we will asign "this" to the variable "self" so that it remains scope-agnostic. We will use it to refer to the parent "checkboxFilter" object so that we can share methods and properties between all parts of the object.

        self.$filterUi = $('#Filters');
        self.$filterGroups = $('.filter-group');
        self.$mobileFilterGroups = $('.mobile-filter-group');
        self.$mobileFilterBtn = $('.mobileFilterBtn');
        self.$reset = $('.clearFilterGames');
        self.$mobileReset = $('.mobileClearFilterBtn');
        self.$filterGameType = $('.gamefilter');
        self.$filterGamePreference = $('.preferenceIcons');
        self.$filterGameName = $('.alphabetSorting');
        self.$filterDefaultGameName = $('.defaultAlphabetSorting');
        self.$allGames = $('.totalGamesCount');
        self.$container = $('#slotContainer');
        self.$slotViewbtn = $('.imagePopulateView');
        self.$minFilterVal = null;
        self.$maxFilterVal = null;
        self.$slotView = "grid";
        self.$filterGroups.each(function () {
            self.groups.push({
                $inputs: $(this).find('input'),
                active: [],
                tracker: false
            });
        });
        self.$mobileFilterGroups.each(function () {
            self.groups.push({
                $inputs: $(this).find('input'),
                active: [],
                tracker: false
            });
        });

        self.bindHandlers();
    },

    // The "bindHandlers" method will listen for whenever a form value changes. 

    bindHandlers: function () {
        var self = this,
            typingDelay = 300,
            typingTimeout = -1,
            resetTimer = function () {
                clearTimeout(typingTimeout);
                typingTimeout = setTimeout(function () {
                    self.parseFilters();
                }, typingDelay);
            };

        self.$filterGroups
          .filter('.checkboxes')
            .on('change', function () {
                self.parseFilters();
            });

        self.$filterGroups
          .filter('.search')
          .on('keyup change', resetTimer);

        self.$filterGroups
          .filter('.payLineValue')
          .on('change', function () {
              self.parseFilters();
          });

        self.$mobileFilterBtn.on('click', function (e) {
            $('input[name="mobileChkBox"][type="hidden"]').val = '';

            if ($('.mobileFilterSortSection').is(":visible")) {
                $('.mobileFilterSortSection').slideUp(200, function () {
                    $('.slot-banner-Wrapper').removeClass("hide");
                    $('.mobileFilterSortSection').removeClass("sectionOpenedMobile");
                });
            }
            var arrMobilVendor = [];
            var arrMobilProduct = [];
            var arrMobilFeatures = [];
            var arrCombine = [];
            var combineHidden = "";
            self.sortString = '';
            e.preventDefault();
            $('.myBoardHolder').empty();
            $('.mobileRadioBoxWrap').each(function () {
                if ($(this).hasClass("active")) {
                    self.sortString = $(this).attr("data-type");
                }
            });
            $('input[name="mobile"][type="checkbox"]').each(function () {
                if ($(this).prop("checked")) {
                    $('.chkBoxFilterResult[value="' +$(this).prop("value") + '"]').icheck('checked');
                    arrMobilVendor.push($(this).attr("value"));
                }
            });
            $('input[name="mobileGameType"][type="checkbox"]').each(function () {
                if ($(this).prop("checked")) {
                    $('.chkBoxFilterResult[value="' + $(this).prop("value") + '"]').icheck('checked');
                    arrMobilProduct.push($(this).attr("value"));
                }
            });
            $('input[name="mobileGameFeatures"][type="checkbox"]').each(function () {
                if ($(this).prop("checked")) {
                    $('.chkBoxFilterResult[value="' + $(this).prop("value") + '"]').icheck('checked');
                    arrMobilFeatures.push($(this).attr("value"));
                }
            });
            if (arrMobilProduct.length > 0 && arrMobilFeatures.length > 0) {
                $.each(arrMobilVendor, function (i, k) {
                    $.each(arrMobilProduct, function (j, v) {
                        $.each(arrMobilFeatures, function (k, p) {
                            arrCombine.push(arrMobilVendor[i] + arrMobilProduct[j] + arrMobilFeatures[k]);
                        });
                    });
                });
                combineHidden = arrCombine.join(',');
            }
            else if (arrMobilProduct.length > 0) {
                if (arrMobilVendor.length > 0) {
                    $.each(arrMobilVendor, function (i, k) {
                        $.each(arrMobilProduct, function (j, v) {
                            arrCombine.push(arrMobilVendor[i] + arrMobilProduct[j]);
                        });
                    });
                } else {
                    $.each(arrMobilProduct, function (j, v) {
                        arrCombine.push(arrMobilProduct[j]);
                    });
                }
                combineHidden = arrCombine.join(',');
            }
            else if (arrMobilFeatures.length > 0) {
                if (arrMobilVendor.length > 0) {
                    $.each(arrMobilVendor, function (i, k) {
                        $.each(arrMobilFeatures, function (k, p) {
                            arrCombine.push(arrMobilVendor[i] + arrMobilFeatures[k]);
                        });
                    });
                } else {
                    $.each(arrMobilFeatures, function (k, p) {
                        arrCombine.push(arrMobilFeatures[k]);
                    });
                }
                combineHidden = arrCombine.join(',');
            }
            else {
                combineHidden = arrMobilVendor.join(',');
            }
            $('input[name="mobileChkBox"][type="hidden"]').val(combineHidden);

            self.parseFilters();
        });

        self.$filterGameType.on('click', function (e) {
            self.sortString = '';
            e.preventDefault();
            $('.myBoardHolder').empty();
            if (!$(this).hasClass("active")) {
                $('.gamefilter,.defaultAlphabetSorting').removeClass("active");
                $(this).addClass("active");
            }
            var filtername = $(this).attr("data-type");
            if (filtername == "myBoard") {
                $('.text-no-game-found').hide();
                $('#noGameFoundSection').hide();
                $('#lessRecentlyPlayedGameFound span').hide();
                $('#favPlayedGame span').hide();
                self.$slotView = "grid";
                $('.gameMasterIcon').removeClass("active");
               
                GrepGameBoardGamesDetails();
              
                if ($('#lastPlayedGamesHolder > div').length > 0) {
                    $('#lastPlayedGamesHolder').parents(".slotholder").removeClass("hide").addClass("show");
                }
               
                var mixer = mixitup('#slotContainer', {
                    animation: {
                        enable: false
                    },
                    selectors: {
                        target: '.mix-target',
                        control: '[data-mixitup-control]'
                    }
                });
                $('#firstLoadPanel').show();
                mixer.destroy();
                $('.gameMasterIcon[data-view="' + self.$slotView + '"]').removeClass("active");
                return;
            } else {
                $('#firstLoadPanel').hide();
            }
            self.sortString = filtername;
            self.parseFilters();
        });

        self.$filterDefaultGameName.on('click', function (e) {
            self.sortString = '';
            $('.myBoardHolder').empty();
            //$('.slotsGamesList > .slotGamePanel').show();
            if (!$(this).hasClass("active")) {
                $('.gamefilter').removeClass("active");
                $(this).addClass("active");
            }
            var filtername = $(this).attr("data-order");
            if (filtername == "name:asc") {
                $(this).attr("data-order", "name:desc");
                $(this).html("A-Z");
            } else {
                $(this).attr("data-order", "name:asc");
                $(this).html("Z-A");
            }
            self.sortString = filtername;
            self.parseFilters();
        });

        self.$slotViewbtn.on("click", function (e) {
            $('.imagePopulateView').removeClass("active");
            $(this).addClass("active");
            var view = $(this).attr("data-view");
            if (view == "list") {
                DisplayListView();
            } else {
                DisplayGridView();
            }
            self.$slotView = view;
            e.preventDefault();
            $('.myBoardHolder').empty();
            //$('.slotsGamesList > .slotGamePanel').show();
            self.parseFilters();
        });

        self.$allGames.on('click', function (e) {
            self.sortString = '';
            e.preventDefault();
            $('.myBoardHolder').empty();
            $('.slotsGamesList > .slotGamePanel').show();
            self.parseFilters();
        });

        self.$reset.on('click', function (e) {
            e.preventDefault();
            //self.$filterUi[0].reset();
            $('input[type="text"]').val('');
            self.outputString = 'all';
            self.parseFilters();
        });

        self.$mobileReset.on('click', function (e) {
            e.preventDefault();
            //self.$filterUi[0].reset();
            $('input[type="text"]').val('');
            $('input[type="hidden"]').val('');
            self.outputString = 'all';
            self.parseFilters();
        });
    },

    // The parseFilters method checks which filters are active in each group:

    parseFilters: function () {
        var self = this;

        // loop through each filter group and add active filters to arrays

        for (var i = 0, group; group = self.groups[i]; i++) {
            group.active = []; // reset arrays
            group.$inputs.each(function () {
                var searchTerm = '',
                        $input = $(this),
                    minimumLength = 1;

                if ($input.is(':checked') && !$input.is('[name="mobile"]') && !$input.is('[name="mobileGameType"]') && !$input.is('[name="mobileGameFeatures"]')) {
                    //$('input[type="hidden"]').val('');
                    group.active.push(this.value);
                }

                //if ($input.is(':checked')) {
                //    group.active.push(this.value);
                //}

                if ($input.is('[type="hidden"]') && !$input.is('[name="mobileChkBox"]') && this.value.length > 0) {
                    group.active.push(this.value);
                    //$('input[type="hidden"]').val('');
                }

                if ($input.is('[type="text"][name="payLineRangeName"]') && this.value.length > 0) {
                    var min = this.value.split(';')[0];
                    var max = this.value.split(';')[1];

                    self.$minFilterVal = min;
                    self.$maxFilterVal = max;
                    group.active.push('.activePayLine');
                }
                if ($input.is('[type="text"][name!="payLineRangeName"]') && this.value.length >= minimumLength) {
                    searchTerm = this.value
                        .trim()
                        .toLowerCase();
                    group.active[0] = '[data-name*="' + searchTerm + '"]';
                }
            });
            group.active.length && (group.tracker = 0);
        }
        
        self.concatenate();
    },

    // The "concatenate" method will crawl through each group, concatenating filters as desired:

    concatenate: function () {
        var self = this,
              cache = '',
              crawled = false,
              checkTrackers = function () {
                  var done = 0;

                  for (var i = 0, group; group = self.groups[i]; i++) {
                      (group.tracker === false) && done++;
                  }

                  return (done < self.groups.length);
              },
          crawl = function () {
              for (var i = 0, group; group = self.groups[i]; i++) {
                  group.active[group.tracker] && (cache += group.active[group.tracker]);

                  if (i === self.groups.length - 1) {
                      self.outputArray.push(cache);
                      cache = '';
                      updateTrackers();
                  }
              }
          },
          updateTrackers = function () {
              for (var i = self.groups.length - 1; i > -1; i--) {
                  var group = self.groups[i];

                  if (group.active[group.tracker + 1]) {
                      group.tracker++;
                      break;
                  } else if (i > 0) {
                      group.tracker && (group.tracker = 0);
                  } else {
                      crawled = true;
                  }
              }
          };

        self.outputArray = []; // reset output array

        do {
            crawl();
        }
        while (!crawled && checkTrackers());
        self.outputString = self.outputArray.join();
        // If the output string is empty, show all rather than none:

        !self.outputString.length && (self.outputString = 'all');

        //console.log(self.outputString);

        // ^ we can check the console here to take a look at the filter string that is produced

        // Send the output string to MixItUp via the 'filter' method:

        //if (self.$container.mixitup('isLoaded')) {
        //    self.$container.mixitup('filter', self.outputString);
        //}

        var mixer = mixitup('#slotContainer', {
            animation: {
                enable: false
            },
            selectors: {
                target: '.mix-target',
                control: '[data-mixitup-control]'
            }
        });

        if (self.$minFilterVal != null && self.$maxFilterVal != null) {
            $('.slotGamePanel').removeClass("activePayLine");
            $('.slotGamePanel').each(function () {
                var payline = Number($(this).attr('data-payline'));
                var isFallUnderPayline = payline >= self.$minFilterVal && payline <= self.$maxFilterVal;
                if (isFallUnderPayline) {
                    $(this).addClass("activePayLine");
                }
            });
        }
        $('.slotsGamesList').find("#lessRecentlyPlayedGameFound").remove();
        $('.slotsGamesList').find("#favPlayedGame").remove();
        
        mixer.filter(self.outputString, function (state) {
            var numItems = state.totalMatching;
            $('#firstLoadPanel').hide();
            $('.totalSlotsNumbers').text(numItems);
            if (numItems === 0 || ((self.sortString.toLowerCase() == "recent:asc" && (Cookie.Get("_lastPlayedGame") === "" || typeof Cookie.Get("_lastPlayedGame") == "undefined")) ||
            (self.sortString.toLowerCase() == "fav:asc" && (Cookie.Get("_favouritesGame") === "" || typeof Cookie.Get("_favouritesGame") == "undefined")))) {
                $('#noGameFoundSection').show();
                $('.text-no-game-found').hide();
                if ((self.sortString.toLowerCase() == "recent:asc" && (Cookie.Get("_lastPlayedGame") === "" || typeof Cookie.Get("_lastPlayedGame") == "undefined"))) {
                    $('.text-no-game-found[data-option="nolastplayedgame"]').show();
                }
                else if ((self.sortString.toLowerCase() == "fav:asc" && (Cookie.Get("_favouritesGame") === "" || typeof Cookie.Get("_favouritesGame") == "undefined"))) {
                    $('.text-no-game-found[data-option="nofavoritegame"]').show();
                }
                else {
                    $('.text-no-game-found[data-option="nogamesfound"]').show();
                    mixer.filter("all");
                }
            } else {
                $('#noGameFoundSection').hide();
            }
        });
        //mixer.destroy();
        if (self.sortString != undefined && self.sortString.length) {
            //mixer.sort('default');
            $('#firstLoadPanel').hide();
            $('.gameMasterIcon[data-view="' + self.$slotView + '"]').addClass("active");
            if (self.sortString.toLowerCase() !== "recent:asc" && self.sortString.toLowerCase() !== "fav:asc") {
                mixer.sort(self.sortString);
            }
            if (self.sortString.toLowerCase() == "recent:asc" && (Cookie.Get("_lastPlayedGame") !== "" && typeof Cookie.Get("_lastPlayedGame") != "undefined")) {

                var arrRecGameId = Cookie.Get("_lastPlayedGame").split(',');
                var numberOfRecentlyPlayedGame = Cookie.Get("_lastPlayedGame").split(',').length;
                if (numberOfRecentlyPlayedGame != null) {
                    $('.lastPlayedGameCount').html(numberOfRecentlyPlayedGame);
                }

                $.each(arrRecGameId, function (i, k) {
                    $('.slotGamePanel[data-game-id="' + k + '"]').attr("data-recent", "0");
                });
                if (numberOfRecentlyPlayedGame != null) {
                    $('.lastPlayedGameCount').html(numberOfRecentlyPlayedGame);
                }

                mixer.sort(self.sortString, function () {
                    if (self.outputString.toLowerCase().indexOf("data-name*=") < 0) {
                        $('.slotsGamesList > .slotGamePanelBoxWrap[data-recent="0"]').last().after($('#lessRecentlyPlayedGameFound').clone(true));
                    }
                });

            }
            if (self.sortString.toLowerCase() == "fav:asc" && (Cookie.Get("_favouritesGame") !== "" && typeof Cookie.Get("_favouritesGame") != "undefined")) {
                var numberOfFavGame = Cookie.Get("_favouritesGame").split(',').length;
                if (numberOfFavGame != null) {
                    $('.favPlayedGameCount').html(numberOfFavGame);
                }
                mixer.sort(self.sortString, function () {
                    if (self.outputString.toLowerCase().indexOf("data-name*=") < 0) {
                        $('.slotsGamesList > .slotGamePanelBoxWrap[data-fav="0"]').last().after($('#favPlayedGame').clone(true));
                    }
                });
            }
        } else {
            $('.gamefilter').removeClass("active");
            $('.gamefilter[data-type="myBoard"]').addClass("active");
            if (Helper.RegionCode !== "ID") {
                $('.gamefilter[data-type="order:asc"]').addClass("active");
                if ($('.gamefilter[data-type="myBoard"]').hasClass("active")) {
                    $('.recommendGame').removeClass("active");
                }               
                mixer.sort("order:asc");
            }
            $('#firstLoadPanel').hide();
            $('.gameMasterIcon[data-view="' + self.$slotView + '"]').addClass("active");
        }
    }
};

// On document ready, initialise our code.

$(function () {
    multiFilter.init();

    //console.log(filtered.length);
    $('.totalSlotsNumbers').html($('.slotsGamesList > .slotGamePanel').length);
});