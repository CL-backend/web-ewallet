var Payment = {
    LocalBank: function (method) {
        var dt = new Date();
        var dtHour = dt.getHours();
        var dtMinute = dt.getMinutes();
        var dtSecond = dt.getSeconds();
        var memberBankAcccount = $('#ddlMemberAccount option').length > 0;
        if (dtHour < 10) {
            dtHour = "0" + dtHour;
        }
        if (dtMinute < 10) {
            dtMinute = "0" + dtMinute;
        }
        if (dtSecond < 10) {
            dtSecond = "0" + dtSecond;
        }
        $('#dateDeposit').val(dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear());
       
        if (method !== "wechat" && Helper.CurrencyCode.toLowerCase() == 'idr') {
            $('#ddlHours').val(dtHour);
            $('#ddlMinutes').val(dtMinute);
            $('#ddlSeconds').val(dtSecond);
        }
        $('#GenerateDetails').on("click", function () {
            if ($("input[name='depositMethod']:checked").val() != undefined) {
                $('#GenerateDetails').parent().hide();
                $('.account-details').toggle("slow");
                $('#stepTwoTick').addClass("icon").addClass("icon-tick-circle-button");
                if ($("#HidResubmitSetting").val() == "" || $("#HidResubmitSetting").val() == undefined) {
                    $("#steps").val(0);
                }
                LocalBank.BindMerchantDetail($("input[name='depositMethod']:checked").val());
            } else {
                Page.message(LB_ChooseMerchantBank);
            }
        });

        $('#ConfirmDetails').on("click", function () {
            if ($("input[name='depositMethod']:checked").val() != undefined) {
                LocalBank.BindMerchantDetail($("input[name='depositMethod']:checked").val());
            }
                $('.hide_LastFive_VND').css("display", "none");
                $('.hide_TransNumber_VND').css("display", "none");
                $('.hide_LastFive_THB').css("display", "none");
                $('.hide_TransNumber_THB').css("display", "none");
                $('#RefNoRemark_Vietcom').hide();
                $('#RefNoRemark_Techcom').hide();
                $('#RefNoRemark_Sacom').hide();
                $('#RefNoRemark_Other').hide();
                if ($("input[name='depositMethod']:checked").val() == "9999") {
                    $('.hide_LastFive_VND').css("display", "block");
                    $('.hide_LastFive_THB').css("display", "block");
                } else if ($("input[name='depositMethod']:checked").val() == "2243") {
                    $('.hide_TransNumber_VND').css("display", "block");
                }
                if (Helper.CurrencyCode.toLowerCase() == "vnd") {
                    var strVal = $("input[name='depositMethod']:checked").next().attr('data-bankName');
                    $(".hide_Ref_VND").hide();
                    if (strVal == "Nếu bạn không tìm thấy tài khoản ngân hàng sử dụng để gửi tiền, vui lòng điền vào 6 chữ số cuối của số tài khoản." || strVal == "If you cannot find the bank account you used for the deposit, please fill in the last 6 digits of the bank account number.") {
                        $(".hide_Ref_VND").show();
                        $("#RefNoRemark_Other").show();
                        $("#txtRefNumber").next().hide();
                        $("#txtRefNumber-error").hide();
                    }
                    else {
                        $("#RefNoRemark_Other").hide();
                        $("#txtRefNumber").next().show();
                    }
                    if (strVal == "Ngân hàng Ngoại Thương" || strVal == "Ngân hàng Ngoại Thương (Vietcombank)" || strVal == "Vietcom Bank") {
                        $('#RefNoRemark_Vietcom').show();
                        $(".hide_Ref_VND").show();
                    }
                    else {
                        $('#RefNoRemark_Vietcom').hide();
                    }

                    if (strVal == "Ngân hàng Kỹ Thương Việt Nam" || strVal == "Ngân hàng Kỹ Thương Việt Nam (Techcombank)" || strVal == "Techcom Bank") {
                        $('#RefNoRemark_Techcom').show();
                        $(".hide_Ref_VND").show();
                    }
                    else {
                        $('#RefNoRemark_Techcom').hide();
                    }

                    if (strVal == "Ngân hàng Sài Gòn Thương Tín" || strVal == "Ngân hàng Sài Gòn Thương Tín (Sacombank)" || strVal == "Sacom Bank") {
                        $('#RefNoRemark_Sacom').show();
                        $(".hide_Ref_VND").show();
                    }
                    else {
                        $('#RefNoRemark_Sacom').hide();
                    }
                }
                if ($("#steps").val() != 2) {
                    LocalBank.Reset();
                    LocalBank.ResetMemberAccountField();
                    $('#ddlNewAccountBankName').val($("#ddlNewAccountBankName option:first").val());
                    if (Helper.CurrencyCode.toLowerCase() == "cny" || Helper.CurrencyCode.toLowerCase() == "rmb") {
                        $('#ddlBankType').val($("#ddlBankType option[value=Alipay]").val());
                    } else {
                        $('#ddlBankType').val($("#ddlBankType option:first").val());
                    }
                    $('#ddlMemberAccount').val($("#ddlMemberAccount option:first").val());
                    $('#ConfirmDetails').parent().hide();
                    $('.transactionDetails').toggle("slow");
                    $('#stepThreeTick').addClass("icon").addClass("icon-tick-circle-button");

                    var dt = new Date();
                    var dtHour = dt.getHours();
                    var dtMinute = dt.getMinutes();
                    var dtSecond = dt.getSeconds();
                    if (dtHour < 10) {
                        dtHour = "0" + dtHour;
                    }
                    if (dtMinute < 10) {
                        dtMinute = "0" + dtMinute;
                    }
                    if (dtSecond < 10) {
                        dtSecond = "0" + dtSecond;
                    }
                    $('#dateDeposit').val(dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear());
                    if (method !== "wechat" && Helper.CurrencyCode.toLowerCase() == 'idr') {
                        $('#ddlHours').val(dtHour);
                        $('#ddlMinutes').val(dtMinute);
                        $('#ddlSeconds').val(dtSecond);
                    }
                    if ($("#HidResubmitSetting").val() == "" || $("#HidResubmitSetting").val() == undefined) {
                        $("#steps").val(2);
                    }
                }
        });

        $('#btnNew').on("click", function () {
            LocalBank.ResetMemberAccountField();
            $("#btnPreferredField").css("display", "block");
            $("#preferredAccount").css("display", "none");
            $("#btnNewField").css("display", "none");
            $("#newAccount").css("display", "block");
            $("#newOld").val('0');
        });

        $('#btnPreferred').on("click", function () {
            if (memberBankAcccount == "") {
            } else {
                LocalBank.ResetMemberAccountField();
                $("#btnPreferredField").css("display", "none");
                $("#preferredAccount").css("display", "block");
                $("#btnNewField").css("display", "block");
                $("#newAccount").css("display", "none");
                $("#newOld").val('1');
            }
        });

        $("#ddlNewAccountBankName").change(function () {
            if ($("#ddlNewAccountBankName").val() === "0") {
                $("#otherBank").css("display", "block");
            } 
            else {
                $("#otherBank").css("display", "none");
            }
        })

        $('.no-copy').on("click", function () {
            if ($(this).hasClass("cn")) {
                $('.copied').removeClass().addClass("no-copy cn");
                $(this).removeClass().addClass("copied");
                $('.no-copy.cn').prev().css("display", "none");
                $(this).prev().css("display", "inline-block");
            } else {
                $('.copied').removeClass().addClass("no-copy");
                $(this).removeClass().addClass("copied");
                $('.no-copy').prev().css("display", "none");
                $(this).prev().css("display", "inline-block");
            }
        });
    },
    LocalBankResubmit: function () {
        var resubmitSetup = $("#HidResubmitSetting").val();
        if (resubmitSetup != undefined && resubmitSetup != "") {
            var lastTransDetail = JSON.parse(resubmitSetup);
            var transactionDate = lastTransDetail.DepositDate;
            var hour = transactionDate.split(':')[0];
            var minute = transactionDate.split(':')[1];
            var second = transactionDate.split(':')[2];
            if (lastTransDetail.MerchantBankCode == "") {
                lastTransDetail.MerchantBankCode = "9999";
            }
            var merchantSelection = $('input[name=depositMethod][id=' + lastTransDetail.MerchantBankCode + ']');
            if (merchantSelection != undefined && merchantSelection != null) {
                merchantSelection.click().change();
                $('#GenerateDetails').trigger("click");
                $('#ConfirmDetails').trigger("click");
                $('#txtAmount').val(parseFloat(lastTransDetail.Amount).toFixed(2));
                $('#txtRefNumber').val(lastTransDetail.TransRefNo);
                if (lastTransDetail.Attachment != null && lastTransDetail.Attachment != "") {
                    $('#fileName').val(lastTransDetail.Attachment);
                }
                $('#ddlBankType').val(lastTransDetail.BankTypeId);
                $('#ddlHours').val(hour);
                $('#ddlMinutes').val(minute);
                $('#ddlSeconds').val(second);
                if (lastTransDetail.PreferredAccountId != null && lastTransDetail.PreferredAccountId != "") {
                    $('#ddlMemberAccount').val(lastTransDetail.PreferredAccountId);
                } else {
                    $('#btnNew').trigger("click");
                }
            }
        }
        $("#HidResubmitSetting").val("");
    },
    Yeepay: function () {
        $('#AmountDDL').change(function () {
            $('#txtDisplayAmount').val($('#AmountDDL').val());
            $('#txtTotalAmount').val(Yeepay.CalculateValue);
        });
    },
    AstroPay: function () {
        $('#txtAmount').change(function () {
            AstroPay.RecalculateAmount();
        });
        $('#txtAstroNumber').change(function () {
            AstroPay.RecalculateAmount();
        });
    },
    Wechat: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            } else {
                $('#ActualAmount').val('');
            }
        });
    },
    OnlineAlipay: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            }else{
                $('#ActualAmount').val('');
            }
        });
    },
    QQWallet: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            } else {
                $('#ActualAmount').val('');
            }
        });
    },
    XPayQR: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            } else {
                $('#ActualAmount').val('');
            }
        });
    },
    UnionPay: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            } else {
                $('#ActualAmount').val('');
            }
        });
    },
    JDPay: function () {
        $('#txtAmount').change(function () {
            if ($('#txtAmount').val() != "") {
                var afterCharges = PaymentCommonFunction.CalculateChargesAmount($('#hidCharges').val(), $('#txtAmount').val());
                if (!isNaN(afterCharges)) {
                    $('#ActualAmount').val(afterCharges);
                } else {
                    $('#ActualAmount').val('');
                }
            } else {
                $('#ActualAmount').val('');
            }
        });
    }
};

var PaymentCommonFunction = {
    GetUrlParameter: function () {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
        if (sPageURL != "") {
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');

                if (sParameterName[0] === "transid") {
                    return sParameterName[1] === (undefined || "") ? true : sParameterName[1];
                }
            }
        }
        return true;
    },

    CalculateChargesAmount: function (paymentCharges, paymentAmount) {
        var charges = 1 - parseFloat(paymentCharges);
        var amount = parseFloat(paymentAmount);
        return parseFloat(amount * charges).toFixed(2);
    },

    ResetForm: function () {
        var dt = new Date();
        var dtMonth = dt.getMonth() + 1;
        if (dtMonth < 10) {
            dtMonth = "0" + dtMonth;
        }
        var dtYear = dt.getFullYear();
        var dtHour = dt.getHours();
        var dtMinute = dt.getMinutes();
        var dtSecond = dt.getSeconds();
        if (dtHour < 10) {
            dtHour = "0" + dtHour;
        }
        if (dtMinute < 10) {
            dtMinute = "0" + dtMinute;
        }
        if (dtSecond < 10) {
            dtSecond = "0" + dtSecond;
        }
        $('#dateDeposit').val(dt.getDate() + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear());
        $('#ddlHours').val(dtHour);
        $('#ddlMinutes').val(dtMinute);
        $('#ddlSeconds').val(dtSecond);
        $('.resetRequired').val('');
        $('#fileAttachment').val('');
        $('#fileName').val('');
        $('#ddlMonth').val(dtMonth);
        $('#ddlYear').val(dtYear);
        $('#BankAccountDDL').val($('#BankAccountDDL option:first').val()).change();
        $('#DDLBankName').val($('#DDLBankName option:first').val()).change();
        if ($('#BankCardDropdown').children(":first") != undefined)
             dropdownlist($('#BankCardDropdown').children(":first"));
        $('#preferOld').click();
        if ($(".resetRequired").hasClass("valid") || $(".resetRequired").hasClass("error")) {
            $(function () {
                $('input').blur();
                formValidator.resetForm();
            });
        }
        return false;
    },

    TrimSpace: function () {
        $("#txtAmount").val($("#txtAmount").val().replace(" ", ""));
        return false;
    },

    Validation: function () {
        if (typeof $("#txtAmount").val() != "undefined") {
            $("#txtAmount").val($("#txtAmount").val().replace(/\s/g, '')).change();
        }
        if ($('.resetRequired').valid()) {
            Page.loading();
            $('.loading').show();
        }
    },

    LocalBankCancel: function () {
        if ($('.resetRequired').is(":visible")) {
            PaymentCommonFunction.ResetForm();
            $('#stepOneTick').removeClass();
            $('#stepTwoTick').removeClass();
            $('#stepThreeTick').removeClass();
            $("input[name='depositMethod']").first().click().change();
        } else {
            return false;
        }
    },

    LocalBankSubmit: function () {

        if ($('#ddlHours').val() === '' || $('#ddlMinutes').val() === '' || $('#ddlSeconds').val() === '') {
            $('.timeErrorMessage').show();
            return false;
        } else if ($('.resetRequired').is(":visible")) {
            PaymentCommonFunction.Validation();
        } else {
            Page.message("Please follow the instruction to complete the transaction!");
            return false;
        }
    },

    GenerateQRCode: function ($element, content, img, opt) {
        var defaults = {
            radius: 0.5,
            minVersion: 8,
            maxVersion: 40,
            ecLevel: 'H',
            background: "#FFF",
            quiet: 1,
            render: "image",
            mode: 4,
            size: 128,
            text: content,
            mSize: 0.35,
        };

        $.extend(defaults, opt);
        if (typeof img !== "undefined" && img.nodeType > 0) {
            defaults.image = img;
            if (img.complete)
                $element.qrcode(defaults);
            else img.onload = function () { $element.qrcode(defaults); }
        } else {
            $element.qrcode(defaults);
        }
    },

    FrontNEndTrim: function(e){
        e.value = e.value.replace(/(^\s*)|(\s*$)/gi, "");
    }

    //commaForThousand: function(){
    //    $('#txtAmount').on("focus", function (e) {
    //        var $this = $(this);
    //        var num = $this.val().replace(/,/g, "");
    //        $this.val(num);

    //    }).on("blur", function (e) {
    //        PaymentCommonFunction.TrimSpace();
    //        var $this = $(this);
    //        var num = $this.val().replace(/[^0-9\.]+/g, '').replace(/,/gi, "").split("").reverse().join("");
    //        var num2 = PaymentCommonFunction.removeRougeChar(num.replace(/(([0-9]{2,}\.)?(\d{3}))/g, "$1,").split("").reverse().join(""));
    //        $this.val(num2);
    //    });
    //},

    //removeRougeChar: function(convertString) {
    //    if (convertString.substring(0, 1) == ",") {
    //        return convertString.substring(1, convertString.length);
    //    }
    //    return convertString;
    //}
};

var LocalBank = {
    ResetMemberAccountField: function () {
        $('#txtAccountHolderName').val("");
        $('#txtNewAccountHolderName').val(Helper.MemberFirstName);
        $('#txtCNYAccountHolderName').val(Helper.MemberFirstName);
        $('#txtAccountBankName').val("");
        $('#txtNewAccountBankName').val("");
        $('#txtAccountNumber').val("");
        $('#txtNewAccountNumber').val("");
        $('#otherBankName').val("");
        $('#ddlNewAccountBankName').val($("#ddlNewAccountBankName option:first").val());
        $('#ddlMemberAccount').val($("#ddlMemberAccount option:first").val());
    },
    Reset: function () {
        $('#txtAmount').val("");
        $('#txtRefNumber').val("");
        $('#txtLastDigit').val("");
        $('#dateDeposit').val("");
        $('#timeDeposit').val("");
        $('#fileAttachment').val("");
        $('#fileName').val("");
        if (Helper.CurrencyCode.toLowerCase() == "cny" || Helper.CurrencyCode.toLowerCase() == "rmb") {
            $('#ddlBankType').val($("#ddlBankType option[value=Alipay]").val());
        } else {
            $('#ddlBankType').val($("#ddlBankType option:first").val());
        }
    },
    BindMerchantDetail: function (e) {
        $.ajax({
            type: "GET",
            url: "/Services/Deposit.ashx",
            data: { processJob: "bindMerchantDetail", bankID: e },
            success: function (data) {
                if (data != null) {
                    if (data.IsSuccess == true) {
                        if (data.BankAccount != null) {
                            var v = data.BankAccount;
                            var province = v.Province, city = v.City, branch = v.Branch;
                            if (v.Province == null || v.Province == "") {
                                province = "-";
                            }
                            if (v.City == null || v.City == "") {
                                city = "-";
                            }
                            if (v.Branch == null || v.Branch == "") {
                                branch = "-";
                            }
                            $('#mercBankName').text(v.BankName);
                            $('#mercAccountName').text(v.AccountHolderName);
                            $('#mercAccountNumber').text(v.AccountNo);
                            $('#mercAccountNumber').val(v.AccountNo);
                            $('#mercProvince').text(province);
                            $('#mercCity').text(city);
                            $('#mercBranch').text(branch);
                            var lastSix = v.AccountNo.slice(-6);
                            $('#txtLastDigit').val(lastSix);
                        }
                    } else {
                        if (!e.is('9999')) {
                            alert(data.Message)
                        }
                      
                    }
                }
            }
        });
    },
    isVNDSpecialBank: function () {
        var currCode = "<%= MemberManager.CurrencyCode %>";

        if (currCode == "VND") {
            var strVal = $("input[name='depositMethod']:checked").next().text();
            if (strVal == "Ngân hàng Ngoại Thương" || strVal == "Ngân hàng Kỹ Thương Việt Nam" || strVal == "Vietcom Bank" || strVal == "Techcom Bank" || strVal == "Ngân hàng Sài Gòn Thương Tín" || strVal == "Sacom Bank" || strVal == "Ngân hàng Ngoại Thương (Vietcombank)" || strVal == "Ngân hàng Kỹ Thương Việt Nam (Techcombank)" || strVal == "Ngân hàng Sài Gòn Thương Tín (Sacombank)") {
                return true;
            }
        }
        if (currCode == "CNY" || currCode == "RMB" || currCode == "IDR") {
            return true;
        }
        return false;
    }

};

var Yeepay = {
    BindVendorSuppAmount: function (vendorCode) {
        var suppAmount = $('#hidVendorSuppAmounts').val();
        var convertSuppAmount = JSON.parse(suppAmount);
        $('#AmountDDL option').slice(1).remove()
        var rptOption = $('#AmountDDL option').eq(0).clone(true);
        $('#AmountDDL option').eq(0).remove();
        $.each(convertSuppAmount, function (k, v) {
            if (vendorCode == v.code) {
                $.each(v.supportedAmount, function (key, value) {
                    rptOption.html(value);
                    rptOption.attr("value", value);
                    $('#AmountDDL').append(rptOption);
                    rptOption = $('#AmountDDL option').eq(0).clone(true);
                });
            }
        });
    },
    CalculateValue: function () {
        var cardCharges = $('#BankAccountOption').attr("data-charges");
        var amount = $('#AmountDDL').val();
        if (cardCharges != undefined && amount != undefined) {
            var afterCharges = parseInt(amount) * (1 - parseFloat(cardCharges));
            return (Math.round(afterCharges * 100) / 100).toFixed(2);
        } else {
            return false;
        }
    },
    BindReminderMessage: function(vendorCode) {
        $.ajax({
            type: "GET",
            url: "/Services/Deposit.ashx",
            data: { processJob: "bindYeepayReminderMessage", vendorID: vendorCode },
            success: function (data) {
                if (data != null) {
                    if (data.IsSuccess == true) {
                        var v = data.Message;
                        $(".yeepayReminder").html(v);
                    } 
                }
            }
        });
    }
};

var AstroPay = {
    RecalculateAmount: function () {
        var amount = $('#txtAmount').val();
        var cardNumber = $('#txtAstroNumber').val();
        if (amount != undefined && amount != "" && cardNumber != undefined && cardNumber != "") {
            $.ajax({
                type: "POST",
                url: "/Services/Deposit.ashx",
                data: { processJob: "calculateAmount", cardNumbers: cardNumber, cardAmount: amount },
                success: function (data) {
                    if (data != null) {
                        if (data.IsSuccess == true) {
                            $('#txtActualAmount').val(data.Message);
                        }
                    }
                }
            });
        }
    }
};