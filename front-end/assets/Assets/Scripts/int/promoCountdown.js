﻿var lastPromoPage = "";
$(document).ready(function () {
    FillUpPromotionPage();

    try {
        var search = document.location.search.toLowerCase().match(/promoid=\d+/);
        if (search.length > 0) {
            Page.loading();
            $.get("/Services/Promotion.ashx?" + search).always(function (d) {
                Page.loadingDismiss();
                Modal.PopupHtml(d.responseText, promoHeader);
            });
        }
    } catch (err) { }

    $("#promoCodeBtn").on("click",
        function () {
            var btn = $("#promoCode");
            if ($(this).hasClass("searchloader"))
                return;
            try {
                $(this).toggleClass("searchloader");
                var code = btn.val();
                if (code.length <= 0) {
                    Modal.message(couponKeyInMsg, infoMessage, infoMessage, null);
                    throw "empty";
                }
                Page.loading();
                $.ajax({
                    type: "get",
                    url: "/Services/Promotion.ashx?CouponCode=" + code,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: ShowCouponResult
                });
            }
            catch (ex) {
                $(this).toggleClass("searchloader");
                Page.loadingDismiss();
            }
        });
    $(".promoOptionBtn").on("click",
        function () {
            $('.promolistholder').remove();
            if (this.id === "myPromo") {
                $('.promoHeader.selectionField').removeClass("history");
                var type = lastPromoPage;
                if (type === "")
                    window.history.pushState('', '', '/' + Helper.UrlCulture + '/promo.htm');
                else
                    window.history.pushState('', '', '/' + Helper.UrlCulture + '/promo/' + type + '.htm');
                GoToAllPage();
            }
            else {
                $('.promoHeader.selectionField').addClass("history");
                window.history.pushState('', '', '/' + Helper.UrlCulture + '/promo/history.htm');
                GoToHistoryPage();
            }
            $("." + activeCls).removeClass(activeCls);
            $(this).addClass(activeCls);
            FillUpPromotionPage();
        });
    StartCountDown();

    $('#promoVersType').on("click", function () {
        var str = document.location.pathname.split('/').pop().split('.')[0];
        if (str !== "history") {
            $("#promoSecondCatList").toggle(200);
            setTimeout(function () {
                $("html").one("click",
                    function (e) {
                        if ($(e.target).parent().attr('id') !== "promoVersType" && $(e.target).attr('id') !== "promoVersType")
                            $("#promoSecondCatList").slideUp(200);
                    });
            },
                200);
        }
    });

    $('#promoCat').on("click", function () {
        var str = document.location.pathname.split('/').pop().split('.')[0];
        if (str !== "history") {
            $("#promoCatList").toggle(200);
            setTimeout(function () {
                $("html").one("click",
                    function (e) {
                        if ($(e.target).parent().attr('id') !== "promoCat" && $(e.target).attr('id') !== "promoCat")
                            $("#promoCatList").slideUp(200);
                    });
            },
                200);
        }
    });

    $("#promoCatList div").on("click", function () {
        var type = $(this).attr("data-type");
        var vers = $('#promoVersType').attr("data-version");
        if (type === "")
            window.history.pushState('', '', '/' + Helper.UrlCulture + '/promo.htm');
        else {

            if (type == "newmember" && (Helper.CultureCode.toLowerCase() == "vi-vn" || Helper.CultureCode.toLowerCase() == "th-th")) {

                if (Helper.CultureCode.toLowerCase() == "vi-vn")
                    window.history.pushState('', '', '/' + 'vn/khuyen-mai/');
                else
                    window.history.pushState('', '', '/' + 'th/โปรโมชั่น/');
            }
            else {
                window.history.pushState('', '', '/' + Helper.UrlCulture + '/promo/' + type + '.htm');
            }




        }
        GoToAllPage();
        FillUpPromotionPage(vers);
    });

    $("#promoSecondCatList div").on("click", function () {
        var version = $(this).attr("data-version");
        $('#promoVersType').attr("data-version", version);
        $("#promoVersType > span").text($("#promoSecondCatList div[data-version='" + version + "']").text());
        FillUpPromotionPage(version);
    });

    if (Helper.IsLogin) {
        $("#myPromoHistory").show();
    }
});

function FillUpPromotionPage(category) {
    $("#container .promotions").remove();
    $("#myPromoUpdatedDate span").text("");
    $("#promoLoading").show();
    var str = document.location.pathname.split('/').pop().split('.')[0];
    if (str === "history") {
        GetHistory();
    } else {
        var tempUrl = decodeURIComponent(document.location.pathname);
        if ((Helper.CultureCode.toLowerCase() == "vi-vn" && tempUrl.indexOf("khuyen-mai") > 1) || (Helper.CultureCode.toLowerCase() == "th-th" && tempUrl.indexOf("โปรโมชั่น") > 1))
            str = "newmember";


        if (str === "promo")
            str = "";

        lastPromoPage = str;
        $("#promoCat > span").text($("#promoCatList div[data-type='" + str + "']").text());
        GetUpperPartPromotionPageDetails(str);
        GetPosts(str, category);
    }
}

function GetPosts(promoType, category) {
    if (promoType == "undefined")
        promoType = "";
    if (typeof category == "undefined")
        category = "";
    $.ajax({
        type: "get",
        url: "/Services/Promotion.ashx?type=" + promoType + "&category=" + category,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetPostsComplete
    });
}

function GetHistory(promoType) {
    if (promoType == "undefined")
        promoType = "";

    $.ajax({
        type: "get",
        url: "/Services/Promotion.ashx?history=1&type=" + promoType + "&category=" + "",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetHistoryComplete
    });
}

function ShowCouponResult(result) {
    Page.loadingDismiss();
    if (result !== "404. Invalid Coupon Code" && result.Id != null) {

        var bal;
        try {
            bal = $(".totalBalanceAmount:first").text().replace(/,/g, '').match(/\d+/)[0];
        } catch (err) {
            bal = 0;
        }

        if (bal > 100 || WalletData.PreferredWallet !== result.Account) {
            Page.loading();
            Modal.GameTransferOpen("?bonusId=" +
                result.Id +
                "&couponCode=" +
                $("#promoCode").val() +
                "&account=" +
                result.Account);
        } else
            window.open(depositPage + "?bonusId=" + result.Id + "&couponCode=" + $("#promoCode").val(), '_blank');
    }
    else {
        Modal.message(invalidCouponKeyIn, infoMessage, infoMessage, null);
    }
    $("#promoCodeBtn").toggleClass("searchloader");
}

function OnGetPostsComplete(posts) {
    $('.promolistholder').remove();
    if ($(".promotions").length === 0) {
        $("#promoLoading").hide();
        if (posts.length === 0) {
            ShowPromoNoContent();
        } else {
            for (var i = 0; i < posts.length; i++) {
                var isDisabled = posts[i].isExpiredServedBonus ? "pointer-events: none;cursor: default;" : "";
                if (posts[i].BonusProductList == null && posts[i].RebateBonusProductList == null) {
                    $("#container.promoRunningList").last()
                        .append("<div class='promotions row expanded collapse' data-url='" +
                            posts[i].Html +
                            "' data-id='" +
                            posts[i].BonusId +
                            "' data-status='" +
                            posts[i].Status +
                            "' data-runningRebate='" +
                            posts[i].RunningRebate +
                            "'><div class='large-5 small-12 column title'><span class='show-for-large icon " +
                            posts[i].SvgClass +
                            "'></span>" +
                            "<span style='" + isDisabled + "' class='tncUrl'" +
                            " data-url='" +
                            posts[i].Html +
                            "'>" +
                            posts[i].Title +
                            "</span>" +
                            "</div><span class='large-3 small-12 column clockHolder'><span class=\"icon icon-clock promoClock\"></span><div class='expireTime' data-time='" +
                            posts[i].ExpireDateTime +
                            "' data-status='" +
                            posts[i].Status +
                            "'>-</div></span><div class='large-3 small-5 column promoButtonAlign'>" +
                            (posts[i].Status === "Serving"
                                ? GeneratePromoProgress(posts[i].Progress)
                                : status(posts[i].Status, posts[i].BonusId, posts[i].Type)) +
                            "</div>"
                            + (posts[i].Status === "" ? "<div class='large-1 large-push-3 small-7 column promoMoreInfoBtn end'>" :
                                "<div class='large-1 small-7 column promoMoreInfoBtn end'>")
                            +
                            (posts[i].IsClaimable ? claimBtn(posts[i].BonusId) : posts[i].isExpiredServedBonus ? details(posts[i].Html, false) : details(posts[i].Html)) +
                            "</div></div>");
                } else if ((posts[i].BonusProductList != null && posts[i].BonusProductList.length === 1) || (posts[i].RebateBonusProductList != null && posts[i].RebateBonusProductList.length === 1)) {
                    if (posts[i].PromotionMasterCategory == "bonuspromotion") {
                        $("#container.promoRunningList").last()
                            .append("<div class='promotions row expanded collapse' data-url='" +
                                posts[i].Html +
                                "' data-id='" +
                                posts[i].BonusProductList[0].BonusID +
                                "' data-status='" +
                                posts[i].BonusProductList[0].Status +
                                "'><div class='large-5 small-12 column title'><span class='show-for-large icon " +
                                posts[i].SvgClass +
                                "'></span><span style='" + isDisabled + "' class='tncUrl' data-url='" +
                                posts[i].Html +
                                "'>" +
                                posts[i].Title + " " + posts[i].BonusProductList[0].BonusSubtitle +
                                "</span></div><span class='large-3 small-12 column clockHolder'><span class=\"icon icon-clock promoClock\"></span><div class='expireTime' data-time='" +
                                posts[i].ExpireDateTime +
                                "' data-status='" +
                                posts[i].Status +
                                "'>-</div></span><div class='large-3 small-5 column promoButtonAlign'>" +
                                (posts[i].BonusProductList[0].Status === "Serving"
                                    ? GeneratePromoProgress(posts[i].BonusProductList[0].Progress)
                                    : status(posts[i].BonusProductList[0].Status, posts[i].BonusProductList[0].BonusID, posts[i].Type)) +
                                "</div><div class='large-1 small-7 column promoMoreInfoBtn end'>" +
                                (posts[i].BonusProductList[0].IsClaimable ? claimBtn(posts[i].BonusProductList[0].BonusID) : posts[i].isExpiredServedBonus ? details(posts[i].Html, false) : details(posts[i].Html)) +
                                "</div></div>");
                    }
                    if (posts[i].PromotionMasterCategory == "rebatepromotion") {
                        $("#container.promoRunningList").last()
                            .append("<div class='promotions row expanded collapse' data-url='" +
                                posts[i].Html +
                                "' data-id='" +
                                posts[i].RebateBonusProductList[0].RebateID +
                                "' data-status='" +
                                posts[i].RebateBonusProductList[0].Status +
                                "' data-runningRebate='" +
                                posts[i].RebateBonusProductList[0].RunningRebate +
                                "'><div class='large-5 small-12 column title'><span class='show-for-large icon " +
                                posts[i].SvgClass +
                                "'></span><span style='" + isDisabled + "' class='tncUrl' data-url='" +
                                posts[i].Html +
                                "'>" +
                                posts[i].Title + " " + posts[i].RebateBonusProductList[0].BonusSubtitle +
                                "</span></div><span class='large-3 small-12 column clockHolder'><span class=\"icon icon-clock promoClock\"></span><div class='expireTime' data-time='" +
                                posts[i].ExpireDateTime +
                                "' data-status='" +
                                posts[i].Status +
                                "'>-</div></span><span class='large-3 small-5 column promoButtonAlign'>" +
                                (posts[i].RebateBonusProductList[0].Status === "Serving"
                                    ? GeneratePromoProgress(posts[i].RebateBonusProductList[0].Progress)
                                    : status(posts[i].RebateBonusProductList[0].Status, posts[i].RebateBonusProductList[0].RebateID, posts[i].Type)) +
                                "</span><div class='large-1 small-7 column promoMoreInfoBtn end'>" +
                                (posts[i].RebateBonusProductList[0].IsClaimable ? claimBtn(posts[i].RebateBonusProductList[0].RebateID) : posts[i].isExpiredServedBonus ? details(posts[i].Html, false) : details(posts[i].Html)) +
                                "</div></div>");
                    }
                } else {
                    var arr = [];
                    arr.push("<div class='promolistholder row expanded collapse' data-status='" +
                        posts[i].Status +
                        "' style=\"position:relative;\">");
                    arr.push("<div class='large-5 small-12 column title'><span class='show-for-large icon " +
                        posts[i].SvgClass +
                        "'></span><span class='tncUrl' data-url='" +
                        posts[i].Html +
                        "'>" +
                        posts[i].Title +
                        "</span></div><span class='large-3 small-12 column clockHolder'><span class=\"icon icon-clock promoClock\"></span><div class='expireTime' data-time='" +
                        posts[i].ExpireDateTime +
                        "' data-status='" +
                        posts[i].Status +
                        "'>");
                    arr.push("-</div></span>");
                    arr.push("<div class='large-3 small-5 column moreGrpingPromo' data-contentid='" +
                        posts[i].Id +
                        "'><span>" +
                        promoExpand +
                        "</span></div>");
                    arr.push("<div class='large-1 small-7 column promoMoreInfoBtn'>" +
                        details(posts[i].Html) +
                        "</div>");
                    if (posts[i].PromotionMasterCategory == "bonuspromotion") {
                        for (var j = 0; j < posts[i].BonusProductList.length; j++) {
                            var subtitle = posts[i].BonusProductList[j].BonusSubtitle;
                            if (subtitle == null || subtitle == "") {
                                subtitle = posts[i].Title;
                            }
                            arr.push("<div style='display:none;' class='promotions row expanded collapse' data-url='" +
                                posts[i].Html +
                                "' data-id='" +
                                posts[i].BonusProductList[j].BonusID +
                                "' data-status='" +
                                posts[i].BonusProductList[j].Status +
                                "'>");
                            arr.push("<div class='large-5 small-7 column title'><span class='show-for-large icon " +
                                posts[i].SvgClass +
                                "'></span><span class='tncUrl groupingTncUrl' data-url='" +
                                posts[i].Html +
                                "'>" +
                                subtitle +
                                "</span></div>");
                            if ((posts[i].BonusProductList[j].Status === "Serving" || posts[i].BonusProductList[j].Status === "Release") 
                                && posts[i].ExpireDateTime != posts[i].BonusProductList[j].ExpireDateTime)
                            {
                                arr.push("<span class='large-3 small-12 column clockHolder'><span class=\"icon icon-clock promoClock\"></span><div class='expireTime' data-time='" +
                                    posts[i].BonusProductList[j].ExpireDateTime +
                                "' data-status='" +
                                posts[i].BonusProductList[j].Status +
                                "'></div></span>");
                                arr.push("<div class='large-3 small-5 column promoButtonAlign end'>");
                            }
                            else
                            {
                                arr.push("<div class='large-push-3 large-1 small-5 column promoButtonAlign end'>");
                            }

                            arr.push((posts[i].BonusProductList[j].Status === "Serving"
                                    ? GeneratePromoProgress(posts[i].BonusProductList[j].Progress)
                                    : status(posts[i].BonusProductList[j].Status,
                                        posts[i].BonusProductList[j].BonusID,
                                        posts[i].Type)) +
                                "</div>");
                            if (posts[i].BonusProductList[j].IsClaimable) {
                                arr.push("<div class='large-1 small-2 column promoMoreInfoBtn expandDetail'>" +
                                    claimBtn(posts[i].BonusProductList[j].BonusID) +
                                    "</div></div>");
                            } else {
                                arr.push(
                                    "<div class='large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large'></div></div>");
                            }
                            //arr.push("<div class='large-1 small-2 column promoMoreInfoBtn expandDetail'>" +
                            //    (posts[i].BonusProductList[j].IsClaimable
                            //        ? claimBtn(posts[i].BonusProductList[j].BonusID)
                            //        : details(posts[i].Html)) +
                            //    "</div></div>");

                        }
                    } else {
                        for (var k = 0; k < posts[i].RebateBonusProductList.length; k++) {
                            var rebateSubtitle = posts[i].RebateBonusProductList[k].BonusSubtitle;
                            if (rebateSubtitle == null || rebateSubtitle == "") {
                                rebateSubtitle = posts[i].Title;
                            }
                            arr.push("<div style='display:none;' class='promotions row expanded collapse' data-url='" +
                                posts[i].Html +
                                "' data-id='" +
                                posts[i].RebateBonusProductList[k].RebateID +
                                "' data-status='" +
                                posts[i].RebateBonusProductList[k].Status +
                                "' data-runningRebate='" +
                                posts[i].RebateBonusProductList[k].RunningRebate +
                                "'>");
                            arr.push("<div class='large-5 small-7 column title'><span class='show-for-large icon " +
                                posts[i].SvgClass +
                                "'></span><span class='tncUrl groupingTncUrl' data-url='" +
                                posts[i].Html +
                                "'>" +
                                rebateSubtitle +
                                "</span></div>");
                            arr.push("<span class='large-push-3 large-1  small-5 column promoButtonAlign end'>" +
                                (posts[i].RebateBonusProductList[k].Status === "Serving"
                                    ? GeneratePromoProgress(posts[i].RebateBonusProductList[k].Progress)
                                    : status(posts[i].RebateBonusProductList[k].Status,
                                        posts[i].RebateBonusProductList[k].RebateID,
                                        posts[i].Type)) +
                                "</span>");

                            if (posts[i].RebateBonusProductList[k].IsClaimable) {
                                arr.push("<div class='large-1 small-2 column promoMoreInfoBtn expandDetail'>" +
                                    claimBtn(posts[i].RebateBonusProductList[k].RebateID) +
                                    "</div></div>");
                            } else {
                                arr.push(
                                    "<div class='large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large'></div></div>");
                            }
                            //arr.push("<div class='large-1 small-2 column promoMoreInfoBtn expandDetail'>" +
                            //    (posts[i].RebateBonusProductList[k].IsClaimable
                            //        ? claimBtn(posts[i].RebateBonusProductList[k].RebateID)
                            //        : details(posts[i].Html)) +
                            //    "</div></div>");

                        }
                    }
                    arr.push("</div>");
                    $("#container.promoRunningList").last()
                        .append(arr.join(''));
                }
            }
            SetOldestDate(posts);
        }
    }
    $('.moreGrpingPromo').on("click",
        function () {
            if (!$(this).hasClass("open")) {
                $(this).addClass("open");
                $(this).parents(".promolistholder").find("div.promotions").show();
            } else {
                $(this).parents(".promolistholder").find("div.promotions").hide();
                $(this).removeClass("open");
            }
        });
    if (Page.getParameterByName("navid") != null) {
        var val = Page.getParameterByName("navid");
        NavigateToBonus(val);
    }
}

function GeneratePromoProgress(progress) {
    var split = progress.split("/");
    if (split.length > 1) {
        return '<div class="large-7 small-7 progressBarMobileSvg" style="margin-top: 10px;margin-bottom: 0px;"><svg viewBox="0 0 100 2" preserveAspectRatio="none" style="display: block;"><path d="M 0,1 L 100,1" stroke="#e3e5e7" stroke-width="2" fill-opacity="0"><title>' + progress + '</title></path><path d="M 0,1 L 100,1" stroke="#1cbd64" stroke-width="2" fill-opacity="0" style="stroke-dasharray: 100, 100; stroke-dashoffset: ' + (100 - ((split[0].replace(/,/g, "") / split[1].replace(/,/g, "") * 100))) + ';"><title>' + progress + '</title></path></svg></div><span class="progressBarMobile" style="display: block;">' + progress + '</span>';
    }
    else {
        return '<span class="progressBarMobile" style="display: block;">' + progress + '</span>';
    }
}

function OnGetHistoryComplete(posts) {
    if ($(".promotions").length === 0) {
        $("#promoLoading").hide();
        if (posts.length === 0) {
            $("#container.promoHistoryList").last().append("<div class='promotions row expanded collapse middleContent' >" +
             noHistory +
             "</br><img src='" + Helper.MediaUrl + "/Assets/images/banner/slot/funambassador/promotionHistory.png'><br/></div>");
        } else {
            for (var i = 0; i < posts.length; i++) {
                var reward = posts[i].Reward;

                if (posts[i].ReleaseType !== "") {
                    reward = "<table class='normalTable promoReward'><tr><td class='mobileAvailableRetrieve'>" +
                        posts[i].Reward +
                        "</td></tr><tr><td>" +
                        posts[i].ReleaseType +
                        "</td></tr></table>";
                }

                $("#container.promoHistoryList").last().append(
                    "<div class='promotions row expanded collapse' style='position: relative;' data-id='" +
                    posts[i].BonusId +
                    "'><div class='historyPromoDate historyData large-5 small-8 column title'><span class='show-for-large icon " +
                    posts[i].SvgClass +
                    "'></span>" +
                    posts[i].Title +
                    "</div><span class='historyData large-3 small-12 historyPromoDate column'>" +
                    posts[i].UpdatedDate +
                    "</span><div class='historyData large-3 small-6 column'>" +
                    reward +
                    "</div><div class='historyData status large-1 small-6 column'>" +
                    status(posts[i].Status) +
                    "</div></div>");
            }
            SetOldestDate(posts, "UpdatedDateTime");
        }
    }
}

function GoToAllPage() {
    $(".promoNonHistory").show();
    $(".promoHistory").hide();
    $(".promoHistoryInvi").hide();
    $("#promoCat .promo-arrow").show();
    $('#promoVersType').show();
    $("#promoCat").css("cursor", "pointer").addClass("large-5").removeClass("large-1");
    $('#promoCategoryHolder').removeClass("large-2").addClass("large-5");
    $('#promoCat').removeClass("large-12 small-12").addClass("large-3 small-3");
    $("#container").addClass("promoRunningList").removeClass("promoHistoryList");
}

function GoToHistoryPage() {
    $("#promoCat > span").text($("#promoCatList div[data-type='']").text());
    $(".promoHistory").show();
    $(".promoNonHistory").hide();
    $(".promoHistoryInvi").show();
    $("#promoCat .promo-arrow").hide();
    $("#promoCat").css("cursor", "default").removeClass("large-5").addClass("large-1");
    $('#promoCategoryHolder').removeClass("large-5").addClass("large-2");
    $('#promoCat').removeClass("large-3 small-3").addClass("large-12 small-12");
    $('#promoVersType').hide();
    $("#container").addClass("promoHistoryList").removeClass("promoRunningList");
}

function ShowPromoNoContent() {
    $("#container.promoRunningList").last().append("<div class='promotions row expanded collapse middleContent'>" +
    comingSoon +
    "</br><img src='" + Helper.MediaUrl + "/Assets/images/banner/slot/funambassador/promotionHistory.png'><br/></div>");
}

function GetOldestDate(array, prop) {
    if (prop == undefined)
        prop = "UpdatedDate";
    var result = new Date(Math.min.apply(null, array.map(function (e) {
        return new Date(e[prop]);
    })));

    return result;
}

function SetOldestDate(array, prop) {
    var result = GetOldestDate(array, prop);

    $("#myPromoUpdatedDate span").text(result.toLocaleString('en-GB'));
}

function GetUpperPartPromotionPageDetails(promoType) {
    $.ajax({
        type: "get",
        url: "/Services/PromotionPage.ashx?type=" + promoType,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnGetPromotionPageDetailsComplete
    });
}

function OnGetPromotionPageDetailsComplete(details) {
    var numbers = $("#myPromoSummary .myPromoNum");
    if (Helper.IsLogin) {
        $('#totalPromoNum').text(details.PromoTotalNumber);
        $('#totalAppliedPromoNum').text(details.PromoApplyNumber);
        $('#totalRemainingPromoNum').text(details.PromoTotalNumber - details.PromoApplyNumber);



        try {
            bar.animate(details.PromoApplyNumber / details.PromoTotalNumber); // Number from 0.0 to 1.0
        } catch (err) {
            bar.animate(0);
        }
    }
 

    var ending = $("#divEndPromoSoon");
    var feature = $("#featuredPromoCont");

    if (details.EndingPromotionItem !== null) {
        var endItem = details.EndingPromotionItem;
        var endItemSpecialPromoBonus = $(ending).find(".specialPromoBonusId");
        $(ending).find(".tncUrl").attr("data-url", endItem.Html);
        $(ending).find(".specialPromoTitle").text(endItem.Title);
        $(ending).find(".promotion-tab-title-logo").removeClass("icon-promo_newmember icon-promo_livecasino icon-promo_special icon-promo_sportsbook icon-promo_slots icon-promo_keno").addClass(endItem.SvgClass);
        $(ending).find(".expireTime").attr("data-time", endItem.ExpireDateTime);
        if (endItem.isExpiredServedBonus) {
            $(ending).find(".specialPromoReadMore").hide();
            $(ending).find(".specialPromoTitle").attr("style", "pointer-events: none;cursor: default;");
        } else {
            $(ending).find(".specialPromoReadMore").show();
            $(ending).find(".specialPromoTitle").attr("style", "pointer-events: unset;cursor: pointer;");
        }
        if (Helper.IsLogin) {
            if (endItem.BonusId != null && endItem.BonusId != undefined && endItem.BonusId != "") {
                if (endItem.Type === "Bonus") {
                    $(endItemSpecialPromoBonus).removeAttr("data-contentid");
                } else {
                    $(endItemSpecialPromoBonus).attr("data-contentid", endItem.BonusId);
                }
                $(endItemSpecialPromoBonus).attr({
                    "data-bonus": endItem.BonusId,
                    "data-bonus-type": endItem.Type,
                    "data-status": endItem.Status
                });
            } else if (endItem.BonusProductList != null && endItem.BonusProductList[0].BonusID != undefined) {
                $(endItemSpecialPromoBonus).removeAttr("data-contentid");
                $(endItemSpecialPromoBonus).attr({
                    "data-bonus": endItem.BonusProductList[0].BonusID,
                    "data-bonus-type": endItem.Type,
                    "data-status": endItem.Status
                });
            } else {
                $(endItemSpecialPromoBonus).removeAttr("data-bonus");
                $(endItemSpecialPromoBonus).attr({
                    "data-contentid": endItem.Id,
                    "data-bonus-type": endItem.Type,
                    "data-status": endItem.Status
                });
            }
        } else {
            $(endItemSpecialPromoBonus).removeAttr("data-bonus");
            $(endItemSpecialPromoBonus).attr({
                "data-contentid": endItem.Id,
                "data-bonus-type": endItem.Type,
                "data-status": endItem.Status
            });
        }
        $(ending).find(".specialPromoCont").show().siblings(".specialPromoComingSoon").hide();
    } else {
        $(ending).find(".specialPromoCont").hide().siblings(".specialPromoComingSoon").show();
    }

    if (details.FeaturePromotionItem !== null) {
        var featureItem = details.FeaturePromotionItem;
        var featureItemSpecialPromoBonus = $(feature).find(".specialPromoBonusId");
        $(feature).find(".tncUrl").attr("data-url", featureItem.Html);
        $(feature).find(".specialPromoTitle").text(featureItem.Title);
        $(feature).find(".promotion-tab-title-logo").removeClass("icon-promo_newmember icon-promo_livecasino icon-promo_special icon-promo_sportsbook icon-promo_slots icon-promo_keno").addClass(featureItem.SvgClass);
        $(feature).find(".expireTime").attr("data-time", featureItem.ExpireDateTime);
        if (details.isExpiredServedBonus) {
            $(feature).find(".specialPromoReadMore").hide();
            $(feature).find(".specialPromoTitle").attr("style", "pointer-events: none;cursor: default;");
        } else {
            $(feature).find(".specialPromoReadMore").show();
            $(feature).find(".specialPromoTitle").attr("style", "pointer-events: unset;cursor: pointer;");
        }
        if (Helper.IsLogin) {
            if (featureItem.BonusId != null && featureItem.BonusId != undefined && featureItem.BonusId != "") {
                if (featureItem.Type === "Bonus") {
                    $(featureItemSpecialPromoBonus).removeAttr("data-contentid");
                } else {
                    $(featureItemSpecialPromoBonus).attr("data-contentid", featureItem.BonusId);
                }
                $(featureItemSpecialPromoBonus).attr({
                    "data-bonus": featureItem.BonusId,
                    "data-bonus-type": featureItem.Type,
                    "data-status": featureItem.Status
                });
            } else if (featureItem.BonusProductList != null && featureItem.BonusProductList[0].BonusID != undefined) {
                $(featureItemSpecialPromoBonus).removeAttr("data-contentid");
                $(featureItemSpecialPromoBonus).attr({
                    "data-bonus": featureItem.BonusProductList[0].BonusID,
                    "data-bonus-type": featureItem.Type,
                    "data-status": featureItem.Status
                });
            } else {
                $(featureItemSpecialPromoBonus).removeAttr("data-bonus");
                $(featureItemSpecialPromoBonus).attr({
                    "data-contentid": featureItem.Id,
                    "data-bonus-type": featureItem.Type,
                    "data-status": featureItem.Status
                });
            }
        } else {
            $(featureItemSpecialPromoBonus).removeAttr("data-bonus");
            $(featureItemSpecialPromoBonus).attr({
                "data-contentid": featureItem.Id,
                "data-bonus-type": featureItem.Type,
                "data-status": featureItem.Status
            });
        }
        $(feature).find(".specialPromoCont").show().siblings(".specialPromoComingSoon").hide();
    } else {
        $(feature).find(".specialPromoCont").hide().siblings(".specialPromoComingSoon").show();
    }

    var reward = $("#rewardCont");
    if (reward != null && Helper.IsLogin) {
        var rewardItem = details.RewardPointInfo;
        $(reward).find("#rewardRank").text(rewardItem.CurrentLevelDescription);
        $(reward).find("#usableRewardPoints")
            .text(Math.floor(rewardItem.CurrentMemberPoint));
        try {
            rewardsBar.animate(rewardItem.CurrentPointLevel / rewardItem.CurrentNextLevelPoint);
        } catch (err) {
            rewardsBar.animate(0);
        }
    }
    $(".specialPromoBonusId").empty();
    $('.specialPromoBonusId').each(function () {
        var link = GetLink();
        var specialPromoStatus = $(this).attr("data-status");
        var specialPromoContentID = $(this).attr("data-contentid");
        if (specialPromoStatus !== "" && specialPromoStatus === "Grouping") {
            $(this).append(nagivateToBtn(specialPromoContentID));
        } else {
            var specialPromoBonus = $(this).attr("data-bonus");
            var specialPromoBonusType = $(this).attr("data-bonus-type");
            if (specialPromoBonus !== "" && specialPromoBonusType === "Bonus") {
                $(this).append(link(specialPromoBonus, specialPromoBonusType));
            }
            else if (specialPromoContentID !== "" && specialPromoBonusType !== "Bonus") {
                $(this).append(link(specialPromoContentID, specialPromoBonusType));
            }
        }
    });

}