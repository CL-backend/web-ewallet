﻿function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

var pageView = "grid";
var countEqual = 0;
var slider, mobileSlider;
var arrPayLine;

$('.mobileRadioBoxWrap').on("click", function () {
    $('#titleSorting').html($(this).html());
    $('#mobile-sort-ddl').slideUp(200);
    if ($(this).hasClass("active")) {
        $(this).removeClass("active");
    } else {
        $('.mobileRadioBoxWrap').removeClass("active");
        $(this).addClass("active");
    }
});

function removeGameArr(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

$('#mobileFilterButton').on("click", function () {
    if ($('.mobileFilterSortSection').is(":visible")) {
        $('.mobileFilterSortSection').slideUp(200, function () {
            $('.slot-banner-Wrapper').removeClass("hide");
            $('.mobileFilterSortSection').removeClass("sectionOpenedMobile");
        });
    } else {
        $('.mobileFilterSortSection').addClass("sectionOpenedMobile");
        $('.slot-banner-Wrapper').addClass("hide");
        $('.mobileFilterSortSection').slideDown(200);
    }
});

$('.closeButtonFilterPanel').on("click", function () {
    $('.mobileFilterSortSection').slideUp(200, function () {
        $('.slot-banner-Wrapper').removeClass("hide");
        $('.mobileFilterSortSection').removeClass("sectionOpenedMobile");
    });
});


function increJackpot(elementClass, incrementValue, incrementPeriod) {
    if (incrementValue == null || incrementValue == '')
        incrementValue = 0.01;
    if (incrementPeriod == null || incrementPeriod == '')
        incrementPeriod = 1000;
    $(elementClass).each(function () {
        var elem = $(this);
        setInterval(function () {
            var jackpotValue = elem.html();
            var value = (jackpotValue.split(",")).join("");
            if (parseFloat(value) > 0) {
                var parts = (parseFloat(value) + incrementValue).toFixed(2).split(".");
                parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                elem.html(parts.join("."));
            }
        }, incrementPeriod);
    });
}

function PushToSearchFilteringBox(vendor, code, sortfeatures) {
    var filteringHolder = [];
    var realPushBoxName = vendor;
    if (realPushBoxName != null && realPushBoxName !== '' && ($('.populateFilteringBtn').attr("data-value") !== vendor)) {
        filteringHolder.push("<div class=\"populateFilteringBtn\" data-sort-type=\"" + sortfeatures + "\" data-value=\"" + code + "\">");
        filteringHolder.push("<span style='vertical-align: middle; font-size: 12px;'>");
        filteringHolder.push(realPushBoxName);
        filteringHolder.push("</span>");
        filteringHolder.push("<span class=\"icon icon-cancel_filter\"></span>");
        filteringHolder.push("</div>");
        $('.searchFilteringHolder').append(filteringHolder.join(''));
        $('.icon-cancel_filter').on("click", function () {
            var chkBoxValue = $(this).parent("div.populateFilteringBtn").attr("data-value");
            if (chkBoxValue == ".favorites") {
                $('.preferenceIcons[data-type=".favorites"]').removeClass("active");
            }
            if (chkBoxValue == ".recentlyplayed") {
                $('.preferenceIcons[data-type=".recentlyplayed"]').removeClass("active");
            }
            $('input.chkBoxFilterResult[value="' + chkBoxValue + '"]').icheck("unchecked");
            $(this).parent().remove();
            $('.filter-group.checkboxes').trigger("change");
        });
    }
}

$('.viewAllBtn').on("click", function () {
    if ($(this).attr("data-type") == "jackpot") {
        $('input.chkBoxFilterResult[value=".Jackpot"]').icheck("checked");
        $('.filter-group.checkboxes').trigger("change");
    } else {
        $('.secondaryFiltering[data-type="' + $(this).attr("data-type") + '"]').click();
    }
});

$('.preferenceIcons').on("click", function () {
    var type = $(this).attr("data-type");
    var categoryName = $(".chkBoxPreferenceGame[value='" + type + "']").parents(".checkBoxListWrap").find("span").html();
    var categoryCode = $(".chkBoxPreferenceGame[value='" + type + "']").parents(".checkBoxListWrap").find("span").attr("data-value");
    if (!$(this).hasClass("active")) {
        $(this).addClass("active");
        $('.chkBoxPreferenceGame[value="' + $(this).attr("data-type") + '"]').icheck("checked");
        PushToSearchFilteringBox(categoryName, categoryCode);
    } else {
        $(this).removeClass("active");
        $('.chkBoxPreferenceGame[value="' + $(this).attr("data-type") + '"]').icheck("unchecked");
        $('div.populateFilteringBtn[data-value="' + categoryCode + '"]').remove();
    }
    $('.filter-group.checkboxes').trigger("change");
});

$('.clearFilterGames').on("click", function () {
    $('.preferenceIcons').removeClass("active");
    $('input#favFilter, input#recentPlayFilter').val('');
    $('input.chkBoxFilterResult').icheck("unchecked");
    $('.populateFilteringBtn').remove();
    if (slider != undefined) {
        slider.reset();
        $('#fromAmount').text("1");
        $('#toAmount').text("1024");
    }
});

$('.mobileClearFilterBtn').on("click", function () {
    $('.mobileFilterRadioBox').removeClass('active');
    $('input.chkBoxFilterResult').icheck("unchecked");
    if (mobileSlider != undefined) {
        mobileSlider.reset();
        $('#mobilefromAmount').text("1");
        $('#mobiletoAmount').text("1024");
    }
});

$('input[type="checkbox"]').on("click", function () {
    var categoryName = $(this).parents(".checkBoxListWrap").find("span").html();
    var categoryCode = $(this).parents(".checkBoxListWrap").find("span").attr("data-value");
    if ($(window).width() < 1024) {
        if ($(this).parents('.mobile-filter-group').find("span").html() != undefined) {
            categoryName = $(this).parents('.mobile-filter-group').find("span").html();
            categoryCode = $(this).parents('.mobile-filter-group').find("span").attr("data-value");
        }
    }

    if ($(this).prop('name') == 'mobile') {
        if ($(this).prop('checked') == false) {
            $('.checkBoxListWrap').find('.chkBoxFilterResult[value="' + categoryCode + '"]').icheck('unchecked');
        } else {
            $('.checkBoxListWrap').find('.chkBoxFilterResult[value="' + categoryCode + '"]').icheck('checked');
        }
    } else {
        if ($(this).prop('checked') == false) {
            $('.mobile-filter-group').find('.chkBoxFilterResult[value="' + categoryCode + '"]').icheck('unchecked');
        } else {
            $('.mobile-filter-group').find('.chkBoxFilterResult[value="' + categoryCode + '"]').icheck('checked');
        }
    }

    if ($(this).prop("checked")) {
        PushToSearchFilteringBox(categoryName, categoryCode);
    } else {
        $('div.populateFilteringBtn[data-value="' + categoryCode + '"]').remove();
    }
});

$('.feelLuckyGamePanel[data-id="download"]').on("click", function () {
    window.open('/download/home.htm', '_blank');
    return;
});

$('#feelLuckySpin').on("click", function () {
    if (!DialogManager_isLogin(Helper.MemberCode)) {
        Member.EnsureLogin(window.location.href);
        return false;
    } else {
        var random = Math.floor(Math.random() * 10);
        $(".slotGamePanel").eq(random).find(".playNowIconOverlay").click();
        return false;
    }
});

function GrepGameBoardGamesDetails() {
    DisplayGridView();
    $('input.chkBoxFilterResult').icheck("unchecked");
    $('.populateFilteringBtn').remove();
    $('.totalSlotsNumbers').html($('.slotsGamesList > .slotGamePanel').length);
    $('.slotsGamesList > .slotGamePanel').hide();
    $('#lastPlayedGamesHolder').append($('.slotsGamesList > .slotGamePanel[data-recent="0"]:lt(4)').clone(true));
    $('#recommendedGamesHolder').append($('.slotsGamesList > .slotGamePanel[data-recommended="0"]:lt(7)').clone(true));
    $("#recommendedGamesHolder").append($("#feelingLuckyHolder").clone(true).removeClass("hide"));
    $('#newGamesHolder').append($('.slotsGamesList > .slotGamePanel[data-new="0"]:lt(4)').clone(true));
    $('#myFavoritesHolder').append($('.slotsGamesList > .slotGamePanel[data-fav="0"]:lt(4)').clone(true));
    $('#jackpotGameHolder').append($('.slotsGamesList > .slotGamePanel.Jackpot:lt(4)').clone(true));
    $('.preferenceIcons').removeClass("active");
    $('.myBoardHolder > div').each(function () {
        if ($(this).length > 0) {
            $(this).show();
        }
    });

    $('.myBoardHolder').each(function () {
        if ($(this).find("div").length < 1) {
            $(this).parents(".slotholder").removeClass("show").addClass("hide");
        }
    });

    $('.slotholder.show:gt(0)').addClass("realign");
    var gamesLength = $('.slotsGamesList > .slotGamePanel').length;
    $('.totalSlotsNumbers').html(gamesLength);
    increJackpot($('.jackpotAmount'), $('.jackpotAmount').attr("data-jp-incre"), $('.jackpotAmount').attr("data-jp-incre-period"));
};

$('.ddlListGames').on("click", function () {
    if ($(this).siblings(".ddlListChilds").is(':visible')) {
        $(this).siblings(".ddlListChilds").slideUp("3000");
    } else {
        $(this).siblings(".ddlListChilds").slideDown("3000");
    }
});

$('.gameSearchBox').focusin(function () {
    $(this).siblings("#search-button-close").fadeIn(300);
    $(this).siblings("#mobile-search-button-close").fadeIn(300);
});

$('.gameSearchBox').focusout(function () {
    $(this).siblings("#search-button-close").fadeOut(300);
    $(this).siblings("#mobile-search-button-close").fadeOut(300);
});

$('.gameSearchBox').on("mouseenter", function () {
    $(this).siblings("#search-button-close").fadeIn(300);
    $(this).siblings("#mobile-search-button-close").fadeIn(300);
}).on("mouseleave", function () {
    if (!$(this).is(":focus")) {
        $(this).siblings("#search-button-close").fadeOut(300);
        $(this).siblings("#mobile-search-button-close").fadeOut(300);
    }
});


$('#mobile-sort-ddl-holder').on("click", function () {
    if ($('#mobile-sort-ddl').is(":visible")) {
        $('#mobile-sort-ddl').slideUp(200);
    } else {
        $('#mobile-sort-ddl').slideDown(200);
    }
});

$('#search-button-close').on("click", function () {
    $('.gameSearchBox').val('');
});

function DisplayListView() {
    $('.slotGamePanel').removeClass("large-3 medium-3 small-6");
    $('.slotGamePanel').addClass("large-6 medium-6 small-12 list");
    $('.slotGameImage').addClass("list");
    $('.game-overlay > img').hide();
    $('.progressiveJackpotHolder').hide();
    $('.gameNameInfoWrap').removeClass("gameNamePositioningGrid").addClass("gameNamePositioningList");
    $('.gameNameInfoWrap').addClass("margin-left-fifty");
    $('.gameSlotInfoWrapper').addClass("margin-left-fifty");
    $('.playNowIconOverlay').hide();
    $('.game-overlay').removeClass("grid");
    $('.game-overlay').addClass("list");
    $('.inactive-fav-icon').css("color", "#dcdcdc");
    $('.freePlayHolder').hide();
    $('.slotGamePanel').attr("data-view", "list");
}

function DisplayGridView() {
    $('.slotGamePanel').removeClass("large-6 medium-6 small-12 list");
    $('.slotGamePanel').addClass("large-3 medium-3 small-6");
    $('.slotGameImage').removeClass("list");
    $('.game-overlay > img').show();
    $('.progressiveJackpotHolder').show();
    $('.gameNameInfoWrap').removeClass("gameNamePositioningList").addClass("gameNamePositioningGrid");
    $('.gameNameInfoWrap').removeClass("margin-left-fifty");
    $('.gameSlotInfoWrapper').removeClass("margin-left-fifty");
    $('.game-overlay').removeClass("list");
    $('.game-overlay').addClass("grid");
    $('.inactive-fav-icon').css("color", "#ffffff");
    $('.freePlayHolder').show();
    $('.slotGamePanel').attr("data-view", "grid");
    $(".slotGamePanel").find(".DemoFree").addClass("inactive");
}

$(document).ready(function () {

    
    $('.loading').show();
    GetTotalJackpotAmount("gamepage");
    var regCode = Helper.LanguageCode.toLowerCase();
    var playIconBg = Helper.MediaUrl + "/Assets/images/Games/Icon/bg_O_2.png";
    var playBtn = Helper.MediaUrl + "/Assets/images/Games/Icon/play.png";
    var slotIconTemp = Helper.MediaUrl + "/Assets/images/Games/Slots/";
    $('#firstLoadPanel').hide();
    $.ajax({
        type: "GET",
        url: "/Services/AllSlotGames.ashx",
        async: true,
        cache: true,
        success: function (data) {
            var r = new Array(), j = -1;
            for (var key = 0, size = data.length; key < size; key++) {
                r[++j] = '<div class=\"mix-target ';
                r[++j] = data[key]["Prov"] + " " + data[key]["GmeCat"].toLocaleLowerCase() + ' column large-3 medium-3 small-6 end slotGamePanel slotGamePanelBoxWrap gridViewSlotHolder\" data-fav=\"1\" data-recent=\"1\" data-recommended=\"';
                r[++j] = data[key]["IsRec"] + "\" data-new=\"" + data[key]["IsNew"] + "\" data-order=\"" + data[key]["Order"] + '\" data-view=\"grid\" data-bo=\"\" data-payline=\"';
                r[++j] = data[key]["PayLine"] + "\" data-game-id=\"" + data[key]["Id"] + "\" data-name=\"";
                r[++j] = data[key]["Keys"] + "\">";
                r[++j] = '<div class=\"game-overlay grid\">';
                r[++j] = '<img onclick=\"currentGameType =\'';
                r[++j] = data[key]["WallCat"];
                r[++j] = "'; Games.EnterGame('" + data[key]["Prov"] + "','" + data[key]["Id"];
                r[++j] = "');\" class=\"slotGameImage lazyload\" data-src=\"" + slotIconTemp + data[key]["Prov"] + "/" + data[key]["OriImg"];
                r[++j] = '\" data-hover=\"';
                r[++j] = slotIconTemp + data[key]["Prov"] + "/" + data[key]["GifImg"];
                r[++j] = '\" width=\"294\" height=\"181\"/>';
                r[++j] = '<div class=\"playNowIconOverlay\" onclick=\"currentGameType =\'' + data[key]["WallCat"];
                r[++j] = "'; Games.EnterGame('" + data[key]["Prov"] + "','" + data[key]["Id"] + "');" + "\">";
                r[++j] = '<div style=\"position: relative;\">';
                r[++j] = '<div class=\"gameh-play-f\">';
                r[++j] = '<img class=\"w20 spinner\" src=\"' + playIconBg + '\" alt =\"loader\" width=\"65\" height=\"64\">';
                r[++j] = '</div>';
                r[++j] = '<div class=\"gameh-centerwrap\">';
                r[++j] = '<img width=\"79\" height=\"80\" class=\"gameh-centerplay w100p\" src=\"' + playBtn + '\" alt=\"play\">';
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '<div class=\"progressiveJackpotHolder ' + data[key]["DisJp"] + '\">';
                r[++j] = '<span class=\"icon icon-fun88_bookmark_star_checked\"></span>';
                r[++j] = '<span class=\"jackpotCurrencySymbol jackpotValueStyle\">';
                r[++j] = data[key]["Curr"];
                r[++j] = '</span>&nbsp;<span class=\"jackpotAmount jackpotValueStyle\" id=\"\" data-jp-incre=\"';
                r[++j] = data[key]["JpIncrVal"] + "\" data-jp-incre-period=\"";
                r[++j] = data[key]["JpIncrPer"] + '\">' + data[key]["JpVal"] + '</span>';
                r[++j] = '</div>';
                r[++j] =
                    '<div class=\"icon icon-fun88_bookmark_heart_unchecked slot-fav-icon inactive-fav-icon\" onclick= \"Games.InsertFavGames(\'';
                r[++j] = data[key]["Id"];
                r[++j] = "');\">";
                r[++j] = '</div>';
                r[++j] = '<div class=\"icon_positioning\">';
                r[++j] = '<div class=\"VendorLogo allProducts ' + data[key]["Prov"].toLowerCase() + 'Products\">';
                r[++j] = data[key]["ProvLogo"];
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '<div class=\"playNowButtonList btnLang' + regCode + '\" onclick=\"currentGameType =\'' + data[key]["WallCat"];
                r[++j] = "'; Games.EnterGame('" + data[key]["Prov"] + "','" + data[key]["Id"] + "');" + "\">";
                r[++j] = playNowButtonText;
                r[++j] = '</div>';
                r[++j] = '<div class=\"tryNowButtonList inactive ' + data[key]["IsDemo"] + ' freeSlot' + regCode + '\" onclick=\"Games.EnterDemoGame(\'' + data[key]["Prov"] + "','" + data[key]["Id"] + "');\">";
                r[++j] = TryNowButtonText;
                r[++j] = '</div>';
                r[++j] =
                    '<div class=\"freePlayHolder small-12 row hide-for-large\" style=\"margin-top: 5px;\" onclick=\"Games.EnterDemoGame(\'' + data[key]["Prov"] + "','" + data[key]["Id"] + "');\">";
                r[++j] = '<p class=\"demoPlayBtn ' + data[key]["IsDemo"] + '\">';
                r[++j] = data[key]["FreeText"];
                r[++j] = '</p>';
                r[++j] = '</div>';
                r[++j] =
                    '<div class=\"small-12 row gameNameInfoWrap\">';
                r[++j] = '<span class=\"newGameTag ' + data[key]["DisNew"] + '\">';
                r[++j] = data[key]["NewText"];
                r[++j] = '</span>';
                r[++j] = '<span class=\"gamesName\">';
                r[++j] = data[key]["GmeName"];
                r[++j] = '</span>';
                r[++j] =
                    '<div class=\"freePlayHolder show-for-large\" style=\"position: absolute; bottom: 3px; right: 0\" onclick =\"Games.EnterDemoGame(\'' + data[key]["Prov"] + "','" + data[key]["Id"] + "');\">";
                r[++j] = '<p class=\"demoPlayBtn ' + data[key]["IsDemo"] + '\">';
                r[++j] = data[key]["FreeText"];
                r[++j] = '</p>';
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '<div class=\"large-12 medium-12 row gameSlotInfoWrapper\">';
                r[++j] = '<span class=\"gameCategoryTextHolder\"><span class=\"gamesTypeName\">' +
                    data[key]["GmeType"] +
                    ',&nbsp;</span></span><span class=\"productProvider\" style=\"float: left;\">';
                r[++j] = data[key]["TransProv"];
                r[++j] = '</span>';
                r[++j] = '</div>';
                r[++j] = '</div>';
                r[++j] = '</div>';
            }
           
            $('#slotContainer').append(r.join(''));
            $('.loading').hide();
            if (Cookie.Get("_lastPlayedGame") != "" && typeof Cookie.Get("_lastPlayedGame") != "undefined") {
                var recGameIdList = Cookie.Get("_lastPlayedGame");
                var arrRecGameId = recGameIdList.split(',');
                $.each(arrRecGameId, function (i, k) {
                    $('.slotGamePanel[data-game-id="' + k + '"]').attr("data-recent", "0");
                });
            }

            if (Cookie.Get("_favouritesGame") != "" && typeof Cookie.Get("_favouritesGame") != "undefined") {
                var favGameIdList = Cookie.Get("_favouritesGame");
                var arrFavGameId = favGameIdList.split(',');
                $.each(arrFavGameId, function (i, k) {
                    $('.slotGamePanel[data-game-id="' + k + '"]').attr("data-fav", "0");
                    $('.slotGamePanel[data-game-id="' + k + '"]').find(".slot-fav-icon").removeClass("icon-fun88_bookmark_heart_unchecked").addClass("icon-fun88_bookmark_heart_checked");
                });

            }
            GrepGameBoardGamesDetails();
            $("img.lazy").lazyload({ effect: "fadeIn", failure_limit: 5, effectTime: 2000, threshold: 50 });
            
            var interestedGame = "";
            var productGroup = "";

            if (vendorquerystring !== null) {
                interestedGame = vendorquerystring;
                $('.mobile-filter-group').find('.chkBoxFilterResult[value=".' + interestedGame.toUpperCase() + '"]').icheck('checked');
                $('.chkBoxGameVendor[value=".' + interestedGame.toUpperCase() + '"]').click();
            }

            if (typequerystring !== null) {
                productGroup = typequerystring.toLowerCase();
                if (productGroup == "new" || productGroup == "recommended" || productGroup == "recent" || productGroup == "fav") {
                    $('.secondaryFiltering[data-type="' + productGroup + ":asc" + '"]').click();
                }
                else if (productGroup == "freespin" || productGroup == "bonusround") {
                    $('.chkBoxGameFeatures[value=".' + productGroup + '"]').click();
                } else {
                    $('.chkBoxGameProductGroup[value=".' + productGroup + '"]').click();
                }
            }
            if (vendorquerystring !== null || typequerystring !== null) {
                $('.filter-group.checkboxes').trigger("change");
            }

            /*if ($('.gamefilter[data-type="myBoard"]').hasClass('active') && vendorquerystring == '') {
                $('.gamefilter[data-type="myBoard"]').trigger("click");
            }*/


            $('.slotGamePanel').on("mouseenter touchstart", { passive: true }, function () {
                if ($(this).hasClass("list")) {
                    $(".slotGamePanel").find(".DemoFree").addClass("inactive");
                    if ($(this).find(".DemoFree").hasClass("inactive")) {
                        $(this).find(".DemoFree").removeClass("inactive");
                    }
                }
            });

            $('.game-overlay').on("mouseenter touchstart", { passive: true }, function () {
                if ($(this).hasClass("grid")) {
                    var hoverImage = $(this).find(".slotGameImage").attr("data-src");
                    if ($(this).find(".slotGameImage").attr("data-hover") !== "") {
                        hoverImage = $(this).find(".slotGameImage").attr("data-hover");
                    }
                    $(this).find(".slotGameImage").attr("src", hoverImage);
                    $('.game-overlay').not($(this)).find(".gameh-play-f").css("opacity", "0");
                    $('.game-overlay').not($(this)).find(".gameh-centerplay").css("opacity", "0");
                    $('.game-overlay').not($(this)).find(".playNowIconOverlay").hide();
                    $(this).find(".gameh-play-f").css("opacity", "1");
                    $(this).find(".gameh-centerplay").css("opacity", "1");
                    $(this).find(".playNowIconOverlay").show();
                    $('.game-overlay').not($(this)).parents(".slotGamePanel").find(".DemoFree").addClass("inactive");
                    if ($(this).parents(".slotGamePanel").find(".DemoFree").hasClass("inactive")) {
                        $(this).parents(".slotGamePanel").find(".DemoFree").removeClass("inactive");
                    }
                }
            }).on("mouseleave touchend", function () {
                if ($(this).hasClass("grid")) {
                    var oriImage = $(this).find(".slotGameImage").attr("data-src");
                    $(this).find(".slotGameImage").attr("src", oriImage);
                    $('.game-overlay').not($(this)).find(".gameh-play-f").css("opacity", "0");
                    $('.game-overlay').not($(this)).find(".gameh-centerplay").css("opacity", "0");
                    $('.game-overlay').not($(this)).find(".playNowIconOverlay").hide();
                    $(this).find(".gameh-play-f").css("opacity", "1");
                    $(this).find(".gameh-centerplay").css("opacity", "1");
                    $(this).find(".playNowIconOverlay").show();
                }
            });
            /*if ((vendorquerystring == null || vendorquerystring == "") && typequerystring == null) {
                $('#firstLoadPanel').show();
            }*/
            
        }
    });

    var firstLoad = true;
    $('input[type="checkbox"]').icheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%',
        cursor: true,
        labelHover: true
    });

    $('#payLineRange').ionRangeSlider({
        type: "double",
        from: 0,
        to: 20,
        values: ["1", "2", "3", "5", "8", "9", "10", "13", "15", "18", "20", "25", "30", "40", "45", "50", "60", "100", "243", "720", "1024"],
        hide_min_max: true,
        hide_from_to: true,
        onStart: function (data) {
            $('#fromAmount').text(1);
            $('#toAmount').text(1024);
        },
        onChange: function (data) {
            firstLoad = false;
            $('#fromAmount').text(data.from_value);
            $('#toAmount').text(data.to_value);
        },
        onFinish: function (data) {
            if (!firstLoad) {
                var joinedData = data.from_value + ";" + data.to_value;
                setTimeout(function () {
                    $('input[type="text"][name="payLineRangeName"]').val(joinedData);
                    $('.filter-group.payLineValue').trigger("change");
                }, 500);
            }
        }
    });

    slider = $("#payLineRange").data("ionRangeSlider");


    $('#mobilepayLineRange').ionRangeSlider({
        type: "double",
        from: 0,
        to: 20,
        values: ["1", "2", "3", "5", "8", "9", "10", "13", "15", "18", "20", "25", "30", "40", "45", "50", "60", "100", "243", "720", "1024"],
        hide_min_max: true,
        hide_from_to: true,
        onStart: function (data) {
            $('#mobilefromAmount').text(1);
            $('#mobiletoAmount').text(1024);
        },
        onChange: function (data) {
            firstLoad = false;
            $('#mobilefromAmount').text(data.from_value);
            $('#mobiletoAmount').text(data.to_value);
        },
        onFinish: function (data) {
            if (!firstLoad) {
                var joinedData = data.from_value + ";" + data.to_value;
                setTimeout(function () {
                    $('input[type="text"][name="payLineRangeName"]').val(joinedData);
                }, 500);
            }
        }
    });
    mobileSlider = $("#mobilepayLineRange").data("ionRangeSlider");


});