﻿var WalletEnum = {
    WalletDisplayPage: { MyAccount: 0, FinancePage: 1, LoginBar: 2, TransferPage: 3, TransferSelfExclusion: 4 },
    _slotWallet: ["PT", "SLOT"],
    _casinoWallet: ["GD", "AG", "SAG", "AB", "SUN", "EA", "OPUS", "BOY2", "CP", "MGSQF", "IPK","LD"], 
    _sportWallet: ["SP", "IPSB", "BFR", "SB", "SBT"],
    _kenoWallet: ["PK", "LBK", "SLC"],
    _otherWallet: ["TotalBal", "MAIN"]
}

var WalletData = null;
var timeout, xhr;
var walletRunning = false;
var triggerClick = true;

function clickReload() { if (timeout) clearTimeout(timeout); if (xhr) xhr.abort(); timeout = setTimeout(function () { $("#btnReload1").click() }, 500) }

function getUniqueWalletList(arr) {
    var unique = [];
    for (var i = 0 ; i < arr.length ; ++i) {
        if (unique.indexOf(arr[i]) == -1)
            unique.push(arr[i]);
    }
    return unique;
}

function getWalletList(action, isForceReload, fromPage) {
    isForceReload = isForceReload != undefined && isForceReload;
   if (WalletData != null && !isForceReload) {
        BindWallet(isForceReload, action, WalletData, fromPage);
   } else {
       WalletData = null;
        if (!walletRunning) {
            walletRunning = true;
            $.ajax({
                type: "GET",
                async:true,
                url: "/Services/AllBalance.ashx?forceReload=" + isForceReload,
                success: function (data) {
                    switch (data) {
                        case "timeout":
                            window.location = "/" + Helper.UrlCulture + "/error/sessiontimedout.htm";
                            break;
                        case "multi":
                            window.location = "/" + Helper.UrlCulture + "/error/multiplelogin.htm";
                            break;
                    }
                    WalletData = data;
                    BindWallet(isForceReload, action, WalletData);
                    walletRunning = false;
                }
            });
        } else {
            setTimeout(function () {
                getWalletList(action, false);
            },
                2000);
        }
    }
}

function GetWalletDataForTransfer() {
    if (WalletData != null) {
        return WalletData.WalletBalances;
    } else {
        return false;
    }
}

function BindWallet(isForceReload, action, data, fromPage) {
    var preferWallet = data.PreferredWallet;
    if (action === WalletEnum.WalletDisplayPage.MyAccount) {
        var uniqueWallet = [];
        var uniqueWalletListHolder = []; 
        if (data && data.WalletBalances) {
            var walletListRpt = $("#walletList .walletListType").eq(0).clone(true);
            $("#walletList .walletListType").eq(0).remove();
            var categoryName = "";
            //var amount = 0.00;
            var totalSportAmount;
            var totalCasinoAmount;
            var totalSlotAmount;
            var totalKenoAmount;
            var sportParentName;
            var casinoParentName;
            var slotParentName;
            var kenoParentName;
            
            $(data.WalletBalances).each(function (index, walletObj) {
                if (walletObj.Name === "SlotMainBal") {
                    totalSlotAmount = walletObj.BalanceWithCurrencySymbol;
                    slotParentName = walletObj.LocalizedName;
                }
                else if (walletObj.Name == "CasinoMainBal") {
                    totalCasinoAmount = walletObj.BalanceWithCurrencySymbol;
                    casinoParentName = walletObj.LocalizedName;
                }
                else if (walletObj.Name == "KenoMainBal") {
                    totalKenoAmount = walletObj.BalanceWithCurrencySymbol;
                    kenoParentName = walletObj.LocalizedName;
                }
                else if (walletObj.Name == "SportMainBal") {
                    totalSportAmount = walletObj.BalanceWithCurrencySymbol;
                    sportParentName = walletObj.LocalizedName;
                }
                switch (true) {
                    case WalletEnum._casinoWallet.indexOf(walletObj.Name) >= 0:
                        $('.wallet-color-bar', walletListRpt).addClass("casino MyAccountSpecialColorBar");
                        categoryName = "casino";
                        break;
                    case WalletEnum._slotWallet.indexOf(walletObj.Name) >= 0:
                        $('.wallet-color-bar', walletListRpt).addClass("slot MyAccountSpecialColorBar");
                        categoryName = "slot";
                        break;
                    case WalletEnum._sportWallet.indexOf(walletObj.Name) >= 0:
                        $('.wallet-color-bar', walletListRpt).addClass("sport MyAccountSpecialColorBar");
                        categoryName = "sport";
                        break;
                    case WalletEnum._kenoWallet.indexOf(walletObj.Name) >= 0:
                        $('.wallet-color-bar', walletListRpt).addClass("keno MyAccountSpecialColorBar");
                        categoryName = "keno";
                        break;
                }

                if (categoryName != "") {
                    uniqueWallet.push(categoryName);
                }
                if (walletObj.Name == "TotalBal" || walletObj.Name == "MAIN") {
                    $('.walletName', walletListRpt).html("<b>" + walletObj.LocalizedName + "</b>");
                } else {
                    $('.walletName', walletListRpt).html(walletObj.LocalizedName);
                }
                

                $('.walletAmount', walletListRpt).html(walletObj.BalanceWithCurrencySymbol);
                $('#walletList').append(walletListRpt);
                walletListRpt = $("#walletList .walletListType").eq(0).clone(true);
                $('.walletListType').eq(index).attr("data-name", walletObj.Name);
                $('.walletListType').eq(index).attr("data-category", categoryName);
                $('.walletListType .wallet-color-bar').addClass("MyAccountSpecialColorBar");
            });
            var uniqueWalletList = getUniqueWalletList(uniqueWallet);
            $.each(uniqueWalletList, function (i, k) {
                var finalAmount = 0.00;
                var walletName = "";
                switch (k) {
                    case 'sport':
                        finalAmount = totalSportAmount;
                        walletName = sportParentName;
                        break;
                    case 'casino':
                        finalAmount = totalCasinoAmount;
                        walletName = casinoParentName;
                        break;
                    case 'slot':
                        finalAmount = totalSlotAmount;
                        walletName = slotParentName;
                        break;
                    case 'keno':
                        finalAmount = totalKenoAmount;
                        walletName = kenoParentName;
                        break;
                }
                uniqueWalletListHolder.push('<div class="walletSlider" data-category="' + k + '">');
                uniqueWalletListHolder.push('<li class="walletListTypeNew parent" data-name="' + k + ' parent" data-action ="' + k + '" style="position:relative;border-top: 1px solid #cecece;padding:0;">');
                uniqueWalletListHolder.push('<span class="wallet-color-bar MyAccountSpecialColorBar ' + k + '" style="width: 10px;"></span>');
                uniqueWalletListHolder.push('<span class="walletName" style="width: 45%; margin-left: 5px;"><h7><b>' + walletName + '</b></h7></span>');
                uniqueWalletListHolder.push('<span class="walletAmount" style="width: 20%; margin-left: 3px; display:inline-block;">' + finalAmount + '</span>');
                uniqueWalletListHolder.push('<span class="walletPreferred" style="width: 14%; margin-left: 5px; vertical-align:middle; display:inline-block;"></span><span class="arrow-icon icon icon-fun88_arrow_thick_down" style="top: 35%; position: absolute;"></span></li><div style="display:none;" class="walletCategoryName">');

                $('.walletListType[data-category="' + k + '"]').each(function () {
                    var walletName = $(this).attr("data-name");
                    uniqueWalletListHolder.push('<li class="walletListTypeNew" data-name="' + k + '" data-wallet="' + walletName + '" style="position:relative;">');
                    uniqueWalletListHolder.push($(this).html());
                    uniqueWalletListHolder.push('</li>');
                    $(this).remove();
                });
                uniqueWalletListHolder.push('</div>');
                uniqueWalletListHolder.push('</div>');
            });
            $('#walletList').append(uniqueWalletListHolder.join(''));
            $('.walletListType[data-name="SlotMainBal"],.walletListType[data-name="CasinoMainBal"],.walletListType[data-name="KenoMainBal"],.walletListType[data-name="SportMainBal"]').remove();
            $('.walletListTypeNew[data-wallet="' + data.PreferredWallet + '"]').find(".walletPreferred").addClass("icon icon-tick-circle-button");
            if (data.PreferredWallet == "MAIN") {
                $('.walletListType[data-name="MAIN"]').children(".walletPreferred").addClass("icon icon-tick-circle-button");
            }
            $(".loading-gif").hide();

            $('.walletListTypeNew.parent').on("click", function () {
                var iconElmtHasClass = $(this).children(".icon").hasClass("icon-fun88_arrow_thick_down");
                var iconElmt = $(this).children(".arrow-icon");
                $(this).siblings("div.walletCategoryName").slideToggle(200, function () {
                    if (iconElmtHasClass) {
                        iconElmt.removeClass("icon-fun88_arrow_thick_down").addClass("icon-fun88_arrow_thick_up");
                        $(this).prev().find(".walletPreferred").removeClass("icon icon-tick-circle-button");
                    } else {
                        iconElmt.removeClass("icon-fun88_arrow_thick_up").addClass("icon-fun88_arrow_thick_down");
                        if ($(this).find(".icon-tick-circle-button").length > 0) {
                            $(this).prev().find(".walletPreferred").addClass("icon icon-tick-circle-button");
                        }
                    }
                });
            });

            $(".icon-tick-circle-button").parents().eq(1).prev().click();
        }
    } else if (action === WalletEnum.WalletDisplayPage.FinancePage) {
        if (data && data.WalletBalances) {
            var totalBalanceAmt, preferredWalletAmt, mainAccBalanceAmt;
            var preferredWalletName = data.PreferredWallet;
            if (preferredWalletName === "") {
                preferredWalletName = "MAIN";
            }
            $(data.WalletBalances).each(function (index, walletObj) {
                switch (walletObj.Name) {
                    case 'TotalBal':
                        if (walletObj.FormattedBalance != 'N/A') {
                            totalBalanceAmt = walletObj.FormattedBalance.replace(/^[a-zA-Z]+/g, '');
                        } else {
                            mainAccBalanceAmt = walletObj.FormattedBalance;
                        }
                        $('#totalBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                        $('#totalBalanceAmt').parent().siblings(".loading-gif-finance").hide();
                        $('#totalBalanceAmt').parent("div.walletAmountPlaceHolder").fadeIn(2000);
                        $('#totalBalance').val(totalBalanceAmt);
                        break;
                    case 'MAIN':
                        if (walletObj.FormattedBalance != 'N/A') {
                            mainAccBalanceAmt = walletObj.FormattedBalance.replace(/^[a-zA-Z]+/g, '');
                        } else {
                            mainAccBalanceAmt = walletObj.FormattedBalance;
                        }
                        $('#mainAccountBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                        $('#mainAccountBalanceAmt').parent().siblings(".loading-gif-finance").hide();
                        $('#mainAccountBalanceAmt').parent("div.walletAmountPlaceHolder").fadeIn(2000);
                        break;
                    default:
                        break;
                }
                if (walletObj.Name === preferredWalletName) {
                    if (walletObj.FormattedBalance != 'N/A') {
                        preferredWalletAmt = walletObj.FormattedBalance.replace(/^[a-zA-Z]+/g, '');
                    } else {
                        preferredWalletAmt = walletObj.FormattedBalance;
                    }
                    $('#mainWalletBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                    $('#mainWalletBalanceAmt').parent().siblings(".loading-gif-finance").hide();
                    $('#mainWalletBalanceAmt').parent("div.walletAmountPlaceHolder").fadeIn(2000);
                }
            });
        }
    } else if (action === WalletEnum.WalletDisplayPage.LoginBar) {
        if ($('#loginPanelBalanceList .walletPopulateList').length > 1) {
            $(data.WalletBalances).each(function (index, walletObj) {
                if (typeof walletObj.Name != "undefined" && walletObj.Name !== "SlotMainBal" && walletObj.Name !== "CasinoMainBal" && walletObj.Name !== "KenoMainBal" && walletObj.Name !== "SportMainBal") {
                    setTimeout(function() {
                        $('#loginPanelBalanceList .walletPopulateList[data-name="' + walletObj.Name + '"]').find(".walletAmount").html(walletObj.BalanceWithCurrencySymbol);
                        if (data.PreferredWallet == walletObj.Name) {
                            $('.walletPopulateList[data-name="' + walletObj.Name + '"]').find(".preferWallet").addClass("icon-tick-circle-button");
                        }
                        if (walletObj.FormattedBalance == "N/A")
                            $('.walletPopulateList[data-name="' + walletObj.Name + '"]').find(".walletAmount").addClass("not-available");
                    }, 100);
                }
            });
            $('.totalBalanceAmount').html(data.WalletBalances[0].BalanceWithCurrencySymbol);

            $(".mobile-total-balance").html(data.WalletBalances[0].BalanceWithCurrencySymbol);
            var frWallet = $('#frWalletOption').attr("data-wallet");
            var toWallet = $('#toWalletOption').attr("data-wallet");
            clickFrWalletOptions($("#frWalletDropdown .walletPopulateList[data-name='" + frWallet + "']"));
            clickToWalletOptions($("#toWalletDropdown .walletPopulateList[data-name='" + toWallet + "']"));
        } else {
            if (data && data.WalletBalances) {
                var walletListLoginRpt = $("#loginPanelBalanceList .walletPopulateList").eq(0).clone(true);
                $("#loginPanelBalanceList .walletPopulateList").eq(0).remove();
                $('.totalBalanceAmount').html(data.WalletBalances[0].BalanceWithCurrencySymbol);
                $(".mobile-total-balance").html(data.WalletBalances[0].BalanceWithCurrencySymbol);
                $(data.WalletBalances).each(function (index, walletObj) {

                    switch (true) {
                        case WalletEnum._casinoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListLoginRpt).addClass("casino");
                            break;
                        case WalletEnum._slotWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListLoginRpt).addClass("slot");
                            break;
                        case WalletEnum._sportWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListLoginRpt).addClass("sport");
                            break;
                        case WalletEnum._kenoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListLoginRpt).addClass("keno");
                            break;
                    }


                    if (preferWallet == walletObj.Name)
                        $('.preferWallet', walletListLoginRpt).addClass("icon-tick-circle-button");

                    if (walletObj.FormattedBalance == "N/A")
                        $('.walletAmount', walletListLoginRpt).addClass("not-available");


                    $('.walletName', walletListLoginRpt).html(walletObj.LocalizedName + ":");


                    $('.walletAmount', walletListLoginRpt).html(walletObj.BalanceWithCurrencySymbol);
                    $('#loginPanelBalanceList').append(walletListLoginRpt);
                    walletListLoginRpt = $("#loginPanelBalanceList .walletPopulateList").eq(0).clone(true);
                    $('.walletPopulateList').eq(index).attr("data-name", walletObj.Name);
                });
                $(".walletPopulateList").each(function () {
                    var wallet = $(this).clone();
                    $("#mobile-wallet-view").append(wallet);
                });
                $('.walletPopulateList[data-name="SlotMainBal"],.walletPopulateList[data-name="CasinoMainBal"],.walletPopulateList[data-name="KenoMainBal"],.walletPopulateList[data-name="SportMainBal"]').remove();
            }
        }
    } else if (action === WalletEnum.WalletDisplayPage.TransferPage) {
        if (data && data.WalletBalances) {
            var walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
            var walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
            $("#frWalletDropdown .walletPopulateList").remove();
            $("#toWalletDropdown .walletPopulateList").remove();

            $(data.WalletBalances).each(function (index, walletObj) {
                if (typeof walletObj.Name != "undefined" && walletObj.Name !== "SlotMainBal" && walletObj.Name !== "CasinoMainBal" && walletObj.Name !== "KenoMainBal" && walletObj.Name !== "SportMainBal") {
                    if (walletObj.Name == "TotalBal") {
                        $('.walletName', walletListFrRpt).html(walletObj.LocalizedName + " " + transferAllText);
                        $('.walletAmount', walletListFrRpt).html(walletObj.BalanceWithCurrencySymbol);
                        $('.walletAmount', walletListFrRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                        $('#totalBalanceAmt').val(walletObj.BalanceWithCurrencySymbol);
                    } else {
                        $('.walletName', walletListFrRpt).html(walletObj.LocalizedName);
                        $('.walletAmount', walletListFrRpt).html(walletObj.BalanceWithCurrencySymbol);
                        $('.walletAmount', walletListFrRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                    }
                    if (walletObj.Name == "MAIN") {
                        $('#mainAccountBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                    }
                    if (walletObj.Name == data.PreferredWallet) {
                        $('#mainWalletBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                    }
                    $(walletListFrRpt).attr('data-name', walletObj.Name);
                    $('.walletName', walletListToRpt).html(walletObj.LocalizedName);
                    $('.walletAmount', walletListToRpt).html(walletObj.BalanceWithCurrencySymbol);
                    $('.walletAmount', walletListToRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                    $(walletListToRpt).attr('data-name', walletObj.Name);
                    switch (true) {
                        case WalletEnum._casinoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("casino");
                            $('.wallet-color-bar', walletListFrRpt).addClass("casino");
                            break;
                        case WalletEnum._slotWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("slot");
                            $('.wallet-color-bar', walletListFrRpt).addClass("slot");
                            break;
                        case WalletEnum._sportWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("sport");
                            $('.wallet-color-bar', walletListFrRpt).addClass("sport");
                            break;
                        case WalletEnum._kenoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("keno");
                            $('.wallet-color-bar', walletListFrRpt).addClass("keno");
                            break;
                    }
                    $('#frWalletDropdown').append(walletListFrRpt);
                    $('#toWalletDropdown').append(walletListToRpt);
                    walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
                    walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
                    $('#frWalletDropdown .walletPopulateList').eq(index).attr("data-name", walletObj.Name);
                    $('#toWalletDropdown .walletPopulateList').eq(index).attr("data-name", walletObj.Name);
                }
            });
            walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
            walletListFrRpt.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"></span>');
            walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
            walletListToRpt.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"></span>');
            $('#frWalletOption').children().remove();
            $('#toWalletOption').children().remove();
            $('#frWalletOption').append(walletListFrRpt);
            $('#frWalletOption').append(walletListFrRpt);
            $('#frWalletOption').children().removeAttr('onclick');
            $('#frWalletOption').children().children().first().css('float', 'left');
            $('#frWalletOption .walletPopulateList .wallet-color-bar').css('margin', '0 5px');
            $('#frWalletOption .walletPopulateList .walletName').css('margin-top', '2px');
            $('#frWalletOption').attr('data-wallet', $('#frWalletDropdown .walletPopulateList').eq(0).attr("data-name"));
            $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
            $('#toWalletOption').append(walletListToRpt);
            $('#toWalletOption').children().removeAttr('onclick');
            $('#toWalletOption').children().children().first().css('float', 'left');
            $('#toWalletOption .walletPopulateList .wallet-color-bar').css('margin', '0 5px');
            $('#toWalletOption .walletPopulateList .walletName').css('margin-top', '2px');
            $('#toWalletOption').attr('data-wallet', $('#toWalletDropdown .walletPopulateList').eq(0).attr("data-name"));
            $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());

            $(window).resize(function () {
                $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
                $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());

                if ($('#frWalletDropDown').width() != $('frWalletOption').width()) {
                    $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
                }
                if ($('#toWalletDropdown').width() != $('toWalletOption').width()) {
                    $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());
                }
            });

            

            var ldWallet = ["NLE", "SAL", "ABT", "TGP", "GDL", "BOL","EBT"];
            if (fromPage == undefined || fromPage === "") {
                $("#toWalletDropdown .walletPopulateList[data-name='" + data.PreferredWallet + "']").click();
            } else if (typeof tempProvider !== "undefined") {
                var checkSlot = ($.inArray(tempProvider, WalletEnum._casinoWallet) > -1 || $.inArray(tempProvider, WalletEnum._sportWallet) > -1 || $.inArray(tempProvider, WalletEnum._kenoWallet) > -1 || $.inArray(tempProvider, WalletEnum._otherWallet) > -1);
                if (checkSlot) {
                    if (tempProvider === "MGSQF") {
                        $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click();
                    } else if (ldWallet.indexOf(tempProvider) > -1) {
                        $("#toWalletDropdown .walletPopulateList[data-name='LD']").click();
                    }
                    else {
                        $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click();
                    }
                } else {
                    if (tempProvider === "PT") {
                        $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click();
                    } else if (ldWallet.indexOf(tempProvider) > -1) {
                        $("#toWalletDropdown .walletPopulateList[data-name='LD']").click();
                    }else {
                        $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click();
                    }
                }

               
            } else {
                $("#toWalletDropdown .walletPopulateList[data-name='" + $("#HiddenToWallet").val() + "']").click();
            }
            $("#frWalletDropdown .walletPopulateList[data-name='MAIN']").click();
            $("#toWalletDropdown .walletPopulateList[data-name='TotalBal']").remove();
            if ($('#AfterTransferTo').val() != '' && $('#AfterTransferFr').val() != '') {
                $("#toWalletDropdown .walletPopulateList[data-name='" + $('#AfterTransferTo').val() + "']").click();
                $("#frWalletDropdown .walletPopulateList[data-name='" + $('#AfterTransferFr').val() + "']").click();
                $('#AfterTransferFr').val('');
                $('#AfterTransferTo').val('');
            }
        }
    } else if (action === WalletEnum.WalletDisplayPage.TransferSelfExclusion) {
        if (data && data.WalletBalances) {
            var walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
            var walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
            $("#frWalletDropdown .walletPopulateList").remove();
            $("#toWalletDropdown .walletPopulateList").remove();

            $(data.WalletBalances).each(function (index, walletObj) {
                if (typeof walletObj.Name != "undefined" && walletObj.Name !== "SlotMainBal" && walletObj.Name !== "CasinoMainBal" && walletObj.Name !== "KenoMainBal" && walletObj.Name !== "SportMainBal") {
                    if (walletObj.Name == "TotalBal") {
                        $('.walletName', walletListFrRpt).html(walletObj.LocalizedName + " " + transferAllText);
                        $('.walletAmount', walletListFrRpt).html(walletObj.BalanceWithCurrencySymbol);
                        $('.walletAmount', walletListFrRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                        $('#totalBalanceAmt').val(walletObj.BalanceWithCurrencySymbol);
                    } else {
                        $('.walletName', walletListFrRpt).html(walletObj.LocalizedName);
                        $('.walletAmount', walletListFrRpt).html(walletObj.BalanceWithCurrencySymbol);
                        $('.walletAmount', walletListFrRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                    }
                    if (walletObj.Name == "MAIN") {
                        $('#mainAccountBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                    }
                    if (walletObj.Name == data.PreferredWallet) {
                        $('#mainWalletBalanceAmt').html(walletObj.BalanceWithCurrencySymbol);
                    }
                    $(walletListFrRpt).attr('data-name', walletObj.Name);
                    $('.walletName', walletListToRpt).html(walletObj.LocalizedName);
                    $('.walletAmount', walletListToRpt).html(walletObj.BalanceWithCurrencySymbol);
                    $('.walletAmount', walletListToRpt).attr('wallet-balance', walletObj.Balance == -1 ? 0 : walletObj.Balance);
                    $(walletListToRpt).attr('data-name', walletObj.Name);
                    switch (true) {
                        case WalletEnum._casinoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("casino");
                            $('.wallet-color-bar', walletListFrRpt).addClass("casino");
                            break;
                        case WalletEnum._slotWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("slot");
                            $('.wallet-color-bar', walletListFrRpt).addClass("slot");
                            break;
                        case WalletEnum._sportWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("sport");
                            $('.wallet-color-bar', walletListFrRpt).addClass("sport");
                            break;
                        case WalletEnum._kenoWallet.indexOf(walletObj.Name) >= 0:
                            $('.wallet-color-bar', walletListToRpt).addClass("keno");
                            $('.wallet-color-bar', walletListFrRpt).addClass("keno");
                            break;
                    }
                    $('#frWalletDropdown').append(walletListFrRpt);
                    walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
                    $('#frWalletDropdown .walletPopulateList').eq(index).attr("data-name", walletObj.Name);
                    if (walletObj.Name === "MAIN") {
                        $('#toWalletDropdown').append(walletListToRpt);
                        walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
                        $('#toWalletDropdown .walletPopulateList').eq(index).attr("data-name", walletObj.Name);
                    }
                }
            });
            walletListFrRpt = $("#frWalletDropdown .walletPopulateList").eq(0).clone(true);
            walletListFrRpt.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"></span>');
            walletListToRpt = $("#toWalletDropdown .walletPopulateList").eq(0).clone(true);
            walletListToRpt.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"></span>');
            $('#frWalletOption').children().remove();
            $('#toWalletOption').children().remove();
            $('#frWalletOption').append(walletListFrRpt);
            $('#frWalletOption').append(walletListFrRpt);
            $('#frWalletOption').children().removeAttr('onclick');
            $('#frWalletOption').children().children().first().css('float', 'left');
            $('#frWalletOption .walletPopulateList .wallet-color-bar').css('margin', '0 5px');
            $('#frWalletOption .walletPopulateList .walletName').css('margin-top', '2px');
            $('#frWalletOption').attr('data-wallet', $('#frWalletDropdown .walletPopulateList').eq(0).attr("data-name"));
            $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
            $('#toWalletOption').append(walletListToRpt);
            $('#toWalletOption').children().removeAttr('onclick');
            $('#toWalletOption').children().children().first().css('float', 'left');
            $('#toWalletOption .walletPopulateList .wallet-color-bar').css('margin', '0 5px');
            $('#toWalletOption .walletPopulateList .walletName').css('margin-top', '2px');
            $('#toWalletOption').attr('data-wallet', $('#toWalletDropdown .walletPopulateList').eq(0).attr("data-name"));
            $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());

            $(window).resize(function () {
                $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
                $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());

                if ($('#frWalletDropDown').width() != $('frWalletOption').width()) {
                    $('#frWalletDropdown').css('width', $('#frWalletOption').outerWidth());
                }
                if ($('#toWalletDropdown').width() != $('toWalletOption').width()) {
                    $('#toWalletDropdown').css('width', $('#toWalletOption').outerWidth());
                }
            });
  
            if (fromPage == undefined || fromPage === "") {
                $("#toWalletDropdown .walletPopulateList[data-name='" + data.PreferredWallet + "']").click();
            } else if (typeof tempProvider !== "undefined") {
                var checkSlot = ($.inArray(tempProvider, WalletEnum._casinoWallet) > -1 || $.inArray(tempProvider, WalletEnum._sportWallet) > -1 || $.inArray(tempProvider, WalletEnum._kenoWallet) > -1 || $.inArray(tempProvider, WalletEnum._otherWallet) > -1);
                if (checkSlot) {
                    if (tempProvider === "MGSQF") {
                        $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click();
                    } else {
                        $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click();
                    }
                } else {
                    if (tempProvider === "PT") {
                        $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click();
                    } else {
                        $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click();
                    }
                }
            } else {
                $("#toWalletDropdown .walletPopulateList[data-name='" + $("#HiddenToWallet").val() + "']").click();
            }
            $("#frWalletDropdown .walletPopulateList[data-name='MAIN']").click();
            $("#toWalletDropdown .walletPopulateList[data-name='TotalBal']").remove();
            if ($('#AfterTransferTo').val() != '' && $('#AfterTransferFr').val() != '') {
                $("#toWalletDropdown .walletPopulateList[data-name='" + $('#AfterTransferTo').val() + "']").click();
                $("#frWalletDropdown .walletPopulateList[data-name='" + $('#AfterTransferFr').val() + "']").click();
                $('#AfterTransferFr').val('');
                $('#AfterTransferTo').val('');
            }
        }
    }
    $('#frWalletOption').removeAttr("disabled");
    $('#toWalletOption').removeAttr("disabled");
    $(document).on("click", function (e) {
        if (e.target != $("#toWalletOption")[0] && e.target != $("#toWalletOption span")[0] && e.target != $("#toWalletOption span")[1]
            && e.target != $("#toWalletOption span")[2] && e.target != $("#toWalletOption span")[3]) {
            if ($('#toWalletDropdown').hasClass("is-open")) {
                $('#toWalletDropdown').removeClass("is-open");
            }
        }
        if (e.target != $("#frWalletOption")[0] && e.target != $("#frWalletOption span")[0] && e.target != $("#frWalletOption span")[1]
            && e.target != $("#frWalletOption span")[2] && e.target != $("#frWalletOption span")[3]) {
            if ($('#frWalletDropdown').hasClass("is-open")) {
                $('#frWalletDropdown').removeClass("is-open");
            }
        }
    });
    //to set the color bar in finance master page
    $('.preferColorBar').addClass($('.preferWallet.icon-tick-circle-button').siblings('.wallet-color-bar').attr('class'));
}

$('#refreshAccountBalance').click(function () {
    $('#loginPanelBalanceList .walletPopulateList .walletAmount').removeClass("not-available");
    $('#loginPanelBalanceList .walletPopulateList .preferWallet').removeClass("icon-tick-circle-button");
    $('#loginPanelBalanceList .walletPopulateList .walletAmount, .totalBalanceAmount').html(loadingText);
    getWalletList(WalletEnum.WalletDisplayPage.LoginBar, true);
});

function clickToWalletOptions(e) {
    var selectColor = $(".wallet-color-bar", e).attr("class");
    $('#toWalletOption').removeClass('hover');
    $('#toWalletOption').attr('aria-expanded', 'false');
    $('#toWalletDropdown').removeClass('is-open');
    $('#toWalletDropdown').attr('aria-hidden', 'true');
    $('#toWalletOption .WalletPopulateList').attr('data-name', e.attr('data-name'));
    $('#toWalletOption .walletAmount').attr('wallet-balance', e.children('.walletAmount').attr('wallet-balance'));
    $('#toWalletOption .walletName').text(e.children('.walletName').text());
    $('#toWalletOption .walletAmount').text(e.children('.walletAmount').text());
    $('#toWalletOption').attr('data-wallet', e.attr('data-name'));
    $('#toWalletOption .wallet-color-bar').removeClass().addClass(selectColor);
    $("#HiddenToWallet").val(e.attr('data-name'));
}

function clickFrWalletOptions(e) {
    var selectColor = $(".wallet-color-bar", e).attr("class");
    $('#frWalletOption').removeClass('hover');
    $('#frWalletOption').attr('aria-expanded', 'false');
    $('#frWalletDropdown').removeClass('is-open');
    $('#frWalletDropdown').attr('aria-hidden', 'true');
    $('#frWalletOption .WalletPopulateList').attr('data-name', e.attr('data-name'));
    $('#frWalletOption .walletAmount').attr('wallet-balance', e.children('.walletAmount').attr('wallet-balance'));
    $('#frWalletOption .walletName').text(e.children('.walletName').text());
    $('#frWalletOption .walletAmount').text(e.children('.walletAmount').text());
    $('#frWalletOption').attr('data-wallet', e.attr('data-name'));
    $('#frWalletOption .wallet-color-bar').removeClass().addClass(selectColor);
}

function dropdownlist(e) {
    if (e.parent().attr('id').indexOf('fr') > -1) {
        var selectColor = $(".wallet-color-bar", e).attr("class");
        $('#frWalletOption').removeClass('hover');
        $('#frWalletOption').attr('aria-expanded', 'false');
        $('#frWalletDropdown').removeClass('is-open');
        $('#frWalletDropdown').attr('aria-hidden', 'true');
        $('#frWalletOption .WalletPopulateList').attr('data-name', e.attr('data-name'));
        $('#frWalletOption .walletAmount').attr('wallet-balance', e.children('.walletAmount').attr('wallet-balance'));
        $('#frWalletOption .walletName').text(e.children('.walletName').text());
        $('#frWalletOption .walletAmount').text(e.children('.walletAmount').text());
        $('#frWalletOption').attr('data-wallet', e.attr('data-name'));
        $('#frWalletOption .wallet-color-bar').removeClass().addClass(selectColor);
        if ($(e).attr('data-name') == "TotalBal") {
            $('#upBonus').hide();
        } else {
            $('#upBonus').show();
        }
        calculateTransferBalance();
    } else {
        var selectColor = $(".wallet-color-bar", e).attr("class");
        $('#toWalletOption').removeClass('hover');
        $('#toWalletOption').attr('aria-expanded', 'false');
        $('#toWalletDropdown').removeClass('is-open');
        $('#toWalletDropdown').attr('aria-hidden', 'true');
        $('#toWalletOption .WalletPopulateList').attr('data-name', e.attr('data-name'));
        $('#toWalletOption .walletAmount').attr('wallet-balance', e.children('.walletAmount').attr('wallet-balance'));
        $('#toWalletOption .walletName').text(e.children('.walletName').text());
        $('#toWalletOption .walletAmount').text(e.children('.walletAmount').text());
        $('#toWalletOption').attr('data-wallet', e.attr('data-name'));
        $('#toWalletOption .wallet-color-bar').removeClass().addClass(selectColor);
        $("#HiddenToWallet").val(e.attr('data-name'));
        if (triggerClick) {
            $("#btnReload").click();
            calculateTransferBalance();
            triggerClick = true;
        }
    }
}

function calculateTransferBalance() {
    var source = parseFloat($('#frWalletOption .walletAmount').attr("wallet-balance"));
    var target = parseFloat($('#toWalletOption .walletAmount').attr("wallet-balance"));
    var deduction = 0;
    if ($('#frWalletOption').attr("data-wallet") == "TotalBal") {
        $('#txtAmount').attr("disabled", "disabled");
        deduction = source - target;
    } else {
        if ($('#txtAmount').attr('disabled') != undefined) {
            $('#txtAmount').removeAttr('disabled');
        }
        deduction = source;
    }
    if ($('#frWalletOption').attr("data-wallet") !== undefined) {
        if (!isNaN(deduction)) {
            if (deduction <= 0) {
                $('#txtAmount').val("").change();
            } else {
                if ($('#frWalletOption').attr("data-wallet") == "TotalBal") {
                    if ($('#toWalletOption').attr("data-wallet") == "BOY2") {
                        $('#txtAmount').val((deduction.toFixed(2).split(".")[0])).change();
                    } else {
                        $('#txtAmount').val(deduction.toFixed(2)).change();
                    }
                } else {
                    $('#txtAmount').val(deduction.toFixed(2)).change();
                }
            }
        }
        else {
            $('#txtAmount').val("").change();
        }
    }
}