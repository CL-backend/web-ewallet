/*! jQuery v3.1.0 | (c) jQuery Foundation | jquery.org/license */ ! function (a, b) {
	"use strict";
	"object" == typeof module && "object" == typeof module.exports ? module.exports = a.document ? b(a, !0) : function (a) {
		if (!a.document) throw new Error("jQuery requires a window with a document");
		return b(a)
	} : b(a)
}
var chineseLanguageSideBanner = "false";
            var chineseCultureLanguageSideBanner = "false";
            $(document).ready(function () {
                if (chineseLanguageSideBanner === "true" && (Cookie.Get("SideBannerShownTemp") !== "" || Cookie.Get("SideBannerShown") !== "")) {
                    $("#leftSideBannerContainer").hide();
                }
                if (chineseCultureLanguageSideBanner === "true") {
                    $("#CloseRouletteOnce").css("padding-right", "30px");
                } else {
                    $("#CloseRouletteOnce").css("padding-right", "35px");
                }
            });
            $('#imgSideBanner, #imgSideBannerMobile').on("click", function () {
                        Member.EnsureLogin(window.location.href);
                });
            function closeSideBanner() {
                if (chineseLanguageSideBanner === "true") {
                    $("#imgSideBannerReminder").css("display", "block");
                } else {
                    $("#leftSideBannerContainer").hide();
                }
            }
            function SetSideBannerCookieTemporarily() {
                Cookie.Create("SideBannerShownTemp", "1");
                $("#leftSideBannerContainer").hide();
            }
            function SetSideBannerCookieForTomorrow() {
                SetCookie("SideBannerShown", "1", 1);
                $("#leftSideBannerContainer").hide();
            }
            function SetCookie(name, value, days) {
                var expires, domain;
                if (days) {
                    var date = new Date();
                    date.setDate(date.getDate() + 1);
                    date.setHours(0, 0, 0, 0);
                    expires = "; expires=" + date.toGMTString();
                } else {
                    expires = "";
                }
                host = location.hostname;
                if (host.split('.').length === 1 || isIPDomain(host)) {
                    domain = host;
                    document.cookie = name + "=" + value + expires + "; path=/; domain=" + domain;
                } else {
                    domainParts = host.split('.');
                    if (host.split('.').length >= 3) {
                        for (i = 1; i <= host.split('.').length - 2; i++) {
                            domainParts.shift();
                        }
                        domain = '.' + domainParts.join('.');
                    }
                    document.cookie = name + "=" + value + expires + "; path=/; domain=" + domain;
                }
            }
            $(document).ready(function () {
             var targetLanguage = '';
                 targetLanguage = 'EN';
             var hotSafariGif = 'https://www.enhuoyea11.net//Assets/Images/PPGamesPromo/' + targetLanguage + '/hot-safari.gif';
             $('#imgPromo').hover(function () {
                 /*mouse over*/
                 $(this).attr('src',hotSafariGif);
             }, function () {
                 /*mouse leave*/
                 $(this).attr('src', $(this).attr('data-orig'));
             }).click(function () {
                 var gaPlatform = 'Desktop_FloatingIcon_BL_' + Helper.RegionCode;
                 if (Helper.IsMobile) {
                     gaPlatform = 'Mobile_FloatingIcon_BL_' + Helper.RegionCode;
                 }
                 Page.sendGaEvent(gaPlatform, 'Click', 'PP_GameChampionship');
             });
         });



            var vendorquerystring = Page.getParameterByName("vendor");
        var typequerystring = Page.getParameterByName("type").toLowerCase();
        var announType;
        if (vendorquerystring.toLocaleLowerCase() === "pt") {
            announType = 'PTSlot';
        } else {
            announType = 'Slot';
        }
        

        if (vendorquerystring.toLocaleLowerCase() === "pt") {
            $('.pt-header').addClass("active");
            $('.mobile-navigation-tr[data-navigation="ptslot"]').addClass("active");
        }else {
            $('.mobile-navigation-tr[data-navigation="slot"]').addClass("active");
            $('.slot-border').addClass("active");
            
            if(Helper.IsMobile){
                $('#slot-menu-list a[data-content-target="' + vendorquerystring + '"]').closest('a').find('.blue').removeClass('blue');
                $('#slot-menu-list a[data-content-target="' + vendorquerystring + '"]').closest('a').addClass('blue');
            }
        }

        function changetToX(input) {
            return input.substring(0, 3) + 'xxx' + input.substring(input.length - 2);
        }

        $(document).ready(function () {
            if (Cookie.Get('announ' + announType) != 'true') {
                if (vendorquerystring.toLocaleLowerCase() === "pt") {
                    Emergency.Announcement('9');
                } else {
                    Emergency.Announcement('10');
                }
            }

            if ($('.slot-banner-Wrapper').height() < 1)
            {
                $('.recentWinnerHolder').css('visibility', 'hidden');
            }
            else 
            {
                $('.recentWinnerHolder').css('visibility', 'visible');
            }

            var rcWinnerArr = [];
            $.ajax({
                type: "GET",
                url: "/Services/SuperRecentWinner.ashx",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var rcObj = data;
                    if (rcObj.length > 0) {
                        for (var i = 0; i < rcObj.length; i++) {
                            if (rcObj[i]["GameName"] != "") {
                                rcWinnerArr.push('<div class="recentWinnerTable"><div style="width: 150px; display: table-cell;">');
                                rcWinnerArr.push('<div style="font-weight: bolder; color: black;">');
                                rcWinnerArr.push(changetToX(rcObj[i]["MemberCode"]));
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('<div id="gameNameSelector" style="cursor:pointer;" onclick="Games.EnterGame(\'' + rcObj[i]["ProviderCode"] + '\',\'' + rcObj[i]["GameIdToEnter"] + '\')">');
                                rcWinnerArr.push(rcObj[i]["GameName"]);
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('<div style="width: 150px; display: table-cell; text-align: center;">');
                                rcWinnerArr.push(
                                    '<div style="padding-left: 35px; font-weight: bolder; color: black; font-size: 12px;">');
                                rcWinnerArr.push(rcObj[i]["Currency"]);
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('<div style="font-weight: bolder; font-size: 17px; color: #00adef;">');
                                rcWinnerArr.push(rcObj[i]["Amount"].toFixed(4));
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('</div>');
                                rcWinnerArr.push('</div>');
                            }

                        }
                        $('.recentWinnerHolder').show();
                    }
                    else {
                        $('.recentWinnerHolder').hide();
                    }

                    $('.recentWinnerPlayLists').append(rcWinnerArr.join(''));
                    $('.recentWinnerPlayLists').slick({
                        slidesToShow: 5,
                        slidesToScroll: 1,
                        autoplay: true,
                        autoplaySpeed: 2000,
                        vertical: true,
                        arrows: false,
                        responsive: [
                            {
                                breakpoint: 1249,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1
                                }
                            },
                            {
                                breakpoint: 480,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1
                                }
                            }
                        ]
                    });
                },
                error: function () {
                    return;
                }
            });

            $(window).on('resize', function () {
                if (window.innerWidth < 639 || $('.slot-banner-Wrapper').height() < 1) {
                    $('.recentWinnerHolder').css("visibility", "hidden");
                } else {
                    $('.recentWinnerHolder').css("visibility", "visible");
                }
            });
  
        });







             var strURLIovation = "https://mpsnare.iesnare.com/snare.js";
         var blackbox = '';
         var e2 = '';
         var strURLE2 = "{{APP_ASSETS}}e2.platform88798.com/E2/EagleEye.js";
         var slotPage = "/id/en/games/slot.htm";
         var casinoPage = "/id/en/games/livecasino.htm";
         var casinoDetailPage = "/id/en/games/livecasinodetail.htm";
         /* Start : [HEADER] Provider Link To For Slot , Slot Game Type , Live Casino */
         $(".withdrawalSelfExlusion").click(function () {
             if (Helper.IsMemberWithdrawalRestriction.toString().toLowerCase() == "true") {
                 Page.message(Helper.AccountWithdrawalLockMsg, Helper.ComplianceLockTitle, "CLOSE");
                 $(".Modal-Close-Button").hide();
                 return false;
             }
         });
         $(".modal-button-message").click(function () {
             $(".Modal-Close-Button").show();
         });
         $(".header-slot-vendor").on("click", function (e) {
             e.preventDefault();
             var vendor = $(this).attr("data-content-target");
             var glocation = "";
             if (vendor != null) {
                 if (Helper.CultureCode.toLowerCase() == "vi-vn" || Helper.CultureCode.toLowerCase() == "th-th")
                     glocation = slotPage + vendor + "/";
                 else
                     glocation = slotPage + "?vendor=" + vendor + "&type=" + gameType;
             } else {
                 glocation = slotPage;
             }
             checkSelfExclusion("bet", glocation, "redirection");
         });
         $(".slot-game-tab").on("click", function (e) {
             e.preventDefault();
             var vendor = $(this).attr("data-vendor");
             var gameType = $(this).attr("data-slot-target");
             var glocation = "";
             if (vendor != null) {
                 if (Helper.CultureCode.toLowerCase() == "vi-vn" || Helper.CultureCode.toLowerCase() == "th-th") {
                     glocation = slotPage + vendor + "/";
                     if (gameType != undefined && gameType != "") {
                         glocation = glocation + gameType + "/";
                     }
                 } else
                     glocation = slotPage + "?vendor=" + vendor + "&type=" + gameType;
             } else {
                 glocation = slotPage;
             }
             checkSelfExclusion("bet", glocation, "redirection");
         });
         $(".casino-game-tab").on("click", function () {
             var vendor = $(this).attr("data-content-target");
             var gameType = "";
             var glocation = "";
             if (vendor != null && gameType != null) {
                 if (Helper.CultureCode.toLowerCase() == "vi-vn" || Helper.CultureCode.toLowerCase() == "th-th")
                     glocation = casinoPage + vendor + "/";
                 else
                     glocation = casinoPage + "?vendor=" + vendor + "&type=" + gameType;
             } else {
                 glocation = casinoPage;
             }
             checkSelfExclusion("bet", glocation, "redirection");
         });
         /* End : Header Provider Link To For Slot , Slot Game Type , Live Casino */
         $(".sidePanelIcon").hover(function () {
             var objChild = $(this).parents(".sidePanel");
             if ($(objChild).find(".slideIcon").hasClass("inactive")) {
                 $(objChild).find(".slideIcon").removeClass("inactive").addClass("active");
             } else {
                 $(objChild).find(".slideIcon").removeClass("active").addClass("inactive");
             }
         });
         function OpenAndNavigateToBonus(contentId) {
             if (DialogManager_isLogin(Helper.MemberCode)) {
                 window.open("/promo.htm?navid=" + contentId, "_blank");
             }
             else {
                 Member.EnsureLogin(window.location.href);
             }
         }
         function NavigateToBonus(contentId) {
             if (contentId != null && contentId != "") {
                 try {
                     $('html, body').animate({
                         scrollTop: $(".moreGrpingPromo[data-contentid=" + contentId + "]").offset().top - 500
                     },
                         1000);
                     $('html, body').promise().done(function () {
                         $(".moreGrpingPromo[data-contentid='" + contentId + "']").click();
         });
                 } catch (err) {
                 }
             }
         }
         function OpenTransferWithBonus(bonusId) {
             var selfExclusion = true;
             $.ajax({
                 type: "GET",
                 url: "/Services/SelfExclusion.ashx",
                 async: false,
                 data: {
                     Check: "bonus",
                 },
                 success: function (data) {
                     if (data != null) {
                         if (data.IsSuccess) {
                             Page.message(data.Message, selfExclusionTitle);
                             selfExclusion = false;
                         }
                     }
                 }
             });
             if (selfExclusion) {
                 if (DialogManager_isLogin(Helper.MemberCode)) {
                     var bal;
                     try {
                         bal = $(".totalBalanceAmount:first").text().replace(/,/g, '').match(/\d+/)[0];
                     } catch (err) {
                         bal = 0;
                     }
                     if (bal > minBal) {
                         Page.loading();
                         Modal.GameTransferOpen("?bonusId=" + bonusId);
                     } else
                         window.open("/id/en/finance/deposit/default.htm?bonusId=" + bonusId, '_blank');
                 } else
                     Member.EnsureLogin(window.location.href);
             }
         }
         function applyBtn(bonusId, type) {
            if (type === undefined || type === null)
                type = "";
            switch (type.toLowerCase()) {
                case "other":
                case "rebate":
                default:
                    return "";
                case "sos":
                    return "<span class='linkDeposit button-Green promoBtn' data-close onclick='SosBonus(" +
                        bonusId +
                        "); return false;'>Apply Now</span>";
                 case "manual":
                     return "<span class='linkDeposit button-Green promoBtn' data-close onclick='ManualPromo(" +
                         bonusId +
                         "); return false;'>Apply Now</span>";
                 case "bonus":
                     return "<span class='linkDeposit button-Green promoBtn' data-close onclick='OpenTransferWithBonus(" +
                         bonusId +
                         "); return false;'>Apply Now</span>";
             }
         }
         function btnOpenAndNavigateToBonus(contentId) {
         return "<span class='linkDeposit button-Green promoBtn' data-close onclick='OpenAndNavigateToBonus(" +
             contentId +
             "); return false;'>Apply Now</span>";
         }
         function nagivateToBtn(contentId) {
            return "<span class='linkDeposit button-Green promoBtn' data-close onclick='NavigateToBonus(" +
                contentId +
                "); return false;'>Apply Now</span>";
         }
         function status(statusCode, bonusId, type) {
             switch (statusCode) {
                 case "NotEligible":
                     return "Not Eligible";
                 case "Available":
                 case "Eligible":
                     var link = GetLink();
                     return link(bonusId, type);
                 case "Release":
                     return "Issued";
                 default:
                     return statusCode;
             }
         }
         function details(url, displayable) {
             if (displayable === false) {
                 return "<span style='display:none;' class='detailsTncUrl promoDetails' data-url='" +
                     url +
                     "'>More Details</span>";
             } else {
                 return "<span class='detailsTncUrl promoDetails' data-url='" + url + "'>More Details</span>";
             }
         }
         /* This Function Should Load After Document Is Fully Load */
         function GenerateIovationValue() {
             $.getScript(strURLIovation, function () {
                 setTimeout(function () {
                     var bb_info = ioGetBlackbox();
                     if (bb_info)
                         blackbox = bb_info.blackbox;
                     if ($('input[id$=hidblackbox]').length)
                         $('input[id$=hidblackbox]').val(blackbox);
                 }, 2000);
             });
             $.getScript(strURLE2, function () {
                 setTimeout(function () {
                     var e2Result = E2GetBlackbox();
                     if (e2Result) {
                         e2 = e2Result.blackbox;
                         if ($('input[id$=hidE2]').length) {
                             $('input[id$=hidE2]').val(e2);
                         }
                     }
                 }, 2000);
             });
         }
         $(window).on('load', function () {
             var notfState = 'true';
             notfState = Cookie.Get("noftState");
             if (window.location.href.toLowerCase().indexOf("home.htm") > -1
                 || window.location.href.toLowerCase().substr(window.location.href.length - 4) == "/vn/"
                     || window.location.href.toLowerCase().substr(window.location.href.length - 4) == "/th/") {
                 if (typeof Notification != "undefined") {
                     if (Notification && Notification.permission !== "granted" && notfState !== "false" && Notification.permission !== "denied") {
                         setTimeout(function () {
                             $('#modal-OuterNotification').hide().slideDown(200, function () {
                                 var img = $('#modal-InnerNotification-img').find("img").attr("data-original");
                                 $('#modal-InnerNotification-img').find("img").attr("src", img);
                             });
                         }, 2000);
                     }
                 }
             }
             /* Analytics */
             initAnalytics();
             /* Piwik */
             initPiwik();
             /* Iovation */
             GenerateIovationValue();
         });
         $('#notf-decline').on("click", function () {
         $('#modal-OuterNotification').slideUp();
             Cookie.Create("noftState", "false", 3);
         });
         $('#notf-allow').on("click", function () {
             $('#modal-OuterNotification').slideUp();
             popOutMiddle('/id/en/member/requestnotification.htm', 600, 600);
         });
         function popOutMiddle(url, width, height) {
             var leftPosition, topPosition;
             leftPosition = (window.screen.width / 2) - ((width / 2) + 10);
             topPosition = (window.screen.height / 2) - ((height / 2) + 50);
             window.open(url, "Window2",
             "status=no,height=" + height + ",width=" + width + ",resizable=yes,left="
             + leftPosition + ",top=" + topPosition + ",screenX=" + leftPosition + ",screenY="
             + topPosition + ",toolbar=no,menubar=no,scrollbars=no,location=no,directories=no");
         }

         