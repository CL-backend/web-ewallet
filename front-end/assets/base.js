function clickReload() {
	timeout && clearTimeout(timeout);
	xhr && xhr.abort();
	timeout = setTimeout(function () {
		$("#btnReload1").click()
	}, 500)
}

function getUniqueWalletList(n) {
	for (var i = [], t = 0; t < n.length; ++t) i.indexOf(n[t]) == -1 && i.push(n[t]);
	return i
}

function getWalletList(n, t, i) {
	t = t != undefined && t;
	WalletData == null || t ? (WalletData = null, walletRunning ? setTimeout(function () {
		getWalletList(n, !1)
	}, 2e3) : (walletRunning = !0, $.ajax({
		type: "GET",
		async: !0,
		url: "/Services/AllBalance.ashx?forceReload=" + t,
		success: function (i) {
			switch (i) {
				case "timeout":
					window.location = "/" + Helper.UrlCulture + "/error/sessiontimedout.htm";
					break;
				case "multi":
					window.location = "/" + Helper.UrlCulture + "/error/multiplelogin.htm"
			}
			WalletData = i;
			BindWallet(t, n, WalletData);
			walletRunning = !1
		}
	}))) : BindWallet(t, n, WalletData, i)
}

function GetWalletDataForTransfer() {
	return WalletData != null ? WalletData.WalletBalances : !1
}

function BindWallet(n, t, i, r) {
	var ot = i.PreferredWallet,
		a, e, s, h, p, w, b, k, d, g, nt, tt, it, rt, ut, v, c, ft, et, o, y, u, f, l;
	if (t === WalletEnum.WalletDisplayPage.MyAccount) {
		if (a = [], e = [], i && i.WalletBalances) {
			s = $("#walletList .walletListType").eq(0).clone(!0);
			$("#walletList .walletListType").eq(0).remove();
			h = "";
			$(i.WalletBalances).each(function (n, t) {
				t.Name === "SlotMainBal" ? (b = t.BalanceWithCurrencySymbol, nt = t.LocalizedName) : t.Name == "CasinoMainBal" ? (w = t.BalanceWithCurrencySymbol, g = t.LocalizedName) : t.Name == "KenoMainBal" ? (k = t.BalanceWithCurrencySymbol, tt = t.LocalizedName) : t.Name == "SportMainBal" && (p = t.BalanceWithCurrencySymbol, d = t.LocalizedName);
				switch (!0) {
					case WalletEnum._casinoWallet.indexOf(t.Name) >= 0:
						$(".wallet-color-bar", s).addClass("casino MyAccountSpecialColorBar");
						h = "casino";
						break;
					case WalletEnum._slotWallet.indexOf(t.Name) >= 0:
						$(".wallet-color-bar", s).addClass("slot MyAccountSpecialColorBar");
						h = "slot";
						break;
					case WalletEnum._sportWallet.indexOf(t.Name) >= 0:
						$(".wallet-color-bar", s).addClass("sport MyAccountSpecialColorBar");
						h = "sport";
						break;
					case WalletEnum._kenoWallet.indexOf(t.Name) >= 0:
						$(".wallet-color-bar", s).addClass("keno MyAccountSpecialColorBar");
						h = "keno"
				}
				h != "" && a.push(h);
				t.Name == "TotalBal" || t.Name == "MAIN" ? $(".walletName", s).html("<b>" + t.LocalizedName + "<\/b>") : $(".walletName", s).html(t.LocalizedName);
				$(".walletAmount", s).html(t.BalanceWithCurrencySymbol);
				$("#walletList").append(s);
				s = $("#walletList .walletListType").eq(0).clone(!0);
				$(".walletListType").eq(n).attr("data-name", t.Name);
				$(".walletListType").eq(n).attr("data-category", h);
				$(".walletListType .wallet-color-bar").addClass("MyAccountSpecialColorBar")
			});
			it = getUniqueWalletList(a);
			$.each(it, function (n, t) {
				var i = 0,
					r = "";
				switch (t) {
					case "sport":
						i = p;
						r = d;
						break;
					case "casino":
						i = w;
						r = g;
						break;
					case "slot":
						i = b;
						r = nt;
						break;
					case "keno":
						i = k;
						r = tt
				}
				e.push('<div class="walletSlider" data-category="' + t + '">');
				e.push('<li class="walletListTypeNew parent" data-name="' + t + ' parent" data-action ="' + t + '" style="position:relative;border-top: 1px solid #cecece;padding:0;">');
				e.push('<span class="wallet-color-bar MyAccountSpecialColorBar ' + t + '" style="width: 10px;"><\/span>');
				e.push('<span class="walletName" style="width: 45%; margin-left: 5px;"><h7><b>' + r + "<\/b><\/h7><\/span>");
				e.push('<span class="walletAmount" style="width: 20%; margin-left: 3px; display:inline-block;">' + i + "<\/span>");
				e.push('<span class="walletPreferred" style="width: 14%; margin-left: 5px; vertical-align:middle; display:inline-block;"><\/span><span class="arrow-icon icon icon-fun88_arrow_thick_down" style="top: 35%; position: absolute;"><\/span><\/li><div style="display:none;" class="walletCategoryName">');
				$('.walletListType[data-category="' + t + '"]').each(function () {
					var n = $(this).attr("data-name");
					e.push('<li class="walletListTypeNew" data-name="' + t + '" data-wallet="' + n + '" style="position:relative;">');
					e.push($(this).html());
					e.push("<\/li>");
					$(this).remove()
				});
				e.push("<\/div>");
				e.push("<\/div>")
			});
			$("#walletList").append(e.join(""));
			$('.walletListType[data-name="SlotMainBal"],.walletListType[data-name="CasinoMainBal"],.walletListType[data-name="KenoMainBal"],.walletListType[data-name="SportMainBal"]').remove();
			$('.walletListTypeNew[data-wallet="' + i.PreferredWallet + '"]').find(".walletPreferred").addClass("icon icon-tick-circle-button");
			i.PreferredWallet == "MAIN" && $('.walletListType[data-name="MAIN"]').children(".walletPreferred").addClass("icon icon-tick-circle-button");
			$(".loading-gif").hide();
			$(".walletListTypeNew.parent").on("click", function () {
				var t = $(this).children(".icon").hasClass("icon-fun88_arrow_thick_down"),
					n = $(this).children(".arrow-icon");
				$(this).siblings("div.walletCategoryName").slideToggle(200, function () {
					t ? (n.removeClass("icon-fun88_arrow_thick_down").addClass("icon-fun88_arrow_thick_up"), $(this).prev().find(".walletPreferred").removeClass("icon icon-tick-circle-button")) : (n.removeClass("icon-fun88_arrow_thick_up").addClass("icon-fun88_arrow_thick_down"), $(this).find(".icon-tick-circle-button").length > 0 && $(this).prev().find(".walletPreferred").addClass("icon icon-tick-circle-button"))
				})
			});
			$(".icon-tick-circle-button").parents().eq(1).prev().click()
		}
	} else t === WalletEnum.WalletDisplayPage.FinancePage ? i && i.WalletBalances && (c = i.PreferredWallet, c === "" && (c = "MAIN"), $(i.WalletBalances).each(function (n, t) {
		switch (t.Name) {
			case "TotalBal":
				t.FormattedBalance != "N/A" ? rt = t.FormattedBalance.replace(/^[a-zA-Z]+/g, "") : v = t.FormattedBalance;
				$("#totalBalanceAmt").html(t.BalanceWithCurrencySymbol);
				$("#totalBalanceAmt").parent().siblings(".loading-gif-finance").hide();
				$("#totalBalanceAmt").parent("div.walletAmountPlaceHolder").fadeIn(2e3);
				$("#totalBalance").val(rt);
				break;
			case "MAIN":
				v = t.FormattedBalance != "N/A" ? t.FormattedBalance.replace(/^[a-zA-Z]+/g, "") : t.FormattedBalance;
				$("#mainAccountBalanceAmt").html(t.BalanceWithCurrencySymbol);
				$("#mainAccountBalanceAmt").parent().siblings(".loading-gif-finance").hide();
				$("#mainAccountBalanceAmt").parent("div.walletAmountPlaceHolder").fadeIn(2e3)
		}
		t.Name === c && (ut = t.FormattedBalance != "N/A" ? t.FormattedBalance.replace(/^[a-zA-Z]+/g, "") : t.FormattedBalance, $("#mainWalletBalanceAmt").html(t.BalanceWithCurrencySymbol), $("#mainWalletBalanceAmt").parent().siblings(".loading-gif-finance").hide(), $("#mainWalletBalanceAmt").parent("div.walletAmountPlaceHolder").fadeIn(2e3))
	})) : t === WalletEnum.WalletDisplayPage.LoginBar ? $("#loginPanelBalanceList .walletPopulateList").length > 1 ? ($(i.WalletBalances).each(function (n, t) {
		typeof t.Name != "undefined" && t.Name !== "SlotMainBal" && t.Name !== "CasinoMainBal" && t.Name !== "KenoMainBal" && t.Name !== "SportMainBal" && setTimeout(function () {
			$('#loginPanelBalanceList .walletPopulateList[data-name="' + t.Name + '"]').find(".walletAmount").html(t.BalanceWithCurrencySymbol);
			i.PreferredWallet == t.Name && $('.walletPopulateList[data-name="' + t.Name + '"]').find(".preferWallet").addClass("icon-tick-circle-button");
			t.FormattedBalance == "N/A" && $('.walletPopulateList[data-name="' + t.Name + '"]').find(".walletAmount").addClass("not-available")
		}, 100)
	}), $(".totalBalanceAmount").html(i.WalletBalances[0].BalanceWithCurrencySymbol), $(".mobile-total-balance").html(i.WalletBalances[0].BalanceWithCurrencySymbol), ft = $("#frWalletOption").attr("data-wallet"), et = $("#toWalletOption").attr("data-wallet"), clickFrWalletOptions($("#frWalletDropdown .walletPopulateList[data-name='" + ft + "']")), clickToWalletOptions($("#toWalletDropdown .walletPopulateList[data-name='" + et + "']"))) : i && i.WalletBalances && (o = $("#loginPanelBalanceList .walletPopulateList").eq(0).clone(!0), $("#loginPanelBalanceList .walletPopulateList").eq(0).remove(), $(".totalBalanceAmount").html(i.WalletBalances[0].BalanceWithCurrencySymbol), $(".mobile-total-balance").html(i.WalletBalances[0].BalanceWithCurrencySymbol), $(i.WalletBalances).each(function (n, t) {
		switch (!0) {
			case WalletEnum._casinoWallet.indexOf(t.Name) >= 0:
				$(".wallet-color-bar", o).addClass("casino");
				break;
			case WalletEnum._slotWallet.indexOf(t.Name) >= 0:
				$(".wallet-color-bar", o).addClass("slot");
				break;
			case WalletEnum._sportWallet.indexOf(t.Name) >= 0:
				$(".wallet-color-bar", o).addClass("sport");
				break;
			case WalletEnum._kenoWallet.indexOf(t.Name) >= 0:
				$(".wallet-color-bar", o).addClass("keno")
		}
		ot == t.Name && $(".preferWallet", o).addClass("icon-tick-circle-button");
		t.FormattedBalance == "N/A" && $(".walletAmount", o).addClass("not-available");
		$(".walletName", o).html(t.LocalizedName + ":");
		$(".walletAmount", o).html(t.BalanceWithCurrencySymbol);
		$("#loginPanelBalanceList").append(o);
		o = $("#loginPanelBalanceList .walletPopulateList").eq(0).clone(!0);
		$(".walletPopulateList").eq(n).attr("data-name", t.Name)
	}), $(".walletPopulateList").each(function () {
		var n = $(this).clone();
		$("#mobile-wallet-view").append(n)
	}), $('.walletPopulateList[data-name="SlotMainBal"],.walletPopulateList[data-name="CasinoMainBal"],.walletPopulateList[data-name="KenoMainBal"],.walletPopulateList[data-name="SportMainBal"]').remove()) : t === WalletEnum.WalletDisplayPage.TransferPage ? i && i.WalletBalances && (u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0), f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0), $("#frWalletDropdown .walletPopulateList").remove(), $("#toWalletDropdown .walletPopulateList").remove(), $(i.WalletBalances).each(function (n, t) {
		if (typeof t.Name != "undefined" && t.Name !== "SlotMainBal" && t.Name !== "CasinoMainBal" && t.Name !== "KenoMainBal" && t.Name !== "SportMainBal") {
			t.Name == "TotalBal" ? ($(".walletName", u).html(t.LocalizedName + " " + transferAllText), $(".walletAmount", u).html(t.BalanceWithCurrencySymbol), $(".walletAmount", u).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance), $("#totalBalanceAmt").val(t.BalanceWithCurrencySymbol)) : ($(".walletName", u).html(t.LocalizedName), $(".walletAmount", u).html(t.BalanceWithCurrencySymbol), $(".walletAmount", u).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance));
			t.Name == "MAIN" && $("#mainAccountBalanceAmt").html(t.BalanceWithCurrencySymbol);
			t.Name == i.PreferredWallet && $("#mainWalletBalanceAmt").html(t.BalanceWithCurrencySymbol);
			$(u).attr("data-name", t.Name);
			$(".walletName", f).html(t.LocalizedName);
			$(".walletAmount", f).html(t.BalanceWithCurrencySymbol);
			$(".walletAmount", f).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance);
			$(f).attr("data-name", t.Name);
			switch (!0) {
				case WalletEnum._casinoWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("casino");
					$(".wallet-color-bar", u).addClass("casino");
					break;
				case WalletEnum._slotWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("slot");
					$(".wallet-color-bar", u).addClass("slot");
					break;
				case WalletEnum._sportWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("sport");
					$(".wallet-color-bar", u).addClass("sport");
					break;
				case WalletEnum._kenoWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("keno");
					$(".wallet-color-bar", u).addClass("keno")
			}
			$("#frWalletDropdown").append(u);
			$("#toWalletDropdown").append(f);
			u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0);
			f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0);
			$("#frWalletDropdown .walletPopulateList").eq(n).attr("data-name", t.Name);
			$("#toWalletDropdown .walletPopulateList").eq(n).attr("data-name", t.Name)
		}
	}), u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0), u.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"><\/span>'), f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0), f.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"><\/span>'), $("#frWalletOption").children().remove(), $("#toWalletOption").children().remove(), $("#frWalletOption").append(u), $("#frWalletOption").append(u), $("#frWalletOption").children().removeAttr("onclick"), $("#frWalletOption").children().children().first().css("float", "left"), $("#frWalletOption .walletPopulateList .wallet-color-bar").css("margin", "0 5px"), $("#frWalletOption .walletPopulateList .walletName").css("margin-top", "2px"), $("#frWalletOption").attr("data-wallet", $("#frWalletDropdown .walletPopulateList").eq(0).attr("data-name")), $("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth()), $("#toWalletOption").append(f), $("#toWalletOption").children().removeAttr("onclick"), $("#toWalletOption").children().children().first().css("float", "left"), $("#toWalletOption .walletPopulateList .wallet-color-bar").css("margin", "0 5px"), $("#toWalletOption .walletPopulateList .walletName").css("margin-top", "2px"), $("#toWalletOption").attr("data-wallet", $("#toWalletDropdown .walletPopulateList").eq(0).attr("data-name")), $("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth()), $(window).resize(function () {
		$("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth());
		$("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth());
		$("#frWalletDropDown").width() != $("frWalletOption").width() && $("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth());
		$("#toWalletDropdown").width() != $("toWalletOption").width() && $("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth())
	}), y = ["NLE", "SAL", "ABT", "TGP", "GDL", "BOL", "EBT"], r == undefined || r === "" ? $("#toWalletDropdown .walletPopulateList[data-name='" + i.PreferredWallet + "']").click() : typeof tempProvider != "undefined" ? (l = $.inArray(tempProvider, WalletEnum._casinoWallet) > -1 || $.inArray(tempProvider, WalletEnum._sportWallet) > -1 || $.inArray(tempProvider, WalletEnum._kenoWallet) > -1 || $.inArray(tempProvider, WalletEnum._otherWallet) > -1, l ? tempProvider === "MGSQF" ? $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click() : y.indexOf(tempProvider) > -1 ? $("#toWalletDropdown .walletPopulateList[data-name='LD']").click() : $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click() : tempProvider === "PT" ? $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click() : y.indexOf(tempProvider) > -1 ? $("#toWalletDropdown .walletPopulateList[data-name='LD']").click() : $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click()) : $("#toWalletDropdown .walletPopulateList[data-name='" + $("#HiddenToWallet").val() + "']").click(), $("#frWalletDropdown .walletPopulateList[data-name='MAIN']").click(), $("#toWalletDropdown .walletPopulateList[data-name='TotalBal']").remove(), $("#AfterTransferTo").val() != "" && $("#AfterTransferFr").val() != "" && ($("#toWalletDropdown .walletPopulateList[data-name='" + $("#AfterTransferTo").val() + "']").click(), $("#frWalletDropdown .walletPopulateList[data-name='" + $("#AfterTransferFr").val() + "']").click(), $("#AfterTransferFr").val(""), $("#AfterTransferTo").val(""))) : t === WalletEnum.WalletDisplayPage.TransferSelfExclusion && i && i.WalletBalances && (u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0), f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0), $("#frWalletDropdown .walletPopulateList").remove(), $("#toWalletDropdown .walletPopulateList").remove(), $(i.WalletBalances).each(function (n, t) {
		if (typeof t.Name != "undefined" && t.Name !== "SlotMainBal" && t.Name !== "CasinoMainBal" && t.Name !== "KenoMainBal" && t.Name !== "SportMainBal") {
			t.Name == "TotalBal" ? ($(".walletName", u).html(t.LocalizedName + " " + transferAllText), $(".walletAmount", u).html(t.BalanceWithCurrencySymbol), $(".walletAmount", u).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance), $("#totalBalanceAmt").val(t.BalanceWithCurrencySymbol)) : ($(".walletName", u).html(t.LocalizedName), $(".walletAmount", u).html(t.BalanceWithCurrencySymbol), $(".walletAmount", u).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance));
			t.Name == "MAIN" && $("#mainAccountBalanceAmt").html(t.BalanceWithCurrencySymbol);
			t.Name == i.PreferredWallet && $("#mainWalletBalanceAmt").html(t.BalanceWithCurrencySymbol);
			$(u).attr("data-name", t.Name);
			$(".walletName", f).html(t.LocalizedName);
			$(".walletAmount", f).html(t.BalanceWithCurrencySymbol);
			$(".walletAmount", f).attr("wallet-balance", t.Balance == -1 ? 0 : t.Balance);
			$(f).attr("data-name", t.Name);
			switch (!0) {
				case WalletEnum._casinoWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("casino");
					$(".wallet-color-bar", u).addClass("casino");
					break;
				case WalletEnum._slotWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("slot");
					$(".wallet-color-bar", u).addClass("slot");
					break;
				case WalletEnum._sportWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("sport");
					$(".wallet-color-bar", u).addClass("sport");
					break;
				case WalletEnum._kenoWallet.indexOf(t.Name) >= 0:
					$(".wallet-color-bar", f).addClass("keno");
					$(".wallet-color-bar", u).addClass("keno")
			}
			$("#frWalletDropdown").append(u);
			u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0);
			$("#frWalletDropdown .walletPopulateList").eq(n).attr("data-name", t.Name);
			t.Name === "MAIN" && ($("#toWalletDropdown").append(f), f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0), $("#toWalletDropdown .walletPopulateList").eq(n).attr("data-name", t.Name))
		}
	}), u = $("#frWalletDropdown .walletPopulateList").eq(0).clone(!0), u.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"><\/span>'), f = $("#toWalletDropdown .walletPopulateList").eq(0).clone(!0), f.children().first().after('<span id="modifyDropdownArrow" class="icon icon-fun88_arrow_down_triangle"><\/span>'), $("#frWalletOption").children().remove(), $("#toWalletOption").children().remove(), $("#frWalletOption").append(u), $("#frWalletOption").append(u), $("#frWalletOption").children().removeAttr("onclick"), $("#frWalletOption").children().children().first().css("float", "left"), $("#frWalletOption .walletPopulateList .wallet-color-bar").css("margin", "0 5px"), $("#frWalletOption .walletPopulateList .walletName").css("margin-top", "2px"), $("#frWalletOption").attr("data-wallet", $("#frWalletDropdown .walletPopulateList").eq(0).attr("data-name")), $("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth()), $("#toWalletOption").append(f), $("#toWalletOption").children().removeAttr("onclick"), $("#toWalletOption").children().children().first().css("float", "left"), $("#toWalletOption .walletPopulateList .wallet-color-bar").css("margin", "0 5px"), $("#toWalletOption .walletPopulateList .walletName").css("margin-top", "2px"), $("#toWalletOption").attr("data-wallet", $("#toWalletDropdown .walletPopulateList").eq(0).attr("data-name")), $("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth()), $(window).resize(function () {
		$("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth());
		$("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth());
		$("#frWalletDropDown").width() != $("frWalletOption").width() && $("#frWalletDropdown").css("width", $("#frWalletOption").outerWidth());
		$("#toWalletDropdown").width() != $("toWalletOption").width() && $("#toWalletDropdown").css("width", $("#toWalletOption").outerWidth())
	}), r == undefined || r === "" ? $("#toWalletDropdown .walletPopulateList[data-name='" + i.PreferredWallet + "']").click() : typeof tempProvider != "undefined" ? (l = $.inArray(tempProvider, WalletEnum._casinoWallet) > -1 || $.inArray(tempProvider, WalletEnum._sportWallet) > -1 || $.inArray(tempProvider, WalletEnum._kenoWallet) > -1 || $.inArray(tempProvider, WalletEnum._otherWallet) > -1, l ? tempProvider === "MGSQF" ? $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click() : $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click() : tempProvider === "PT" ? $("#toWalletDropdown .walletPopulateList[data-name='" + tempProvider + "']").click() : $("#toWalletDropdown .walletPopulateList[data-name='SLOT']").click()) : $("#toWalletDropdown .walletPopulateList[data-name='" + $("#HiddenToWallet").val() + "']").click(), $("#frWalletDropdown .walletPopulateList[data-name='MAIN']").click(), $("#toWalletDropdown .walletPopulateList[data-name='TotalBal']").remove(), $("#AfterTransferTo").val() != "" && $("#AfterTransferFr").val() != "" && ($("#toWalletDropdown .walletPopulateList[data-name='" + $("#AfterTransferTo").val() + "']").click(), $("#frWalletDropdown .walletPopulateList[data-name='" + $("#AfterTransferFr").val() + "']").click(), $("#AfterTransferFr").val(""), $("#AfterTransferTo").val("")));
	$("#frWalletOption").removeAttr("disabled");
	$("#toWalletOption").removeAttr("disabled");
	$(document).on("click", function (n) {
		n.target != $("#toWalletOption")[0] && n.target != $("#toWalletOption span")[0] && n.target != $("#toWalletOption span")[1] && n.target != $("#toWalletOption span")[2] && n.target != $("#toWalletOption span")[3] && $("#toWalletDropdown").hasClass("is-open") && $("#toWalletDropdown").removeClass("is-open");
		n.target != $("#frWalletOption")[0] && n.target != $("#frWalletOption span")[0] && n.target != $("#frWalletOption span")[1] && n.target != $("#frWalletOption span")[2] && n.target != $("#frWalletOption span")[3] && $("#frWalletDropdown").hasClass("is-open") && $("#frWalletDropdown").removeClass("is-open")
	});
	$(".preferColorBar").addClass($(".preferWallet.icon-tick-circle-button").siblings(".wallet-color-bar").attr("class"))
}

function clickToWalletOptions(n) {
	var t = $(".wallet-color-bar", n).attr("class");
	$("#toWalletOption").removeClass("hover");
	$("#toWalletOption").attr("aria-expanded", "false");
	$("#toWalletDropdown").removeClass("is-open");
	$("#toWalletDropdown").attr("aria-hidden", "true");
	$("#toWalletOption .WalletPopulateList").attr("data-name", n.attr("data-name"));
	$("#toWalletOption .walletAmount").attr("wallet-balance", n.children(".walletAmount").attr("wallet-balance"));
	$("#toWalletOption .walletName").text(n.children(".walletName").text());
	$("#toWalletOption .walletAmount").text(n.children(".walletAmount").text());
	$("#toWalletOption").attr("data-wallet", n.attr("data-name"));
	$("#toWalletOption .wallet-color-bar").removeClass().addClass(t);
	$("#HiddenToWallet").val(n.attr("data-name"))
}

function clickFrWalletOptions(n) {
	var t = $(".wallet-color-bar", n).attr("class");
	$("#frWalletOption").removeClass("hover");
	$("#frWalletOption").attr("aria-expanded", "false");
	$("#frWalletDropdown").removeClass("is-open");
	$("#frWalletDropdown").attr("aria-hidden", "true");
	$("#frWalletOption .WalletPopulateList").attr("data-name", n.attr("data-name"));
	$("#frWalletOption .walletAmount").attr("wallet-balance", n.children(".walletAmount").attr("wallet-balance"));
	$("#frWalletOption .walletName").text(n.children(".walletName").text());
	$("#frWalletOption .walletAmount").text(n.children(".walletAmount").text());
	$("#frWalletOption").attr("data-wallet", n.attr("data-name"));
	$("#frWalletOption .wallet-color-bar").removeClass().addClass(t)
}

function dropdownlist(n) {
	var t;
	n.parent().attr("id").indexOf("fr") > -1 ? (t = $(".wallet-color-bar", n).attr("class"), $("#frWalletOption").removeClass("hover"), $("#frWalletOption").attr("aria-expanded", "false"), $("#frWalletDropdown").removeClass("is-open"), $("#frWalletDropdown").attr("aria-hidden", "true"), $("#frWalletOption .WalletPopulateList").attr("data-name", n.attr("data-name")), $("#frWalletOption .walletAmount").attr("wallet-balance", n.children(".walletAmount").attr("wallet-balance")), $("#frWalletOption .walletName").text(n.children(".walletName").text()), $("#frWalletOption .walletAmount").text(n.children(".walletAmount").text()), $("#frWalletOption").attr("data-wallet", n.attr("data-name")), $("#frWalletOption .wallet-color-bar").removeClass().addClass(t), $(n).attr("data-name") == "TotalBal" ? $("#upBonus").hide() : $("#upBonus").show(), calculateTransferBalance()) : (t = $(".wallet-color-bar", n).attr("class"), $("#toWalletOption").removeClass("hover"), $("#toWalletOption").attr("aria-expanded", "false"), $("#toWalletDropdown").removeClass("is-open"), $("#toWalletDropdown").attr("aria-hidden", "true"), $("#toWalletOption .WalletPopulateList").attr("data-name", n.attr("data-name")), $("#toWalletOption .walletAmount").attr("wallet-balance", n.children(".walletAmount").attr("wallet-balance")), $("#toWalletOption .walletName").text(n.children(".walletName").text()), $("#toWalletOption .walletAmount").text(n.children(".walletAmount").text()), $("#toWalletOption").attr("data-wallet", n.attr("data-name")), $("#toWalletOption .wallet-color-bar").removeClass().addClass(t), $("#HiddenToWallet").val(n.attr("data-name")), triggerClick && ($("#btnReload").click(), calculateTransferBalance(), triggerClick = !0))
}

function calculateTransferBalance() {
	var t = parseFloat($("#frWalletOption .walletAmount").attr("wallet-balance")),
		i = parseFloat($("#toWalletOption .walletAmount").attr("wallet-balance")),
		n = 0;
	$("#frWalletOption").attr("data-wallet") == "TotalBal" ? ($("#txtAmount").attr("disabled", "disabled"), n = t - i) : ($("#txtAmount").attr("disabled") != undefined && $("#txtAmount").removeAttr("disabled"), n = t);
	$("#frWalletOption").attr("data-wallet") !== undefined && (isNaN(n) ? $("#txtAmount").val("").change() : n <= 0 ? $("#txtAmount").val("").change() : $("#frWalletOption").attr("data-wallet") == "TotalBal" ? $("#toWalletOption").attr("data-wallet") == "BOY2" ? $("#txtAmount").val(n.toFixed(2).split(".")[0]).change() : $("#txtAmount").val(n.toFixed(2)).change() : $("#txtAmount").val(n.toFixed(2)).change())
}

function getRandomArbitrary(n, t) {
	return Math.random() * (t - n) + n
}

function GetTotalJackpotAmount(n) {
	var i = getRandomArbitrary(.01, 4.99),
		r = getRandomArbitrary(50, 400),
		t = Helper.CultureCode + "jackpotAmt";
	$.ajax({
		type: "GET",
		url: "/Services/JackpotAmount.ashx",
		async: !0,
		beforeSend: function (u) {
			if (localStorage.getItem(t) !== "" && localStorage.getItem(t) !== null) return n == "gamepage" && $("#desktopSlotJp, #mobileSlotJp").jOdometer({
				increment: i,
				counterStart: localStorage.getItem(t),
				delayTime: 100,
				easing: "linear",
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			}), n == "slotheader" && ($("#headerJp").empty(), $("#headerJp").jOdometer({
				increment: i,
				counterStart: localStorage.getItem(t),
				delayTime: 100,
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			})), n == "ptheader" && ($("#headerPtJp").empty(), $("#headerPtJp").jOdometer({
				increment: i,
				counterStart: localStorage.getItem(t),
				delayTime: 100,
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			})), u.abort(), !1
		},
		success: function (u) {
			var f = u.amount.toFixed(2),
				e, o;
			localStorage.getItem(t) === "" || localStorage.getItem(t) === null ? localStorage.setItem(t, f) : u.reload === !0 && (e = localStorage.getItem(t), e[t] = f, localStorage.setItem(t, f));
			o = localStorage.getItem(t);
			n == "gamepage" && $("#desktopSlotJp, #mobileSlotJp").jOdometer({
				increment: i,
				counterStart: o,
				delayTime: 100,
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			});
			n == "slotheader" && ($("#headerJp").empty(), $("#headerJp").jOdometer({
				increment: i,
				counterStart: localStorage.getItem(t),
				delayTime: 100,
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			}));
			n == "ptheader" && ($("#headerPtJp").empty(), $("#headerPtJp").jOdometer({
				increment: i,
				counterStart: localStorage.getItem(t),
				delayTime: 100,
				speed: r,
				counterEnd: !1,
				numbersImage: Helper.MediaUrl + "/Assets/images/Games/Icon/jackpotNumberPic.png",
				formatNumber: !0,
				widthNumber: 15,
				heightNumber: 31
			}))
		}
	})
}

function GetSpecialAnnouncement(n, t, i, r) {
	$(".specialAnnouncement").length > 0 && $(".specialAnnouncement").data("options") === i || $.get("/Services/Announcement.ashx?newsType=" + n + "&lang=" + t + "&options=" + i + "&membercode=" + r, function (n) {
		var u = n.Announcement,
			t = [],
			r;
		if (n.Announcement == null || n === "empty") {
			console.log("No Announcement");
			return
		}
		for (r = 0; r < u.length; r++) t.push('<li class="specialAnnouncement" style="padding-bottom: 10px" data-options="' + i + '">'), t.push('<a style="color:black;" data-href=' + announcementPage + "><span>"), t.push(u[r].UpdatedDate), t.push('<\/span> <span style="font-weight: bold">'), t.push(u[r].AnnouncementList), t.push("<\/span><\/a><\/li>");
		$(".announcementHolderList.newsoption-" + i).append(t.join(""));
		$(".specialAnnouncementList").scrollbox({
			startDelay: 5,
			linear: !0,
			step: 1,
			delay: 0,
			speed: 100
		})
	})
}

function OpenBankStatusMessage(n) {
	($(".bank-status-container").removeClass("active"), $(".bank-status-message").slideUp(), $(n).next().find(".bank-status-message").css("display") != "block") && ($(n).addClass("active"), $(n).next().find(".bank-status-message").slideDown())
}

function HomeFooterGame() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) footerGameSlickMode == !0 && (setTimeout(function () {
		$("#home-footer-game-slick").slick("unslick")
	}, 250), footerGameSlickMode = !1);
	else if (footerGameSlickMode == !1) {
		$("#home-footer-game-slick").slick({
			infinite: !1,
			speed: 300,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					centerMode: !0
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					centerMode: !0,
					initialSlide: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: !0,
					initialSlide: 1
				}
			}, ]
		});
		footerGameSlickMode = !0;
		$("#home-footer-game-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function HomePrefrenceDemoSlot() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) slotDemoGameSlickMode == !0 && (setTimeout(function () {
		$("#home-demo-slot-slick").slick("unslick")
	}, 250), slotDemoGameSlickMode = !1);
	else if (slotDemoGameSlickMode == !1) {
		$("#home-demo-slot-slick").slick({
			infinite: !1,
			speed: 300,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			initialSlide: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}, ]
		});
		slotDemoGameSlickMode = !0;
		$("#home-demo-slot-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function HomePrefrenceRecommnedSlot() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) slotRecommendGameSlickMode == !0 && (setTimeout(function () {
		$("#home-recommend-slot-slick").slick("unslick")
	}, 250), slotRecommendGameSlickMode = !1);
	else if (slotRecommendGameSlickMode == !1) {
		$("#home-recommend-slot-slick").slick({
			infinite: !1,
			speed: 300,
			initialSlide: 1,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					centerMode: !0
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					centerMode: !0,
					initialSlide: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: !0,
					initialSlide: 1
				}
			}, ]
		});
		slotRecommendGameSlickMode = !0;
		$("#home-recommend-slot-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function HomeFilterCasino() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) caisnoFilterGameSlickMode == !0 && (setTimeout(function () {
		$("#casino-game-filter-slick").slick("unslick")
	}, 250), caisnoFilterGameSlickMode = !1);
	else if (caisnoFilterGameSlickMode == !1) {
		$("#casino-game-filter-slick").slick({
			infinite: !1,
			speed: 300,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					centerMode: !0
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					centerMode: !0,
					initialSlide: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: !0,
					initialSlide: 1
				}
			}, ]
		});
		caisnoFilterGameSlickMode = !0;
		$("#casino-game-filter-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function HomeRecommendedCasino() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) caisnoRecommendedGameSlickMode == !0 && (setTimeout(function () {
		$("#casino-game-recommended-slick").slick("unslick")
	}, 250), caisnoRecommendedGameSlickMode = !1);
	else if (caisnoRecommendedGameSlickMode == !1) {
		$("#casino-game-recommended-slick").slick({
			infinite: !1,
			speed: 300,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					centerMode: !0
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					centerMode: !0,
					initialSlide: 2
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: !0,
					initialSlide: 1
				}
			}, ]
		});
		caisnoRecommendedGameSlickMode = !0;
		$("#casino-game-recommended-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function HomePreferenceKeno() {
	if (!Helper.IsMobile && $(window).innerWidth() >= 1007 || Helper.IsMobile && $(window).innerWidth() >= 1024) kenoSlickMode == !0 && (setTimeout(function () {
		$("#keno-game-slick").slick("unslick")
	}, 250), kenoSlickMode = !1);
	else if (kenoSlickMode == !1) {
		$("#keno-game-slick").slick({
			infinite: !1,
			initialSlide: 3,
			speed: 300,
			arrows: !1,
			slidesToShow: 3,
			slidesToScroll: 3,
			centerMode: !0,
			responsive: [{
				breakpoint: 1023,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					centerMode: !0
				}
			}, {
				breakpoint: 800,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2,
					centerMode: !0
				}
			}, {
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					centerMode: !0
				}
			}, ]
		});
		kenoSlickMode = !0;
		$("#keno-game-slick").on("swipe", function () {
			SlickSwipeLazyLoadImage(this)
		})
	}
}

function SlickSwipeLazyLoadImage(n) {
	$(n).find(".ImageLazy:not(.Loaded)").lazyload({
		effect: "fadeIn",
		load: function () {
			$(this).addClass("Loaded")
		}
	})
}

function HeaderSportbookDataManipulate() {
	$(".vendor-hover.sportsbookHover.blue").trigger("mouseenter")
}

function HeaderFishingGameDataManipulate() {
	$(".vendor-hover.blue.moreHover").trigger("mouseenter")
}

function GenerateKenoTHVNNextDrawCountDown() {
	$(".kenoSLC-countdown").each(function () {
		KenoSLCFMPromoCountdownInit($(this), $(this).attr("data-end"), "end")
	})
}

function KenoSLCFMPromoCountdownInit(n, t, i) {
	var f = t.lastIndexOf("+"),
		v = t.substring(f - 1);
	t = t.substring(0, f - 1);
	var e = new Date(t.replace(/GMT.*/, "")),
		o = new Date,
		s = o.getTime(),
		y = e - s + (v * 60 + o.getTimezoneOffset()) * 6e4,
		h = Math.floor(y / 1e3),
		c = Math.floor(h / 60),
		l = Math.floor(c / 60),
		a = Math.floor(l / 24),
		p = h % 60,
		r = c % 60,
		u = l % 24;
	if (r < 59 ? r += 1 : (r = 00, u += 1, u == 24 && (u = 00, a += 1)), e < s) switch (typeof i) {
		case "function":
			i(n);
			break;
		case "string":
			n.find("div.countdown_title").children().text(i)
	} else n.text(a + day + " " + u + hour + " " + r + minute)
}

function LBKEnterGame(n, t) {
	return currentGameType = "lbk", Games.EnterGame(n, t), !1
}

function redirectToPath(n) {
	window.open(n)
}

function PreferenceBlissArticleControlInit() {
	$(".BlissArticleInactiveDots")[0] != undefined && ($(".BlissArticleInactiveDots, .BlissArticleCategoryTitle").click(function () {
		var n;
		$(".BlissArticleActiveDots").hide();
		$(".BlissArticleInactiveDots").show();
		$(this).next().show();
		$(this).hide();
		$(".BlissArticleCategoryTitle").removeClass("active");
		$(this).siblings(".BlissArticleCategoryTitle").addClass("active");
		$(this).addClass("active");
		$(this).siblings(".BlissArticleActiveDots").show();
		$(this).siblings(".BlissArticleInactiveDots").hide();
		$("#bliss-article-footer").hide();
		var t = $(this).data("showfor"),
			r = $(this).data("categorytitle"),
			i = '<a href="' + $(this).data("categoryurl") + '" target="_blank">';
		$(this).data("categoryicon") == "bliss" ? ($("#bliss-tab-title").html("<span onclick=\"redirectToPath('" + $(this).data("categoryurl") + '\')" style="cursor:pointer">' + $(this).data("categorytitle") + "<\/span>"), $("#bliss-article-footer").show()) : $("#bliss-tab-title").html($(this).data("categorytitle"));
		n = '<span class="icon icon-' + $(this).data("categoryicon") + ' home-preference-icon"><\/span><\/a>';
		$("#bliss-tab-title").append(i + n);
		$(".bliss-article-table").hide();
		$("#BlissArticle-" + t).show()
	}), $(".BlissArticleInactiveDots")[0].click())
}

function PreferenceHomeSlotJackpotControlInit() {
	$("#home-jackpot-wrapper").slick({
		slidesToScroll: 1,
		autoplay: !1,
		autoplaySpeed: 8e3,
		arrows: !1,
		dots: !1
	});
	increJackpotChart(".game-calculate-answer");
	var n = $($(".home-jackpot-wrapper.slick-active")[0]).attr("id").split("-");
	Slot.Generategraph($("#Gamechart-" + n[1] + "-" + n[2]));
	$(".JackpotScrapperActiveDots").hide();
	$($(".JackpotScrapperInactiveDots")[0]).next().show();
	$($(".JackpotScrapperInactiveDots")[0]).hide();
	$("#home-jackpot-wrapper").on("afterChange", function (n, t) {
		$(".JackpotScrapperActiveDots").hide();
		$(".JackpotScrapperInactiveDots").show();
		var i = $(t.$slides[t.currentSlide]).attr("id").split("-"),
			r = $('.JackpotScrapperInactiveDots[data-showfor="' + i[1] + "-" + i[2] + '"]');
		$(r).next().show();
		$(r).hide();
		Slot.Generategraph($("#Gamechart-" + i[1] + "-" + i[2]))
	});
	$(".JackpotScrapperInactiveDots").click(function (n) {
		n.preventDefault();
		var t = $(this).index(".JackpotScrapperInactiveDots");
		$("#home-jackpot-wrapper").slick("slickGoTo", parseInt(t))
	})
}

function PreferenceKenoLBChinaLotteryResultInit() {
	$("#keno-lbk-result").slick({
		slidesToScroll: 1,
		autoplay: !0,
		autoplaySpeed: 8e3,
		arrows: !1
	});
	$(".KenoResultActiveDots").hide();
	$(".KenoResultInactiveDots").show();
	$($(".KenoResultInactiveDots")[0]).next().show();
	$($(".KenoResultInactiveDots")[0]).hide();
	$("#keno-lbk-result").on("afterChange", function (n, t) {
		$(".KenoResultActiveDots").hide();
		$(".KenoResultInactiveDots").show();
		var r = $(t.$slides[t.currentSlide]).attr("id").split("-")[1] + "-" + $(t.$slides[t.currentSlide]).attr("id").split("-")[2],
			i = $('.KenoResultInactiveDots[data-showfor="' + r + '"]');
		$(i).next().show();
		$(i).hide()
	});
	$(".KenoResultInactiveDots").click(function (n) {
		n.preventDefault();
		var t = $(this).index(".KenoResultInactiveDots");
		$("#keno-lbk-result").slick("slickGoTo", parseInt(t))
	})
}

function PreferenceKenoSLCNextDrawalControlInit() {
	$(".kenoSLC-countdown-wrapper").on("mouseenter", function () {
		$(".gamespinner").removeClass("block");
		$(".gameplay").removeClass("block");
		$(this).find(".gamespinner").addClass("block");
		$(this).find(".gameplay").addClass("block")
	}).on("touchstart", {
		passive: !0
	}, function () {
		$(".gamespinner").removeClass("block");
		$(".gameplay").removeClass("block");
		this.find(".gamespinner").addClass("block");
		this.find(".gameplay").addClass("block")
	});
	window.setInterval(GenerateKenoTHVNNextDrawCountDown, 1e3)
}

function PreferenceSportsbookMatchControlInit() {
	$(".live-match-container").slick({
		slidesToScroll: 1,
		autoplay: !0,
		autoplaySpeed: 8e3,
		arrows: !1
	});
	$($(".SportBookLiveMatchInactiveDots")[0]).next().show();
	$($(".SportBookLiveMatchInactiveDots")[0]).hide();
	$(".live-match-container").on("afterChange", function (n, t) {
		$(".SportBookLiveMatchActiveDots").hide();
		$(".SportBookLiveMatchInactiveDots").show();
		var r = $(t.$slides[t.currentSlide]).data("feedeventid"),
			i = $('.SportBookLiveMatchInactiveDots[data-showfor="' + r + '"]');
		$(i).next().show();
		$(i).hide()
	});
	$(".SportBookLiveMatchInactiveDots").click(function (n) {
		n.preventDefault();
		var t = $(this).index(".SportBookLiveMatchInactiveDots");
		$(".live-match-container").slick("slickGoTo", parseInt(t))
	})
}

function PreferenceBettorLogicControlInit() {
	$("#home-bettorlogic-wrapper").slick({
		slidesToScroll: 1,
		autoplay: !1,
		autoplaySpeed: 8e3,
		arrows: !1,
		dots: !1
	});
	$(".SportBookBettorLogicActiveDots").hide();
	$($(".SportBookBettorLogicInactiveDots")[0]).next().show();
	$($(".SportBookBettorLogicInactiveDots")[0]).hide();
	$("#home-bettorlogic-wrapper").on("afterChange", function (n, t) {
		$(".SportBookBettorLogicActiveDots").hide();
		$(".SportBookBettorLogicInactiveDots").show();
		var r = $(t.$slides[t.currentSlide]).attr("id").split("-")[1],
			i = $('.SportBookBettorLogicInactiveDots[data-showfor="' + r + '"]');
		$(i).next().show();
		$(i).hide()
	});
	$(".SportBookBettorLogicInactiveDots").click(function (n) {
		n.preventDefault();
		var t = $(this).index(".SportBookBettorLogicInactiveDots");
		$("#home-bettorlogic-wrapper").slick("slickGoTo", parseInt(t))
	})
}

function increJackpotChart(n) {
	$(n).each(function () {
		var t = $(this),
			n = t.parents(".home-jackpot-wrapper").attr("id"),
			i = 100,
			r = .07;
		t.hasClass("calculate-average") && (n = n + "average", i = 300, r = .01);
		typeof n != "undefined" && (localStorage.getItem(n) === "" || localStorage.getItem(n) === null) && localStorage.setItem(n, t.html());
		setInterval(function () {
			var e = localStorage.getItem(n),
				u, f, i, o;
			e != null && (u = e.split(",").join(""), parseFloat(u) > 0 && (f = (parseFloat(u) + r).toFixed(2), f != null && (i = f.split("."), i[0] = i[0].replace(/\B(?=(\d{3})+(?!\d))/g, ","), t.html(i.join(".")), o = localStorage.getItem(n), o[n] = i.join("."), localStorage.setItem(n, i.join(".")))))
		}, i)
	})
}

function GetTopBetGame(n, t, i, r) {
	$.ajax({
		type: "GET",
		url: "/Services/TopGameBetService.ashx?GameProvider=" + n + "&GamePlatform=" + (Helper.IsMobile ? "Mobile" : "Web") + "&IsHeaderContrl=" + t + "&GameType=" + r,
		async: !0,
		timeout: 2e4,
		success: function (n) {
			n !== null && n !== undefined ? ($(i).find(".slot-image-on-header").empty(), $(i).find(".game-name-header").empty(), $(i).find(".game-type-header").empty(), $(i).find(".games-vendor-name-header").empty(), $(i).find(".slot-play-button-header").empty(), $(i).find(".slot-demo-button-header").empty(), $.each(n, function (n, t) {
				var u = $(i).find(".slot-image-on-header")[0],
					f = $(i).find(".game-name-header")[0],
					e = $(i).find(".game-type-header")[0],
					o = $(i).find(".games-vendor-name-header")[0];
				u.innerHTML = r == "slot" ? '<img width="294" height="181" onerror="this.src=\'' + Helper.MediaUrl + "/Assets/images/Games/Icon/coming_soon.gif';\" class='header-vendor-center-content header-slot-game-image-service Loaded' src='" + t.ImagePathOri + "' data-ori='" + t.ImagePathOri + "' data-hover='" + t.ImagePath + "'/>" : '<img width="294" height="181" onerror="this.src=\'' + Helper.MediaUrl + "/Assets/images/Games/Icon/coming_soon.gif';\" class='header-vendor-center-content header-slot-game-image-service Loaded' src='" + t.ImagePathOri + "' data-ori='" + t.ImagePathOri + "'/>";
				f.innerHTML = t.GameName;
				e.innerHTML = t.gamesTypeName;
				o.innerHTML = t.providerFullName;
				$(i).find(".game-overlay").attr("onclick", "Games.EnterGame('" + t.ProviderCode + "', '" + t.GameId + "');")
			})) : console.log("empty top game");
			$(".header-slot-game-image-service").on("mouseenter touchstart", {
				passive: !0
			}, function () {
				$(this).attr("src", $(this).attr("data-hover"))
			}).on("mouseleave touchend", function () {
				$(this).attr("src", $(this).attr("data-ori"))
			})
		}
	})
}

function bannerAction(n) {
	var r = n.getAttribute("data-action"),
		t = n.getAttribute("data-url"),
		i = n.getAttribute("data-promoid");
	if (t.indexOf("/sportsbook/") != -1 && !Helper.IsLogin && Helper.IsMobile) return Modal.PopupLogin(), !1;
	switch (r) {
		case "1":
			t.indexOf("http://") >= 0 || t.indexOf("https://") >= 0 ? window.open(t, "_blank") : location.href = t;
			break;
		case "2":
			"/th/".toLocaleLowerCase() == t || "/vn/".toLocaleLowerCase() == t ? window.open(t, "_blank") : "/home.htm".toLocaleLowerCase() == t && Helper.CultureCode.toLocaleLowerCase() == "th-th" ? window.open("/th/", "_blank") : "/home.htm".toLocaleLowerCase() == t && Helper.CultureCode.toLocaleLowerCase() == "vi-vn" ? window.open("/vn/", "_blank") : t.indexOf("http://") >= 0 || t.indexOf("https://") >= 0 ? window.open(t, "_blank") : window.open("/" + Helper.UrlCulture + t, "_blank");
			break;
		case "3":
		case "4":
		case "5":
		case "15":
			i != undefined && (location.href = "/" + Helper.UrlCulture + "/promo.htm?promoId=" + i);
			break;
		case "6":
			window.open(promotionPage, "_blank");
			break;
		case "7":
			window.open(depositPage, "_blank");
			break;
		case "8":
			window.open(registerPage, "_blank");
			break;
		case "9":
			window.open(transferPage, "_blank");
			break;
		case "10":
			window.open(spSportBookPage, "_blank");
			break;
		case "11":
			window.open(PTSlotPage, "_blank");
			break;
		case "12":
			window.open(Slotpage, "_blank");
			break;
		case "13":
			window.open(liveCasinoPage, "_blank");
			break;
		case "14":
			window.open(kenoPage, "_blank")
	}
	return !1
}

function GenerateKenoCountDown() {
	$(".timer").each(function () {
		KenoCountDownInit($(this))
	})
}

function KenoCountDownInit(n) {
	var r = new Date(n.attr("data-end").substring(0, n.attr("data-end").lastIndexOf(" ")) + n.attr("data-end").substring(n.attr("data-end").lastIndexOf(" ") + 1)),
		t = new Date(n.attr("data-start").substring(0, n.attr("data-start").lastIndexOf(" ")) + n.attr("data-start").substring(n.attr("data-start").lastIndexOf(" ") + 1)),
		i = n.attr("data-duration");
	if (new Date < t) n.hide(), n.siblings(".keno-market-close").show();
	else if (new Date > r) n.hide(), n.siblings(".keno-market-close").show();
	else {
		n.siblings(".keno-market-close").hide();
		var u = Math.floor((new Date - t) / 1e3 / i),
			a = new Date(t.setSeconds(t.getSeconds() + (u + 1) * i)),
			f = t.getDate(),
			e = getWordMonth(t.getMonth()),
			o = t.getFullYear(),
			s = pad(t.getHours()),
			h = pad(t.getMinutes()),
			c = pad(t.getSeconds()),
			l = f + " " + e + " " + o + " " + s + ":" + h + ":" + c + " GMT +08";
		FMKenoCountdownInit(n, l, "keno")
	}
}

function getWordMonth(n) {
	switch (n + 1) {
		case 1:
			return "Jan";
		case 2:
			return "Feb";
		case 3:
			return "Mar";
		case 4:
			return "Apr";
		case 5:
			return "May";
		case 6:
			return "Jun";
		case 7:
			return "Jul";
		case 8:
			return "Aug";
		case 9:
			return "Sep";
		case 10:
			return "Oct";
		case 11:
			return "Nov";
		case 12:
			return "Dec"
	}
}

function pad(n) {
	return n < 10 ? "0" + n.toString() : n.toString()
}

function FMKenoCountdownInit(n, t) {
	var u = t.lastIndexOf("+"),
		h = t.substring(u - 1);
	t = t.substring(0, u - 1);
	var c = new Date(t.replace(/GMT.*/, "")),
		f = new Date,
		l = f.getTime(),
		a = c - l + (h * 60 + f.getTimezoneOffset()) * 6e4,
		e = Math.floor(a / 1e3),
		o = Math.floor(e / 60),
		s = Math.floor(o / 60),
		v = Math.floor(s / 24),
		y = e % 60,
		i = o % 60,
		r = s % 24;
	i < 59 ? i += 0 : (i = 00, r += 1, r == 24 && (r = 00, v += 1));
	n.text(i + ":" + pad(y))
}

function resizeIframe(n) {
	n.style.height = n.contentWindow.document.body.scrollHeight + "px";
	$(".modal-content").scrollTop(0)
}

function SosBonus() {
	var n = !0;
	$.ajax({
		type: "GET",
		url: "/Services/SelfExclusion.ashx",
		async: !1,
		data: {
			Check: "bonus"
		},
		success: function (t) {
			t != null && t.IsSuccess && (Page.message(t.Message, selfExclusionTitle), n = !1)
		}
	});
	n && (DialogManager_isLogin(Helper.MemberCode) ? (Page.loading(), Modal.SosBonus()) : Member.EnsureLogin(window.location.href))
}

function ManualPromo(n) {
	DialogManager_isLogin(Helper.MemberCode) ? (Page.loading(), $.ajax({
		type: "get",
		url: "/Services/Promotion.ashx?bonusid=" + n,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		success: function (t) {
			t != null ? Modal.IsPromotionValid(n, t.IsSuccess) : Modal.message(PromotionNotFoundError)
		},
		error: function () {
			Modal.message(PromotionNotFoundError)
		}
	})) : Member.EnsureLogin(window.location.href)
}

function ClaimBonus(n) {
	DialogManager_isLogin(Helper.MemberCode) ? (Page.loading(), $.ajax({
		type: "POST",
		url: n,
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		complete: function (n) {
			Page.loadingDismiss();
			Page.message(n.responseJSON)
		}
	})) : Member.EnsureLogin(window.location.href)
}

function claimBtn(n) {
	return "<a href='/Services/Promotion.ashx?bonusId=" + n + "' onclick='ClaimBonus(this.href); return false;'>" + claim + "<\/a>"
}

function GetLink() {
	return applyBtn
}

function StartCountDown() {
	promoCountdownRunning || (window.setInterval(GenerateCountDown, 1e3), promoCountdownRunning = !0)
}

function GeneratePromoButton(n) {
	$("#modal-promo .promotioncontent-button").each(function () {
		n == undefined && (n = $(this).attr("data-bonus"));
		n == "" && (n = $(".linkDeposit.button-Green.promoBtn").parent().attr("data-bonus"));
		var t = GetLink();
		$(this).children(".promotionProgressbar").append(t(n, $(this).attr("data-type")))
	})
}

function AppendTnc() {
	$("#modal-promo-body").append("<tr id='tncAutoGen'><td>" + tncAutoGen + "<\/td><\/tr>");
	var n = $("#modal-promo-body #tncAutoGen").find("a").attr("href");
	n !== undefined && $("#modal-promo-body #tncAutoGen").find("a").attr("href", "/" + Helper.UrlCulture.toLowerCase() + n)
}

function GenerateCountDown() {
	$("#modal-promo").find(".expireTime").each(function () {
		FMPromoCountdownInit($(this), $(this).attr("data-time"), $(this).attr("data-status"), "", P3PromotionDisplay)
	});
	$("#SpecialPromo").find(".expireTime").each(function () {
		FMPromoCountdownInit($(this), $(this).attr("data-time"), $(this).attr("data-status"), HideSpecialPromo)
	});
	$("#container").find(".expireTime").each(function () {
		FMPromoCountdownInit($(this), $(this).attr("data-time"), $(this).attr("data-status"), RemovePromo)
	})
}

function HideSpecialPromo(n) {
	$(n).parents(".specialPromoCont").hide().siblings(".specialPromoComingSoon").show()
}

function P3PromotionDisplay(n, t, i, r, u, f, e) {
	var o = $("<span/>"),
		s = $("<span/>"),
		h = $("<span/>");
	AddLetterIntoSpan(o, t.zeroPad());
	AddLetterIntoSpan(s, r.zeroPad());
	AddLetterIntoSpan(h, f.zeroPad());
	var c = $("<span>").append(o, $("<span/>", {
			text: i
		})),
		l = $("<span>").append(s, $("<span/>", {
			text: u
		})),
		a = $("<span>").append(h, $("<span/>", {
			text: e
		}));
	n.html("").append(c, l, a)
}

function AddLetterIntoSpan(n, t) {
	for (var r = t.toString().split(""), i = 0; i < r.length; i++) $("<span>", {
		text: r[i]
	}).appendTo(n)
}

function RemovePromo(n) {
	$(n).parents(".promotions").remove();
	$(".promotions").length === 0 && ShowPromoNoContent()
}

function FMPromoCountdownInit(n, t, i, r, u) {
	var o, h;
	if (t !== undefined) {
		o = t.lastIndexOf("+");
		h = t.substring(o - 1);
		t = t.substring(0, o - 1);
		var c = new Date(t.replace(/GMT.*/, "")),
			l = new Date,
			a = l.getTime(),
			w = c - a + (h * 60 + l.getTimezoneOffset()) * 6e4,
			v = Math.floor(w / 1e3),
			y = Math.floor(v / 60),
			p = Math.floor(y / 60),
			s = Math.floor(p / 24),
			b = v % 60,
			f = y % 60,
			e = p % 24;
		if (f < 59 ? f += 1 : (f = 00, e += 1, e == 24 && (e = 00, s += 1)), c < a) {
			if (i == "Release" || i == "Serving") {
				$(n).siblings().hide();
				$(n).html(" ");
				return
			}
			switch (typeof r) {
				case "function":
					r(n);
					break;
				case "string":
					n.find("div.countdown_title").children().text(r)
			}
		} else switch (typeof u) {
			case "function":
				u(n, s, day, e, hour, f, minute);
				break;
			default:
				n.text(s + day + " " + e + hour + " " + f + minute)
		}
	}
}

function checkSelfExclusion(n, t, i, r, u) {
	$.ajax({
		type: "GET",
		url: "/Services/SelfExclusion.ashx",
		async: !1,
		data: {
			Check: n
		},
		success: function (f) {
			if (f != null) {
				if (f.IsSuccess) return n != "transfer" && Page.message(f.Message, selfExclusionTitle), !1;
				if (!u)
					if (r) $(function () {
						$link.attr("onclick")
					});
					else return t != undefined && t != "" ? i != undefined && i != "" ? i == "open" ? window.open(t, "_blank") : window.location.href = t : window.location.href = t : $link.attr("target") != undefined && $link.attr("target").toLowerCase() == "_blank" ? window.open($link.attr("href"), "_blank") : window.location.href = $link.attr("href"), !0;
				return !0
			}
			return !0
		}
	})
}
$(document).ready(function () {
	$("#css-async-cover").fadeOut()
});
! function (n, t, i, r) {
	var u = n(t);
	n.fn.lazyload = function (f) {
		function s() {
			var t = 0;
			o.each(function () {
				var i = n(this);
				if ((!e.skip_invisible || i.is(":visible")) && !n.abovethetop(this, e) && !n.leftofbegin(this, e))
					if (n.belowthefold(this, e) || n.rightoffold(this, e)) {
						if (++t > e.failure_limit) return !1
					} else i.trigger("appear"), t = 0
			})
		}
		var h, o = this,
			e = {
				threshold: 0,
				failure_limit: 0,
				event: "scroll",
				effect: "show",
				container: t,
				data_attribute: "original",
				skip_invisible: !0,
				appear: null,
				load: null,
				placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
			};
		return f && (r !== f.failurelimit && (f.failure_limit = f.failurelimit, delete f.failurelimit), r !== f.effectspeed && (f.effect_speed = f.effectspeed, delete f.effectspeed), n.extend(e, f)), h = e.container === r || e.container === t ? u : n(e.container), 0 === e.event.indexOf("scroll") && h.bind(e.event, function () {
			return s()
		}), this.each(function () {
			var i = this,
				t = n(i);
			i.loaded = !1;
			(t.attr("src") === r || t.attr("src") === !1) && t.is("img") && t.attr("src", e.placeholder);
			t.one("appear", function () {
				if (!this.loaded) {
					if (e.appear) {
						var r = o.length;
						e.appear.call(i, r, e)
					}
					n("<img />").bind("load", function () {
						var r = t.attr("data-" + e.data_attribute),
							u, f;
						t.hide();
						t.is("img") ? t.attr("src", r) : t.css("background-image", "url('" + r + "')");
						t[e.effect](e.effect_speed);
						i.loaded = !0;
						u = n.grep(o, function (n) {
							return !n.loaded
						});
						(o = n(u), e.load) && (f = o.length, e.load.call(i, f, e))
					}).attr("src", t.attr("data-" + e.data_attribute))
				}
			});
			0 !== e.event.indexOf("scroll") && t.bind(e.event, function () {
				i.loaded || t.trigger("appear")
			})
		}), u.bind("resize", function () {
			s()
		}), /(?:iphone|ipod|ipad).*os 5/gi.test(navigator.appVersion) && u.bind("pageshow", function (t) {
			t.originalEvent && t.originalEvent.persisted && o.each(function () {
				n(this).trigger("appear")
			})
		}), n(i).ready(function () {
			s()
		}), this
	};
	n.belowthefold = function (i, f) {
		var e;
		return e = f.container === r || f.container === t ? (t.innerHeight ? t.innerHeight : u.height()) + u.scrollTop() : n(f.container).offset().top + n(f.container).height(), e <= n(i).offset().top - f.threshold
	};
	n.rightoffold = function (i, f) {
		var e;
		return e = f.container === r || f.container === t ? u.width() + u.scrollLeft() : n(f.container).offset().left + n(f.container).width(), e <= n(i).offset().left - f.threshold
	};
	n.abovethetop = function (i, f) {
		var e;
		return e = f.container === r || f.container === t ? u.scrollTop() : n(f.container).offset().top, e >= n(i).offset().top + f.threshold + n(i).height()
	};
	n.leftofbegin = function (i, f) {
		var e;
		return e = f.container === r || f.container === t ? u.scrollLeft() : n(f.container).offset().left, e >= n(i).offset().left + f.threshold + n(i).width()
	};
	n.inviewport = function (t, i) {
		return !(n.rightoffold(t, i) || n.leftofbegin(t, i) || n.belowthefold(t, i) || n.abovethetop(t, i))
	};
	n.extend(n.expr[":"], {
		"below-the-fold": function (t) {
			return n.belowthefold(t, {
				threshold: 0
			})
		},
		"above-the-top": function (t) {
			return !n.belowthefold(t, {
				threshold: 0
			})
		},
		"right-of-screen": function (t) {
			return n.rightoffold(t, {
				threshold: 0
			})
		},
		"left-of-screen": function (t) {
			return !n.rightoffold(t, {
				threshold: 0
			})
		},
		"in-viewport": function (t) {
			return n.inviewport(t, {
				threshold: 0
			})
		},
		"above-the-fold": function (t) {
			return !n.belowthefold(t, {
				threshold: 0
			})
		},
		"right-of-fold": function (t) {
			return n.rightoffold(t, {
				threshold: 0
			})
		},
		"left-of-fold": function (t) {
			return !n.rightoffold(t, {
				threshold: 0
			})
		}
	})
}(jQuery, window, document),
function (n) {
	n.fn.scrollbox = function (t) {
		return t = n.extend({
			linear: !1,
			startDelay: 2,
			delay: 3,
			step: 5,
			speed: 32,
			switchItems: 1,
			direction: "vertical",
			distance: "auto",
			autoPlay: !0,
			onMouseOverPause: !0,
			paused: !1,
			queue: null,
			listElement: "ul",
			listItemElement: "li",
			infiniteLoop: !0,
			switchAmount: 0,
			afterForward: null,
			afterBackward: null,
			triggerStackable: !1
		}, t), t.scrollOffset = t.direction === "vertical" ? "scrollTop" : "scrollLeft", t.queue && (t.queue = n("#" + t.queue)), this.each(function () {
			var i = n(this),
				r, u = null,
				f = null,
				s = !1,
				l, c, e, v, a, y, p, w, h = 0,
				o = 0;
			t.onMouseOverPause && (i.bind("mouseover", function () {
				s = !0
			}), i.bind("mouseout", function () {
				s = !1
			}));
			r = i.children(t.listElement + ":first-child");
			t.infiniteLoop === !1 && t.switchAmount === 0 && (t.switchAmount = r.children().length);
			a = function () {
				if (!s) {
					var v, y, a, c, p;
					if (v = r.children(t.listItemElement + ":first-child"), c = t.distance !== "auto" ? t.distance : t.direction === "vertical" ? v.outerHeight(!0) : v.outerWidth(!0), t.linear ? a = Math.min(i[0][t.scrollOffset] + t.step, c) : (p = Math.max(3, parseInt((c - i[0][t.scrollOffset]) * .3, 10)), a = Math.min(i[0][t.scrollOffset] + p, c)), i[0][t.scrollOffset] = a, a >= c) {
						for (y = 0; y < t.switchItems; y++) t.queue && t.queue.find(t.listItemElement).length > 0 ? (r.append(t.queue.find(t.listItemElement)[0]), r.children(t.listItemElement + ":first-child").remove()) : r.append(r.children(t.listItemElement + ":first-child")), ++h;
						if (i[0][t.scrollOffset] = 0, clearInterval(u), u = null, n.isFunction(t.afterForward) && t.afterForward.call(i, {
								switchCount: h,
								currentFirstChild: r.children(t.listItemElement + ":first-child")
							}), t.triggerStackable && o !== 0) {
							l();
							return
						}
						if (t.infiniteLoop === !1 && h >= t.switchAmount) return;
						t.autoPlay && (f = setTimeout(e, t.delay * 1e3))
					}
				}
			};
			y = function () {
				if (!s) {
					var a, v, c, y, p;
					if (i[0][t.scrollOffset] === 0) {
						for (v = 0; v < t.switchItems; v++) r.children(t.listItemElement + ":last-child").insertBefore(r.children(t.listItemElement + ":first-child"));
						a = r.children(t.listItemElement + ":first-child");
						y = t.distance !== "auto" ? t.distance : t.direction === "vertical" ? a.height() : a.width();
						i[0][t.scrollOffset] = y
					}
					if (t.linear ? c = Math.max(i[0][t.scrollOffset] - t.step, 0) : (p = Math.max(3, parseInt(i[0][t.scrollOffset] * .3, 10)), c = Math.max(i[0][t.scrollOffset] - p, 0)), i[0][t.scrollOffset] = c, c === 0) {
						if (--h, clearInterval(u), u = null, n.isFunction(t.afterBackward) && t.afterBackward.call(i, {
								switchCount: h,
								currentFirstChild: r.children(t.listItemElement + ":first-child")
							}), t.triggerStackable && o !== 0) {
							l();
							return
						}
						t.autoPlay && (f = setTimeout(e, t.delay * 1e3))
					}
				}
			};
			l = function () {
				o !== 0 && (o > 0 ? (o--, f = setTimeout(e, 0)) : (o++, f = setTimeout(c, 0)))
			};
			e = function () {
				clearInterval(u);
				u = setInterval(a, t.speed)
			};
			c = function () {
				clearInterval(u);
				u = setInterval(y, t.speed)
			};
			p = function () {
				t.autoPlay = !0;
				s = !1;
				clearInterval(u);
				u = setInterval(a, t.speed)
			};
			w = function () {
				s = !0
			};
			v = function (n) {
				t.delay = n || t.delay;
				clearTimeout(f);
				t.autoPlay && (f = setTimeout(e, t.delay * 1e3))
			};
			t.autoPlay && (f = setTimeout(e, t.startDelay * 1e3));
			i.bind("resetClock", function (n) {
				v(n)
			});
			i.bind("forward", function () {
				t.triggerStackable ? u !== null ? o++ : e() : (clearTimeout(f), e())
			});
			i.bind("backward", function () {
				t.triggerStackable ? u !== null ? o-- : c() : (clearTimeout(f), c())
			});
			i.bind("pauseHover", function () {
				w()
			});
			i.bind("forwardHover", function () {
				p()
			});
			i.bind("speedUp", function (n, i) {
				i === "undefined" && (i = Math.max(1, parseInt(t.speed / 2, 10)));
				t.speed = i
			});
			i.bind("speedDown", function (n, i) {
				i === "undefined" && (i = t.speed * 2);
				t.speed = i
			});
			i.bind("updateConfig", function (i, r) {
				t = n.extend(t, r)
			})
		})
	}
}(jQuery),
function (n) {
	n.fn.jOdometer = function (t) {
		function v(n) {
			n != undefined ? (clearInterval(c), s = n, l(s)) : (l(s), s = s + t.increment);
			t.counterEnd != !1 && s >= t.counterEnd && (clearInterval(c), l(t.counterEnd))
		}

		function l(t) {
			if (u = String(t).split("."), r.length > 0)
				for (i = 0; i < r.length; i++) oldDigit = r[i], u[1] && (r[i] = u[1].charAt(i)), r[i] == "" && (r[i] = "0"), y(n(".jodometer_decimal_" + i, a), parseInt(r[i]), parseInt(oldDigit));
			for (h = u[0], o = h.length - 1, i = 0; i < e.length; i++) oldDigit = e[i], e[i] = h.charAt(o), e[i] == "" && (e[i] = "0"), y(n(".jodometer_integer_" + i, a), parseInt(e[i]), parseInt(oldDigit)), o--
		}

		function y(n, i, r) {
			i != r && (n.stop(), i == 0 ? n.animate({
				top: -10 * t.heightNumber + f
			}, t.speed, t.easing).animate({
				top: f
			}, 1, "linear") : i < r ? n.animate({
				top: -10 * t.heightNumber + f
			}, t.speed * ((10 - r) / 10), "linear").animate({
				top: f
			}, 1, "linear").animate({
				top: i * t.heightNumber * -1 + f
			}, t.speed * r / 10, t.easing) : n.animate({
				top: i * t.heightNumber * -1 + f
			}, t.speed, t.easing))
		}
		var o, i, c;
		if (this.length > 1) return this.each(function () {
			n(this).jOdometer(t)
		}), this;
		t = n.extend({}, n.fn.jOdometer.defaults, t);
		this.goToNumber = function (n) {
			v(n)
		};
		var a = n(this),
			k = this,
			f = -t.heightNumber,
			s = parseFloat(t.counterStart),
			e = [],
			r = [],
			u = String(t.counterStart).split("."),
			p = 0,
			w = 0;
		if (u[1]) {
			for (o = 0, i = u[1].length - 1; i > -1; i--) r[i] = u[1].charAt(i), n(this).append('<div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:' + (parseInt(r[i]) * t.heightNumber * -1 + f) + 'px;" class="jodometer_decimal_' + i + '" src="' + t.numbersImage + '" alt="Decimal ' + (i + 1) + '" /><\/div>'), o++;
			n(this).append('<div class="icon SlotJackpotFrame comma" style="width: 10px;"><div style="background:url(' + t.numbersImage + ') no-repeat center bottom;background-position: 0 -413px;" class="dotSeperator"><\/div><\/div>');
			p = u[1].length;
			w = t.widthDot
		}
		var h = u[0],
			o = h.length - 1,
			b = 0;
		for (i = 0; i < h.length; i++) e[i] = h.charAt(o), t.formatNumber && i > 0 && i % 3 == 0 && (n(this).append('<div class="icon SlotJackpotFrame comma" style="width: 6px;"><div style="background:url(' + t.numbersImage + ') no-repeat center bottom;" class="commaSeperator"><\/div><\/div>'), b += t.widthDot + t.spaceNumbers), n(this).append('<div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:' + (parseInt(e[i]) * t.heightNumber * -1 + f) + 'px;" class="jodometer_' + Helper.LanguageCode + " jodometer_integer_" + i + '" src="' + t.numbersImage + '" alt="Integer ' + (i + 1) + '" /><\/div>'), o--;
		return (parseFloat(t.counterStart) != t.counterEnd || t.counterEnd.toString() == "false" && parseFloat(t.counterStart) == 0) && (c = setInterval(function () {
			v()
		}, t.delayTime)), this
	};
	n.fn.jOdometer.defaults = {
		counterStart: "0000.00",
		counterEnd: !1,
		delayTime: 1e3,
		increment: .01,
		speed: 500,
		easing: "swing",
		numbersImage: "/images/jodometer-numbers.png",
		formatNumber: !1,
		heightNumber: 31,
		widthNumber: 14,
		offsetRight: 0,
		spaceNumbers: 0,
		widthDot: 10
	}
}(jQuery);
var WalletEnum = {
		WalletDisplayPage: {
			MyAccount: 0,
			FinancePage: 1,
			LoginBar: 2,
			TransferPage: 3,
			TransferSelfExclusion: 4
		},
		_slotWallet: ["PT", "SLOT"],
		_casinoWallet: ["GD", "AG", "SAG", "AB", "SUN", "EA", "OPUS", "BOY2", "CP", "MGSQF", "IPK", "LD"],
		_sportWallet: ["SP", "IPSB", "BFR", "SB", "SBT"],
		_kenoWallet: ["PK", "LBK", "SLC"],
		_otherWallet: ["TotalBal", "MAIN"]
	},
	WalletData = null,
	timeout, xhr, walletRunning = !1,
	triggerClick = !0;
$("#refreshAccountBalance").click(function () {
	$("#loginPanelBalanceList .walletPopulateList .walletAmount").removeClass("not-available");
	$("#loginPanelBalanceList .walletPopulateList .preferWallet").removeClass("icon-tick-circle-button");
	$("#loginPanelBalanceList .walletPopulateList .walletAmount, .totalBalanceAmount").html(loadingText);
	getWalletList(WalletEnum.WalletDisplayPage.LoginBar, !0)
});
var slotDemoGameSlickMode = !1,
	slotDemoGameSlickMode = !1,
	slotRecommendGameSlickMode = !1,
	caisnoFilterGameSlickMode = !1,
	caisnoRecommendedGameSlickMode = !1,
	kenoSlickMode = !1,
	slotRecommendGameSlickMode = !1,
	minBal = 100,
	promoCountdownRunning = !1,
	footerGameSlickMode = !1;
$(window).on("load", function () {
	(Helper.RegionCode.toUpperCase() == "ID" || Helper.RegionCode.toUpperCase() == "VN" || Helper.RegionCode.toUpperCase() == "TH") && BankStatus.GenerateBankStatus();
	(document.getElementById("home-baccarat-roadmap-wrapper") != null || document.getElementById("home-baccarat-roadmap-wrapper") != undefined) && Casino.LoadBaccaratRoadMap()
});
$(document).ready(function () {
	var i, n, u, f, r, t;
	try {
		$(window).width() <= 1024 && ($(".banner-Title > span > span, .banner-Title > p > span").each(function () {
			var n = parseInt($(this).css("font-size"));
			n = n * 2 + "px";
			$(this).css({
				"font-size": n
			})
		}), $(".banner-Mini-Desc > span, .banner-Desc > span").each(function () {
			var n = parseInt($(this).css("font-size"));
			n = n * 2 + "px";
			$(this).css({
				"font-size": n
			})
		}))
	} catch (e) {}
	$(".spinner.lazy, .gamespinner.lazy, .gameplay.playLazy, .gameh-centerplay.playLazy, .roadmap-badge.lazy, div.lazyBanner, .lazyVendor.liveCasinoVendorImg").lazyload({
		effect: "fadeIn"
	});
	$(".game-wrapper-home,.spinner.lazy, .gamespinner.lazy, .gameplay.playLazy, .gameh-centerplay.playLazy, .home-game-image-wrapper").on("mouseover", function () {
		var n = $(this).find("img.lazy"),
			t = $(this).find("img.playLazy");
		n.attr("src", n.data("original"));
		t.attr("src", t.data("original"))
	});
	i = $(".customlazyload");
	i.each(function () {
		var n = $(this),
			t = $(this).find("img.gamespinner"),
			i = $(this).find("img.gameplay");
		n.attr("src", n.data("original-custom"));
		t.attr("src", t.data("original-custom"));
		i.attr("src", i.data("original-custom"));
		n.addClass("Loaded")
	});
	$("div.lazy, span.lazy").lazyload({
		effect: "fadeIn"
	});
	$(".header-menu-Item").attr("data-grabbed", "0");
	f = 1500;
	$(".header-menu-Item").hover(function () {
		if ($(this).hasClass("slotHeader") && (GetTotalJackpotAmount("slotheader"), Slot.SlotHeaderCategory()), $(this).hasClass("pt-header") && GetTotalJackpotAmount("ptheader"), $(this).hasClass("livecasinoheader") && Casino.CasinoHeaderCategory(), clearTimeout(u), $(this).data("grabbed") == "0") {
			GetSpecialAnnouncement("true", Helper.LanguageCode, $(this).data("announcement"), Helper.MemberCode);
			$(this).data("grabbed", "1");
			$(".specialAnnouncement").off("click").on("click", function () {
				return DialogManager_isLogin(Helper.MemberCode) ? (window.open($(this).find("a").attr("data-href"), "_blank"), !1) : (Member.EnsureLogin(window.location.href), !1)
			})
		}
		if ($(".header-menu-Item > a ").removeClass("open"), $(this).data("target") != undefined) {
			$(this).find("a").addClass("open");
			var t = $("#" + $(this).data("target"));
			t && $(".header-menu-content").removeClass("open");
			$(t).addClass("open");
			clearTimeout(n);
			$(".subMenu-Wrapper").addClass("open")
		} else clearTimeout(n), $(".subMenu-Wrapper").removeClass("open").removeClass("delayclose"), $(".header-menu-content").removeClass("open")
	}, function () {
		n = setTimeout(function () {
			$(".header-menu-Item > a ").removeClass("open");
			$(".subMenu-Wrapper").removeClass("open").removeClass("delayclose")
		}, 200)
	});
	$(".subMenu-Wrapper").hover(function () {
		clearTimeout(n)
	}, function () {
		$(".header-menu-Item > a ").removeClass("open");
		$(".subMenu-Wrapper").removeClass("open").removeClass("delayclose")
	});
	$(".BettorLogicQuickView")[0] != undefined && PreferenceBettorLogicControlInit();
	$(".SportsbookMatchControl")[0] != undefined && PreferenceSportsbookMatchControlInit();
	$(".KenoSLCNextDrawControl")[0] != undefined && PreferenceKenoSLCNextDrawalControlInit();
	$(".KenoLBChinaLotteryResult")[0] != undefined && PreferenceKenoLBChinaLotteryResultInit();
	$(".HomeSlotJackpotControl")[0] != undefined && PreferenceHomeSlotJackpotControlInit();
	$(".BlissArticleControl")[0] != undefined && PreferenceBlissArticleControlInit();
	(Cookie.Get("_lastPlayedGame") === "" || typeof Cookie.Get("_lastPlayedGame") == "undefined") && $('.slot-game-tab[data-slot-target="recent:asc"]').parent().hide();
	(Cookie.Get("_favouritesGame") == "" || typeof Cookie.Get("_favouritesGame") == "undefined") && $('.slot-game-tab[data-slot-target="fav:asc"]').parent().hide();
	window.setInterval(GenerateKenoCountDown, 1e3);
	$(".menuCheckLogin").on("click", function () {
		return DialogManager_isLogin(Helper.MemberCode) ? !0 : (Member.EnsureLogin(window.location.href), !1)
	});
	$(".vendor-hover").hover(function () {
		Helper.CultureCode.toLowerCase() == "vi-vn" || Helper.CultureCode.toLowerCase() == "th-th" ? $(".slot-game-tab, .pt-slot-game-tab").attr("data-vendor", $(this).data("content-target-seo")) : $(".slot-game-tab, .pt-slot-game-tab").attr("data-vendor", $(this).data("content-target"));
		$(".slot-game-tab").parent().show();
		(Cookie.Get("_lastPlayedGame") === "" || typeof Cookie.Get("_lastPlayedGame") == "undefined") && $('.slot-game-tab[data-slot-target="recent:asc"]').parent().hide();
		(Cookie.Get("_favouritesGame") == "" || typeof Cookie.Get("_favouritesGame") == "undefined") && $('.slot-game-tab[data-slot-target="fav:asc"]').parent().hide();
		var n = $("." + $(this).data("producttype") + "Hover");
		n.removeClass("blue");
		$(this).addClass("blue");
		$("." + $(this).data("content-target")).css("display", "block");
		$(".vendor-Content." + $(this).data("producttype") + ":not(." + $(this).data("content-target") + ")").css("display", "none");
		$(this).attr("data-content-target") == "tg" && ($('.slot-game-tab[data-slot-target="Multiplayer"]').parent().hide(), $('.slot-game-tab[data-slot-target="Jackpot"]').parent().hide(), $('.slot-game-tab[data-slot-target="TableGame"]').parent().hide());
		$(this).attr("data-content-target") == "lx" && ($('.slot-game-tab[data-slot-target="Multiplayer"]').parent().hide(), $('.slot-game-tab[data-slot-target="TableGame"]').parent().hide());
		($(this).attr("data-content-target") == "bsg" || $(this).attr("data-content-target") == "iwg") && $('.slot-game-tab[data-slot-target="Multiplayer"]').parent().hide();
		$(this).attr("data-content-target") == "swf" && ($('.slot-game-tab[data-slot-target="Jackpot"]').parent().hide(), $('.slot-game-tab[data-slot-target="TableGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="OthersGame"]').parent().hide());
		$(this).attr("data-content-target") == "pt" && ($('.slot-game-tab[data-slot-target="TableGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="OthersGame"]').parent().hide());
		$(this).attr("data-content-target") == "cqg" && ($('.slot-game-tab[data-slot-target="recommended"]').parent().hide(), $('.slot-game-tab[data-slot-target="Multiplayer"]').parent().hide(), $('.slot-game-tab[data-slot-target="Jackpot"]').parent().hide(), $('.slot-game-tab[data-slot-target="TableGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="OthersGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="Freespin"]').parent().hide());
		$(this).attr("data-content-target") == "tgp_slot" && ($('.slot-game-tab[data-slot-target="recommended"]').parent().hide(), $('.slot-game-tab[data-slot-target="Multiplayer"]').parent().hide(), $('.slot-game-tab[data-slot-target="Jackpot"]').parent().hide(), $('.slot-game-tab[data-slot-target="TableGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="hotgames"]').parent().hide(), $('.slot-game-tab[data-slot-target="OthersGame"]').parent().hide(), $('.slot-game-tab[data-slot-target="Freespin"]').parent().hide());
		($(this).attr("data-content-target") == "DailyDeals" || "NewMember" || "Sportsbook" || "LiveCasino" || "Games" || "HeaderKeno" || "BrandSponsorship" || "Exclusive" || "HeaderVip") && $("#promo-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/Promotion.jpg");
		$(this).attr("data-content-target") == "IPSB" && ($("#header-sportbook-livematch-image").attr("src", Helper.MediaUrl + "/Assets/images/Sportsbook/nav_IM.jpg"), $("#header-sport-url-btn").attr("href", $("#header-sport-url-btn").attr("data-ipsb")), $(".header-sport-play-button").css("display", "inline-block"));
		$(this).attr("data-content-target") == "IMSPORT" && ($("#header-sportbook-livematch-image").attr("src", Helper.MediaUrl + "/Assets/images/sportbook-images/imesports_dropdown_thumbnail.jpg"), $("#header-sport-url-btn").attr("href", $("#header-sport-url-btn").attr("data-ipsb")), $(".header-sport-play-button").css("display", "inline-block"));
		$(this).attr("data-content-target") == "BTF" && ($("#header-sportbook-livematch-image").attr("src", Helper.MediaUrl + "/Assets/images/Sportsbook/nav_Betfair.jpg"), $("#header-sport-url-btn").attr("href", $("#header-sport-url-btn").attr("data-bfair")), $(".header-sport-play-button").css("display", "inline-block"));
		$(this).attr("data-content-target") == "SP" && ($("#header-sportbook-livematch-image").attr("src", Helper.MediaUrl + "/Assets/images/Sportsbook/nav_sb.jpg"), $("#header-sport-url-btn").attr("href", $("#header-sport-url-btn").attr("data-sp")), $(".header-sport-play-button").css("display", "inline-block"));
		$(this).attr("data-content-target") == "Fun88Sports" && ($("#header-sportbook-livematch-image").attr("src", Helper.MediaUrl + "/Assets/images/Sportsbook/nav_fun88sports.jpg"), $("#header-sport-url-btn").attr("href", $("#header-sport-url-btn").attr("data-sbt")), $(".header-sport-play-button").css("display", "inline-block"));
		$(this).attr("data-content-target") == "LB" && $(".firstChild.LB").children().first().find("a").trigger("mouseenter");
		$(this).attr("data-content-target") == "MoreFishing" ? ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/Fishing/fishing_banner.jpg"), $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/fishing/home.htm")) : $(this).attr("data-content-target") == "WorldCup" ? ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/P4-Banner-1.jpg"), $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/promo/sponsorship.htm")) : $(this).attr("data-content-target") == "MoreVip" ? ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/VIP.jpg"), $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/member/vip/home.htm")) : $(this).attr("data-content-target") == "MoreAffiliate" ? (affiliateUrl = Helper.LanguageCode.toLowerCase() == "zh" ? "/lm/?lng=CN" : "/lm/?lng=" + Helper.LanguageCode.toUpperCase(), $("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/affiliate_2.jpg"), $("#game_image_redirect").attr("href", affiliateUrl)) : $(this).attr("data-content-target") == "MoreReward" ? ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/rewards.jpg"), $("#game_image_redirect").attr("href", "/Rewards/Login.aspx")) : $(this).attr("data-content-target") == "MoreDownload" ? ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/downloads.jpg"), $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/download/home.htm")) : $(this).attr("data-content-target") == "MoreBliss" ? (Helper.CultureCode.toLowerCase() == "zh-cn" ? $("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/more-tab-banner-zh.jpg") : $("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/more-tab-banner-en.jpg"), $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/Maintenance/BlissMaintenance.htm")) : $(this).attr("data-content-target") == "MoreFriend" && ($("#header-fishing-game-image").attr("src", Helper.MediaUrl + "/Assets/images/homecontrol/header/Refer-A-Friend.jpg"), Helper.CultureCode.toLowerCase() == "vi-vn" ? $("#game_image_redirect").attr("href", "/vn/moi-ban-be/") : $("#game_image_redirect").attr("href", "/" + Helper.UrlCulture + "/member/introafriend.htm"))
	});
	$(".totalAmountHolder").on("click", function () {
		$("#balanceDisplayList").is(":visible") ? $("#balanceDisplayList").slideUp(200, function () {
			$("#mainPageTotalBal,.totalAmountHolder,.icon-fun88_arrow_down_triangle").removeClass("active").addClass("inactive");
			$("#refreshAccountBalance").removeClass("walletListActive")
		}) : ($("#mainPageTotalBal,.totalAmountHolder,.icon-fun88_arrow_down_triangle").removeClass("inactive"), $("#mainPageTotalBal,.totalAmountHolder,.icon-fun88_arrow_down_triangle").addClass("active"), $("#refreshAccountBalance").addClass("walletListActive"), $("#balanceDisplayList").slideDown(200))
	});
	$("html").on("click", function (n) {
		$(n.target).parents().hasClass("balanceView") || $("#balanceDisplayList").is(":visible") && $("#balanceDisplayList").slideUp(200, function () {
			$("#mainPageTotalBal,.totalAmountHolder,.icon-fun88_arrow_down_triangle").removeClass("active").addClass("inactive");
			$("#refreshAccountBalance").removeClass("walletListActive")
		})
	});
	if ($("#backToTopDiv").length) {
		r = 100;
		t = function () {
			var n = $(window).scrollTop();
			n > r ? $("#backToTopDiv").addClass("show") : $("#backToTopDiv").removeClass("show")
		};
		t();
		$(window).on("scroll", function () {
			t()
		});
		$("#backToTopDiv").on("click", function (n) {
			n.preventDefault();
			$("html,body").animate({
				scrollTop: 0
			}, 700)
		})
	}
	$('.sidePanelBtm[id!="backToTopDiv"], .sidePanel[id!="backToTopDiv"]').on("click", function () {
		var n = $(this).find(".sidePanelIcon").attr("data-href");
		n != "" && (window.location.href = n)
	});
	$(".home-game-image-wrapper").on("mouseenter", function () {
		var n = $(this).children(".game-image");
		n.addClass("scale");
		n.siblings(".play-button-wrapper").find(".gamespinner").addClass("block");
		n.siblings(".play-button-wrapper").find(".gameplay").addClass("block");
		n.parent().siblings(".home-game-info").find(".home-game-tryplay").show();
		n.data("gifimage") != "" && n.attr("src", n.data("gifimage"))
	}).on("mouseleave", function () {
		$(".game-image").removeClass("scale");
		$(".home-game-tryplay").hide();
		$(".gamespinner").removeClass("block");
		$(".gameplay").removeClass("block");
		var n = $(this).children(".game-image");
		n.attr("src", n.data("original"))
	}).on("touchstart", {
		passive: !0
	}, function () {
		var n = $(this).children(".game-image");
		$(".game-image").removeClass("scale");
		$(".gamespinner").removeClass("block");
		$(".gameplay").removeClass("block");
		$(".home-game-tryplay").hide();
		n.addClass("scale");
		n.siblings(".play-button-wrapper").find(".gamespinner").addClass("block");
		n.siblings(".play-button-wrapper").find(".gameplay").addClass("block");
		n.parent().siblings(".home-game-info").find(".home-game-tryplay").show();
		n.data("gifimage") != "" && n.attr("src", n.data("gifimage"))
	});
	Cookie.Get("PreferenceChange") == "true" && Modal.PlayerPreference();
	(document.getElementById("home-footer-game-slick") != null || document.getElementById("home-footer-game-slick") != undefined) && HomeFooterGame();
	(document.getElementById("home-demo-slot-slick") != null || document.getElementById("home-demo-slot-slick") != undefined) && HomePrefrenceDemoSlot();
	(document.getElementById("casino-game-filter-slick") != null || document.getElementById("casino-game-filter-slick") != undefined) && HomeFilterCasino();
	(document.getElementById("casino-game-recommended-slick") != null || document.getElementById("casino-game-recommended-slick") != undefined) && HomeRecommendedCasino();
	(document.getElementById("keno-game-slick") != null || document.getElementById("keno-game-slick") != undefined) && HomePreferenceKeno();
	(document.getElementById("home-recommend-slot-slick") != null || document.getElementById("home-recommend-slot-slick") != undefined) && HomePrefrenceRecommnedSlot();
	(document.getElementById("recommned-slot-game-wrapper") != null || document.getElementById("recommned-slot-game-wrapper") != undefined) && $(".recommendGameColumn").width($(".home-slot-recommend-game-tab-wrapper").width() / 2 - 10);
	$("#header-sportbook").on("mouseenter", function () {
		$("#header-sportbook").hasClass("loaded") || ($("#header-sportbook").addClass("loaded"), HeaderSportbookDataManipulate())
	});
	$("#header-more").on("mouseenter", function () {
		$("#header-more").hasClass("loaded") || ($("#header-more").addClass("loaded"), HeaderFishingGameDataManipulate())
	});
	$(".ImageLazy").lazyload({
		effect: "fadeIn",
		load: function () {
			$(this).addClass("Loaded")
		}
	});
	$(".keno-countdown-game-link").hover(function () {
		$(".keno-countdown-wrapper").hide();
		$("#" + $(this).data("showfor")).show();
		SlickSwipeLazyLoadImage($("#" + $(this).data("showfor")))
	});
	$("input").focus(function () {
		Helper.IsMobile && $("#mobileFooter").hide()
	});
	$("input").focusout(function () {
		$("#mobileFooter").show()
	});
	$("#container, #SpecialPromo, .promotion-table").on("click", ".tncUrl, .detailsTncUrl", function () {
		var n = $(this).parents(".promotions");
		return n.length <= 0 && (n = $(this).parents(".promotion-tab-title-wrapper")), n.length <= 0 && (n = $(this).parents(".specialPromoCont").find(".specialPromoBonusId")), Modal.PopupHtml($(this).attr("data-url"), promoHeader, n.attr("data-status"), n.attr("data-id")), Helper.IsLogin ? $(".promotion-after-login").hide() : $(".promotion-before-login").hide(), !1
	})
});
$(window).resize(function () {
	$(window).width() <= 1024 && ($(".balance-wrapper").hide(), $("#mainPageTotalBal").removeClass("active"));
	(document.getElementById("home-footer-game-slick") != null || document.getElementById("home-footer-game-slick") != undefined) && HomeFooterGame();
	(document.getElementById("home-demo-slot-slick") != null || document.getElementById("home-demo-slot-slick") != undefined) && HomePrefrenceDemoSlot();
	(document.getElementById("casino-game-filter-slick") != null || document.getElementById("casino-game-filter-slick") != undefined) && HomeFilterCasino();
	(document.getElementById("casino-game-recommended-slick") != null || document.getElementById("casino-game-recommended-slick") != undefined) && HomeRecommendedCasino();
	(document.getElementById("recommned-slot-game-wrapper") != null || document.getElementById("recommned-slot-game-wrapper") != undefined) && $(".home-recommended-slot-game-wrapper").width($(".home-slot-recommend-game-tab-wrapper").width() / 2 - 5);
	(document.getElementById("keno-game-slick") != null || document.getElementById("keno-game-slick") != undefined) && HomePreferenceKeno();
	(document.getElementById("home-recommend-slot-slick") != null || document.getElementById("home-recommend-slot-slick") != undefined) && HomePrefrenceRecommnedSlot();
	($(".mobile-menu").hasClass("open") || $(".mobile-navi").hasClass("open")) && $(document).width() > 1007 && offCanvas.TriggerCloseMenu()
});
$(".slotHeader").on("mouseenter", function () {
	var n = ["tgp_slot", "cqg", "pt", "mgsqf", "tg", "lx", "bsg", "iwg", "swf"];
	vendorName = n[Math.floor(Math.random() * n.length)];
	$(".vendor-hover.slotHover.blue").trigger("mouseenter");
	GetTopBetGame(vendorName.toUpperCase(), "true", $("#" + $(this).data("target")), "slot")
});
$(".kenoHeader").on("mouseenter", function () {
	$(".kenoHover.blue").trigger("mouseenter")
});
$(".header-slot-vendor").on("mouseenter", function () {
	var n = $(this).attr("data-content-target");
	GetTopBetGame(n, "true", $("#" + $($(this)).parents(".header-menu-content").attr("id")), "slot")
}).on("click", function (n) {
	n.preventDefault();
	var t = $(this).attr("data-content-target"),
		i = "";
	i = t != null ? "/games/slot.htm?vendor=" + t : "/games/slot.htm";
	checkSelfExclusion("bet", i, "redirection")
});
$(".pt-header").on("mouseenter", function () {
	GetTopBetGame("PT", "true", $("#" + $(this).data("target")), "slot")
});
$(".casino-game-tab").on("mouseenter", function () {
	var n = $(this).attr("data-content-target");
	GetTopBetGame(n, "true", $("#" + $($(this)).parents(".header-menu-content").attr("id")), "casino")
});
$(document).keypress(function (n) {
	n = n ? n : event ? event : null;
	var t = n.target ? n.target : n.srcElement ? n.srcElement : null;
	return n.keyCode == 13 && (t.type == "text" || t.type == "password") ? (t.id == "txtUsername" && t.value && ($("#txtPassword").val() ? $(".loginItem .btnLogin").click() : $("#txtPassword").focus()), t.id == "txtPassword" && t.value && ($("#txtUsername").val() ? $(".loginItem .btnLogin").click() : $("#txtUsername").focus()), t.id == "txtMobileUsername" && t.value && ($("#txtMobilePassword").val() ? $("#mobileLogin").click() : $("#txtMobilePassword").focus()), t.id == "txtMobilePassword" && t.value && ($("#txtMobileUsername").val() ? $("#mobileLogin").click() : $("#txtMobileUsername").focus()), t.id == "txtPopUpUsername" && t.value && ($("#txtPopUpPassword").val() ? $("#btnPopupLogin").click() : $("#txtPopUpPassword").focus()), t.id == "txtPopUpPassword" && t.value && ($("#txtPopUpUsername").val() ? $("#btnPopupLogin").click() : $("#txtPopUpUsername").focus()), t.id == "txtUsnLarge" && t.value && ($("#txtPwdLarge").val() ? $(".loginItem .login-button").click() : $("#txtPwdLarge").focus()), t.id == "txtPwdLarge" && t.value && ($("#txtUsnLarge").val() ? $(".loginItem .login-button").click() : $("#txtUsnLarge").focus()), t.id == "txtUsnMobile" && t.value && ($("#txtPwdMobile").val() ? $(".loginItem .login-button").click() : $("#txtPwdMobile").focus()), t.id == "txtPwdMobile" && t.value && ($("#txtUsnMobile").val() ? $(".loginItem .login-button").click() : $("#txtUsnMobile").focus()), !1) : !0
});
Number.prototype.zeroPad = function (n) {
	n = n || 2;
	var t = [] + this;
	return t.length >= n ? this : (new Array(n).join("0") + this).slice(n * -1)
};
$(".depositSelfExlusion").click(function (n) {
	if (Helper.IsMemberDepositRestrisction.toString().toLowerCase() == "true") return Page.message(Helper.AccountDepositLockMsg, Helper.ComplianceLockTitle, "CLOSE"), $(".Modal-Close-Button").hide(), !1;
	n.preventDefault();
	$link = $(this);
	var t = checkSelfExclusion("deposit")
});
$('.game-link, .kenoHeader a, .livecasinoheader a, .slotHeader a, .pt-header a, #header-sportbook a, .mobile-navigation-tr[data-navigation="ptslot"] a, .mobile-navigation-tr[data-navigation="slot"] a, .mobile-navigation-tr[data-navigation="livecasino"] a, .mobile-navigation-tr[data-navigation="sportsbook"] a, .mobile-navigation-tr[data-navigation="poker"] a, .mobile-navigation-tr[data-navigation="keno"] a, .mobile-navigation-tr[data-navigation="fishing"] a, a.featured-box').click(function (n) {
	$link = $(this);
	($link.attr("onclick") === undefined || $link.attr("onclick") !== undefined && $link.attr("onclick") == "") && (n.preventDefault(), checkSelfExclusion("bet"))
});
$(".playNowIconOverlay").click(function () {
	if ($link = $(this), $link.attr("onclick") === undefined || $link.attr("onclick") !== undefined && $link.attr("onclick") == "") return !0;
	checkSelfExclusion("bet", "", "", !0)
});
$(".vendor-hover").click(function (n) {
	var t = $(this).attr("onClick");
	if (t === undefined || t == "") {
		if (n.preventDefault(), $link = $(this), $link.attr("href") === undefined) return !1;
		checkSelfExclusion("bet")
	}
})