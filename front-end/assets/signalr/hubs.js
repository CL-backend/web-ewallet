/*!
 * ASP.NET SignalR JavaScript Library v2.3.0
 * http://signalr.net/
 *
 * Copyright (c) .NET Foundation. All rights reserved.
 * Licensed under the Apache License, Version 2.0. See License.txt in the project root for license information.
 *
 */
(function (n) {
	"use strict";

	function r(t, i) {
		return function () {
			i.apply(t, n.makeArray(arguments))
		}
	}

	function i(t, i) {
		var e, u, f, o, s;
		for (e in t)
			if (t.hasOwnProperty(e)) {
				if (u = t[e], !u.hubName) continue;
				s = i ? u.on : u.off;
				for (f in u.client)
					if (u.client.hasOwnProperty(f)) {
						if (o = u.client[f], !n.isFunction(o)) continue;
						s.call(u, f, r(u, o))
					}
			}
	}
	if (typeof n.signalR != "function") throw new Error("SignalR: SignalR is not loaded. Please ensure jquery.signalR-x.js is referenced before ~/signalr/js.");
	var t = n.signalR;
	n.hubConnection.prototype.createHubProxies = function () {
		var n = {};
		return this.starting(function () {
			i(n, !0);
			this._registerSubscribedHubs()
		}).disconnected(function () {
			i(n, !1)
		}), n.chatHub = this.createHubProxy("chatHub"), n.chatHub.client = {}, n.chatHub.server = {}, n
	};
	t.hub = n.hubConnection("/signalr", {
		useDefaultPath: !1
	});
	n.extend(t, t.hub.createHubProxies())
});