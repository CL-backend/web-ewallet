function isIPDomain(n) {
	return /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/.test(n)
}

function DialogManager_isLogin(n) {
	return n == "" ? !1 : !0
}
var Cookie = {
		Create: function (n, t, r) {
			var u, f, e;
			if (r ? (e = new Date, e.setTime(e.getTime() + r * 864e5), u = "; expires=" + e.toGMTString()) : u = "", host = location.hostname, host.split(".").length === 1 || isIPDomain(host)) f = host, document.cookie = n + "=" + t + u + "; path=/; domain=" + f;
			else {
				if (domainParts = host.split("."), host.split(".").length >= 3) {
					for (i = 1; i <= host.split(".").length - 2; i++) domainParts.shift();
					f = "." + domainParts.join(".")
				}
				document.cookie = n + "=" + t + u + "; path=/; domain=" + f
			}
		},
		Delete: function (n) {
			Cookie.Create(n, "", -1);
			document.cookie = n + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT"
		},
		Get: function (n) {
			for (var t, r = n + "=", u = document.cookie.split(";"), i = 0; i < u.length; i++)
				if (t = $.trim(u[i]), t.indexOf(r) === 0) return t.substring(r.length, t.length);
			return ""
		}
	},
	Storage = {
		Save: function (n, t, i) {
			if (!localStorage) return !1;
			var r = i * 6e4,
				u = {
					value: JSON.stringify(t),
					timestamp: (new Date).getTime() + r
				};
			return localStorage.setItem(n, JSON.stringify(u)), t
		},
		Load: function (n) {
			if (!localStorage) return !1;
			var t = JSON.parse(localStorage.getItem(n));
			return t ? (new Date).getTime() > t.timestamp ? (localStorage.removeItem(n), !1) : JSON.parse(t.value) : !1
		}
	},
	currentGameType, checkRequired = !0,
	slotcheckRequired = !0,
	SlotVendor = ["BSG", "CQG", "WG", "LX", "MGSQF", "PT", "SWF", "TG", "TGP_SLOT"],
	CasinoVendor = ["ABT", "BOL", "EBT", "GDL", "NLE", "SAL", "TGP"],
	Member = {
		AffiliateCode: function () {
			return Cookie.Get("AFF")
		},
		MemberPaymentRisk: function () {
			return Cookie.Get("MPR")
		},
		LifeTimeDepositClassName: function () {
			return Cookie.Get("LTDCN")
		},
		PlayerLatestLifeCycle: function () {
			return Cookie.Get("PLLC")
		},
		MemberLevel: function () {
			return Cookie.Get("MemberLevel")
		},
		EnsureLogin: function () {
			Modal.PopupLogin()
		},
		LoginViaMobile: function () {
			var n = $("#txtMobileUsername").val().trim(),
				t = $("#txtMobilePassword").val().trim();
			n === "" || n == undefined || t === "" || t === undefined ? Page.message(LoginUsernamePasswordError) : RegExp(LRegex).test(n) && RegExp(LRegex).test(t) ? Member.Headerlogin(n, t, !1) : Page.message(LoginUsernamePasswordRegex)
		},
		LoginViaHeader: function () {
			var n = $("#txtUsername").val().trim(),
				t = $("#txtPassword").val().trim();
			n === "" || n == undefined || t === "" || t === undefined ? Page.message(LoginUsernamePasswordError) : RegExp(LRegex).test(n) && RegExp(LRegex).test(t) ? Member.Headerlogin(n, t, !1) : Page.message(LoginUsernamePasswordRegex)
		},
		Headerlogin: function (n, t, i) {
			Page.loading();
			$.post("/Services/Login.ashx?action=login", {
				login: n,
				password: t,
				QuickRegLogin: i
			}, function (t) {
				t.Description === "Success" ? (Cookie.Create("isLogin", 1, 0), Cookie.Create("MC", n, 0), Rewards.syncLogin(!1), window.location.href = window.location.href.split("?")[0], Cookie.Delete("_lastPlayedGame"), Cookie.Delete("_favouritesGame")) : t.Description === "MEM00140" ? (Modal.closedAcc(), Page.loadingDismiss()) : (Page.message(t.Description), Page.loadingDismiss())
			}, "json")
		},
		QRPlogin: function (n, t, i) {
			n.trim() === "" || n.trim() === undefined || t.trim() === "" || t.trim() === undefined ? Page.message(usernameEmpty) : $.post("/Services/Login.ashx?action=login", {
				login: n,
				password: t,
				QuickRegLogin: i
			}, function (n) {
				if (n.Description === "Success") {
					if ($("#refreshAccountBalance").trigger("click"), Rewards.syncLogin(!1), Page.getParameterByName("isRegSuccess")) {
						window.location.href = "/" + window.Helper.UrlCulture + "/Member/RegisterSuccess.htm?IsQuickReg=1";
						return
					}
					if (Boolean(Page.getParameterByName("isQuickReg"))) {
						window.location.href = "/" + window.Helper.UrlCulture + "/home.htm";
						return
					}
				} else Page.message(n.Description)
			}, "json")
		},
		logout: function () {
			Page.loading();
			try {
				Cookie.Get("_lastPlayedGame") !== "" && Cookie.Get("_lastPlayedGame") !== "undefined" && $.post("/Services/LastPlayedGame.ashx")
			} catch (n) {
				console.log(n)
			}
			try {
				Cookie.Get("_favouritesGame") !== "" && Cookie.Get("_favouritesGame") !== "undefined" && $.post("/Services/FavouritesGameService.ashx")
			} catch (n) {
				console.log(n)
			}
			$.post("/Services/Login.ashx?action=logout", {
				dbLogout: !0
			}, function (n) {
				n.IsLogout && (Cookie.Delete("isLogin"), Cookie.Delete("latestDeposit"), Cookie.Delete("latestWithdrawal"), Cookie.Delete("announTransfer"), Cookie.Delete("announWithdrawal"), Cookie.Delete("announDeposit"), Cookie.Delete("announSlot"), Cookie.Delete("announPTSlot"), Cookie.Delete("announSportsbook"), Cookie.Delete("announCasino"), Cookie.Delete("announKeno"), Cookie.Delete("announPoker"), Cookie.Delete("chkBanner"), window.location.href = isfromsafehouse && isfromsafehouse == "isfromsafehouse" ? "/zh-cn/SafeHouse/SafeHouseLoginPage.htm" : "/")
			}, "json");
			$.post("/Rewards/RewardService/Logins.ashx?action=logout", {
				dbLogout: !0
			}, function () {}, "json")
		},
		titanDetailUpdate: function (n, t, i, r, u) {
			return ($("#modal-TitanLoginDetail").css("z-index", "40"), Page.loading(), window.Page_ClientValidate("TitanValidation"), !window.Page_IsValid) ? (Page.loadingDismiss(), $("#modal-TitanLoginDetail").css("z-index", "9999"), !1) : ($.post("/Services/LoginDetailPopup.ashx?action=titanLogin", {
				answer: n,
				question: t,
				password: i,
				confirmPassword: r,
				blackbox: u
			}, function (n) {
				if (n.Code === "Success") $("#modal-TitanLoginDetail").hide(), Page.message(n.Description), Page.loadingDismiss();
				else {
					Page.loadingDismiss();
					$("#modal-TitanLoginDetail").css("z-index", "9999");
					$("#titanError").html(n.Description);
					return
				}
			}, "json"), !1)
		},
		QRPDetailUpdate: function (n, t, i, r, u) {
			if (n === "0" || t === "0" || i === "0") return $("#ddlDOBReq").show(), !1;
			if (window.Page_ClientValidate("QuickRegValidation"), !window.Page_IsValid) return !1;
			$("#modal-QRPLoginDetail").css("z-index", "40");
			Page.loading();
			var f = i + "-" + t + "-" + n;
			return $.post("/Services/LoginDetailPopup.ashx?action=QRPLogin", {
				answer: r,
				question: u,
				dob: f
			}, function (n) {
				n.Code === "Success" ? (Page.loadingDismiss(), QuickReg.SendReferrerGA(), Page.message(n.Description), $("#modal-QRPLoginDetail").hide()) : (Page.loadingDismiss(), $("#modal-QRPLoginDetail").css("z-index", "9999"), $("#qrpError").html(n.Description))
			}, "json"), !1
		},
		UpdatePlayerPreference: function () {
			$.post("/Services/PlayerPreferenceService.ashx", {
				action: "updatepreference"
			}, function (n) {
				n.success && alert("success")
			})
		},
		UpdatePlayerPreferenceFlag: function () {
			$.post("/Services/PlayerPreferenceService.ashx", {
				action: "updatepreferenceflag"
			}, function (n) {
				n.success && alert("success")
			})
		},
		ApplyNow: function (n, t, i, r) {
			var u = {
				mobile: t,
				email: i,
				comment: r
			};
			$.post("Service/Promotion.ashx?ApplyNow=" + n, u, function (n) {
				n.success && alert("success")
			})
		},
		MemberDevice: function () {
			return Helper.IsMobile ? Helper.IsMobileApp ? "Mobile App" : "Mobile Web" : Helper.IsDesktopApp ? "Desktop App" : "Desktop Web"
		}
	},
	Modal = {
		loading: function () {
			return $("#modal-loading").reveal({
				size: "full",
				closeOnClick: !1,
				closeOnEsc: !1,
				multipleOpened: !0
			})
		},
		confirm: function (n, t, i) {
			var u = new $("#modal-confirm").reveal({
					size: "full",
					type: "content",
					content: n,
					closeOnClick: !1,
					closeOnEsc: !1
				}).$element,
				r, f, e;
			return t && u.find("header").text(t), r = u.find("nav.modal-actions"), f = r.find(".reveal-close-button"), r.empty(), i && i.length > 0 && $(i).each(function () {
				var n = $("<button/>", {
					text: this.label,
					click: this.action,
					"class": "modal-button"
				});
				$(r).prepend(n)
			}), f != undefined ? $(r).append(f) : (e = $("<button/>", {
				text: "OK",
				"class": "modal-button reveal-close-button"
			}), e.attr("data-close", "modal-confirm"), $(r).append(e)), u
		},
		bonusMessage: function (n, t, i) {
			var r = $("#modal-bonus-message"),
				u, f;
			if (r.find(".modal-content-message").html(n), t ? ($(r).find(".modal-header-title-custom").text(t), $(r).find(".modal-header-title-custom").show(), $(r).find(".modal-header-title-fix").hide()) : ($(r).find(".modal-header-title-fix").show(), $(r).find(".modal-header-title-custom").hide()), u = $(r).find(".functionButton"), i)
				if (u.html(i.label), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			f = new Foundation.Reveal(r);
			f.open()
		},
		message: function (n, t, i) {
			var r, u, f;
			if (Page.loadingDismiss(), r = $("#modal-message"), r.find(".modal-content-message").html(n), t ? ($(r).find("#modal-header-title-custom").text(t), $(r).find("#modal-header-title-custom").show(), $(r).find("#modal-header-title-fix").hide()) : ($(r).find("#modal-header-title-fix").show(), $(r).find("#modal-header-title-custom").hide()), u = $(r).find(".functionButton"), f = $(r).find(".closeButton"), i)
				if (u.text(i.label), i.action === "uniquevalidation" && (f.text(i.label), f.css({
						margin: "auto",
						width: "50%",
						float: "none"
					})), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			$(r).foundation("open")
		},
		closedAcc: function () {
			Page.loadingDismiss();
			var n = $("#modal-closed-acc");
			$(n).foundation("open")
		},
		closedAcc: function () {
			Page.loadingDismiss();
			var n = $("#modal-closed-acc");
			$(n).foundation("open")
		},
		messageKeno: function (n, t, i, r) {
			var u = $("#modal-message"),
				f, e;
			if (u.find(".modal-content-message").html(n), t ? ($(u).find("#modal-header-title-custom").text(t), $(u).find("#modal-header-title-custom").show(), $(u).find("#modal-header-title-fix").hide()) : ($(u).find("#modal-header-title-fix").show(), $(u).find("#modal-header-title-custom").hide()), f = $(u).find(".functionButton"), i)
				if (f.html(i.label), i.action && typeof i.action == "function") {
					f.show();
					f.on("click", function () {
						i.action();
						f.off();
						u.foundation("close")
					})
				} else f.hide();
			else f.hide();
			if (e = $(u).find(".lightblue"), r)
				if (e.html(r.label), r.action && typeof r.action == "function") {
					e.show();
					e.on("click", function () {
						r.action();
						e.off();
						u.foundation("close")
					})
				} else e.hide();
			else e.hide();
			$(u).foundation("open")
		},
		messageUpdateFullName: function (n, t, i) {
			var r = $("#modal-UpdateFullName"),
				u;
			if (r.find(".modal-content-message").html(n), t ? ($(r).find("#modal-header-title-custom").text(t), $(r).find("#modal-header-title-custom").show(), $(r).find("#modal-header-title-fix").hide()) : ($(r).find("#modal-header-title-fix").show(), $(r).find("#modal-header-title-custom").hide()), u = $(r).find(".functionButton"), i)
				if (u.html(i.label), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			$(r).foundation("open")
		},
		messageWithdrawalVerification: function (n, t, i) {
			var r = $("#modal-message-withdrawal-verification"),
				u;
			if (r.find(".modal-content-message").html(n), t ? ($(r).find("#modal-header-title-custom").text(t), $(r).find("#modal-header-title-custom").show(), $(r).find("#modal-header-title-fix").hide()) : ($(r).find("#modal-header-title-fix").show(), $(r).find("#modal-header-title-custom").hide()), u = $(r).find(".functionButton"), i)
				if (u.html(i.label), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			$(r).foundation("open")
		},
		slcmobilemessage: function (n, t) {
			var i = $("#modal-slc-mobile-message"),
				r;
			i.find(".modal-content-message").html(n);
			$(i).find("#modal-header-title-fix").show();
			$(i).find("#modal-header-title-custom").hide();
			r = $(i).find(".functionButton");
			r.show();
			r.on("click", function () {
				i.foundation("close");
				t ? Games.EnterGamePage("SLC", 1163, 0, 0) : Modal.GameTransferOpen()
			});
			$(i).foundation("open")
		},
		roulettemessage: function (n, t, i, r) {
			var u = $("#modal-Roulette-Message"),
				e, f;
			if (u.find(".modal-content-message").html(n), u.find(".prizesWon").html(""), t ? ($(u).find("#modal-header-title-fix").show(), $(u).find("#modal-header-title-custom").hide()) : ($(u).find("#modal-header-title-fix").show(), $(u).find("#modal-header-title-custom").hide()), t != "" && (r ? (e = rouletteSuccessTitle + "-" + t, $(u).find(".prizesWon").html(e)) : $(u).find(".prizesWon").html(t)), f = $(u).find(".functionButton"), i)
				if (f.html(i.label), i.action && typeof i.action == "function") {
					f.show();
					f.on("click", function () {
						i.action();
						f.off();
						$(u).foundation("close")
					})
				} else f.hide();
			else f.hide();
			$(u).foundation("open")
		},
		angpaomessage: function (n, t, i, r) {
			var u = $("#modal-Angpao-Message"),
				f;
			if (u.find(".modal-content-message").html(n), u.find(".prizesWon").html(""), t ? ($(u).find("#modal-header-title-fix").show(), $(u).find("#modal-header-title-custom").hide()) : ($(u).find("#modal-header-title-fix").show(), $(u).find("#modal-header-title-custom").hide()), t != "" && (r || u.find(".modal-header-title").html(t)), f = $(u).find(".functionButton"), i)
				if (f.html(i.label), i.action && typeof i.action == "function") {
					f.show();
					f.on("click", function () {
						i.action();
						f.off();
						u.foundation("close")
					})
				} else f.hide();
			else f.hide();
			$(u).foundation("open")
		},
		emergencymessage: function (n, t, i) {
			var r = $("#modal-emergency-message"),
				f, u;
			if (r.find(".modal-content-message").html(n), t ? ($(r).find("#modal-emergency-header-title-custom").text(t), $(r).find("#modal-emergency-header-title-custom").show(), $(r).find("#modal-emergency-header-title-fix").hide()) : ($(r).find("#modal-emergency-header-title-fix").show(), $(r).find("#modal-emergency-header-title-custom").hide()), f = $(r).find(".doNotShowChkBox"), f.click(function () {
					$(this).is(":checked") ? Cookie.Create("announ" + announType, "true", 1) : Cookie.Delete("announ" + announType)
				}), u = $(r).find(".functionButton"), i)
				if (u.html(i.label), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			$(r).foundation("open")
		},
		playTechMessage: function (n, t, i) {
			var r = $("#modal-pt-message"),
				u;
			if (r.find(".modal-content-message").html(n), t ? ($(r).find("#modal-pt-header-title-custom").text(t), $(r).find("#modal-pt-header-title-custom").show(), $(r).find("#modal-pt-header-title-fix").hide()) : ($(r).find("#modal-pt-header-title-fix").show(), $(r).find("#modal-pt-header-title-custom").hide()), u = $(r).find(".functionButton"), i)
				if (u.html(i.label), i.action && typeof i.action == "function") {
					u.show();
					u.on("click", function () {
						i.action();
						u.off();
						r.foundation("close")
					})
				} else u.hide();
			else u.hide();
			$(r).foundation("open")
		},
		messageWithSpecialCancel: function (n, t, i) {
			var u = $("#modal-specialCancel"),
				r;
			if (u.find(".modal-content-message").html(n), t && $(u).find(".modal-header-title").text(t), r = $(u).find(".functionButton"), i)
				if (r.html(i.label), i.action && typeof i.action == "function") {
					r.show();
					r.on("click", function () {
						i.action();
						r.off();
						u.foundation("close")
					})
				} else r.hide();
			else r.hide();
			$(u).foundation("open")
		},
		verificationMessage: function (n) {
			return new $("#modal-EmailVerification").reveal({
				size: "full",
				type: "content",
				content: n,
				closeOnClick: !1,
				closeOnEsc: !1
			}).$element
		},
		updateProfileMessage: function (n) {
			var t = new $("#modal-UpdateProfile").reveal({
				size: "full",
				type: "content",
				content: n,
				closeOnClick: !1,
				closeOnEsc: !1
			}).$element;
			return parent.$(".modaliconUpdateProfile").attr("class", "modaliconUpdateProfile icon-finance_fail"), t
		},
		UnfinishGame: function (n) {
			var r = [],
				u = [],
				f = JSON.parse(n),
				t, i, e;
			$("#gameItemHolder").empty();
			t = f[0];
			i = f[1];
			$("#desktopGameCounts").html(t.length);
			$("#mobileGamesCount").html(i.length);
			$.each(t, function (n) {
				Helper.IsMobile ? r.push('<div style="cursor: default; display: none;" class="desktopGame gameItem gameChildUnfinish">') : r.push("<div onclick=Games.EnterGame('" + t[n].Provider + "','" + t[n].GameId + '\'); class="desktopGame gameItem gameChildUnfinish" style="display: block; position:relative;">');
				r.push('<img class="gameItemImage" src=' + t[n].ImageName + '><div class="gameItemGameName">' + t[n].GameName + "<\/div>");
				Helper.IsMobile || r.push('<div class="overlayContainer"><span class="playNowButton">' + playNowButtonText + "<\/span><\/div>");
				r.push("<\/div>")
			});
			$.each(i, function (n) {
				Helper.IsMobile ? u.push("<div onclick=Games.EnterGame('" + i[n].Provider + "','" + i[n].GameId + '\'); class="mobileGame gameItem gameChildUnfinish" style="display: block; position:relative;">') : u.push('<div style="cursor: default; display: none;" class="mobileGame gameItem gameChildUnfinish">');
				u.push('<img class="gameItemImage" src=' + i[n].ImageName + '><div class="gameItemGameName">' + i[n].GameName + "<\/div>");
				Helper.IsMobile && u.push('<div class="overlayContainer"><span class="playNowButton">' + playNowButtonText + "<\/span><\/div>");
				u.push("<\/div>")
			});
			$("#gameItemHolder").append(r.join(""));
			$("#gameItemHolder").append(u.join(""));
			$(".bsPlatform").click(function () {
				$(".bsPlatform").removeClass("active");
				$(this).addClass("active");
				var n = $(this).data("platform");
				$(".gameItem").hide();
				$(".gameItem." + n).show();
				$(".Remark").hide();
				$("." + n + ".Remark").show()
			});
			Helper.IsMobile && $(".bsPlatform").addClass("mobile");
			e = $("#modal-unfinishedgame");
			e.trigger("open")
		},
		SmsVerification: function () {
			var n = $("#modal-SmsVerification"),
				t = smsVerification;
			$.ajax(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			})
		},
		PlayerPreference: function () {
			var n = $("#modal-PlayerPreference"),
				t = funPreference;
			$.ajax(t).done(function (t) {
				n.html(t).foundation("open")
			})
		},
		SelfExclusion: function () {
			var n = $("#modal-SelfExclusion"),
				t = selfExclusionModal;
			$.ajax(t).done(function (t) {
				n.html(t).foundation("open")
			})
		},
		QRPDetailLogin: function () {
			var n = $("#modal-QRPLoginDetail"),
				t = qrpPopUpForm;
			$.ajax(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			})
		},
		TitanDetailLogin: function () {
			var n = $("#modal-TitanLoginDetail"),
				t = titanPopUpForm;
			$.get(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			})
		},
		PopupHtml: function (n, t, i, r) {
			var u = $("#modal-promo");
			t == undefined && (t = "");
			i == undefined && (i = "");
			$.ajax(n).done(function (n) {
				u.html(t + '<article><div id="modal-promo-body" >' + n + "<\/div><\/article>");
				StartCountDown();
				i != "NotEligible" && i != "Serving" && i != "Release" && i != "" && GeneratePromoButton(r)
			}).fail(function () {
				u.html(t + "<div>Url Not Found<\/div>")
			}).always(function () {
				u.trigger("open");
				AppendTnc()
			})
		},
		SosBonus: function () {
			var n = $("#modal-sosBonus");
			n.find(".modal-content-message").html("<iframe frameborder='0' scrolling='no' onload='resizeIframe(this); parent.Page.loadingDismiss();' src='" + sosBonusForm + "'><\/iframe>");
			n.trigger("open")
		},
		SosBonusClose: function () {
			$("#modal-sosBonus").foundation("close").find(".modal-content-message").html("")
		},
		IsPromotionValid: function (n, t) {
			$.ajax({
				type: "GET",
				url: "/Services/IsPromotionValid.ashx?BonusID=" + n,
				contentType: "application/json",
				dataType: "json",
				success: function (i) {
					i.IsSuccess ? t ? Modal.ApplyNow(n) : Modal.ApplyBefore(n) : Modal.message(PromotionNotFoundError)
				},
				error: function () {
					Modal.message(PromotionNotFoundError)
				}
			})
		},
		ApplyNow: function (n) {
			var t = $("#modal-ApplyNow"),
				i = applyNowForm;
			$.ajax(i + "?id=" + n).done(function (n) {
				t.html(n);
				t.trigger("open")
			})
		},
		ApplyNowClose: function () {
			$("#modal-ApplyNow").html("").foundation("close")
		},
		ApplyBefore: function () {
			var n = $("#modal-applyBefore");
			n.trigger("open");
			Page.loadingDismiss()
		},
		PopupLogin: function () {
			$("#modal-PopupLogin").foundation("open")
		},
		PTRegister: function () {
			var n = $("#modal-ptRegister"),
				t = ptRegistration;
			$.ajax(t).done(function (t) {
				n.html(t).foundation("open");
				Page.loadingDismiss()
			})
		},
		GameTransferOpen: function (n) {
			var i = $("#modal-GameTransfer"),
				t;
			n === undefined && (n = "");
			t = quickTransfer + n;
			$.ajax(t).done(function (n) {
				i.html(n).foundation("open");
				Page.loadingDismiss()
			})
		},
		GameTransferClose: function () {
			if ($("#modal-GameTransfer").foundation("close"), typeof tempProvider != "undefined") {
				if (Helper.FromPage && Helper.FromPage == "CashierPage") return;
				tempGameType == "Sportsbook" ? tempLaunchGameMethod.toLowerCase() == "popup" || Helper.IsMobile ? Page.popupWindow(tempGameUrl, "", 1020, 500) : tempLaunchGameMethod.toLowerCase() == "redirect" ? window.location.href = tempGameUrl : window.open(tempGameUrl) : tempProvider == "PT" ? Games.EnterGamePage(tempProvider, tempGameCode, 0, 0, tempIsValidPTUser) : Games.EnterGamePage(tempProvider, tempGameCode, 0, 0)
			}
		},
		GameTransferMessage: function (n, t, i, r) {
			var u = $("#modal-GameTransferMessage");
			$("#modal-GameTransferMessage").find("article").html(n);
			t && $(u).find(".modal-title").text(t);
			r ? tempGameType == "Sportsbook" ? tempLaunchGameMethod.toLowerCase() == "popup" ? $(u).find(".modal-button").attr("onclick", "Page.popupWindow(tempGameUrl, '', 1020, 500)") : $(u).find(".modal-button").attr("onclick", "window.open(tempGameUrl)") : tempProvider == "PT" ? $(u).find(".modal-button").attr("onclick", 'Games.EnterGame("' + tempProvider + '","' + tempGameCode + '","' + tempIsValidPTUser + '")') : $(u).find(".modal-button").attr("onclick", 'Games.EnterGame("' + tempProvider + '","' + tempGameCode + '")') : $(u).find(".modal-button").attr("onclick", "Modal.GameTransferOpen()");
			i == "INFO" ? parent.$(".modalicon").attr("class", "modalicon icon-finance_exclamationpoint") : i == "ERROR" ? parent.$(".modalicon").attr("class", "modalicon icon-member_selfexclusion") : i == "SUCCESS" ? parent.$(".modalicon").attr("class", "modalicon icon-finance_success") : i == "WARNING" && parent.$(".modalicon").attr("class", "modalicon icon-finance_fail");
			$("#modal-GameTransferMessage").foundation("open")
		},
		QQLiveChat: function () {
			$("#modal-QQLiveChat").foundation("open")
		},
		WithdrawalVerificationMessage: function (n, t, i) {
			var u = $("#modal-EmailVerification"),
				r;
			if (u.find(".modal-content-message").html(n), t && $(u).find(".modal-header-title").text(t), r = $(u).find(".functionButton"), i)
				if (r.html(i.label), i.action && typeof i.action == "function") {
					r.show();
					r.on("click", function () {
						i.action();
						r.off();
						u.foundation("close")
					})
				} else r.hide();
			else r.hide();
			$("#modal-EmailVerification").foundation("open")
		},
		PokerRegister: function () {
			var n = $("#modal-pokerRegister"),
				t = pokerRegisterUrl;
			$.ajax(t).done(function (t) {
				n.html(t).foundation("open");
				Page.loadingDismiss()
			})
		},
		FunLineQRScan: function () {
			var n = $("#modal-FunLineQRScan"),
				t = funLineQR;
			$.ajax(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			})
		},
		PopupRouletteSpin: function () {
			var n = $("#modal-Roulette"),
				t = funRoulette;
			$.ajax(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			});
			$("#leftSideBannerContainer").hide()
		},
		ClosePopupRouletteSpin: function () {
			$("#modal-Roulette").foundation("close")
		},
		Intro: function () {
			var n = $("#modal-Intro"),
				t = intro;
			$.ajax(t).done(function (t) {
				n.html(t);
				n.trigger("open")
			})
		},
		InstallationGuide: function () {
			$("#modal-tutotrial").foundation("open")
		},
		InstallationEBTGuide: function () {
			$("#modal-ebt-tutotrial").foundation("open")
		},
		ShowDepositImgSlider: function (n) {
			var t = $("#modal-depositImgSlider"),
				i = depositTutorial + "?btnValue=" + n;
			$.ajax(i).done(function (n) {
				t.html(n);
				t.trigger("open")
			})
		},
		EbetAppGuide: function () {
			$("#modal-ebetApp-tutotrial").foundation("open")
		}
	},
	Page = {
		currentLiveChat: undefined,
		_loadingModal: undefined,
		loading: function () {
			var n = $("#PageLoad");
			n.is(":visible") ? n.css("display", "none") : n.css("display", "block")
		},
		loadingDismiss: function () {
			var n = $("#PageLoad");
			n.css("display", "none")
		},
		popUpLiveChat: function () {
			Page.currentLiveChat != undefined && Page.currentLiveChat.close();
			Page.currentLiveChat = window.open(liveChatURL, "Live Chat", "height=600, width=540,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no, status=yes,top=0")
		},
		popupWindow: function (n, t, i, r) {
			var u = t ? t : "FUN88",
				f = r ? r : screen.availHeight - 70,
				e = i ? "height=" + f + ", width=" + i : "fullscreen=yes";
			return window.open(n, u, e + ", resizable=yes, scrollbars=yes, toolbar=no, menubar=no, location=no, directories=no, status=yes")
		},
		message: function (n, t, i, r) {
			return Modal.message(n, t, {
				label: i,
				action: r
			})
		},
		messageKeno: function (n, t, i, r, u, f) {
			return Modal.messageKeno(n, t, {
				label: i,
				action: r
			}, {
				label: u,
				action: f
			})
		},
		messageUpdateFullName: function (n, t, i, r) {
			return Modal.messageUpdateFullName(n, t, {
				label: i,
				action: r
			})
		},
		messageWithdrawalVerification: function (n, t, i, r) {
			return Modal.messageWithdrawalVerification(n, t, {
				label: i,
				action: r
			})
		},
		playTechMessage: function (n, t, i) {
			return Modal.playTechMessage(n, t, {
				action: i
			})
		},
		messageWithSpecialCancel: function (n, t, i, r) {
			return Modal.messageWithSpecialCancel(n, t, {
				label: i,
				action: r
			})
		},
		getParameterByName: function (n) {
			n = n.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var i = new RegExp("[\\?&]" + n + "=([^&#]*)"),
				t = i.exec(parent.location.search);
			return t === null ? "" : decodeURIComponent(t[1].replace(/\+/g, " "))
		},
		downloadLiveChat: function () {
			window.location = Helper.MediaUrl + "/Download/LiveChat/" + Helper.LanguageCode + "/Fun88LiveChat.zip"
		},
		sendGaEvent: function (n, t, i) {
			ga("send", "event", n, t, i)
		},
		withdrawalEmailVerificationMessage: function (n, t, i, r) {
			return Modal.WithdrawalVerificationMessage(n, t, {
				label: i,
				action: r
			})
		},
		PageRefresh: function (n) {
			window.location = n
		},
		BackToTop: function () {
			$("html,body").animate({
				scrollTop: 0
			}, 200)
		}
	},
	Rewards = {
		syncLogin: function (n) {
			var t = Rewards.GenerateToken(!0);
			t != "" && t != "undefined" && (Boolean(n) ? Helper.IsLogin && $.post("/Rewards/Login.aspx", {
				token: t,
				languageCode: Helper.LanguageCode.toLowerCase() == "en" ? "en-US" : Helper.CultureCode
			}).done(function () {
				Rewards.GenerateToken(!1)
			}) : $.post("/Rewards/Login.aspx", {
				token: t,
				languageCode: Helper.LanguageCode.toLowerCase() == "en" ? "en-US" : Helper.CultureCode,
				Fun88Login: !0
			}).done(function () {
				Rewards.GenerateToken(!1)
			}))
		},
		GenerateToken: function (n) {
			var t = "";
			return $.ajax({
				type: "POST",
				url: "/UI/Home.aspx/GenerateToken",
				data: "{'firstLoad':'" + n + "'}",
				contentType: "application/json; charset=utf-8",
				async: !1,
				dataType: "json",
				success: function (n) {
					t = n.d
				}
			}), t
		}
	},
	offCanvas = {
		TriggerCloseMenu: function () {
			$(".mobile-navi").removeClass("open");
			$(".mobile-menu").removeClass("open");
			$("#overlayBase").css("display", "none")
		},
		TriggerMobileMenu: function () {
			var n, t;
			this.TriggerCloseMenu();
			n = $("#overlayBase");
			$(".mobile-menu").hasClass("open") ? ($(".mobile-menu").removeClass("open"), n.css("display", "none")) : (n.css("display", "block"), $(".mobile-menu").addClass("open"));
			t = $(".mobile-navigation-tr.active");
			t != undefined && $(".mobile-navigation-tr.active").find(".MobileMenuParent").text() == "+" && $(".mobile-navigation-tr.active").find(".MobileMenuParent").click()
		},
		TriggerMobileNavi: function () {
			var n, t, i;
			this.TriggerCloseMenu();
			n = $("#overlayBase");
			$(".mobile-navi").hasClass("open") ? ($(".mobile-navi").removeClass("open"), n.css("display", "none")) : (n.css("display", "block"), $(".mobile-navi").addClass("open"), Helper.IsLogin && (t = $(".mobile-navi-action[data-target='mobile-navi-banking-extend']"), i = $("." + $(t).data("target")), i.removeClass("open"), sidePanel.MobileSettingToggle(t)))
		},
		TriggerMobileBalance: function () {
			var n, t, i;
			this.TriggerCloseMenu();
			n = $("#overlayBase");
			$(".mobile-navi").hasClass("open") ? ($(".mobile-navi").removeClass("open"), n.css("display", "none")) : (n.css("display", "block"), $(".mobile-navi").addClass("open"), Helper.IsLogin && (t = $(".mobile-navi-action[data-target='mobile-navi-wallet-extend']"), i = $("." + $(t).data("target")), i.removeClass("open"), sidePanel.MobileSettingToggle(t)))
		}
	},
	sidePanel = {
		ResetPanelNaviStatus: function () {
			$(".mobile-navi-extend").slideUp().removeClass("open");
			$(".mobile-navi-action").removeClass("active");
			$(".mobile-navi-down-arrow").show();
			$(".mobile-navi-up-arrow").hide()
		},
		MobileSettingToggle: function (n) {
			var t = $("." + $(n).data("target"));
			$(t).hasClass("open") ? ($(t).stop().slideUp().removeClass("open"), $(n).removeClass("active"), $(n).find(".mobile-navi-down-arrow").show(), $(n).find(".mobile-navi-up-arrow").hide()) : (this.ResetPanelNaviStatus(), $(t).stop().slideDown().addClass("open"), $(n).addClass("active"), $(n).find(".mobile-navi-down-arrow").hide(), $(n).find(".mobile-navi-up-arrow").show())
		},
		OpenSidePanel: function () {
			$(".side-panel-setting-content-wrapper").removeClass("open");
			$(".side-panel-setting-notification-content-wrapper").removeClass("open");
			$(".side-panel-content-wrapper").hasClass("open") || ($(".side-panel-content-wrapper").addClass("open"), $(".desktop-bullet-menu").hide(), $(".desktop-bullet-menu-close").show())
		},
		CloseSidePanel: function () {
			$(".side-panel-table").find(".icon").removeClass("active");
			$(".side-panel-content-wrapper").hasClass("open") && ($(".side-panel-content-wrapper").removeClass("open"), $(".side-panel-setting-content-wrapper").removeClass("open"), $(".side-panel-setting-notification-content-wrapper").removeClass("open"), $(".desktop-bullet-menu").show(), $(".desktop-bullet-menu-close").hide())
		},
		SettingToggle: function (n) {
			var t = $("." + $(n).data("target"));
			$(".side-panel-table").find(".icon").removeClass("active");
			$(t).hasClass("open") ? (this.CloseSidePanel(), $(t).removeClass("open")) : (this.OpenSidePanel(), $(t).addClass("open"))
		}
	},
	Emergency = {
		Announcement: function (n) {
			try {
				$.ajax({
					type: "GET",
					url: "/Services/EmergencyAnnouncement.ashx",
					data: {
						EmergencyType: n
					},
					async: !0,
					success: function (n) {
						n !== "500" && n.announcement != null && n.announcement[1] != null && (n.announcement[0] != null ? Modal.emergencymessage(n.announcement[1], n.announcement[0]) : Modal.emergencymessage(n.announcement[1]))
					}
				})
			} catch (t) {}
		}
	},
	QuickReg = {
		Referrer: function (n, t, i) {
			var r = "";
			r = n == null || n == "" ? "" : "|site=" + n;
			r += t == null || t == "" ? "" : "|source=" + t;
			r += i == null || i == "" ? "" : "|medium=" + i;
			r != "" && Cookie.Create("qr_referrer", r, 3)
		},
		SendReferrerGA: function () {
			if (Cookie.Get("qr_referrer") != null) {
				var n = Cookie.Get("qr_referrer");
				ga("send", "event", "Quick Registration", "Registered Success", "UserID=" + window.Helper.GaUserId + n);
				Cookie.Delete("qr_referrer")
			}
		}
	},
	Fishing = {
		sendFishingData: function (n, t) {
			Page.sendGaEvent("Fishing Games", n, t)
		}
	},
	Common = {
		addCommas: function (n) {
			n += "";
			x = n.split(".");
			x1 = x[0];
			x2 = x.length > 1 ? "." + x[1] : "";
			for (var t = /(\d+)(\d{3})/; t.test(x1);) x1 = x1.replace(t, "$1,$2");
			return x1 + x2
		}
	},
	tempProvider, tempGameCode, tempIsValidPTUser, tempGameType, tempGameUrl, tempLaunchGameMethod, Games = {
		currentGameLobbyWindow: undefined,
		InsertFavGames: function (n) {
			var i = this.CheckGameCookieExist("_favouritesGame"),
				t = $('.slotGamePanel[data-game-id="' + n + '"]'),
				r, u;
			t.find(".slot-fav-icon").hasClass("icon-fun88_bookmark_heart_checked") ? (removeGameArr(i, n), $("#myFavoritesHolder").find(t).remove(), $('.slotGamePanel[data-game-id="' + n + '"]').find(".slot-fav-icon").addClass("inactive-fav-icon"), t.attr("data-fav", "1"), $('.slotGamePanel[data-game-id="' + n + '"]').find(".slot-fav-icon").removeClass("icon-fun88_bookmark_heart_checked").addClass("icon-fun88_bookmark_heart_unchecked")) : (i.unshift(n), t.addClass("favorites"), t.attr("data-fav", "0"), t.find(".slot-fav-icon").removeClass("inactive-fav-icon"), t.find(".slot-fav-icon").removeClass("icon-fun88_bookmark_heart_unchecked").addClass("icon-fun88_bookmark_heart_checked"), $('.slotsGamesList > .slotGamePanel[data-game-id="' + n + '"]').clone(!0).appendTo("#myFavoritesHolder"));
			r = i.filter(function (n, t, i) {
				return i.indexOf(n) === t
			});
			u = r.length < 1 ? "refresh" : r.join();
			Cookie.Create("_favouritesGame", u, 1);
			$("#myFavoritesHolder > div:lt(4)").show();
			$("#myFavoritesHolder > div").length < 1 && $("#myFavoritesHolder").parents(".slotholder").removeClass("show").addClass("hide")
		},
		EnterGamePage: function (n, t, i, r, u) {
			if (i === 0 && !DialogManager_isLogin(Helper.MemberCode)) return Member.EnsureLogin(window.location.href), !1;
			if (Helper.IsGameLock.toLowerCase() === "true") return Page.message(Helper.AccountGameLockMsg, Helper.AccountGameLockTitle, "ERROR", "CLOSE"), !1;
			if (n == "PT")
				if (r === 1) DownloadPtGameClient();
				else if (i === 1) {
				popupTitle = "Game";
				popupHeight = "";
				var e = popupTitle ? popupTitle : "Game",
					o = popupHeight ? popupHeight : screen.availHeight - 70,
					s = "height=" + o + ", width=1500",
					f;
				f = document.location.protocol == "https:" && $.inArray(n.toUpperCase(), Helper.SSLVendorList) == -1 ? "http://" + sslRedirectSubdomain + "." + document.location.hostname.match(/.*\.(.*\..*)/)[1] + "/Gamelobby/" + n + "/" + t + ".do?isDemo=1" : "/Gamelobby/" + n + "/" + t + ".do?isDemo=1";
				Games.currentGameLobbyWindow != undefined && Games.currentGameLobbyWindow.close();
				Games.currentGameLobbyWindow = window.open(f, e, s + ", resizable=yes, scrollbars=yes, toolbar=no, menubar=no, location=no, directories=no, status=yes")
			} else if (u) {
				popupTitle = "Game";
				popupHeight = "";
				var e = popupTitle ? popupTitle : "Game",
					o = popupHeight ? popupHeight : screen.availHeight - 70,
					s = "height=" + o + ", width=1500",
					f;
				f = document.location.protocol == "https:" && $.inArray(n.toUpperCase(), Helper.SSLVendorList) == -1 ? "http://" + sslRedirectSubdomain + "." + document.location.hostname.match(/.*\.(.*\..*)/)[1] + "/Gamelobby/" + n + "/" + t + ".do?isDemo=0" : "/Gamelobby/" + n + "/" + t + ".do?isDemo=0";
				Games.currentGameLobbyWindow != undefined && Games.currentGameLobbyWindow.close();
				Games.currentGameLobbyWindow = window.open(f, e, s + ", resizable=yes, scrollbars=yes, toolbar=no, menubar=no, location=no, directories=no, status=yes")
			} else window.open(Helper.PTAccountCreation, "_blank");
			else if (n != "IPK" || Helper.IsValidPokerUser)
				if (r == 0) {
					popupTitle = "Game";
					popupHeight = "";
					var e = popupTitle ? popupTitle : "Game",
						o = popupHeight ? popupHeight : screen.availHeight - 70,
						s = "height=" + o + ", width=1500",
						f;
					f = document.location.protocol == "https:" && $.inArray(n.toUpperCase(), Helper.SSLVendorList) == -1 ? "http://" + sslRedirectSubdomain + "." + document.location.hostname.match(/.*\.(.*\..*)/)[1] + "/Gamelobby/" + n + "/" + t + ".do?isdemo=" + i : "/Gamelobby/" + n + "/" + t + ".do?isdemo=" + i;
					Games.currentGameLobbyWindow != undefined && Games.currentGameLobbyWindow.close();
					Games.currentGameLobbyWindow = window.open(f, e, s + ", resizable=yes, scrollbars=yes, toolbar=no, menubar=no, location=no, directories=no, status=yes")
				} else {
					popupTitle = "Game";
					popupHeight = "";
					var e = popupTitle ? popupTitle : "Game",
						o = popupHeight ? popupHeight : screen.availHeight - 70,
						s = "height=" + o + ", width=1500",
						f;
					f = document.location.protocol == "https:" && $.inArray(n.toUpperCase(), Helper.SSLVendorList) == -1 ? "http://" + sslRedirectSubdomain + "." + document.location.hostname.match(/.*\.(.*\..*)/)[1] + "/Gamelobby/" + n + "/" + t + ".do?platform=Download" : "/Gamelobby/" + n + "/" + t + ".do?platform=Download";
					Games.currentGameLobbyWindow != undefined && Games.currentGameLobbyWindow.close();
					Games.currentGameLobbyWindow = window.open(f, e, s + ", resizable=yes, scrollbars=yes, toolbar=no, menubar=no, location=no, directories=no, status=yes")
				}
			else Modal.PokerRegister("");
			return !1
		},
		EnterDemoGame: function (n, t, i) {
			$(i).hasClass("disabled") || Games.EnterGamePage(n, t, 1, 0, Helper.IsValidPTUser)
		},
		EnterGame: function (n, t) {
			var i, r, u, f, e;
			try {
				if (Helper.IsGameLock.toLowerCase() === "true") {
					Page.message(Helper.AccountGameLockMsg, Helper.AccountGameLockTitle);
					return
				}
				if (i = !0, $.ajax({
						type: "GET",
						url: "/Services/SelfExclusion.ashx",
						async: !1,
						data: {
							Check: "bet"
						},
						success: function (n) {
							n != null && n.IsSuccess && (Page.message(n.Message, selfExclusionTitle), i = !1)
						}
					}), !i) return;
				if (!CheckUserEligibility(n)) return;
				r = this.CheckGameCookieExist("_lastPlayedGame");
				r.unshift(t);
				u = r.filter(function (n, t, i) {
					return i.indexOf(n) === t
				});
				$(".slotGamePanel").length > 0 && (f = $('.slotGamePanel[data-game-id="' + t + '"]'), f.attr("data-recent") === "1" && (f.attr("data-recent", "0"), $('.slotsGamesList > .slotGamePanel[data-game-id="' + t + '"]').clone(!0).appendTo("#lastPlayedGamesHolder")));
				e = u.length < 1 ? "refresh" : u.join();
				Cookie.Create("_lastPlayedGame", e, 1);
				$("#lastPlayedGamesHolder").length > 0 && ($("#lastPlayedGamesHolder > div:lt(4)").show(), $("#lastPlayedGamesHolder > div").length < 1 && $("#lastPlayedGamesHolder").parents(".slotholder").removeClass("show").addClass("hide"))
			} catch (o) {}
			if (DialogManager_isLogin(Helper.MemberCode)) {
				if (Page.loading(), n.toLowerCase() == "pt" && !Helper.IsValidPTUser) return Modal.PTRegister();
				if (!n.toLowerCase() == "ipk" && !Helper.IsValidPokerUser) return Modal.PokerRegister();
				Games.CheckQuickTransfer(n, t)
			} else return Member.EnsureLogin(window.location.href), !1
		},
		DownloadPtGameClient: function () {
			var n = "http://cdn.jackpotmatrix.com/rb88prod/d/setup.exe";
			Helper.IsMobile ? (n = "http://mgames.jackpotmatrix.com", window.open(n, "_blank")) : window.location = n
		},
		CheckGameCookieExist: function (n) {
			var t = [],
				i = Cookie.Get(n);
			return i !== "" && i != "refresh" && (t = i.split(","), t.splice(16)), t
		},
		GameAccess: function (n) {
			try {
				n ? (typeof tempProvider != "undefined" && (tempGameType == "Sportsbook" ? tempLaunchGameMethod.toLowerCase() == "popup" ? Page.popupWindow(tempGameUrl, "", 1020, 500) : window.open(tempGameUrl) : tempProvider == "PT" ? Games.EnterGamePage(tempProvider, tempGameCode, 0, 0, tempIsValidPTUser) : Games.EnterGamePage(tempProvider, tempGameCode, 0, 0)), $("#modal-specialCancel").foundation("close")) : Modal.GameTransferOpen()
			} catch (t) {
				$("#modal-specialCancel").foundation("close")
			}
		},
		CheckQuickTransfer: function (n, t) {
			if (n.toUpperCase() == "BOY" && (n = "BOY2"), WalletData != null && WalletData != undefined) {
				var i = WalletData.WalletBalances,
					f = 0,
					r = 0,
					u = !1,
					e = SlotVendor.indexOf(n) > -1,
					o = CasinoVendor.indexOf(n) > -1;
				$.each(i, function (t) {
					if (i[t].Name.toLowerCase() == "totalbal" && (f = i[t].Balance), e && i[t].Name.toLowerCase() == "slot") {
						r = i[t].Balance;
						i[t].Status == "4" && (u = !0);
						return
					}
					if (o && i[t].Name.toLowerCase() == "ld") {
						r = i[t].Balance;
						i[t].Status == "4" && (u = !0);
						return
					}
					if (i[t].Name.toLowerCase() === n.toLowerCase()) {
						r = i[t].Balance;
						i[t].Status == "4" && (u = !0);
						return
					}
				});
				f > 0 ? r > 0 ? (Page.loadingDismiss(), n == "PT" ? Games.EnterGamePage(n, t, 0, 0, Helper.IsValidPTUser) : Games.EnterGamePage(n, t, 0, 0)) : u ? (Page.loadingDismiss(), n == "PT" ? Games.EnterGamePage(n, t, 0, 0, Helper.IsValidPTUser) : Games.EnterGamePage(n, t, 0, 0)) : (tempProvider = n.toUpperCase(), tempGameCode = t, tempIsValidPTUser = Helper.IsValidPTUser, Modal.GameTransferOpen()) : (tempProvider = n.toUpperCase(), tempGameCode = t, tempIsValidPTUser = Helper.IsValidPTUser, Modal.GameTransferOpen())
			} else setTimeout(function () {
				Games.CheckQuickTransfer(n, t)
			}, 1e3)
		}
	},
	SportsBook = {
		GenerateNextMatch: function () {
			$.ajax({
				type: "GET",
				url: "/Services/LiveOddsService.ashx?action=match&num=1",
				timeout: 2e4,
				success: function (n) {
					if (n !== null && n !== undefined) {
						var t = $(".sport-next-Match");
						Helper.LanguageCode == "zh" ? t.append(n.teams[2] + " vs " + n.teams[3]) : t.append(n.teams[0] + " vs " + n.teams[1])
					}
				}
			})
		}
	},
	Casino = {
		FilterGame: function (n, t) {
			$("#home-game-casino-filter-loading").show();
			$.ajax({
				type: "GET",
				url: "/Services/HomeCasinoGameService.ashx?RequestedFrom=home&GameVendor=" + n + "&GameCategory=" + t,
				timeout: 2e4,
				success: function (n) {
					if (caisnoFilterGameSlickMode == !0 && $("#casino-game-filter-slick").slick("unslick"), $("#casino-game-filter-slick").empty(), n !== null && n !== undefined && n.games !== undefined) {
						$.each(n.games, function (n, t) {
							var i = Helper.MediaUrl + t.ImageFileName,
								r = Helper.MediaUrl + t.ImageFileNameGif,
								u = "<img class='ImageLazy game-image Loaded' data-original=" + i + " src=" + i + " data-gifimage=" + r + " />",
								f = " <div class='play-button-wrapper'><div class='play-button'><img class='gamespinner' src='" + Helper.MediaUrl + "/Assets/images/Games/Icon/bg_O_2.png' /><img width=\"79\" height=\"80\" class='gameplay' src='" + Helper.MediaUrl + "/Assets/images/Games/Icon/play.png' /><\/div><\/div>",
								e = "onclick=\"currentGameType='livecasino';Games.EnterGame('" + t.Provider + "','" + t.GameId + "');\"",
								o = "<div class='home-game-image-wrapper' " + e + ">" + u + f + "<\/div>",
								s = "<div class='home-game-vendor-logo " + t.Provider + "'>" + t.Provider + "<\/div>",
								h = "<div class='home-game-info'><div><p class='home-game-Name home-casino-Game-Type-Name'>" + t.GameName + "<\/p><\/div>  <div class='home-game-info-type'><span class='home-game-vendor'>" + t.ProviderFullName + "<\/span><\/div><\/div>";
							$("#casino-game-filter-slick").append("<div class='column small-6 medium-4 home-game-column end'><div class='game-wrapper'>" + o + s + h + "<\/div><\/div>")
						});
						$(".home-game-image-wrapper").on("mouseenter", function () {
							var n = $(this).children(".game-image");
							$(".game-image").removeClass("scale");
							$(".gamespinner").removeClass("block");
							$(".gameplay").removeClass("block");
							$(".home-game-tryplay").hide();
							n.addClass("scale");
							n.siblings(".play-button-wrapper").find(".gamespinner").addClass("block");
							n.siblings(".play-button-wrapper").find(".gameplay").addClass("block");
							n.parent().siblings(".home-game-info").find(".home-game-tryplay").show()
						}).on("mouseout", function () {
							var n = $(this).siblings(".home-game-Image");
							n.removeClass("scale");
							n.attr("src", n.data("original"))
						}).on("touchstart", {
							passive: !0
						}, function () {
							var n = $(this).children(".game-image");
							$(".game-image").removeClass("scale");
							$(".gamespinner").removeClass("block");
							$(".gameplay").removeClass("block");
							$(".home-game-tryplay").hide();
							n.addClass("scale");
							n.siblings(".play-button-wrapper").find(".gamespinner").addClass("block");
							n.siblings(".play-button-wrapper").find(".gameplay").addClass("block");
							n.parent().siblings(".home-game-info").find(".home-game-tryplay").show()
						});
						if (caisnoFilterGameSlickMode == !0) {
							$("#casino-game-filter-slick").slick({
								infinite: !1,
								speed: 300,
								arrows: !1,
								slidesToShow: 3,
								slidesToScroll: 3,
								centerMode: !0,
								responsive: [{
									breakpoint: 1023,
									settings: {
										slidesToShow: 3,
										slidesToScroll: 3,
										centerMode: !0
									}
								}, {
									breakpoint: 800,
									settings: {
										slidesToShow: 2,
										slidesToScroll: 2,
										centerMode: !0,
										initialSlide: 2
									}
								}, {
									breakpoint: 480,
									settings: {
										slidesToShow: 1,
										slidesToScroll: 1,
										centerMode: !0,
										initialSlide: 1
									}
								}, ]
							});
							$("#casino-game-filter-slick").on("swipe", function () {
								SlickSwipeLazyLoadImage(this)
							})
						}
					}
					n == null || n.games.length <= 3 ? ($("#divCasinoFilterGame").show(), n == null ? $("#home-filter-casino-game-count").html(0) : $("#home-filter-casino-game-count").html(n.games.length)) : $("#divCasinoFilterGame").hide()
				}
			}).always(function () {
				$("#home-game-casino-filter-loading").hide()
			})
		},
		LoadBaccaratRoadMap: function () {
			Storage.Load("BaccaratRoadMap" + Helper.CultureCode + Helper.Platform) ? Casino.PopulateBaccaratRoadMap(Storage.Load("BaccaratRoadMap" + Helper.CultureCode + Helper.Platform)) : $.ajax({
				type: "GET",
				url: "/Services/BaccaratRoadMapService.ashx",
				timeout: 2e4,
				async: !0,
				success: function (n) {
					Storage.Save("BaccaratRoadMap" + Helper.CultureCode + Helper.Platform, n, 5);
					Casino.PopulateBaccaratRoadMap(n)
				}
			})
		},
		PopulateBaccaratRoadMap: function (n) {
			var t, i, r;
			t = Helper.LanguageCode == "zh" ? "cn" : "en";
			i = "";
			r = 0;
			$.each(n.baccaratRoadMapDetailList, function (n, i) {
				for (var u, e, o = "<div class='home-baccarat-roadmap-wrapper'><div class='home-roadmap-wrapper'>", c = "<p class='roadmap-game-title'>" + i.GameName + "<\/p>", l = "<div><a class='roadmap-game-vendor' style='display:inline-block;' href='" + i.GamePageLink + "'>" + i.ProviderFullName + "<\/a><p class='roadmap-game-vendor' style='display:inline-block;float:right'>" + i.BaccaratTableName + "<\/p><\/div>", f = "<table class='roadMapTable'><tbody>", a = "onclick=\"Games.EnterGame('" + i.Vendor + "','" + i.GameId + "')\"", v = "<div class='home-baccarat-play-button-wrapper'><span class='home-baccarat-play-button'" + a + ">" + i.PlayNow + "<\/span><\/div>", r = 0, s = 0, h = 0; h < 6; h++) {
					for (u = "", e = 0; e < 10; e++) {
						if (i.Roadmaps[r] != undefined) {
							switch (i.Roadmaps[r]) {
								case "0":
									i.Roadmaps[r] = "3"
							}
							u += "<td><div><img class='roadmap-badge lazy' src='" + Helper.MediaUrl + "/assets/images/roadmap/" + t + "/" + i.Roadmaps[r] + "0.png'/><\/div><\/td>"
						} else u += "<td><div><\/div><\/td>";
						r += 6
					}
					s++;
					r = s;
					f += "<tr>" + u + "<\/tr>"
				}
				f += "<\/tbody><\/table>";
				o += f + "<\/div>" + v + "<\/div>";
				n == 0 && $("#home-baccarat-roadmap-wrapper").empty();
				$("#home-baccarat-roadmap-wrapper").append("<div class='home-barracat-roadmap-game' style='' id='game-" + i.Index + "'>" + c + l + o + "<\/div>");
				$("#home-baccarat-selector").append("<li><span class='icon icon-fun88_pagination_radio_unchecked inactiveDot baccaratInactiveRadioButton' data-showfor='" + i.Index + "'><\/span><span class='icon icon-fun88_pagination_radio_checked activeDot baccaratActiveRadioButton'><\/span><\/li>")
			});
			$("#home-baccarat-roadmap-wrapper").slick({
				slidesToScroll: 1,
				autoplay: !0,
				autoplaySpeed: 8e3,
				arrows: !1,
				dots: !1
			});
			$(".baccaratActiveRadioButton").hide();
			$($(".baccaratInactiveRadioButton")[0]).next().show();
			$($(".baccaratInactiveRadioButton")[0]).hide();
			$("#home-baccarat-roadmap-wrapper").on("afterChange", function (n, t) {
				$(".baccaratActiveRadioButton").hide();
				$(".baccaratInactiveRadioButton").show();
				var r = $(t.$slides[t.currentSlide]).attr("id").split("-")[1],
					i = $('.baccaratInactiveRadioButton[data-showfor="' + r + '"]');
				$(i).next().show();
				$(i).hide()
			});
			$(".baccaratInactiveRadioButton").click(function (n) {
				n.preventDefault();
				var t = $(this).index(".baccaratInactiveRadioButton");
				$("#home-baccarat-roadmap-wrapper").slick("slickGoTo", parseInt(t))
			})
		},
		CasinoHeaderCategory: function () {
			checkRequired && (checkRequired = !1, $.ajax({
				type: "GET",
				url: "/Services/GameProviderTag.ashx?GameType=livecasino",
				async: !0,
				success: function (n) {
					var t, i, r, u;
					Helper.IsMobile ? (t = $("#casinoMobileCategory li").eq(0).clone(!0), $("#casinoMobileCategory li").remove(), n != null && (n.providerTagList.splice(0, 1), $.each(n.providerTagList, function (n, i) {
						var r;
						i.TagName.toUpperCase() != "NLE" && i.TagName.toUpperCase() != "EBT" ? (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), t.find(".vendor-hover-title").attr("id", i.TagName.toLowerCase()), t.find(".vendor-hover-title").removeClass().addClass("vendor-hover-title"), i.TagName.toUpperCase() == "EBT" ? (r = Helper.LanguageCode == "zh" ? "推荐" : "HOT", t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName + '<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">' + r + "<\/sup>")) : t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName), $("#casinoMobileCategory").append(t), t = $("#casinoMobileCategory li").eq(0).clone(!0)) : i.TagName.toUpperCase() == "NLE" ? Helper.MemberCode.length <= 18 && (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), t.find(".vendor-hover-title").attr("id", i.TagName.toLowerCase()), t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName + "<span> <\/span>"), $("#casinoMobileCategory").append(t), t = $("#casinoMobileCategory li").eq(0).clone(!0)) : i.TagName.toUpperCase() == "EBT" && (Helper.IsMobileApp || (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), t.find(".vendor-hover-title").attr("id", i.TagName.toLowerCase()), t.find(".vendor-hover-title").removeClass().addClass("vendor-hover-title"), r = Helper.LanguageCode == "zh" ? "推荐" : "HOT", t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName + '<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">' + r + "<\/sup>"), $("#casinoMobileCategory").append(t), t = $("#casinoMobileCategory li").eq(0).clone(!0)))
					}), $("#casinoMobileCategory").attr("data-required", "false"), Helper.LanguageCode.toLowerCase() == "en" || Helper.LanguageCode.toLowerCase() == "zh" || Helper.LanguageCode.toLowerCase() == "id" ? i = window.location.search.substring(1).split("&")[0].split("=")[1] : (r = window.location.pathname, u = r.split("/").length - 2, i = r.split("/")[u]), i != undefined && i != "" && $("#" + i).parent().addClass("vendor-hover blue"))) : (t = $("#casinoCategory li").eq(0).clone(!0), $("#casinoCategory li").remove(), n != null && (n.providerTagList.splice(0, 1), $.each(n.providerTagList, function (n, i) {
						if (i.TagName.toUpperCase() != "NLE") {
							if (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), i.TagName.toUpperCase() == "EBT") {
								var r = Helper.LanguageCode == "zh" ? "推荐" : "HOT";
								t.find(".vendor-hover-title").html(i.ResourceTagName + '<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">' + r + "<\/sup>")
							} else t.find(".vendor-hover-title").html(i.ResourceTagName);
							$("#casinoCategory").append(t);
							t = $("#casinoCategory li").eq(0).clone(!0)
						} else i.TagName.toUpperCase() == "NLE" && Helper.MemberCode.length <= 18 && (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), t.find(".vendor-hover-title").html(i.ResourceTagName + "<span> <\/span>"), $("#casinoCategory").append(t), t = $("#casinoCategory li").eq(0).clone(!0))
					}), $("#casinoCategory").attr("data-required", "false"), $(".livecasinoHover").first().addClass("blue"), vendorName = $("#casinoCategory").find(".livecasinoHover").hasClass("blue") ? $("#casinoCategory").find(".livecasinoHover.blue").attr("data-content-target") : $("#casinoCategory").find(".livecasinoHover").first().attr("data-content-target"), vendorName == "" && (vendorName = "gd"), GetTopBetGame(vendorName.toUpperCase(), "true", $("#header-casino-menu"), "casino"), Casino.CasinoHeaderGame()))
				}
			}))
		},
		CasinoHeaderGame: function () {
			Storage.Load("CasinoHeaderGame" + Helper.CultureCode + Helper.Platform) ? Casino.PopulateCasinoHeaderGame(Storage.Load("CasinoHeaderGame" + Helper.CultureCode + Helper.Platform)) : $.ajax({
				type: "GET",
				url: "/Services/HomeCasinoGameService.ashx?RequestedFrom=header&GameCategory=",
				async: !0,
				timeout: 2e4,
				success: function (n) {
					console.log("CasinoHeaderGame Initilize");
					Storage.Save("CasinoHeaderGame" + Helper.CultureCode + Helper.Platform, n, 15);
					Casino.PopulateCasinoHeaderGame(n)
				}
			})
		},
		PopulateCasinoHeaderGame: function (n) {
			var t = $("#casinoGame ul").eq(0).clone(!0),
				i = $("#casinoGame ul li").eq(0).clone(!0),
				r = $("#casinoGame").find(".livecasino-game-tab.game-link")[0],
				e = $("#casinoGame div").eq(0).clone(!0),
				f, u;
			n != null && (games = n.games, $.each(games, function (n) {
				t.addClass(n.toLowerCase());
				filteredGame = games[n];
				$.each(filteredGame, function (n) {
					if (n < 7) i.find("a").attr("onclick", 'Games.EnterGame("' + filteredGame[n].Provider + '","' + filteredGame[n].ID + '")'), i.find("a").html(filteredGame[n].GameName), t.append(i), i = $("#casinoGame ul li").eq(0).clone(!0), filteredGame.length - n >= 1 && (filteredGame[n].Provider.toLowerCase() == "ea" || filteredGame[n].Provider.toLowerCase() == "boy" || filteredGame[n].Provider.toLowerCase() == "ag") && t.append(r);
					else return !1
				});
				f = filteredGame[0].Provider;
				$(t).find("li").first().remove();
				$("#casinoGame").append(t);
				t = $("#casinoGame ul").eq(0).clone(!0);
				i = $("#casinoGame ul li").eq(0).clone(!0);
				r = $("#casinoGame").find(".livecasino-game-tab.game-link")[0]
			}), u = $(".livecasinoHover.blue").attr("data-content-target"), $(".vendor-Content.livecasino").first().remove(), $("#casinoGame").find("li:first-child .livecasino-game-tab.game-link").hide(), $(".vendor-Content.livecasino." + u).show())
		},
		EnterGameLobby: function (n) {
			var t = "0";
			n == "BOY" ? t = "1174" : n == "MGSQF" ? t = "1195" : n == "SUN" && (t = "780");
			currentGameType = "livecasino";
			Games.EnterGame(n, t)
		}
	},
	Keno = {
		LuckyNumber: [0, 1, 2, 3, 4, 5, 6, 7],
		GenerateLuckyNumber: function () {
			var n = [];
			$.each(Keno.LuckyNumber, function (t) {
				var i = $(".keno-lucky-number")[t];
				n[t] = setInterval(function () {
					i.innerHTML = Math.floor(Math.random() * 80) + 1
				}, 50)
			});
			$.each(Keno.LuckyNumber, function (t) {
				var i = $(".keno-lucky-number")[t];
				setInterval(function () {
					clearInterval(n[t])
				}, (t + 1) * 500)
			})
		},
		ChangeKenoRegionSelection: function () {
			var i = document.getElementById("kenoVietSelect"),
				n = i.options[i.selectedIndex].value,
				t;
			$(".keno-viet-lottery-region").hide();
			$(".kenoResultTable").hide();
			$(".keno-viet-lottery-region." + n).show();
			$($(".keno-viet-lottery-region." + n)[0]).addClass("selected");
			t = $($(".keno-viet-lottery-region." + n)[0]).data("showfor");
			$("#resultTable_" + t).show();
			$("#kenoVietResultDate").html($("#resultTable_" + t).data("dateshow"))
		},
		ChangeSubMarketTable: function (n) {
			$(".keno-viet-lottery-region").removeClass("selected");
			$(n).addClass("selected");
			$(".kenoResultTable").hide();
			var t = $(n).data("showfor");
			$("#resultTable_" + t).show();
			$("#kenoVietResultDate").html($("#resultTable_" + t).data("dateshow"))
		}
	},
	Slot = {
		MonthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		Generategraph: function (n) {
			var t = [],
				i = [];
			chartprovider = $(n).data("chartprovider");
			chartgamename = $(n).data("chartgamename");
			targetChart = $(n).data("targetchart");
			$.ajax({
				type: "GET",
				url: "/Services/JackpotScrapperGraphStatisticsService.ashx?GameProvider=" + chartprovider + "&GameName=" + chartgamename,
				async: !0,
				success: function (n) {
					$.each(n, function (r, u) {
						var e = new Date(u.Date),
							f = Slot.MonthNames[e.getMonth()];
						i.push(u.HitValue);
						r < n.length - 1 ? t.indexOf(f) < 0 ? t.push(f) : t.push("") : t.push("Now")
					});
					new Chartist.Line("#Gamechart-" + targetChart, {
						labels: t,
						series: [i, ]
					}, {
						fullWidth: !0,
						showPoint: !1,
						showArea: !0,
						chartPadding: {
							left: 10,
							right: 10
						},
						axisY: {
							onlyInteger: !0,
							labelInterpolationFnc: function (n) {
								return n > 1e6 ? n / 1e6 + "m" : n / 1e3 + "k"
							}
						}
					})
				}
			})
		},
		SlotHeaderCategory: function () {
			slotcheckRequired && (slotcheckRequired = !1, $.ajax({
				type: "GET",
				url: "/Services/GameProviderTag.ashx?GameType=slot",
				async: !0,
				success: function (n) {
					var u, t, i, r, f;
					Helper.IsMobile ? (t = $("#slotMobileCategory li").eq(0).clone(!0), $("#slotMobileCategory li").remove(), n != null && (n.providerTagList.splice(0, 1), $.each(n.providerTagList, function (n, i) {
						if (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), t.find(".vendor-hover-title").attr("id", i.TagName.toLowerCase()), t.find(".vendor-hover-title").removeClass().addClass("vendor-hover-title"), i.TagName.toUpperCase() == "MGSQF") {
							t.find("a").attr("data-content-target-seo", "microgaming");
							var r = Helper.LanguageCode == "zh" ? "推荐" : "HOT";
							t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName + '<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">' + r + "<\/sup>")
						} else i.TagName.toUpperCase() == "TG" ? t.find("a").attr("data-content-target-seo", "pragmaticplay") : i.TagName.toUpperCase() == "TGP_SLOT" ? t.find("a").attr("data-content-target-seo", "tgp_slot") : i.TagName.toUpperCase() == "CQG" ? t.find("a").attr("data-content-target-seo", "cq9") : i.TagName.toUpperCase() == "LX" ? t.find("a").attr("data-content-target-seo", "laxino") : i.TagName.toUpperCase() == "BSG" ? t.find("a").attr("data-content-target-seo", "betsoft") : i.TagName.toUpperCase() == "SWF" ? t.find("a").attr("data-content-target-seo", "skywind") : i.TagName.toUpperCase() == "PT" ? t.find("a").attr("data-content-target-seo", "playtech") : t.find("a").removeAttr("data-content-target-seo"), t.find(".vendor-hover-title").addClass(i.TagName.toLowerCase()).html(i.ResourceTagName);
						$("#slotMobileCategory").append(t);
						t = $("#slotMobileCategory li").eq(0).clone(!0)
					}), $("#slotMobileCategory").attr("data-required", "false"), Helper.LanguageCode.toLowerCase() == "en" || Helper.LanguageCode.toLowerCase() == "zh" || Helper.LanguageCode.toLowerCase() == "id" ? i = window.location.search.substring(1).split("&")[0].split("=")[1] : (r = window.location.pathname, f = r.split("/").length - 2, i = r.split("/")[f]), i != undefined && i != "" && $("#" + i).parent().addClass("vendor-hover blue"))) : (t = $("#slotCategory li").eq(0).clone(!0), u = $("#slotCategory li").eq(1).clone(!0), $("#slotCategory li").remove(), n != null && (n.providerTagList.splice(0, 1), $.each(n.providerTagList, function (n, i) {
						if (t.find("a").attr("data-content-target", i.TagName.toLowerCase()), i.TagName.toUpperCase() == "MGSQF") {
							t.find("a").attr("data-content-target-seo", "microgaming");
							var r = Helper.LanguageCode == "zh" ? "推荐" : "HOT";
							t.find(".vendor-hover-title").html(i.ResourceTagName + '<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">' + r + "<\/sup>")
						} else i.TagName.toUpperCase() == "TG" ? t.find("a").attr("data-content-target-seo", "pragmaticplay") : i.TagName.toUpperCase() == "TGP_SLOT" ? t.find("a").attr("data-content-target-seo", "tgp_slot") : i.TagName.toUpperCase() == "CQG" ? t.find("a").attr("data-content-target-seo", "cq9") : i.TagName.toUpperCase() == "LX" ? t.find("a").attr("data-content-target-seo", "laxino") : i.TagName.toUpperCase() == "BSG" ? t.find("a").attr("data-content-target-seo", "betsoft") : i.TagName.toUpperCase() == "SWF" ? t.find("a").attr("data-content-target-seo", "skywind") : i.TagName.toUpperCase() == "PT" ? t.find("a").attr("data-content-target-seo", "playtech") : t.find("a").removeAttr("data-content-target-seo"), t.find(".vendor-hover-title").html(i.ResourceTagName);
						t.find(".vendor-hover").addClass("header-slot-vendor");
						$("#slotCategory").append(t);
						t = $("#slotCategory li").eq(0).clone(!0)
					}), $("#slotCategory").attr("data-required", "false"), $(".slotHover").first().addClass("blue"), vendorName = $("#slotCategory").find(".slotHover").hasClass("blue") ? $("#slotCategory").find(".slotHover.blue").attr("data-content-target") : $("#slotCategory").find(".slotHover").first().attr("data-content-target"), vendorName == "" && (vendorName = "mgsqf"), $("#slotCategory").append(u), GetTopBetGame(vendorName.toUpperCase(), "true", $("#header-casino-menu"), "slot")))
				}
			}))
		}
	},
	Deposit = {
		methods: {
			wechat: {
				key: "WeChat",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/Wechat.htm"
			},
			onlinealipay: {
				key: "OnlineAlipay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/OnlineAlipay.htm"
			},
			chinadebitcard: {
				key: "ChinaDebitCard",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/BankCard.htm"
			},
			fastbaht: {
				key: "FastBaht",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/BankCard.htm"
			},
			help2pay: {
				key: "Help2Pay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/BankCard.htm"
			},
			onlinedeposit: {
				key: "OnlineDeposit",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/BankCard.htm"
			},
			bitcoin: {
				key: "BitCoin",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/BitCoin.htm"
			},
			baokim: {
				key: "Baokim",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/Baokim.htm"
			},
			iwallet: {
				key: "IWallet",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/IWallet.htm"
			},
			moneybooker: {
				key: "MoneyBooker",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/MoneyBooker.htm"
			},
			qqwallet: {
				key: "QQWallet",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/QQWallet.htm"
			},
			unionpay: {
				key: "UnionPay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/UnionPay.htm"
			},
			quickpay: {
				key: "QuickPay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/QuickPay.htm"
			},
			xpay: {
				key: "XPay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/XPay.htm"
			},
			xpayqr: {
				key: "XPayQR",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/XPayQR.htm"
			},
			jdpay: {
				key: "JDPay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/JDPay.htm"
			},
			eeziepay: {
				key: "EeziePay",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/EeziePay.htm"
			},
			paysec: {
				key: "PaySec",
				verifyPage: "/" + Helper.UrlCulture + "/Finance/Verify/PaySec.htm"
			}
		},
		SubmitOnlineDeposit: function (n, t, i, r, u, f, e, o, s) {
			var c = {
					Amount: u,
					BonusID: typeof e == "undefined" ? 0 : e,
					CouponCode: o,
					BlackBoxValue: blackbox,
					E2BlackBoxValue: e2,
					BankName: typeof s == "undefined" ? "" : s,
					MethodCode: i,
					Email: f,
					DepositingWallet: n
				},
				h = Helper.IsMobile || Helper.IsMobileApp || Helper.IsDesktopApps === "true" ? undefined : Deposit.PopupVerifyWindow(r.verifyPage + "?load=1");
			return $.ajax({
				type: "POST",
				url: "/Services/OnlineDepositService.ashx?action=" + r.key + "&updatepreferwallet=" + t,
				dataType: "html",
				data: JSON.stringify(c),
				success: function (n) {
					var t = JSON.parse(n);
					if (t.success)
						if (Helper.IsMobileApp && (Page.loadingDismiss(), t.message ? Page.info(t.message, function () {
								Deposit.PopupVerifyWindow(r.verifyPage)
							}) : Deposit.PopupVerifyWindow(r.verifyPage)), Helper.IsDesktopApps === "true") r.key === "ChinaDebitCard" ? window.location.href = r.verifyPage : (window.open(r.verifyPage, "_blank", "width=650,height=600"), t.message ? Page.info(t.message, function () {
							window.location.reload()
						}) : window.location.reload());
						else if (Helper.IsMobile) window.location.href = r.verifyPage;
					else if (h.location = r.verifyPage, h.focus(), t.message) Page.message(t.message, function () {
						window.location.reload()
					});
					else {
						h.opener.location.reload();
						return
					} else typeof h != "undefined" && (h.focus(), h.close()), t.uniquevalidation ? Helper.LanguageCode.toLowerCase() == "en" ? Page.message(t.message, "Friendly Reminder", "Got It", "uniquevalidation") : Page.message(t.message, "温馨提示", "我清楚明白", "uniquevalidation") : Page.message(t.message);
					Page.loadingDismiss()
				},
				error: function () {
					typeof h != "undefined" && (h.focus(), h.close());
					Page.message(DepositError);
					Page.loadingDismiss()
				}
			}), h
		},
		PopupVerifyWindow: function (n) {
			if (Helper.IsMobileApp) window.location = n;
			else {
				var t = window.open(n, "_blank", "width=930,height=600");
				return t.blur(), t
			}
		}
	},
	BankStatus = {
		StatusAtSidePanel: function (n) {
			$("#side-panel-bank-status-body").empty();
			$.each(n._bankList, function (n, t) {
				var i, r, u;
				i = t.IsUnderMaintenance ? "Offline" : "Online";
				r = "<tr class='bank-status-container " + i + "' onclick='OpenBankStatusMessage(this);' style='cursor: pointer'><td><span class='icon " + t.BankIconSVG + " bank-status-icon'><\/span><span style='display: block; margin-top: 5px;' class='bankName'>" + t.EnName + "<\/span><\/td><td><span class='BankStatus BankStatus-" + i + "'>" + t.Status + "<\/span><\/td><\/tr>";
				u = "<tr><td colspan='2' style='background-color: #e8e8e8; padding: 0 !important'><div class='bank-status-message' style='padding: 15px; background-color: #d3d3d3;'><span class='Blueboth'>" + t.strRegularDescOne + "<\/span><div>" + t.strRegularDescTwo + "<\/div><br/><span class='Blueboth'>" + t.strEmergencyDescOne + "<\/span><div>" + t.strEmergencyDescOne + "<\/div><br/><p>" + t.BSTimeZone + "<\/p><\/div><\/td><\/tr>";
				$("#side-panel-bank-status-body").append(r + u)
			})
		},
		StatusForMobileView: function (n) {
			$("#mobile-bank-status-view").empty();
			$.each(n._bankList, function (n, t) {
				var i, r;
				i = t.IsUnderMaintenance ? "Offline" : "Online";
				r = "<li><span class='icon " + t.BankIconSVG + "' style='color: black'><\/span><span style='color: black'>" + t.EnName + "<\/span><span class='mobileBankStatus " + i + "' style='float: right'>" + t.Status + "<\/span><\/li>";
				$("#mobile-bank-status-view").append(r)
			})
		},
		StatusDetailForMobileView: function (n) {
			$("#banks-status-mobile-view-detail").empty();
			$.each(n._bankList, function (n, t) {
				var i = "<div class='column medium-6 end' style='padding-top: 1rem;'> <div style='background-color: white;'><div style='background-color: #8e8e8e; color: white; padding: 5px 10px; font-size: 25px;'>" + t.TranslatedBankName + " <span class='icon icon-fun88_arrow_thick_right' style='float: right; line-height: 39px;'><\/span><\/div><div style='display: inline-block'><div class='mobile-bank-status-bank-image " + Helper.RegionCode.toLowerCase() + " " + t.IconPictureClass + "'><\/div><\/div><div class='mobile-bank-status-message'><span class='Blueboth'>" + t.strRegularDescOne + "<\/span><div>" + t.strRegularDescTwo + "<\/div><br/><span class='Blueboth'>" + t.strEmergencyDescOne + "<\/span><div>" + t.strEmergencyDescOne + "<\/div><br/><div>" + t.BSTimeZone + "<\/div><\/div><\/div><\/div>";
				$("#banks-status-mobile-view-detail").append(i)
			})
		},
		GenerateBankStatus: function () {
			Storage.Load("BanksStatus" + Helper.CultureCode) ? (BankStatus.StatusAtSidePanel(Storage.Load("BanksStatus" + Helper.CultureCode)), BankStatus.StatusForMobileView(Storage.Load("BanksStatus" + Helper.CultureCode)), BankStatus.StatusDetailForMobileView(Storage.Load("BanksStatus" + Helper.CultureCode))) : $.ajax({
				type: "GET",
				url: "/Services/BankStatusService.ashx",
				async: !0,
				timeout: 2e4,
				success: function (n) {
					Storage.Save("BanksStatus" + Helper.CultureCode, n, 10);
					BankStatus.StatusAtSidePanel(n);
					BankStatus.StatusForMobileView(n);
					BankStatus.StatusDetailForMobileView(n)
				}
			})
		}
	}