// -- Date -- //
   var todayDate = new Date();
   var currentDateYear = todayDate.getFullYear();
   var currentDateMonth = todayDate.getMonth() + 1;
   var currentDateDay = todayDate.getDate();
   var date18Year = new Date((currentDateYear - 18) + '/' + currentDateMonth + '/' + currentDateDay);
   var date68Year = new Date((currentDateYear - 68) + '/' + 01 + '/' + 01);
   var datePickerL = "en";
   var tncUrl = '#';
   var historyItem = [];
   var isSubmit = false;
   function SetSubmitStatus() {
       isSubmit = true;
   }
   $(document).ready(function () {
       /* GA Form Tracking */
       $(".inputfield").on('focusout', function (e) {
           if (e.relatedTarget === null || e.relatedTarget.type !== 'submit') {
               var name = $(this).data('gatrackfield');
               if (name === undefined)
                   name = $(this).parent().attr('id');
               historyItem.push(name);
               var eventAction = $(this).val().length > 0 ? 'Completed' : 'Skipped';
               // ga('send', 'event', {
               //     eventCategory: 'RegistrationP4',
               //     eventAction: eventAction,
               //     eventLabel: name
               // });
           }
       });
       window.addEventListener('beforeunload', function (e) {
           if (historyItem.length) {
               ga('send', 'event', {
                   eventCategory: 'RegistrationP4',
                   eventAction: isSubmit ? 'Form Submission' : 'Form Abandonment',
                   eventLabel: historyItem.join(' > ')
               });
           }
       });
       
       /* GA Form Tracking */
       var contact = $("#txtContactNum");
       var postal = $("#txtPostal");
       var city = $("#txtCity");
       var ddlCity = $("#ddlCity");
       var referral = $("#txtReferralCode");
       var email = $("#txtEmail");
       var membercode = $("#txtRegUsername");
       var confirmPassword = $("#txtConfirmPassword");
       if ($("#hidRegDOB").val() != "") {
           $("#txtDob").val($("#hidRegDOB").val());
       }
       if ($("#hidGender").val() == "0") {
           $("#btnFemale").removeClass("genderselected genderNonselected");
           $("#btnMale").removeClass("genderselected genderNonselected");
           $("#btnMale").addClass("genderselected");
           $("#btnFemale").addClass("genderNonselected");
       } else {
           $("#btnFemale").removeClass("genderselected genderNonselected");
           $("#btnMale").removeClass("genderselected genderNonselected");
           $("#btnFemale").addClass("genderselected");
           $("#btnMale").addClass("genderNonselected");
       }
       $("#AccountInput").hide();
       $("#ContactInput").hide();
       // -- auto complete email -- //
       $('#txtEmail').autoEmail(['126.com','sina.com','163.com','qq.com','gmail.com','yahoo.com','live.com','hotmail.com','naver.com','daum.net','nate.com'], false);
       // -- load first + last name -- //
       loadFullname($("#ddlPreferredCurrency").val());
       $("#hidRegDOB").val($("#txtDob").val());
       $("#ddlCountry").change(function () {
           GetCity();
       });
       $('#txtDob').on('change blur', function () {
           $("#hidRegDOB").val($("#txtDob").val());
           $('#txtDob').valid();
       });
       $("#ddlCity").change(function () {
           $('#hidCity').val($('#ddlCity').val());
       });
       $("#txtCity").change(function () {
           $('#hidCity').val($("#txtCity").val());
       });
       $("#ddlPreferredCurrency").change(function () {
           rebindWallet();
           var mobile = $("#txtContactNum").val();
           if (mobile.length !== 0)
               $("#txtContactNum").valid();
           if (this.value === "IDR") {
               $("#mandatoryField").html("&nbsp");
           } else {
               $("#mandatoryField").text("*");
           }
           loadFullname(this.value);
           $("#txtSurname").valid();
       });
       $("#btnMale").click(function () {
           $("#btnFemale").removeClass("genderselected genderNonselected");
           $("#btnMale").removeClass("genderselected genderNonselected");
           $("#btnMale").addClass("genderselected");
           $("#btnFemale").addClass("genderNonselected");
           $("#hidGender").val("0");
           var name = $(this).data('gatrackfield');
           if (name === undefined)
               name = $(this).parent().attr('id');
           historyItem.push(name);
           ga('send', 'event', {
               eventCategory: 'RegistrationP4',
               eventAction: "Completed",
               eventLabel: "Gender"
           });
       });
       $("#btnFemale").click(function () {
           $("#btnFemale").removeClass("genderselected genderNonselected");
           $("#btnMale").removeClass("genderselected genderNonselected");
           $("#btnFemale").addClass("genderselected");
           $("#btnMale").addClass("genderNonselected");
           $("#hidGender").val("1");
       });
       // -- form validate -- //
       $.validator.addMethod('SurnameRequireChecking', function (value, element) {
           if ($("#ddlPreferredCurrency").val() === 'IDR') {
               return true;
           }
           else {
               var surname = $.trim($("#txtSurname").val()).length > 0;
               return surname;
           }
       }, "");
       $.validator.addMethod('NameRequireChecking', function (value, element) {
           var name = $.trim($("#txtName").val()).length > 0;
           return name;
       }, "");
       $.validator.addMethod('SurnameRegexChecking', function (value, element) {
           var regex = new RegExp("^[a-zA-Z0-9'\u4e00-\u9fa5\u00e0-\u1EF4\u0e00-\u0e7f\u1100-\u11FF|\u3130-\u318F|\uA960-\uA97F|\uAC00-\uD7AF|\uD7B0-\uD7FF|\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC\s ]{0,50}$");
           var surnameTest = regex.test($("#txtSurname").val());
           return surnameTest;
       }, "");
       $.validator.addMethod('NameRegexChecking', function (value, element) {
           var regex = new RegExp("^[a-zA-Z0-9'\u4e00-\u9fa5\u00e0-\u1EF4\u0e00-\u0e7f\u1100-\u11FF|\u3130-\u318F|\uA960-\uA97F|\uAC00-\uD7AF|\uD7B0-\uD7FF|\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC\s ]{0,50}$");
           if (Boolean(true) || Boolean(false)) {
               regex = new RegExp("^[a-zA-Z0-9'\u4e00-\u9fa5\u00e0-\u1EF4\u0e00-\u0e7f\u1100-\u11FF|\u3130-\u318F|\uA960-\uA97F|\uAC00-\uD7AF|\uD7B0-\uD7FF|\u0041-\u005A\u0061-\u007A\u00AA\u00B5\u00BA\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02C1\u02C6-\u02D1\u02E0-\u02E4\u02EC\u02EE\u0370-\u0374\u0376\u0377\u037A-\u037D\u0386\u0388-\u038A\u038C\u038E-\u03A1\u03A3-\u03F5\u03F7-\u0481\u048A-\u0527\u0531-\u0556\u0559\u0561-\u0587\u05D0-\u05EA\u05F0-\u05F2\u0620-\u064A\u066E\u066F\u0671-\u06D3\u06D5\u06E5\u06E6\u06EE\u06EF\u06FA-\u06FC\u06FF\u0710\u0712-\u072F\u074D-\u07A5\u07B1\u07CA-\u07EA\u07F4\u07F5\u07FA\u0800-\u0815\u081A\u0824\u0828\u0840-\u0858\u08A0\u08A2-\u08AC\u0904-\u0939\u093D\u0950\u0958-\u0961\u0971-\u0977\u0979-\u097F\u0985-\u098C\u098F\u0990\u0993-\u09A8\u09AA-\u09B0\u09B2\u09B6-\u09B9\u09BD\u09CE\u09DC\u09DD\u09DF-\u09E1\u09F0\u09F1\u0A05-\u0A0A\u0A0F\u0A10\u0A13-\u0A28\u0A2A-\u0A30\u0A32\u0A33\u0A35\u0A36\u0A38\u0A39\u0A59-\u0A5C\u0A5E\u0A72-\u0A74\u0A85-\u0A8D\u0A8F-\u0A91\u0A93-\u0AA8\u0AAA-\u0AB0\u0AB2\u0AB3\u0AB5-\u0AB9\u0ABD\u0AD0\u0AE0\u0AE1\u0B05-\u0B0C\u0B0F\u0B10\u0B13-\u0B28\u0B2A-\u0B30\u0B32\u0B33\u0B35-\u0B39\u0B3D\u0B5C\u0B5D\u0B5F-\u0B61\u0B71\u0B83\u0B85-\u0B8A\u0B8E-\u0B90\u0B92-\u0B95\u0B99\u0B9A\u0B9C\u0B9E\u0B9F\u0BA3\u0BA4\u0BA8-\u0BAA\u0BAE-\u0BB9\u0BD0\u0C05-\u0C0C\u0C0E-\u0C10\u0C12-\u0C28\u0C2A-\u0C33\u0C35-\u0C39\u0C3D\u0C58\u0C59\u0C60\u0C61\u0C85-\u0C8C\u0C8E-\u0C90\u0C92-\u0CA8\u0CAA-\u0CB3\u0CB5-\u0CB9\u0CBD\u0CDE\u0CE0\u0CE1\u0CF1\u0CF2\u0D05-\u0D0C\u0D0E-\u0D10\u0D12-\u0D3A\u0D3D\u0D4E\u0D60\u0D61\u0D7A-\u0D7F\u0D85-\u0D96\u0D9A-\u0DB1\u0DB3-\u0DBB\u0DBD\u0DC0-\u0DC6\u0E01-\u0E30\u0E32\u0E33\u0E40-\u0E46\u0E81\u0E82\u0E84\u0E87\u0E88\u0E8A\u0E8D\u0E94-\u0E97\u0E99-\u0E9F\u0EA1-\u0EA3\u0EA5\u0EA7\u0EAA\u0EAB\u0EAD-\u0EB0\u0EB2\u0EB3\u0EBD\u0EC0-\u0EC4\u0EC6\u0EDC-\u0EDF\u0F00\u0F40-\u0F47\u0F49-\u0F6C\u0F88-\u0F8C\u1000-\u102A\u103F\u1050-\u1055\u105A-\u105D\u1061\u1065\u1066\u106E-\u1070\u1075-\u1081\u108E\u10A0-\u10C5\u10C7\u10CD\u10D0-\u10FA\u10FC-\u1248\u124A-\u124D\u1250-\u1256\u1258\u125A-\u125D\u1260-\u1288\u128A-\u128D\u1290-\u12B0\u12B2-\u12B5\u12B8-\u12BE\u12C0\u12C2-\u12C5\u12C8-\u12D6\u12D8-\u1310\u1312-\u1315\u1318-\u135A\u1380-\u138F\u13A0-\u13F4\u1401-\u166C\u166F-\u167F\u1681-\u169A\u16A0-\u16EA\u1700-\u170C\u170E-\u1711\u1720-\u1731\u1740-\u1751\u1760-\u176C\u176E-\u1770\u1780-\u17B3\u17D7\u17DC\u1820-\u1877\u1880-\u18A8\u18AA\u18B0-\u18F5\u1900-\u191C\u1950-\u196D\u1970-\u1974\u1980-\u19AB\u19C1-\u19C7\u1A00-\u1A16\u1A20-\u1A54\u1AA7\u1B05-\u1B33\u1B45-\u1B4B\u1B83-\u1BA0\u1BAE\u1BAF\u1BBA-\u1BE5\u1C00-\u1C23\u1C4D-\u1C4F\u1C5A-\u1C7D\u1CE9-\u1CEC\u1CEE-\u1CF1\u1CF5\u1CF6\u1D00-\u1DBF\u1E00-\u1F15\u1F18-\u1F1D\u1F20-\u1F45\u1F48-\u1F4D\u1F50-\u1F57\u1F59\u1F5B\u1F5D\u1F5F-\u1F7D\u1F80-\u1FB4\u1FB6-\u1FBC\u1FBE\u1FC2-\u1FC4\u1FC6-\u1FCC\u1FD0-\u1FD3\u1FD6-\u1FDB\u1FE0-\u1FEC\u1FF2-\u1FF4\u1FF6-\u1FFC\u2071\u207F\u2090-\u209C\u2102\u2107\u210A-\u2113\u2115\u2119-\u211D\u2124\u2126\u2128\u212A-\u212D\u212F-\u2139\u213C-\u213F\u2145-\u2149\u214E\u2183\u2184\u2C00-\u2C2E\u2C30-\u2C5E\u2C60-\u2CE4\u2CEB-\u2CEE\u2CF2\u2CF3\u2D00-\u2D25\u2D27\u2D2D\u2D30-\u2D67\u2D6F\u2D80-\u2D96\u2DA0-\u2DA6\u2DA8-\u2DAE\u2DB0-\u2DB6\u2DB8-\u2DBE\u2DC0-\u2DC6\u2DC8-\u2DCE\u2DD0-\u2DD6\u2DD8-\u2DDE\u2E2F\u3005\u3006\u3031-\u3035\u303B\u303C\u3041-\u3096\u309D-\u309F\u30A1-\u30FA\u30FC-\u30FF\u3105-\u312D\u3131-\u318E\u31A0-\u31BA\u31F0-\u31FF\u3400-\u4DB5\u4E00-\u9FCC\uA000-\uA48C\uA4D0-\uA4FD\uA500-\uA60C\uA610-\uA61F\uA62A\uA62B\uA640-\uA66E\uA67F-\uA697\uA6A0-\uA6E5\uA717-\uA71F\uA722-\uA788\uA78B-\uA78E\uA790-\uA793\uA7A0-\uA7AA\uA7F8-\uA801\uA803-\uA805\uA807-\uA80A\uA80C-\uA822\uA840-\uA873\uA882-\uA8B3\uA8F2-\uA8F7\uA8FB\uA90A-\uA925\uA930-\uA946\uA960-\uA97C\uA984-\uA9B2\uA9CF\uAA00-\uAA28\uAA40-\uAA42\uAA44-\uAA4B\uAA60-\uAA76\uAA7A\uAA80-\uAAAF\uAAB1\uAAB5\uAAB6\uAAB9-\uAABD\uAAC0\uAAC2\uAADB-\uAADD\uAAE0-\uAAEA\uAAF2-\uAAF4\uAB01-\uAB06\uAB09-\uAB0E\uAB11-\uAB16\uAB20-\uAB26\uAB28-\uAB2E\uABC0-\uABE2\uAC00-\uD7A3\uD7B0-\uD7C6\uD7CB-\uD7FB\uF900-\uFA6D\uFA70-\uFAD9\uFB00-\uFB06\uFB13-\uFB17\uFB1D\uFB1F-\uFB28\uFB2A-\uFB36\uFB38-\uFB3C\uFB3E\uFB40\uFB41\uFB43\uFB44\uFB46-\uFBB1\uFBD3-\uFD3D\uFD50-\uFD8F\uFD92-\uFDC7\uFDF0-\uFDFB\uFE70-\uFE74\uFE76-\uFEFC\uFF21-\uFF3A\uFF41-\uFF5A\uFF66-\uFFBE\uFFC2-\uFFC7\uFFCA-\uFFCF\uFFD2-\uFFD7\uFFDA-\uFFDC\s ]{2,50}$");
           }
           var fn = regex.test($("#txtName").val());
           return fn;
       }, "");
       $.validator.addMethod('email', function (value, element) {
           return this.optional(element) || /^\w+[-.`~!@$%^*()={}|?\w]+@([\w.]{2,})+?\.[a-zA-Z]{2,9}$/i.test(value);
       }, 'Invalid email format.');
       $.validator.addMethod('selectNoneDDL', function (value, element) {
           return this.optional(element) || (value != -1);
       }, "Please select one option");
       $.validator.addMethod('selectNoneDDLNation', function (value, element) {
           return this.optional(element) || (value != "0");
       }, "Please select one option");
       $.validator.addMethod('SingaporeChecking', function (value, element) {
           return this.optional(element) || (value != "SG");
       }, "Please select one option");
       $.validator.addMethod("regex", function (value, element, regexp) {
           var re = new RegExp(regexp);
           return this.optional(element) || re.test(value);
       }, "");
       var validator = $("#baseForm").validate({
           onsubmit: false,
           focusInvalid: true,
           rules: {
               'LastName': {
                   SurnameRequireChecking: true,
                   SurnameRegexChecking: true
               },
               'firstName': {
                   NameRequireChecking: true,
                   NameRegexChecking: true
               },
               'username': {
                   required: true,
                   regex: "^[a-zA-Z0-9]+$",
                   remote: {
                       url: "#",
                       type: "POST",
                       data: {
                           MemberCode: function () {
                               return $("#txtRegUsername").val();
                           }
                       }
                   }
               },
               'date': {
                   required: true
               },
               'password': {
                   required: true,
                   regex: "^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9])+$",
                   minlength: 6
               },
               'confirm': {
                   required: true,
                   regex: "^(?=.*[0-9])(?=.*[a-zA-Z])([a-zA-Z0-9])+$",
                   equalTo: "#txtRegPassword"
               },
               'number': {
                   required: true,
                   minlength: function () {
                       var countrycode = $("#txtCountryCode").text();
                       if (countrycode.trim() === "86")
                           return 11;
                       else if (countrycode.trim() === "82")
                           return 10;
                       else if (countrycode.trim() === "84" ||
                           countrycode.trim() === "66" ||
                           countrycode.trim() === "62" ||
                           countrycode.trim() === "81") //VN, TH, ID , JP
                           return 9;
                       else if (countrycode.trim().length <= 1) {
                           return 16;
                       } else //default
                           return 5;
                   },
                   maxlength: function () {
                       var countrycode = $("#txtCountryCode").text();
                       if (countrycode.trim() === "86")
                           return 11;
                       else if (countrycode.trim() === "82" || countrycode.trim() === "81")
                           return 10;
                       else if (countrycode.trim() === "62")
                           return 13;
                       else if (countrycode.trim() === "84")
                           return 10;
                       else if (countrycode.trim() === "66")
                           return 9;
                       else //default
                           return 15;
                   },
                   regex: "^[-+0-9 ]+$",
                   remote: {
                       url: "#",
                       type: "POST",
                       data: {
                           PhoneNumber: function () {
                               return $("#txtContactNum").val();
                           },
                           CountryCode: function () {
                               return $("#txtCountryCode").text();
                           }
                       },
                       dataFilter: function (data) {
                           return JSON.parse(data).RESULT;
                       }
                   }
               },
               'address': {
                   required: {
                       depends: function () {
                           $(this).val($.trim($(this).val()));
                           return true;
                       }
                   },
                   regex: "^[^;:：；<>《》]+$"
               },
               'country': {
                   selectNoneDDL: true
               },
               'postal': {
                   required: true,
                   regex: "^[a-zA-Z0-9\-]+$"
               },
               'city2': {
                   required: {
                       depends: function () {
                           $(this).val($.trim($(this).val()));
                           return true;
                       }
                   },
                   regex: "^[^;:：；<>《》]+$"
               },
               'city': {
                   required: true
               },
               'preferred': {
                   selectNoneDDL: true
               },
               'wallet': {
                   selectNoneDDL: true
               },
               'refferal': {
                   regex: "^[a-zA-Z0-9]+$"
               },
               'place': {
                   selectNoneDDLNation: true,
                   SingaporeChecking: true
               },
               'nationality': {
                   selectNoneDDLNation: true,
                   SingaporeChecking: true
               }
           },
           messages: {
               'LastName': {
                   SurnameRequireChecking: "Please enter your last name. ",
                   SurnameRegexChecking: "Name can not contain invalid characters. "
               },
               'firstName': {
                   NameRequireChecking: "Please enter your first name. ",
                   NameRegexChecking: function (err) {
                       var errMsg;
                       if (Boolean(true) || Boolean(false)) {
                           errMsg = "The First Name must have at least 2 letters and can not use invalid letters. "
                       }
                       else {
                           errMsg = "Name can not contain invalid characters. "
                       }
                       return errMsg;
                   }
               },
               'email': {
                   required: "Email address can not be empty. ",
                   email: "Email format is invalid. "
               },
               'username': {
                   required: "Username can not be empty. ",
                   regex: "Just accept 'A-Z', 'a-z' and '0-9' "
               },
               'date': {
                   required: "Date of birth can not be empty. "
               },
               'password': {
                   required: function () {
                       var txtPw = $("#txtRegPassword").val();
                       var validationMessagePw;
                       if (txtPw.length === 0)
                           validationMessagePw = "Password can not be empty. ";
                       else
                           validationMessagePw = "Just accept 'A-Z', 'a-z' and '0-9' <br/> ";
                       return validationMessagePw;
                   },
                   regex: "Just accept 'A-Z', 'a-z' and '0-9' <br/> ",
                   minlength: "The minimum password length is 6 characters. <br/> "
               },
               'confirm': {
                   required: function () {
                       var txtConfirmPw = $("#txtConfirmPassword").val();
                       var validationMessageCPw;
                       if (txtConfirmPw.length === 0)
                           validationMessageCPw = "Confirm password can not be empty <br/> ";
                       else
                           validationMessageCPw = "Just accept 'A-Z', 'a-z' and '0-9' <br/> ";
                       return validationMessageCPw;
                   },
                   regex: "Just accept 'A-Z', 'a-z' and '0-9' <br/> ",
                   equalTo: "Password and Confirm Password not the same."
               },
               'number': {
                   required: "Phone number can not be empty. ",
                   regex: "Only accept '0-9' ",
                   minlength: function () {
                       var countrycode = $("#txtCountryCode").text();
                       var validationMessage;
                       if (countrycode.trim() === "86") //CN
                           validationMessage = "Phone number must have 11 numbers. ";
                       else if (countrycode.trim() === "66") //TH
                           validationMessage = "Phone number must have 9 numbers. ";
                       else if (countrycode.trim() === "84") //VN
                           validationMessage = "Phone numbers must have 9-10 numbers. ";
                       else if (countrycode.trim() === "62") //ID
                           validationMessage = "Phone numbers must have 9-13 numbers.";
                       else if (countrycode.trim() === "81") //JP
                           validationMessage = "Phone numbers must have 9-10 numbers. ";
                       else if (countrycode.trim().length <= 1)
                           validationMessage = "The phone number is incorrect ";
                       else //default
                           validationMessage = "Phone numbers must have 5-15 numbers. ";
                       return validationMessage;
                   },
                   maxlength: function () {
                       var countrycode = $("#txtCountryCode").text();
                       var validationMessage;
                       if (countrycode.trim() === "86") //CN
                           validationMessage = "Phone number must have 11 numbers. ";
                       else if (countrycode.trim() === "66") //TH
                           validationMessage = "Phone number must have 9 numbers. ";
                       else if (countrycode.trim() === "84") //VN
                           validationMessage = "Phone numbers must have 9-10 numbers. ";
                       else if (countrycode.trim() === "62") //ID
                           validationMessage = "Phone numbers must have 9-13 numbers.";
                       else if (countrycode.trim() === "81") //JP
                           validationMessage = "Phone numbers must have 9-10 numbers. ";
                       else //default
                           validationMessage = "Phone numbers must have 5-15 numbers. ";
                       return validationMessage;
                   },
                   remote: "The phone number is incorrect "
               },
               'address': {
                   required: "Address can not be empty. Please enter your full address. ",
                   regex: "Do not enter special symbols "
               },
               'country': {
                   selectNoneDDL: "Country selection is required. "
               },
               'postal': {
                   required: "Postal code can not be empty. ",
                   regex: "Just accept 'A-Z', 'a-z' and '0-9' "
               },
               'city2': {
                   required: "City can not be empty. ",
                   regex: "Do not enter special symbols "
               },
               'city': {
                   required: "City can not be empty. "
               },
               'preferred': {
                   selectNoneDDL: "Preferred currency selection is required."
               },
               'wallet': {
                   selectNoneDDL: "Wallet required. "
               },
               'refferal': {
                   regex: "Just accept 'A-Z', 'a-z' and '0-9' "
               },
               'place': {
                   selectNoneDDLNation: "Place of birth is required. ",
                   SingaporeChecking: "Sorry, registration is not permitted by the laws of the country of your choice. "
               },
               'nationality': {
                   selectNoneDDLNation: "Citizenship is required. ",
                   SingaporeChecking: "Sorry, registration is not permitted by the laws of the country of your choice. "
               }
           },
           onkeyup: false,
           onfocusout: function (element) {
               this.element(element);
           },
           showErrors: function () {
               this.defaultShowErrors();
           },
           errorPlacement: function (error, element) {
               if (element[0] === postal[0]) {
                   $("#txtPostalField").parent().find(".lastColumn-postal > .error").remove();
                   error.prependTo($("#txtPostalField").parent().find(".lastColumn-postal"));
                   $("#txtPostalField").find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   $("#txtPostalField").find(".inputResult").addClass("icon-close-circle-button");
               }
               else if (element[0] === city[0] || element[0] === ddlCity[0]) {
                   $("#txtCityField").parent().find(".lastColumn-city > .error").remove();
                   error.prependTo($("#txtCityField").parent().find(".lastColumn-city"));
                   $("#txtCityField").find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   $("#txtCityField").find(".inputResult").addClass("icon-close-circle-button");
               }
               else if (element[0] === contact[0]) {
                   element.parent().parent().parent().find(".lastColumn > .error").remove();
                   error.prependTo(element.parent().parent().parent().find(".lastColumn"));
                   element.parent().parent().parent().find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   element.parent().parent().parent().find(".inputResult").addClass("icon-close-circle-button");
               }
               else if (element[0] === referral[0] && $("#txtReferralCode").val().length == "0") {
                   element.parent().parent().find(".lastColumn > .error").remove();
                   element.parent().parent().find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
               } else {
                   element.parent().parent().find(".lastColumn > .error").remove();
                   error.prependTo(element.parent().parent().find(".lastColumn"));
                   element.parent().parent().find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   element.parent().parent().find(".inputResult").addClass("icon-close-circle-button");
               }
               element.addClass("inputFail");
           },
           success: function (label) {
               if (label[0].htmlFor === postal[0].id) {
                   $("#txtPostalField").find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   $("#txtPostalField").find(".inputResult").addClass("icon-tick-circle-button");
                   label.parent().parent().find(".lastColumn-postal > .success").remove();
               }
               else if (label[0].htmlFor === city[0].id || label[0].htmlFor === ddlCity[0].id) {
                   $("#txtCityField").find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   $("#txtCityField").find(".inputResult").addClass("icon-tick-circle-button");
                   label.parent().parent().find(".lastColumn-city > .success").remove();
               }
               else if (label[0].htmlFor === referral[0].id && $("#txtReferralCode").val().length == "0") {
                   label.parent().parent().find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   label.parent().parent().find(".lastColumn > .success").remove();
               }
               else {
                   label.parent().parent().find(".inputResult").removeClass("icon-close-circle-button icon-tick-circle-button");
                   label.parent().parent().find(".inputResult").addClass("icon-tick-circle-button");
                   label.parent().parent().find(".lastColumn > .success").remove();
               }
               if (label[0].htmlFor === email[0].id) {
                   BasicSlide();
               }
               if (label[0].htmlFor === confirmPassword[0].id) {
                   AccountSlide();
               }
               label.text("").removeClass("error success");
               label.text("").addClass("success");
           }
       });
       $("#btnSubmit").click(function () {
           var isValid = $('#baseForm').valid();
           if (isValid == true) {
               $("#Contact").removeClass("icon-tick-circle-button icon-close-circle-button");
               $("#Contact").addClass("icon-tick-circle-button");
               if ($("#chkTnC").is(":checked") && $("#chkAcknowledge").is(":checked")) {
                       Page.loading();
                       return true;
                   }
                   else {
                       if (!$("#chkTnC").is(":checked")) {
                           Page.message("Please agree to the terms, conditions, rules and regulations. ", "JOIN NOW");
                           return false;
                       }
                       else if (!$("#chkAcknowledge").is(":checked")) {
                           Page.message("Please confirm your age is at least 18 years old. ", "JOIN NOW");
                               return false;
                           }
                   }
           }
           else {
               $("#Contact").removeClass("icon-tick-circle-button icon-close-circle-button");
               $("#Contact").addClass("icon-close-circle-button");
               var errors = validator.numberOfInvalids();
               if (errors) {
                   validator.errorList[0].element.focus();
               }
               return false;
           }
       });
       $("#ddlPlaceofBirth").valid();
       $("#ddlNationality").valid();
       GetCity();
       if ($("#hidCity").val() != "") {
           $("#txtCity").val($("#hidCity").val());
       }
   });
   function loadFullname(currency) {
       switch (currency) {
           case "CNY":
           case "VND":
               $("#mandatoryField").text("*");
               surLastName();
               break;
           case "IDR":
               $("#mandatoryField").html("&nbsp");
               lastSurname();
               break;
           case "EUR":
           case "THB":
               $("#mandatoryField").text("*");
               lastSurname();
               break;
           case "USD":
               {
                   if (Boolean(false))
               surLastName();
           else
               lastSurname();
       }
   }
   }
   function lastSurname() {
   $("#rowSurname").insertAfter($("#rowName"));
   $("#txtName").attr("tabindex", "1");
   $("#txtSurname").attr("tabindex", "2");
   }
   function surLastName() {
   $("#rowSurname").insertBefore($("#rowName"));
   $("#txtSurname").attr("tabindex", "1");
   $("#txtName").attr("tabindex", "2");
   }
   function GetCity() {
   var country = $("#ddlCountry").val();
   $('#ddlCity').empty();
   $('#txtCity').val("");
   $('#txtCityField .inputResult').removeClass('icon-tick-circle-button icon-close-circle-button');
   $.post("/Services/MemberService.ashx?action=CityList",
   { countryCode: country },
    function (data) {
        if (data.CITY !== "-1") {
            var city = data.CITY.trim().split(';');
            for (var i = 0; i < city.length - 1; i++) {
                $('#ddlCity').append(new Option(city[i], city[i]));
            }
            $('#ddlCity option:first').attr("selected", "selected");
            $('#hidCity').val($('#ddlCity').val());
            $("#ddlCity").css("display", "block");
            $("#txtCity").css("display", "none");
        } else {
            $("#txtCity").css("display", "block");
            $("#ddlCity").css("display", "none");
        }
    }, "json").fail(function () {
        $("#txtCity").css("display", "block");
        $("#ddlCity").css("display", "none");
    });
   }
   function rebindWallet() {
   var currency = $("#ddlPreferredCurrency").val();
   $('#ddlPreferredWallet').empty();
   $.post("#",
   { currency: currency },
    function (data) {
        if (data.WALLET !== "-1") {
            var wallet = data.WALLET.split(';');
            var walletName = data.WALLETNAME.split(';');
            for (var i = 0; i < wallet.length - 1; i++) {
                $('#ddlPreferredWallet').append(new Option(walletName[i], wallet[i]));
            }
            $('#ddlPreferredWallet option:first').attr("selected", "selected");
        }
    });
   }
   function BasicSlide() {
   var basicInput = $("#BasicInfoSection .inputTextField").length;
   var currentInput = 0;
   $("#BasicInfoSection .inputResult").each(function () {
   if ($(this).hasClass("icon-tick-circle-button")) {
       currentInput += 1;
   }
   });
   if (currentInput == basicInput) {
   AccountFocus();
   $("#Basic").removeClass("icon-tick-circle-button icon-close-circle-button");
   $("#Basic").addClass("icon-tick-circle-button");
   }
   else {
   BasicFocus();
   }
   }
   function AccountSlide() {
   var accountInput = $("#AccountInfoSection .inputTextField").length;
   var currentInput = 0;
   $("#AccountInfoSection .inputResult").each(function () {
   if ($(this).hasClass("icon-tick-circle-button")) {
       currentInput += 1;
   }
   });
   if (currentInput == accountInput) {
   ContactFocus();
   $("#Account").removeClass("icon-tick-circle-button icon-close-circle-button");
   $("#Account").addClass("icon-tick-circle-button");
   } else {
   AccountFocus();
   }
   }
   function StepToggle(step) {
   if (step == "1") {
   if ($("#BasicInput").css("display") != "block") {
       BasicFocus();
   }
   }
   else if (step == "2") {
   var basicBool = true;
   $("#BasicInput .inputTextField input").each(function () {
       if ($(this).hasClass("pending")) {
           basicBool = false;
           return;
       }
       var result = $(this).valid();
       if (Boolean(!result)) {
           basicBool = false;
       }
   });
   $("#BasicInput .inputTextField select").each(function () {
       var result = $(this).valid();
       if (Boolean(!result)) {
           basicBool = false;
       }
   });
   if (basicBool) {
       AccountFocus();
   }
   else {
       BasicFocus();
       $("#Basic").removeClass("icon-tick-circle-button icon-close-circle-button");
       $("#Basic").addClass("icon-close-circle-button");
   }
   }
   else {
   if (!$("#Basic").hasClass("icon-tick-circle-button")) {
       return;
   }
   var accountBool = true;
   $("#AccountInput .inputTextField input").each(function () {
       if ($(this).hasClass("pending")) {
           return
       }
       var result = $(this).valid();
       if (Boolean(!result)) {
           accountBool = false;
       }
   });
   if (accountBool) {
       ContactFocus();
   }
   else {
       AccountFocus();
       $("#Account").removeClass("icon-tick-circle-button icon-close-circle-button");
       $("#Account").addClass("icon-close-circle-button");
   }
   }
   }
   function SetCursor(part) {
   if (part == "1") {
   $("#BasicPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#BasicPointer").addClass('icon-fun88_arrow_thick_up');
   $("#AccountPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#AccountPointer").addClass('icon-fun88_arrow_thick_down');
   $("#ContactPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#ContactPointer").addClass('icon-fun88_arrow_thick_down');
   }
   else if (part == "2") {
   $("#BasicPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#BasicPointer").addClass('icon-fun88_arrow_thick_down');
   $("#AccountPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#AccountPointer").addClass('icon-fun88_arrow_thick_up');
   $("#ContactPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#ContactPointer").addClass('icon-fun88_arrow_thick_down');
   }
   else {
   $("#BasicPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#BasicPointer").addClass('icon-fun88_arrow_thick_down');
   $("#AccountPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#AccountPointer").addClass('icon-fun88_arrow_thick_down');
   $("#ContactPointer").removeClass('icon-fun88_arrow_thick_up icon-fun88_arrow_thick_down');
   $("#ContactPointer").addClass('icon-fun88_arrow_thick_up');
   }
   }
   function BasicFocus() {
   $("#BasicInput").show();
   $("#registrationleftBanner").attr("src", 'assets/www.enhuoyea11.net/' + "/Assets/images/Member/Registration/Banner/" + 'en' + "/ambassadors.png");
   $("#AccountInput").hide(500);
   $("#ContactInput").hide(500);
   SetCursor("1");
   }
   function AccountFocus() {
   $("#AccountInput").show();
   $("#registrationleftBanner").attr("src", 'assets/www.enhuoyea11.net/' + "/Assets/images/Member/Registration/Banner/" + 'en' + "/ambassadors1.png");
   $("#BasicInput").hide(500);
   $("#ContactInput").hide(500);
   SetCursor("2");
   }
   function ContactFocus() {
   $("#ContactInput").show();
   $("#registrationleftBanner").attr("src", 'assets/www.enhuoyea11.net/' + "/Assets/images/Member/Registration/Banner/" + 'en' + "/ambassadors2.png");
   $("#registrationleftBannerContainer").attr("style", 'margin-top: 90px;');
   $("#AccountInput").hide(500);
   $("#BasicInput").hide(500);
   SetCursor("3");
   $("#ddlCity").valid();
   $("#ddlCountry").valid();
   $("#ddlPreferredCurrency").valid();
   $("#ddlPreferredWallet").valid();
   }
   $(function () {
   var options = $.extend(
      {},
       $.datepicker.regional[datePickerL],
       {   
           yearRange: (currentDateYear - 68) + ':' + (currentDateYear - 18),
           minDate: date68Year,
           maxDate: date18Year,
           dateFormat: 'mm/dd/yy',
           changeMonth: true,
           changeYear: true
       }
   );
   $("#txtDob").datepicker(options);
   $("#btnDOB .icon-history").click(function () {
   $("#txtDob").focus();
   });
   });