<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Works extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "Cypherlabs Work | ".APP_NAME
			];
		return view('content.cypherlabs-work',$data);
	}
	
}