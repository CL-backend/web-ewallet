<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "About | ".APP_NAME
			];
		return view('content.about',$data);
	}
	
}
