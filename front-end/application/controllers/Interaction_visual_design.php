<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Interaction_visual_design extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "UI - Design | ".APP_NAME
			];
		return view('content.ui-design',$data);
	}
	
}