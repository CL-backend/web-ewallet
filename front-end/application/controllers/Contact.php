<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "Contact Us | ".APP_NAME
			];
		return view('content.contact-us',$data);
	}
	
}
