<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ux_research_evaluation extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "UX - Research | ".APP_NAME
			];
		return view('content.ux-research',$data);
	}
	
}