<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web_software_development extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "Web Design & Development | ".APP_NAME
			];
		return view('content.web-dev',$data);
	}
	
}