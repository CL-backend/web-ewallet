<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Digital_marketing extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "Digital Marketing | ".APP_NAME
			];
		return view('content.digital-marketing',$data);
	}
	
}