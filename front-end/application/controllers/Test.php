<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {
	
	public function index()
	{
		$data = [
			'title'	    => "Home | ".APP_NAME
			];
		return view('content.home',$data);
	}
	public function ux()
	{
		$data = [
			'title'	    => "UX - Research | ".APP_NAME
			];
		return view('content.ux-research',$data);
	}
	public function ui()
	{
		$data = [
			'title'	    => "UI - Design | ".APP_NAME
			];
		return view('content.ui-design',$data);
	}
	public function dev()
	{
		$data = [
			'title'	    => "Web Design & Development | ".APP_NAME
			];
		return view('content.web-dev',$data);
	}
	public function digital()
	{
		$data = [
			'title'	    => "Digital Marketing | ".APP_NAME
			];
		return view('content.digital-marketing',$data);
	}
	public function work()
	{
		$data = [
			'title'	    => "Cypherlabs Work | ".APP_NAME
			];
		return view('content.cypherlabs-work',$data);
	}
	public function contact()
	{
		$data = [
			'title'	    => "Contact Us | ".APP_NAME
			];
		return view('content.contact-us',$data);
	}
	public function about()
	{
		$data = [
			'title'	    => "About | ".APP_NAME
			];
		return view('content.about',$data);
	}
}