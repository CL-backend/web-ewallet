@extends('main.base_head_foot')
@section('title', $title) 
@section('css') 
<link href="{{APP_ASSETS}}dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div id="registrationPart" style="background: url('{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Member/Registration/real_madrid_football_club_team_cup_award_27762_1920x1080.png') no-repeat;">
   <div class="row divRegis">
      <div class="large-6 column show-for-large" id="registrationleftBannerContainer2">
         <img id="registrationleftBanner2" style="padding-top: 10px;" src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Member/Registration/Banner/en/ambassadors3.png" />
      </div> 
      <div class="small-12 large-6 column">
         <div class="row registerTitle">
          <div class="small-12 column subtitle" onclick="StepToggle('1');">
            <span id="Basic" class="part icon"></span>
            <span class="registerTitle">REGISTER E-WALLET</span>
          </div>
         </div>
         <div class="row" style="padding-top: 8px;">
            <div class="small-12 column">
               <div id="divMsg">
                  <span id="currentMsg"></span>
               </div>
            </div>
         </div>
         <section id="BasicInfoSection" class="inputSection">
            <div class="row">
               <div class="small-12 column bgTrans">
                  <div class="row">
                     <div class="small-12 column subtitle" >
                        <span id="Basic" class="part icon"></span>
                        <span class="registerTitle">Fill Your Personal Account Detail</span>
                        <span id="BasicPointer" class="step icon icon-fun88_arrow_thick_down"></span>
                     </div>
                  </div>
                  <section id="BasicInput">
                     <div class="row" id="rowSurname">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_Label1">Last Name</span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="lastName" type="text" maxlength="50" id="txtSurname" tabindex="1" class="inputfield" data-gatrackfield="LastName" placeholder="Last Name: Sanjaya " />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span id="mandatoryField">*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row" id="rowName">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblName">First name </span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="firstName" type="text" maxlength="50" id="txtName" tabindex="2" class="inputfield" data-gatrackfield="FirstName" placeholder="First Name: Agung " />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn" style="margin-bottom: 10px;">
                        </div>
                     </div>
                     <div class="row" style="margin-bottom: 10px;">
                        <div class="small-11 column" style="color: #000; font-size: 13px; margin-top: -10px;">
                           Please fill in your name in accordance with your bank account name for deposit and withdrawal. <br /> Example: Agung Sanjaya 
                        </div>
                     </div>
                     <div class="row rowDOB" style="margin-top: 10px; margin-bottom: 10px; padding-left: 0 !important; padding-right: 0 !important;">
                        <div class="small-4 column gender" style="height: 34px;">
                           <div class="small-12 column" id="MainContainer">
                              <div id="btnMale" class="genderselected"  data-gatrackfield="Gender" style="float: left; width: 50%; text-align: center; height: 100%; line-height: 33px; border-radius: 3px; cursor: pointer;">
                                 Male
                              </div>
                              <div id="btnFemale" class="genderNonselected" data-gatrackfield="Gender" style="float: left; width: 50%; text-align: center; height: 100%; line-height: 33px; border-radius: 3px; cursor: pointer;">
                                 Female
                              </div>
                           </div>
                        </div>
                        <div class="small-7 column inputTextField dob" id="btnDOB" style="padding-left: 0 !important;">
                           <input name="date" readonly="readonly" id="txtDob" class="white inputfield" data-gatrackfield="DOB" type="text" name="DOB" placeholder="Date of birth " />
                           <span class="inputResult icon"></span>
                           <span class="icon icon-history" style="cursor: pointer; color: #3f97ff; position: absolute; height: 22px; width: 22px; right: 22%; top: 15%; font-size: 20px;"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row" id="rowEmail">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblEmail">Email</span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="email" maxlength="50" id="txtEmail" tabindex="4" class="inputfield" data-gatrackfield="Email" type="text" autocapitalize="none" name="email" placeholder="Email Address" style="font-family:Email;" />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblNationality">Nationality</span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <select class="js-example-basic-single" name="nationality" id="ddlNationality"    >
                              <option value="1">China (People&#39;s Rep)</option>
                              <option value="150">Thailand</option>
                              <option value="169">Vietnam</option>
                              <option selected="selected" value="76">Indonesia</option>
                              <option value="86">Korea (Rep)</option>
                              <option value="83">Japan</option>
                              <option value="3">Albania</option>
                              <option value="4">Algeria</option>
                              <option value="5">Angola</option>
                              <option value="6">Anguilla</option>
                              <option value="7">Argentina</option>
                              <option value="8">Armenia</option>
                              <option value="9">Aruba</option>
                              <option value="10">Australia</option>
                              <option value="11">Austria</option>
                              <option value="12">Azerbaijan</option>
                              <option value="13">Bahamas</option>
                              <option value="14">Bahrain</option>
                              <option value="15">Bangladesh</option>
                              <option value="16">Barbados</option>
                              <option value="17">Belarus</option>
                              <option value="18">Belgium</option>
                              <option value="19">Belize</option>
                              <option value="20">Benin</option>
                              <option value="21">Bermuda</option>
                              <option value="22">Bhutan</option>
                              <option value="23">Bolivia</option>
                              <option value="24">Bosnia and Herzegovina</option>
                              <option value="25">Botswana</option>
                              <option value="26">Brazil</option>
                              <option value="27">Brunei Darussalam</option>
                              <option value="28">Bulgaria</option>
                              <option value="29">Burkina Faso</option>
                              <option value="30">Burundi</option>
                              <option value="31">Cambodia</option>
                              <option value="32">Cameroon</option>
                              <option value="33">Canada</option>
                              <option value="34">Cape Verde</option>
                              <option value="35">Cayman Islands</option>
                              <option value="36">Central African Rep</option>
                              <option value="37">Chad</option>
                              <option value="38">Chile</option>
                              <option value="39">Colombia</option>
                              <option value="40">Congo(Rep)</option>
                              <option value="41">Cook Islands</option>
                              <option value="42">Costa Rica</option>
                              <option value="43">Cote d&#39;Ivoire (Rep)</option>
                              <option value="44">Croatia</option>
                              <option value="45">Cyprus</option>
                              <option value="46">Czech (Rep)</option>
                              <option value="47">Dem Rep of Congo</option>
                              <option value="49">Dominica (Commonwealth of)</option>
                              <option value="50">Dominican Rep.</option>
                              <option value="51">Ecuador</option>
                              <option value="52">Egypt</option>
                              <option value="53">El Salvador</option>
                              <option value="54">Eritrea</option>
                              <option value="55">Estonia</option>
                              <option value="56">Ethiopia</option>
                              <option value="57">Fiji</option>
                              <option value="58">Finland</option>
                              <option value="60">French Polynesia</option>
                              <option value="61">Gabon</option>
                              <option value="62">Georgia</option>
                              <option value="64">Ghana</option>
                              <option value="65">Gibraltar</option>
                              <option value="66">Greece</option>
                              <option value="67">Grenada</option>
                              <option value="68">Guatemala</option>
                              <option value="69">Guinea</option>
                              <option value="70">Guyana</option>
                              <option value="71">Haiti</option>
                              <option value="72">Honduras</option>
                              <option value="73">Hungary</option>
                              <option value="74">Iceland</option>
                              <option value="75">India</option>
                              <option value="82">Jamaica</option>
                              <option value="84">Jordan</option>
                              <option value="85">Kenya</option>
                              <option value="87">Kuwait</option>
                              <option value="89">Latvia</option>
                              <option value="90">Lesotho</option>
                              <option value="91">Liechtenstein</option>
                              <option value="92">Luxembourg</option>
                              <option value="93">Macao</option>
                              <option value="94">Macedonia</option>
                              <option value="95">Madagascar</option>
                              <option value="96">Malawi</option>
                              <option value="97">Maldives</option>
                              <option value="98">Mali</option>
                              <option value="99">Malta</option>
                              <option value="100">Mauritania</option>
                              <option value="101">Mauritius</option>
                              <option value="102">Mexico</option>
                              <option value="103">Moldova</option>
                              <option value="104">Mongolia</option>
                              <option value="105">Morocco</option>
                              <option value="107">Namibia</option>
                              <option value="108">Nauru</option>
                              <option value="109">Nepal</option>
                              <option value="111">New Caledonia</option>
                              <option value="112">New Zealand</option>
                              <option value="113">Niger</option>
                              <option value="114">Nigeria</option>
                              <option value="115">Norway</option>
                              <option value="116">Oman</option>
                              <option value="117">Pakistan</option>
                              <option value="118">Panama</option>
                              <option value="120">Paraguay</option>
                              <option value="121">Peru</option>
                              <option value="122">Poland</option>
                              <option value="123">Portugal</option>
                              <option value="124">Qatar</option>
                              <option value="125">Romania</option>
                              <option value="126">Russian Federation</option>
                              <option value="127">Rwanda</option>
                              <option value="128">Saint Christopher (St. Kitts) and Nevis</option>
                              <option value="129">Saint Lucia</option>
                              <option value="130">Saint Vincent and the Grenadines</option>
                              <option value="131">Sao Tome and Principe</option>
                              <option value="132">Saudi Arabia</option>
                              <option value="133">Senegal</option>
                              <option value="134">Seychelles</option>
                              <option value="135">Sierra Leone</option>
                              <option value="136">Slovakia</option>
                              <option value="137">Slovenia</option>
                              <option value="138">Solomon Islands</option>
                              <option value="139">Somalia</option>
                              <option value="140">South Africa</option>
                              <option value="141">Spain</option>
                              <option value="142">Sri Lanka</option>
                              <option value="143">Sudan</option>
                              <option value="144">Suriname</option>
                              <option value="145">Swaziland</option>
                              <option value="146">Sweden</option>
                              <option value="149">Tanzania</option>
                              <option value="151">Togo</option>
                              <option value="152">Trinidad and Tobago</option>
                              <option value="153">Tunisia</option>
                              <option value="155">Uganda</option>
                              <option value="156">Ukraine</option>
                              <option value="157">United Arab Emirates</option>
                              <option value="159">Uruguay</option>
                              <option value="160">Venezuela</option>
                              <option value="161">Western Samoa</option>
                              <option value="164">Zambia</option>
                              <option value="165">Zimbabwe</option>
                              <option value="172">Isle of Man</option>
                              <option value="173">The Republic of Serbia</option>
                           </select>
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblPlaceofBirth">Place of Birth</span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <select name="place" id="ddlPlaceofBirth" tabindex="17" class="select23">
                              <option value="CN">China (People&#39;s Rep)</option>
                              <option value="TH">Thailand</option>
                              <option value="VN">Vietnam</option>
                              <option selected="selected" value="ID">Indonesia</option>
                              <option value="KR">South Korea</option>
                              <option value="JP">Japan</option>
                              <option value="AF">Afghanistan</option>
                              <option value="AL">Albania</option>
                              <option value="DZ">Algeria</option>
                              <option value="AS">American Samoa</option>
                              <option value="AD">Andorra</option>
                              <option value="AO">Angola</option>
                              <option value="AI">Anguilla</option>
                              <option value="AQ">Antarctica</option>
                              <option value="AG">Antigua and Barbuda</option>
                              <option value="AR">Argentina</option>
                              <option value="AM">Armenia</option>
                              <option value="AW">Aruba</option>
                              <option value="AU">Australia</option>
                              <option value="AT">Austria</option>
                              <option value="AZ">Azerbaijan</option>
                              <option value="BS">Bahamas</option>
                              <option value="BH">Bahrain</option>
                              <option value="BD">Bangladesh</option>
                              <option value="BB">Barbados</option>
                              <option value="BY">Belarus</option>
                              <option value="BE">Belgium</option>
                              <option value="BZ">Belize</option>
                              <option value="BJ">Benin</option>
                              <option value="BM">Bermuda</option>
                              <option value="BT">Bhutan</option>
                              <option value="BO">Bolivia</option>
                              <option value="BA">Bosnia and Herzegovina</option>
                              <option value="BW">Botswana</option>
                              <option value="BV">Bouvet Island</option>
                              <option value="BR">Brazil</option>
                              <option value="BQ">British Antarctic Territory</option>
                              <option value="IO">British Indian Ocean Territory</option>
                              <option value="VG">British Virgin Islands</option>
                              <option value="BN">Brunei</option>
                              <option value="BG">Bulgaria</option>
                              <option value="BF">Burkina Faso</option>
                              <option value="BI">Burundi</option>
                              <option value="KH">Cambodia</option>
                              <option value="CM">Cameroon</option>
                              <option value="CA">Canada</option>
                              <option value="CT">Canton and Enderbury Islands</option>
                              <option value="CV">Cape Verde</option>
                              <option value="KY">Cayman Islands</option>
                              <option value="CF">Central African Republic</option>
                              <option value="TD">Chad</option>
                              <option value="CL">Chile</option>
                              <option value="CX">Christmas Island</option>
                              <option value="CC">Cocos [Keeling] Islands</option>
                              <option value="CO">Colombia</option>
                              <option value="KM">Comoros</option>
                              <option value="CG">Congo - Brazzaville</option>
                              <option value="CD">Congo - Kinshasa</option>
                              <option value="CK">Cook Islands</option>
                              <option value="CR">Costa Rica</option>
                              <option value="HR">Croatia</option>
                              <option value="CU">Cuba</option>
                              <option value="CY">Cyprus</option>
                              <option value="CZ">Czech Republic</option>
                              <option value="CI">C&#244;te d’Ivoire</option>
                              <option value="DK">Denmark</option>
                              <option value="DJ">Djibouti</option>
                              <option value="DM">Dominica</option>
                              <option value="DO">Dominican Republic</option>
                              <option value="NQ">Dronning Maud Land</option>
                              <option value="DD">East Germany</option>
                              <option value="EC">Ecuador</option>
                              <option value="EG">Egypt</option>
                              <option value="SV">El Salvador</option>
                              <option value="GQ">Equatorial Guinea</option>
                              <option value="ER">Eritrea</option>
                              <option value="EE">Estonia</option>
                              <option value="ET">Ethiopia</option>
                              <option value="FK">Falkland Islands</option>
                              <option value="FO">Faroe Islands</option>
                              <option value="FJ">Fiji</option>
                              <option value="FI">Finland</option>
                              <option value="FR">France</option>
                              <option value="GF">French Guiana</option>
                              <option value="PF">French Polynesia</option>
                              <option value="TF">French Southern Territories</option>
                              <option value="FQ">French Southern and Antarctic Territories</option>
                              <option value="GA">Gabon</option>
                              <option value="GM">Gambia</option>
                              <option value="GE">Georgia</option>
                              <option value="DE">Germany</option>
                              <option value="GH">Ghana</option>
                              <option value="GI">Gibraltar</option>
                              <option value="GR">Greece</option>
                              <option value="GL">Greenland</option>
                              <option value="GD">Grenada</option>
                              <option value="GP">Guadeloupe</option>
                              <option value="GU">Guam</option>
                              <option value="GT">Guatemala</option>
                              <option value="GG">Guernsey</option>
                              <option value="GN">Guinea</option>
                              <option value="GW">Guinea-Bissau</option>
                              <option value="GY">Guyana</option>
                              <option value="HT">Haiti</option>
                              <option value="HM">Heard Island and McDonald Islands</option>
                              <option value="HN">Honduras</option>
                              <option value="HK">Hong Kong SAR China</option>
                              <option value="HU">Hungary</option>
                              <option value="IS">Iceland</option>
                              <option value="IN">India</option>
                              <option value="IM">Isle of Man</option>
                              <option value="IL">Israel</option>
                              <option value="IT">Italy</option>
                              <option value="JM">Jamaica</option>
                              <option value="JE">Jersey</option>
                              <option value="JT">Johnston Island</option>
                              <option value="JO">Jordan</option>
                              <option value="KZ">Kazakhstan</option>
                              <option value="KE">Kenya</option>
                              <option value="KI">Kiribati</option>
                              <option value="KW">Kuwait</option>
                              <option value="KG">Kyrgyzstan</option>
                              <option value="LV">Latvia</option>
                              <option value="LB">Lebanon</option>
                              <option value="LS">Lesotho</option>
                              <option value="LR">Liberia</option>
                              <option value="LY">Libya</option>
                              <option value="LI">Liechtenstein</option>
                              <option value="LT">Lithuania</option>
                              <option value="LU">Luxembourg</option>
                              <option value="MO">Macau SAR China</option>
                              <option value="MK">Macedonia</option>
                              <option value="MG">Madagascar</option>
                              <option value="MW">Malawi</option>
                              <option value="MY">Malaysia</option>
                              <option value="MV">Maldives</option>
                              <option value="ML">Mali</option>
                              <option value="MT">Malta</option>
                              <option value="MH">Marshall Islands</option>
                              <option value="MQ">Martinique</option>
                              <option value="MR">Mauritania</option>
                              <option value="MU">Mauritius</option>
                              <option value="YT">Mayotte</option>
                              <option value="FX">Metropolitan France</option>
                              <option value="MX">Mexico</option>
                              <option value="FM">Micronesia</option>
                              <option value="MI">Midway Islands</option>
                              <option value="MD">Moldova</option>
                              <option value="MC">Monaco</option>
                              <option value="MN">Mongolia</option>
                              <option value="ME">Montenegro</option>
                              <option value="MS">Montserrat</option>
                              <option value="MA">Morocco</option>
                              <option value="MZ">Mozambique</option>
                              <option value="MM">Myanmar [Burma]</option>
                              <option value="NA">Namibia</option>
                              <option value="NR">Nauru</option>
                              <option value="NP">Nepal</option>
                              <option value="NL">Netherlands</option>
                              <option value="AN">Netherlands Antilles</option>
                              <option value="NT">Neutral Zone</option>
                              <option value="NC">New Caledonia</option>
                              <option value="NZ">New Zealand</option>
                              <option value="NI">Nicaragua</option>
                              <option value="NE">Niger</option>
                              <option value="NG">Nigeria</option>
                              <option value="NU">Niue</option>
                              <option value="NF">Norfolk Island</option>
                              <option value="KP">North Korea</option>
                              <option value="VD">North Vietnam</option>
                              <option value="MP">Northern Mariana Islands</option>
                              <option value="NO">Norway</option>
                              <option value="OM">Oman</option>
                              <option value="PC">Pacific Islands Trust Territory</option>
                              <option value="PK">Pakistan</option>
                              <option value="PW">Palau</option>
                              <option value="PS">Palestinian Territories</option>
                              <option value="PA">Panama</option>
                              <option value="PZ">Panama Canal Zone</option>
                              <option value="PY">Paraguay</option>
                              <option value="YD">People&#39;s Democratic Republic of Yemen</option>
                              <option value="PE">Peru</option>
                              <option value="PH">Philippines</option>
                              <option value="PN">Pitcairn Islands</option>
                              <option value="PL">Poland</option>
                              <option value="PT">Portugal</option>
                              <option value="PR">Puerto Rico</option>
                              <option value="QA">Qatar</option>
                              <option value="RO">Romania</option>
                              <option value="RU">Russia</option>
                              <option value="RW">Rwanda</option>
                              <option value="RE">R&#233;union</option>
                              <option value="BL">Saint Barth&#233;lemy</option>
                              <option value="SH">Saint Helena</option>
                              <option value="KN">Saint Kitts and Nevis</option>
                              <option value="LC">Saint Lucia</option>
                              <option value="MF">Saint Martin</option>
                              <option value="PM">Saint Pierre and Miquelon</option>
                              <option value="VC">Saint Vincent and the Grenadines</option>
                              <option value="WS">Samoa</option>
                              <option value="SM">San Marino</option>
                              <option value="SA">Saudi Arabia</option>
                              <option value="SN">Senegal</option>
                              <option value="RS">Serbia</option>
                              <option value="CS">Serbia and Montenegro</option>
                              <option value="SC">Seychelles</option>
                              <option value="SL">Sierra Leone</option>
                              <option value="SG">Singapore</option>
                              <option value="SK">Slovakia</option>
                              <option value="SI">Slovenia</option>
                              <option value="SB">Solomon Islands</option>
                              <option value="SO">Somalia</option>
                              <option value="ZA">South Africa</option>
                              <option value="GS">South Georgia and the South Sandwich Islands</option>
                              <option value="ES">Spain</option>
                              <option value="LK">Sri Lanka</option>
                              <option value="SD">Sudan</option>
                              <option value="SR">Suriname</option>
                              <option value="SJ">Svalbard and Jan Mayen</option>
                              <option value="SZ">Swaziland</option>
                              <option value="SE">Sweden</option>
                              <option value="CH">Switzerland</option>
                              <option value="ST">S&#227;o Tom&#233; and Pr&#237;ncipe</option>
                              <option value="TW">Taiwan</option>
                              <option value="TJ">Tajikistan</option>
                              <option value="TZ">Tanzania</option>
                              <option value="TL">Timor-Leste</option>
                              <option value="TG">Togo</option>
                              <option value="TK">Tokelau</option>
                              <option value="TO">Tonga</option>
                              <option value="TT">Trinidad and Tobago</option>
                              <option value="TN">Tunisia</option>
                              <option value="TR">Turkey</option>
                              <option value="TM">Turkmenistan</option>
                              <option value="TC">Turks and Caicos Islands</option>
                              <option value="TV">Tuvalu</option>
                              <option value="UM">U.S. Minor Outlying Islands</option>
                              <option value="PU">U.S. Miscellaneous Pacific Islands</option>
                              <option value="VI">U.S. Virgin Islands</option>
                              <option value="UG">Uganda</option>
                              <option value="UA">Ukraine</option>
                              <option value="SU">Union of Soviet Socialist Republics</option>
                              <option value="AE">United Arab Emirates</option>
                              <option value="GB">United Kingdom</option>
                              <option value="US">United States</option>
                              <option value="ZZ">Unknown or Invalid Region</option>
                              <option value="UY">Uruguay</option>
                              <option value="UZ">Uzbekistan</option>
                              <option value="VU">Vanuatu</option>
                              <option value="VA">Vatican City</option>
                              <option value="VE">Venezuela</option>
                              <option value="WK">Wake Island</option>
                              <option value="WF">Wallis and Futuna</option>
                              <option value="EH">Western Sahara</option>
                              <option value="ZM">Zambia</option>
                              <option value="ZW">Zimbabwe</option>
                              <option value="AX">&#197;land Islands</option>
                           </select>
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <br>
                  </section>
               </div>
            </div>
         </section>
         <section id="AccountInfoSection" class="inputSection">
            <div class="row">
               <div class="small column bgTrans">
                  <div class="row">
                     <div class="small-12 column subtitle" onclick="StepToggle('2');">
                        <span id="Account" class="part icon"></span>
                        <span class="registerTitle">Settings Your Account Security Detail</span>
                        <span id="AccountPointer" class="step icon icon-fun88_arrow_thick_down"></span>
                     </div>
                  </div>
                  <section id="AccountInput">
                     <div class="row">
                        <div class="small-12 column" style="padding-top: 10px; font-weight: bold; padding-bottom: 10px;">
                           <span>WE ARE YOUR SECURITY PERDITION </span>
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblUsername">Username </span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="username" maxlength="14" id="txtRegUsername" tabindex="5" class="inputfield" name="MemberCode" data-gatrackfield="MemberCode" type="text" autocorrect="off" autocapitalize="none" placeholder="Username " />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblPassword">Password</span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="password" type="password" maxlength="20" id="txtRegPassword" tabindex="6" class="inputfield" name="Password" data-gatrackfield="Password" autocomplete="off" placeholder="Password" />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn" style="margin-bottom: 10px;">
                        </div>
                     </div>
                     <div class="row" style="margin-bottom: 10px;">
                        <div class="small-11 column" style="color: #000; font-size: 13px; margin-top: -10px;">
                           Please choose a password that is easy to remember. Must consist of 6-20 characters with a combination of letters & numbers only. 
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-12 column" style="font-weight: bold;">
                           <span id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_lblConfirmPassword" name="ConfirmPassword">Confirm Password </span>&nbsp;:
                        </div>
                        <div class="small-11 column inputTextField">
                           <input name="confirm" type="password" maxlength="20" id="txtConfirmPassword" tabindex="7" class="inputfield" data-gatrackfield="ConfirmPassword" autocomplete="off" placeholder="Confirm password " />
                           <span class="inputResult icon"></span>
                        </div>
                        <div class="small-1 column requiredField">
                           <span>*</span>
                        </div>
                        <div class="small-11 column lastColumn">
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-11 column">
                           <table id="chkRegTable">
                              <tr>
                                 <td>
                                    <input id="chkTnC" type="checkbox" name="checkTerm" />
                                 </td>
                                 <td>By checking this box I accept the <a onclick="window.open(tncUrl);" target='_blank'> Terms and Conditions</a> and confirm that I am over 18 years of age</td>
                              </tr>
                              <tr>
                                 <td>
                                    <input id="chkAcknowledge" type="checkbox" name="checkKnowlage" />
                                 </td>
                                 <td>
                                    I acknowledge I am at least 18 years of age.
                                 </td>
                              </tr>
                           </table>
                        </div>
                     </div>
                     <br>
                     <div class="row hide-for-large">
                        <div class="small-11 column">
                           <table id="reminder">
                              <tr>
                                 <td>* This information is required </td>
                              </tr>
                           </table>
                        </div>
                     </div>
                     <div class="row">
                        <div class="small-6 medium-4 column" style="padding-bottom: 15px;">
                           <a onclick="SetSubmitStatus();" id="btnSubmit" class="submitButton" href="javascript:__doPostBack(&#39;submit&#39;,&#39;&#39;)">Submit</a>
                        </div>
                        <div class="small-6 medium-4 column">
                           <a id="btnCancel" class="cancelButton" href="javascript:__doPostBack(&#39;cancel&#39;,&#39;&#39;)">Cancel</a>
                        </div>
                        <div class="medium-4 column show-for-large">
                           * This information is required 
                        </div>
                     </div>
                  </section>
               </div>
            </div>
         </section>

               </div>
            </div>
         </section>
      </div>
   </div>
</div>
@endsection
@section('javascript')

<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-3.1.1.min.js"></script>
<script src="{{APP_ASSETS}}jquery.main/main.js"></script>
<script src="{{APP_ASSETS}}js.main/main.js"></script>
<script src="{{APP_ASSETS}}bundles/js/require.js"></script>
<script src="{{APP_ASSETS}}bundles/js/module.js"></script>
<script src="{{APP_ASSETS}}bundles/js/base.js"></script>
<script src="{{APP_ASSETS}}bundles/js/home.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/DatePicker/foundation-datepicker.en.js" type="text/javascript"></script>
<!-- <script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.validate.min.js"></script> -->
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.email.autocomplete.js" type="text/javascript"></script>
<script type="text/javascript" src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-ui-i18n.min.js"></script>
@endsection


