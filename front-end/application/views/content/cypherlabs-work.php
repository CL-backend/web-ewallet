@extends('main.base_head_foot')
@section('title', $title)
@section('css') 
<link href="{{APP_ASSETS}}bundles/css/slickCss_v_A99IqCvyYnVRP3m5BoT3BFJ9QqJ8_UrMOpF0GdLCd0g1.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" media="all" href="https://coolcarousels.frebsite.nl/css/layout.css" />
<style type="text/css" media="all">
html, body {
	height: 100%;
	padding: 0;
	margin: 0;
}
body {
	background: #fff;
	min-height: 600px;
}
#wrapper {
	width: 100%;
	min-width: 900px;
	height: 500px;
	position: relative;
	top: 50%;
	left: 0;
}
#carousel div {
	height: 500px;
	float: left;
}
#carousel img {
	min-width: 100%;
	min-height: 100%;
}
#pager {
	border-radius: 20px;
	background: #fff;
	text-align: center;
	width: 120px;
	height: 50px;
	padding-top: 2px;
	margin: 235px 0 0 -60px;
	position: absolute;
	top: 50%;
	left: 50%;
}
#pager a.selected span {
	background: #666;
}
#pager a {
	display: inline-block;
	padding: 5px;
}
#pager span {
	border-radius: 10px;
	background: #ccc;
	text-indent: -100px;
	line-height: 0;
	display: inline-block;
	width: 10px;
	height: 10px;
	overflow: hidden;
}
.center {
	display: block;
	margin-left: auto;
	margin-right: auto;
	padding-top: 20px;
	padding-bottom: 20px;
}

</style>
<!-- <style type="text/css">
#wrapper3 {
	box-shadow: 0 0 20px #110;
	width: 700px;
	height: 350px;
	overflow: hidden;
	position: absolute;

}
#images3 {
	width: 700px;
	height: 350px;
}
#images3 img {
	display: block;
	float: left;
}
#timer,
#captions {
	background: rgba( 0, 0, 0, 0.5 );
	width: 700px;
	height: 50px;
	position: absolute;
	left: 0;
	bottom: 0;
}
#timer {
	background: rgba( 200, 0, 0, 0.5 );
}
#captions .carousel > div {
	color: #fff;
	font-size: 22px;
	font-weight: bold;
	line-height: 52px;
	text-transform: uppercase;
	text-indent: 50px;
	width: 700px;
	height: 50px;
	float: left;
}
.pager {
	padding: 16px 50px 0 0;
	float: right;
}
.pager a {
	border: 1px solid #fff;
	border-radius: 10px;
	display: none;
	width: 10px;
	height: 10px;
	margin: 0 3px;
}
.pager a:hover {
	background: rgba( 255, 255, 255, 0.3 );
}
.pager a.selected {
	background: #fff;
}
.cod .pager .p1,
.cod .pager .p2,
.cod .pager .p3,
.gta .pager .p4,
.gta .pager .p5,
.gta .pager .p6,
.mgs .pager .p7,
.mgs .pager .p8,
.mgs .pager .p9 {
	display: inline-block;
}
</style> -->
<style type="text/css">
#wrapper4 {
	width: 830px;
	height: 500px;
	margin: -250px 0 0 -415px;
	overflow: hidden;
	position: absolute;
	left: 50%;
	top: 50%;
}
#wrapper4 * {
	color: #ccc;
}

#wrapper4 .carousel4 {
	display: block;
	float: left;
	width: 275px;
	height: 250px;
	margin: 0 1px 1px 0;
}
#wrapper4 .carousel4 img {
	display: block;
}
#wrapper4 .carousel4 div {
	background-color: black;
	width: auto;
	height: 250px;
	padding: 0 30px;
}
#wrapper4 .carousel4 div h3 {
	font-size: 20px;
	line-height: 50px;
	height: 50px;
	margin: 0;
	padding: 0;
}
#wrapper4 .carousel4 div p {
	height: 160px;
	margin: 0;
}
#wrapper5 {
	border-top: 1px solid #cdc;
	border-bottom: 1px solid #cdc;
	background-color: #efe;
	width: 100%;
	height: 240px;
	margin: -100px 0 0 0;
	position: absolute;
	top: 50%;
	left: 0;
}
#carousel5 div {
	text-align: center;
	width: 300px;
	height: 220px;
	float: left;
	position: relative;
	padding-top: 50px;
}
#carousel5 div img {
	border: none;
}
#carousel5 div span {
	text-align: center;
	color: #333;
	font-size: 14px;
	font-weight: bold;
	display: block;
	margin-top: -20px;
}
@media(max-width:1023px) {
	#carousel5 div {
		height: auto;
	}
	#carousel5 div img {
		
	}
	#carousel5 div span {
		padding-top: 3px;
		text-align: center;
	}
}
@media(max-width:600px) {
	#carousel5 div {
		height: auto;
	}
	#carousel5 div img {
		
	}
	#carousel5 div span {
		padding-top: 10px;
		font-size: 80%;
		text-align: center;
	}
}
.tab-slider--nav {
	width: 100%;
	float: left;
	margin-bottom: 20px;
}

.tab-slider--tabs {
	display: block;
	float: left;
	margin: 0;
	padding: 0;
	list-style: none;
	position: relative;
	border-radius: 35px;
	overflow: hidden;
	background: #fff;
	height: 35px;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}
.tab-slider--tabs:after {
	content: "";
	width: 50%;
	background: #345F90;
	height: 100%;
	position: absolute;
	top: 0;
	left: 0;
	-webkit-transition: all 250ms ease-in-out;
	transition: all 250ms ease-in-out;
	border-radius: 35px;
}
.tab-slider--tabs.slide:after {
	left: 50%;
}

.tab-slider--trigger {
	font-size: 12px;
	line-height: 1;
	font-weight: bold;
	color: #345F90;
	text-transform: uppercase;
	text-align: center;
	padding: 11px 20px;
	position: relative;
	z-index: 2;
	cursor: pointer;
	display: inline-block;
	-webkit-transition: color 250ms ease-in-out;
	transition: color 250ms ease-in-out;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}
.tab-slider--trigger.active {
	color: #fff;
}

.tab-slider--body {
	margin-bottom: 20px;
}
</style>
<style type="text/css">
.tabs {
	display: flex;
	flex-wrap: wrap;
	max-width: 700px;
	background: #e5e5e5;
	box-shadow: 0 48px 80px -32px rgba(0,0,0,0.3);
}
.input {
	position: absolute;
	opacity: 0;
}
.label {
	width: 100%;
	padding: 20px 30px;
	background: #e5e5e5;
	cursor: pointer;
	font-weight: bold;
	font-size: 18px;
	color: #7f7f7f;
	transition: background 0.1s, color 0.1s;
}

.label:hover {
	background: #d8d8d8;
}

.label:active {
	background: #ccc;
}

.input:focus + .label {
	box-shadow: inset 0px 0px 0px 3px #0747A6;
	z-index: 1;
}

.input:checked + .label {
	background: #fff;
	color: #000;
}

@media (min-width: 600px) {
	.label {
		width: auto;
	}
}
.panel {
	display: none;
	padding: 20px 30px 30px;
	background: #fff;
}

@media (min-width: 600px) {
	.panel {
		order: 99;
	}
}
@media (max-width: 600px) {
	.label.visible2{
		visibility:collapse; ;
	}
}
@media (min-width: 1024px) {
	.label.visible2{
		visibility:collapse; ;
	}
}
@media (min-width: 1620px) {
	.label.visible2{
		visibility: unset; ;
	}
}


.input:checked + .label + .panel {
	display: block;
}
</style>
		<!-- <style type="text/css">
			html, body {
				height: 100%;
				padding: 0;
				margin: 0;
			}
			#wrapper2 {
				width: 900px;
				height: 400px;
				margin: -10px 0 0 -450px;
				padding-top: 80px;
				position: absolute;
				top: 50%;
				left: 50%;
			}
			#carousel2 img {
				display: block;
				float: left;
				border: none;
				position: relative;
			}
		</style> -->
		@endsection
		@section('content')


		<div id="wrapper">
			<div id="carousel">
				<div><img src="{{APP_ASSETS}}images/snow1.jpg" /></div>
				<div><img src="{{APP_ASSETS}}images/snow2.jpg" /></div>
				<div><img src="{{APP_ASSETS}}images/snow3.jpg" /></div>
				<div><img src="{{APP_ASSETS}}images/snow4.jpg" /></div>
			</div>
			<div id="pager"></div>
		</div>

		<!-- <section class="content-Wrapper" style="padding-top: 360px;background-color: black;">
				<div id="wrapper2">
					<div id="carousel2">
						<img src="{{APP_ASSETS}}images/fireworks1.jpg" width="600" height="400" />
						<img src="{{APP_ASSETS}}images/fireworks2.jpg" width="600" height="400" />
						<img src="{{APP_ASSETS}}images/fireworks3.jpg" width="600" height="400" />
						<img src="{{APP_ASSETS}}images/fireworks4.jpg" width="600" height="400" />
						<img src="{{APP_ASSETS}}images/fireworks5.jpg" width="600" height="400" />
					</div>
                    <div class="titleWrapper">
                        <h4 class="preference-content-title en">
                         <span>
                          <p align="center">Games</p>
                      </span>
                  </h4>
              </div>
				</div>
			</section> -->

			<section class="content-Wrapper">
				<div class="row collapse expanded content-wrapper-row">
					<div class="columns large-6 middleLeftContent IntroSteps Intro_MiddleLeftContent">
						<link href="{{APP_ASSETS}}bundles/css/slickCss_v_A99IqCvyYnVRP3m5BoT3BFJ9QqJ8_UrMOpF0GdLCd0g1.css" rel="stylesheet" />
						<div class="topPreferenceWrapper SportsbookMatchControl IntroSteps Intro_SportsbookMatchControl">
							<div class="titleWrapper">
								<h4 class="preference-content-title en">
									<span onclick="window.open('#')" style="cursor: pointer">
										E-WALLET Sports
									</span>
									<a href="#" target="_blank">

									</a>
								</h4>
							</div>
							<div class="banner-Wrapper preferenceSportLiveMatchWrapper" style="max-height:100%;">
								<div class="sub-banner-slider">
									<div>
										<a href="#">
											<img class="preferenceSportLiveMatchWrapper mobileImage" style="max-width: 100%; width: 90%;" src="{{APP_ASSETS}}images/wallpaper2you_141171.jpg" />
											<img class="preferenceSportLiveMatchWrapper desktopImage" style="max-width: 100%; width: 100%;" src="{{APP_ASSETS}}images/wallpaper2you_141171.jpg" />
										</a>
									</div>
								</div>
								<div class="sub-banner-paging">
								</div>
							</div>
						</div>
					</div>
					<div class="columns large-6 middleRightContent IntroSteps Intro_MiddleRightContent">
						<div class="topPreferenceWrapper">
							<div class="titleWrapper game">
								<h4 class="preference-content-title en">PLAY THE COOLEST FREE GAMES
								</h4>
							</div>
							<div class="row expanded collapse home-game-wrapper" id="home-demo-slot-slick">
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','27');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_2360169_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif"  />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Age Of The Gods Roulette
												</p>
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limit,</span>
												<span class="home-game-vendor">£1 - £100</span>

											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','343');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_5829946_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Spin a Win
												</p>
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£0.1 - £5000</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','296');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_6157997_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif"  />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Unlimited Blackjack
												</p>  
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£5 - £1000</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','177');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_6846084_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Dragon's Myth
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£1 - £100</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','1679');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_7120102_newlobby.png' src="{{APP_ASSETS}}/www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Diamond Empire
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£50 - £100</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','461');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_10508504_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Reel Gems
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£0.5 - £100</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- row 2 -->
						<div class="topPreferenceWrapper">
							<div class="titleWrapper game">
								<h4 class="preference-content-title en">

								</h4>
							</div>
							<div class="row expanded collapse home-game-wrapper" id="home-demo-slot-slick">
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','27');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_2360169_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif"  />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Age Of The Gods Roulette
												</p>
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limit,</span>
												<span class="home-game-vendor">£1 - £100</span>

											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','343');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_5829946_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Spin a Win
												</p>
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£0.1 - £5000</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','296');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_6157997_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif"  />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Unlimited Blackjack
												</p>  
											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£5 - £1000</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','177');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_6846084_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Dragon's Myth
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£1 - £100</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','1679');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_7120102_newlobby.png' src="{{APP_ASSETS}}/www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Diamond Empire
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£50 - £100</span>
											</div>
										</div>
									</div>
								</div>
								<div class="column small-6 medium-4 home-game-column end">
									<div class="game-wrapper game-wrapper-home">
										<div class="home-game-image-wrapper" onclick="currentGameType='slot';Games.EnterGame('MGSQF','461');">
											<img class="game-image customlazyload" data-original-custom='{{APP_ASSETS}}images/flash_10508504_newlobby.png' src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/coming_soon.gif" />
											<div class="play-button-wrapper customlazyload">
												<div class="play-button">
													<img class="gamespinner" data-original-custom="{{APP_ASSETS}}images/bg_O_2.png" />
													<img width="79" height="80" class="gameplay" data-original-custom="{{APP_ASSETS}}images/play.png" />
												</div>
											</div>
										</div>
										<div class="home-game-info">
											<div>
												<p class="home-game-Name">
													Reel Gems
												</p>

											</div>
											<div class="home-game-info-type">
												<span class="home-game-category">Bet Limits,</span>
												<span class="home-game-vendor">£0.5 - £100</span>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end row 2 -->
					</div>
				</div>

				<div class="row collapse expanded content-wrapper-row">
					<div class="column large-4">
						<div class="tab-slider--nav">
							<ul class="tab-slider--tabs">
								<li class="tab-slider--trigger active" rel="tab1">SPORTS</li>
								<li class="tab-slider--trigger" rel="tab2">EXCLUSIVE</li>
							</ul>
						</div>
						<div class="tab-slider--container">
							<div id="tab1" class="tab-slider--body">
								<table class="bliss-article-table">
									<tbody>
										<tr>
											<td>
												<table class="bliss-article-Detail-Table">
													<tr>
														<td>
															<div>
																<a href='#' style="color: black" target="_blank">
																	<p class="bliss-article-title">
																		2017-18 EPL FIXTURES: CRYSTAL PALACE COULD PUSH NEWCASTLE INTO THE RELEGATION ZONE
																	</p>
																</a>
																<p class="bliss-article-description">
																	E-WALLET Bliss:Newcastle are only a couple of losses away from the bottom of the table. Can they create some breathing room against the Eagles on Sunday?
																</p>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div>
																<a href='#' style="color: black" target="_blank">
																	<p class="bliss-article-title">
																		2017/18 LA LIGA FIXTURES: REAL'S TROUBLES CONTINUE AHEAD OF BIG VALENCIA FIXTURE
																	</p>
																</a>
																<p class="bliss-article-description">
																	Real Madrid’s fixture against Valencia on the 27th will be a pivotal for the champions. Can they beat the odds to stay in the La Liga race?
																</p>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div>
																<a href='#' style="color: black" target="_blank">
																	<p class="bliss-article-title">
																		2017-18 EPL FIXTURE: CHELSEA CAN STILL SLIP UP AGAINST BRIGHTON
																	</p>
																</a>
																<p class="bliss-article-description">
																	The Blues won their last fixture against the Seagulls, but failed to put the 12th-ranked side to rout. Will they fare better this Saturday?
																</p>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div id="tab2" class="tab-slider--body">
								<table class="bliss-article-table">
									<tbody>
										<tr>
											<td>
												<table class="bliss-article-Detail-Table">
													<tr>
														<td>
															<div>
																<a href='#' style="color: black" target="_blank">
																	<p class="bliss-article-title">
																		2017-18 EPL Fixtures: Chelsea Can Still Slip up against Brighton
																	</p>
																</a>
																<p class="bliss-article-description">
																	E-WALLET:The Blues won their last fixture against the Seagulls, but failed to put the 12th-ranked side to rout. Will they fare better this Saturday?
																</p>
															</div>
														</td>
													</tr>
													<tr>
														<td>
															<div>
																<a href='#' style="color: black" target="_blank">
																	<p class="bliss-article-title">
																		2017-18 EPL Fixtures: Will Newcastle’s Defense Frustrate City Once Again?
																	</p>
																</a>
																<p class="bliss-article-description">
																	E-WALLET：The league leaders struggled to score against the Magpies last December. Will they have an easier win when the two meet again this weekend?
																</p>
															</div>
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="column large-4">
						<div class="bottom-preference-center">
							<div class="topPreferenceWrapper middlePreferenceWrapper">
								<div>
									<div class="tabs">
										<label for="tab-1" class="label visible2">ROADMAP BAKARAT</label>
										<input name="tabs" type="radio" id="tab-1" checked="checked" class="input"/>
										<label for="tab-1" class="label">1</label>
										<div class="panel">
											<div class="home-baccarat-roadmap-wrapper">
												<div class="home-roadmap-wrapper">
													<table class="roadMapTable">
														<tbody>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="home-baccarat-play-button-wrapper"><span class="home-baccarat-play-button" onclick="Games.EnterGame('GDL','0')">Play</span></div>
											</div>

										</div>

										<input name="tabs" type="radio" id="tab-2" class="input"/>
										<label for="tab-2" class="label">2</label>
										<div class="panel">
											<div class="home-baccarat-roadmap-wrapper">
												<div class="home-roadmap-wrapper">
													<table class="roadMapTable">
														<tbody>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/30.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="home-baccarat-play-button-wrapper"><span class="home-baccarat-play-button" onclick="Games.EnterGame('GDL','0')">Play</span></div>
											</div>
										</div>

										<input name="tabs" type="radio" id="tab-3" class="input"/>
										<label for="tab-3" class="label">3</label>
										<div class="panel">
											<div class="home-baccarat-roadmap-wrapper">
												<div class="home-roadmap-wrapper">

													<table class="roadMapTable">
														<tbody>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/20.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
															<tr>
																<td>
																	<div><img class="roadmap-badge lazy" src="{{APP_ASSETS}}images/10.png"></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
																<td>
																	<div></div>
																</td>
															</tr>
														</tbody>
													</table>
												</div>
												<div class="home-baccarat-play-button-wrapper"><span class="home-baccarat-play-button" onclick="Games.EnterGame('GDL','0')">Play</span></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="column large-4">
						<div class="bottom-preference-right">
							<div class="topPreferenceWrapper middlePreferenceWrapper">
								<div class="titleWrapper">
									<h4 class="preference-content-title en">
										<span onclick="window.open('#')" style="cursor:pointer">
											PROMOTION
										</span>
									</h4>
								</div>
								<div class="promotion-tab-wrapper">
									<table id="promotionTable" class="promotion-table">
										<tbody>
											<tr>

												<td class="promotion-tab-title-wrapper" data-status="Grouping" data-id="117890">
													<div><span data-url="#" class="promoTitle tncUrl">30% 100% &amp; 150% BONUS SIGN UP LIVE CASINO ANYTIME WD</span></div>
												</td>
												<td><span class="specialPromoBonusId" data-bonus="117890" data-bonus-type="Bonus" data-status="Grouping" data-id="13987"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenAndNavigateToBonus(13987); return false;">Apply Now</span></span>
												</td>
											</tr>
											<tr>
												<td class="promotion-tab-title-wrapper" data-status="Grouping" data-id="117889">
													<div><span data-url="#" class="promoTitle tncUrl">100% LIFE INSURANCE NEW MEMBER</span></div>
												</td>
												<td><span class="specialPromoBonusId" data-bonus="117889" data-bonus-type="Bonus" data-status="Grouping" data-id="13986"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenAndNavigateToBonus(13986); return false;">Apply Now</span></span>
												</td>
											</tr>
											<tr>

												<td class="promotion-tab-title-wrapper" data-status="Grouping" data-id="117887">
													<div><span data-url="#" class="promoTitle tncUrl">100% &amp; 150% SIGN UP SPORTSBOOK ONEWORKS ANYTIME WD</span></div>
												</td>
												<td><span class="specialPromoBonusId" data-bonus="117887" data-bonus-type="Bonus" data-status="Grouping" data-id="13985"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenAndNavigateToBonus(13985); return false;">Apply Now</span></span>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</section>
				<section class="footer-section-wrapper">
					<div id="carousel5">
						<div>
							<img src="{{APP_ASSETS}}images/BCA.png"  width="100%" height="auto" />
							<span>BCA</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BNI.png"  width="100%" height="auto" />
							<span>BNI</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BRI.png"  width="100%" height="auto" />
							<span>BRI</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/Mandiri.png"  width="100%" height="auto" />
							<span>Mandiri</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/CIMB.png"  width="100%" height="auto" />
							<span>CIMB</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BCA.png"  width="100%" height="auto" />
							<span>BCA</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BNI.png" width="100%" height="auto" />
							<span>BNI</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BRI.png" width="100%" height="auto" />
							<span>BRI</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/Mandiri.png"  width="100%" height="auto" />
							<span>Mandiri</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/CIMB.png"  width="100%" height="auto" />
							<span>CIMB</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BCA.png"  width="100%" height="auto" />
							<span>BCA</span>
						</div>
						<div>
							<img src="{{APP_ASSETS}}images/BNI.png"  width="100%" height="auto" />
							<span>BNI</span>
						</div>
					</div>	
				</section>
				
				@endsection

				@section('javascript')
				<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-3.1.1.min.js"></script>
				<script src="{{APP_ASSETS}}jquery.main/main.js"></script>
				<script src="{{APP_ASSETS}}js.main/main.js"></script>
				<script src="{{APP_ASSETS}}bundles/js/require.js"></script>
				<script src="{{APP_ASSETS}}bundles/js/module.js"></script>
				<script src="{{APP_ASSETS}}bundles/js/base.js"></script>
				<script src="{{APP_ASSETS}}bundles/js/home.js"></script>
				<script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>
				<script>
					$('.sub-banner-slider').slick({
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: 4000,
						dots: false,
						arrows: false
					});
					$('.home-game-wrapper').slick({
						slidesToShow: 3,
						slidesToScroll: 1,
						autoplay: true,
						autoplaySpeed: 2000,
						arrows: false
					});
					$('.slider2').slick({
						infinite: true,
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					});


				</script>
				<script type="text/javascript" src="https://coolcarousels.frebsite.nl/js/jquery.carouFredSel.js"></script>
				<!-- <script type="text/javascript" src="https://coolcarousels.frebsite.nl/js/jquery.js"></script> -->
				<script type="text/javascript">
					$(function() {
						$('#carousel').carouFredSel({
							responsive: true,
							items: {
								visible: 1,
								width: 900,
								height: 500
							},
							scroll: {
								duration: 250,
								timeoutDuration: 2500,
								fx: 'uncover-fade'
							},
							pagination: '#pager'
						});
					});
	// $(function() {
	// 			var _center = {
	// 				width: 600,
	// 				height: 400,
	// 				marginLeft: 0,
	// 				marginTop: 0,
	// 				marginRight: 0
	// 			};
	// 			var _left = {
	// 				width: 300,
	// 				height: 200,
	// 				marginLeft: 0,
	// 				marginTop: 150,
	// 				marginRight: -150
	// 			};
	// 			var _right = {
	// 				width: 300,
	// 				height: 200,
	// 				marginLeft: -150,
	// 				marginTop: 150,
	// 				marginRight: 0
	// 			};
	// 			var _outLeft = {
	// 				width: 150,
	// 				height: 100,
	// 				marginLeft: 150,
	// 				marginTop: 200,
	// 				marginRight: -200
	// 			};
	// 			var _outRight = {
	// 				width: 150,
	// 				height: 100,
	// 				marginLeft: -200,
	// 				marginTop: 200,
	// 				marginRight: 50
	// 			};
	// 			$('#carousel2').carouFredSel({
	// 				width: 900,
	// 				height: 400,
	// 				align: false,
	// 				items: {
	// 					visible: 3,
	// 					width: 100
	// 				},
	// 				scroll: {
	// 					items: 1,
	// 					duration: 400,
	// 					onBefore: function( data ) {
	// 						data.items.old.eq( 0 ).animate(_outLeft);
	// 						data.items.visible.eq( 0 ).animate(_left);
	// 						data.items.visible.eq( 1 ).animate(_center);
	// 						data.items.visible.eq( 2 ).animate(_right).css({ zIndex: 1 });
	// 						data.items.visible.eq( 2 ).next().css(_outRight).css({ zIndex: 0 });

	// 						setTimeout(function() {
	// 							data.items.old.eq( 0 ).css({ zIndex: 1 });
	// 							data.items.visible.eq( 0 ).css({ zIndex: 2 });
	// 							data.items.visible.eq( 1 ).css({ zIndex: 3 });
	// 							data.items.visible.eq( 2 ).css({ zIndex: 2 });
	// 						}, 200);
	// 					}
	// 				}
	// 			});
	// 			$('#carousel2').children().eq( 0 ).css(_left).css({ zIndex: 2 });
	// 			$('#carousel2').children().eq( 1 ).css(_center).css({ zIndex: 3 });
	// 			$('#carousel2').children().eq( 2 ).css(_right).css({ zIndex: 2 });
	// 			$('#carousel2').children().eq( 3 ).css(_outRight).css({ zIndex: 1 });
	// 		});
</script>
<script type="text/javascript">
	$(function() {
		var $imgs = $('#images3 .carousel'),
		$capt = $('#captions .carousel'),
		$timr = $('#timer');

		$imgs.carouFredSel({
			circular: false,
			scroll: {
				easing: 'quadratic',
				duration: 2,
				timeoutDuration: 3000,
				onBefore: function( data ) {
					$capt.trigger( 'slideTo', [ '.' + data.items.visible.first().attr( 'alt' ) ] );
					$timr.stop().animate({
						opacity: 0
					}, data.scroll.duration);
				},
				onAfter: function() {
					$timr.stop().animate({
						opacity: 1
					}, 150);
				}
			},
			auto: {
				progress: '#timer'
			},
			pagination: {
				container: '.pager',
				anchorBuilder: function( i ) {
					return '<a class="p' + i + '" href="#"></a>';
				}
			}
		});
		$capt.carouFredSel({
			circular: false,
			auto: false,
			scroll: {
				easing: 'quadratic',
				duration: 2
			}
		});
	});
</script>
<script type="text/javascript">
	$(function() {
		$('.carousel4').each(function() {
			var $cfs = $(this);
			$cfs.carouFredSel({
				direction: 'up',
				circular: false,
				infinite: false,
				align: false,
				width: 275,
				height: 250,
				items: 1,
				auto: false,
				scroll: {
					queue: 'last'
				}
			});
			$cfs.hover(
				function() {
					$cfs.trigger('next');
				},
				function() {
					$cfs.trigger('prev');
				}
				);
		});
	});
	$(function() {

		$('#carousel5').carouFredSel({
			responsive: true,
			scroll: 1,
			items: {
				width: 300,
				visible: {
					min: 3,
					max: 10
				}
			}
		});

	});
	$("document").ready(function(){
		$(".tab-slider--body").hide();
		$(".tab-slider--body:first").show();
	});

	$(".tab-slider--nav li").click(function() {
		$(".tab-slider--body").hide();
		var activeTab = $(this).attr("rel");
		$("#"+activeTab).fadeIn();
		if($(this).attr("rel") == "tab2"){
			$('.tab-slider--tabs').addClass('slide');
		}else{
			$('.tab-slider--tabs').removeClass('slide');
		}
		$(".tab-slider--nav li").removeClass("active");
		$(this).addClass("active");
	});
</script>
@endsection