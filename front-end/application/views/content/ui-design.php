@extends('main.base_head_foot')
@section('title', $title)
@section('css') 

<link href="{{APP_ASSETS}}bundles/css/payment.css" rel="stylesheet"/>


@endsection
@section('content')
<div class="content-Wrapper">
   <div class="bank_top_panel">
      <input type="hidden" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$totalBalance" id="totalBalance" />
      <div class="bank_top_title">
         <h3><b>Bank Saya</b></h3>
      </div>
      <div class="bank_top_summary">
         <h6>Ringkasan Akun</h6>
      </div>
      <div class="ui_seperator"></div>
      <div class="row modify-row-width balance_panel" style="margin: 15px 0;">
         <div class="columns large-3 account_summary_div" data-equalizer>
            <div class="account_summary_outer">
               <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                  <span style="margin-bottom: 0"><b class="balance_type">TOTAL SALDO</b></span>
               </div>
               <div class="columns large-12 small-7" data-equalizer-watch>
                  <div class="walletAmountPlaceHolder TotalBalanceAmount">
                     <b id="totalBalanceAmt" class="l_font">3066.90</b>
                  </div>
               </div>
            </div>
         </div>
         <div class="columns large-3 account_summary_div" data-equalizer>
            <div class="account_summary_outer">
               <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                  <span style="margin-bottom: 0"><b class="balance_type">AKUN UTAMA</b></span>
               </div>
               <div class="columns large-12 small-7" data-equalizer-watch>
                  <div class="walletAmountPlaceHolder TotalWalletAmount">
                     <b class="m_font" id="mainAccountBalanceAmt">1066.90</b>
                  </div>
               </div>
            </div>
         </div>
         <div class="columns large-6" style="padding: 0;" data-equalizer>
            <div style="display: block">
               <div class="account_summary_wallet large-6">
                  <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                     <span style="margin-bottom: 0"><b class="balance_type">Dompet Pilihan: <span class="preferColorBar" style="vertical-align:text-bottom;margin:0 5px;"></span>AKUN UTAMA</b></span>
                  </div>
                  <div class="columns large-12 small-7" data-equalizer-watch>
                     <div class="walletAmountPlaceHolder TotalPreferAmount">
                        <b class="m_font" id="mainWalletBalanceAmt">500.00</b>
                     </div>
                  </div>
               </div>
               <div class="account_summary_deposit large-5">
                  <a href="#" class="depositSelfExlusion">
                  <span>
                  <b style="font-size: 16px;color: #000;">Deposit</b>
                  </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="ui_seperator"></div>
   <div id="financeTypeSelection" class="row modify-row-width large-up-6 medium-up-6 small-up-6" style="text-align: center;" data-equalizer>
      <a href="#" id="wallet" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-my-account bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Akun Saya</b>
            </span>
         </div>
      </a>
      <a href="#" id="deposit" class="depositSelfExlusion columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_deposit bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Deposit</b>
            </span>
         </div>
      </a>
      <a href="#" id="transfer" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_transfer bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Transfer</b>
            </span>
         </div>
      </a>
      <a href="#" id="withdrawal" class="columns column-block financeType withdrawalSelfExlusion" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_withdrawal bank-icon withdrawalSelfExlusion" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Penarikan</b>
            </span>
         </div>
      </a>
      <a href="#" id="bankdetail" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Perincian Bank</b>
            </span>
         </div>
      </a>
      <a href="#" id="history" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-history bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Riwayat</b>
            </span>
         </div>
      </a>
   </div>
   <div id="paymentMethodSlider" class="row modify-row-width" style="border-bottom:1px solid #dcdcdc;margin-top:40px;text-align:left;">

      <a href="#" style="display:inline-block;text-align:center" onclick="CreateCookie();">
       <div class="withdrawal-methods" data-page="localbankwithdrawal" data-index ="0">
         <span><b>Local Bank</b></span>
      </div>
   </a>

</div>
<div class="Rules">
  <span id="cdcRules_1">Sekali penarikan <span class="min-max">Minimum：Rp 100.00，Maksimum：Rp 50,000.00 per transaksi.</span></span><br />
</div>
<div class="row payment-Content">
  <div class="column large-6">
   <div class="large-12 columns form-wrapper">
<div class="large-12 row">
  <div class="column large-3 medium-4 form-field">
   <span></span>
</div>
<!-- <div class="column large-9 medium-8 form-field">
   <span id="preferOld">Akun Pilihan</span>
</div> -->
</div>
<div class="column large-3 medium-4 form-field">
      <span><b>Jumlah Penarikan :</b></span>
   </div>
   <div class="column large-9 medium-8 form-field">
      <div class="input-field">
       <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$txtAmount" type="number" id="txtAmount" class="resetRequired" onblur="PaymentCommonFunction.TrimSpace();" oncopy="return false;" onpaste="return false" />
       <span class="amount_IDR" style="display: none;">,000</span>
       <span class="mandatory">*</span>
    </div>
    <label id="txtAmount-error" class="error diverror amount_Error_IDR" for="txtAmount"></label>
 </div>
<div id="newAccDetailsSegment">
   <div class="column large-3 medium-4 form-field">
    <span><b>Nama Bank :</b></span>
 </div>
 <div class="column large-9 medium-8 form-field">
    <div class="input-field">
     <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$DDLBankName" id="DDLBankName">
      <option selected="selected" value="Mandiri">Mandiri</option>
      <option value="Bank Central Asia(BCA)">Bank Central Asia(BCA)</option>
      <option value="Bank Rakyat Indonesia(BRI)">Bank Rakyat Indonesia(BRI)</option>
      <option value="Bank Negara Indonesia(BNI)">Bank Negara Indonesia(BNI)</option>
      <option value="0">Bank lain</option>
   </select>
   <span class="mandatory">*</span>
</div>
</div>
<div class="large-12 column" id="otherBankField" style="display: none">
 <div class="column large-3 medium-4 form-field">
  <span></span>
</div>
<div class="column large-9 medium-8 form-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$otherBankName" type="text" id="otherBankName" class="resetRequired" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <label id="otherBankName-error" class="error diverror" for="otherBankName"></label>
</div>
</div>
<div class="column large-3 medium-4 form-field end">
 <span><b>Nama Lengkap Akun Bank :</b></span>
</div>
<div class="column large-9 medium-8 form-field">
 <div class="input-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newAccHolderName" type="text" id="newAccHolderName" class="resetRequired" maxlength="50" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <span class="mandatory">*</span>
</div>
<label id="newAccHolderName-error" class="error diverror" for="newAccHolderName"></label>
</div>
<div class="column large-3 medium-4 form-field">
 <span><b>Nomor Bank Akun :</b></span>
</div>
<div class="column large-9 medium-8 form-field">
 <div class="input-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newBankAccNumber" type="text" id="newBankAccNumber" class="resetRequired" maxlength="20" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <span class="mandatory">*</span>
</div>
<label id="newBankAccNumber-error" class="error diverror" for="newBankAccNumber"></label>
</div>
<div class="column large-3 medium-4 form-field">
 <span><b>Propinsi :</b></span>
</div>
<div class="column large-9 medium-8 form-field">
 <div class="input-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newBankProvince" type="text" id="newBankProvince" class="resetRequired" maxlength="20" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newSpecialBankProvince" type="text" id="newSpecialBankProvince" class="resetRequired" style="display: none" placeholder="请用中文输入所需信息" maxlength="20" />
  <span class="mandatory">*</span>
</div>
<label id="newBankProvince-error" class="error diverror" for="newBankProvince"></label>
<label id="newSpecialBankProvince-error" class="error diverror" for="newSpecialBankProvince"></label>
</div>
<div class="column large-3 medium-4 form-field">
 <span><b>Kota :</b></span>
</div>
<div class="column large-9 medium-8 form-field">
 <div class="input-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newBankCity" type="text" id="newBankCity" class="resetRequired" maxlength="20" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newSpecialBankCity" type="text" id="newSpecialBankCity" class="resetRequired" style="display: none" placeholder="请用中文输入所需信息" maxlength="20" />
  <span class="mandatory">*</span>
</div>
<label id="newBankCity-error" class="error diverror" for="newBankCity"></label>
<label id="newSpecialBankCity-error" class="error diverror" for="newSpecialBankCity"></label>
</div>
<div class="column large-3 medium-4 form-field">
 <span><b>Cabang :</b></span>
</div>
<div class="column large-9 medium-8 form-field">
 <div class="input-field">
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newBankBranch" type="text" id="newBankBranch" class="resetRequired" maxlength="50" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
  <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$newSpecialBankBranch" type="text" id="newSpecialBankBranch" class="resetRequired" style="display: none" placeholder="请用中文输入所需信息" maxlength="50" />
  <span class="mandatory">*</span>
</div>
<label id="newBankBranch-error" class="error diverror" for="newBankBranch"></label>
<label id="newSpecialBankBranch-error" class="error diverror" for="newSpecialBankBranch"></label>
</div>
<div class="column large-3 medium-4 form-field">
 <span></span>
</div>
<div class="column large-9 medium-8">
 <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$chkRmb" type="checkbox" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderWithdrawal_chkRmb" cientidmode="static" checked="checked" />
 <label for="chkRmb">Ingat perincian bank saya</label>
</div>
</div>
</div>

<div class="large-12 column btnGroup submitGroup">
  <input type="submit" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$submit" value="Ajukan" onclick="#" id="submit" class="BtnSubmit button-blue" />
  <input type="submit" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderWithdrawal$cancel" value="Batal" onclick="#" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderWithdrawal_cancel" class="BtnCancel button-White" />
</div>

</div>
<div class="large-6 columns">
</div>
</div>


</div>
   
@endsection
@section('javascript')
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-3.1.1.min.js"></script>
<script src="{{APP_ASSETS}}jquery.main/main.js"></script>
<script src="{{APP_ASSETS}}js.main/main.js"></script>
<script src="{{APP_ASSETS}}bundles/js/require.js"></script>
<script src="{{APP_ASSETS}}bundles/js/module.js"></script>
<script src="{{APP_ASSETS}}bundles/js/base.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/DatePicker/foundation-datepicker.min.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/wallets.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/payment.js" type="text/javascript"></script> 
<script src="{{APP_ASSETS}}Assets/Scripts/ext/iCheck.js" type="text/javascript" ></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>    
<script src="{{APP_ASSETS}}bundles/js/validation.js"></script>
<script type="text/javascript">
   var form = $("#baseForm");
   var txtAmount = $("#txtAmount");
   var txtRefNumber = $("#txtRefNumber");
   var txtOldAccountNumberLastSix = $("#txt_OldAccountNumber");
   var txtLastDigit = $("#txtLastDigit");
   var txtAccountHolderName = $("#txtAccountHolderName");
   var txtNewAccountHolderName = $("#txtNewAccountHolderName");
   var txtCNYAccountHolderName = $("#txtCNYAccountHolderName");
   var txtAccountHolderName = $("#txtAccountHolderName");
   var otherBankName = $("#otherBankName");
   var txtNewAccountNumber = $("#txtNewAccountNumber");
   var txtYeePayCardNumber = $("#txtCardNumber");
   var txtYeePayPassword = $("#txtPassword");
   var txtNewAccountNumber = $("#txtNewAccountNumber");
   var txtName = $("#txtName");
   var txtCardNumber = $("#txtCardNumber");
   var txtCVV = $("#txtCVV");
   var txtAstroNumber = $("#txtAstroNumber");
   var txtAstroSafePin = $("#txtAstroSafePin");
   var txtBitCardNumber = $("#txtCardSerialNumber");
   var txtBitPassword = $("#txtPassword");
   var txtCashCardNumber = $("#txtCashCardNumber");
   var txtCashCardPW = $("#txtCashCardPW");
   var txtIBanqUserID = $("#txtUserID");
   var txtIBanqPassword = $("#txtPassword");
   var txtMBEmail = $("#txtEmail");
   var minAmount = "50";
   var maxAmount = "50000";
   var fileName = $('#fileName');
   var hoursField = $('#ddlHours');
   var minField = $('#ddlMinutes');
   var secondField = $('#ddlSeconds');
       //var txtCaptcha = $("#txtCaptcha");
       var selectDepositTargetWallet = $("#ddlTargetWallet");
   
       $.validator.addMethod("noWhiteSpace", function (value) {
           return value.indexOf(" ") === -1;
       });
   
       $.validator.addMethod("regex", function (value, element, regexp) {
           return new RegExp(regexp).test(value);
       });
   
       $.validator.addMethod("isNotZeroAmount", function (value) {
           value = parseFloat(value);
           return !isNaN(value) && value > 0;
       });
   
       $.validator.addMethod("maxTrasnfer", function (value) {
           value = parseFloat(value);
           return value >= 0.01 && value <= 1000000;
       });
   
       $.validator.addMethod("CheckRequired", function (value) {
           if ("False".toLowerCase() == "true") {
               return true;
           } else {
               return (value.length > 0);
           }
       });
   
       $.validator.addMethod("CheckRegex", function (value, element, regexp) {
           if ("IDR" == "VND") {
               return true;
           } else {
               return new RegExp(regexp).test(value);
           }
       });
   
       $.validator.addMethod("CheckSpecialRegex", function (value, element, regexp) {
           if($("#newOld").val() != "0"){
               return true;
           }else{
               return new RegExp(regexp).test(value);
           }
       });
   
       $.validator.addMethod("MaxLengthInput", function (value, element, maxlength) {
           return value.length <= maxlength;
       });
   
       $.validator.addMethod("AWcnRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{16,20}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("AWccRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{3,5}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("APcnRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{16}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("APccRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{4}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod('email', function (value, element) {
           return this.optional(element) || /^\w+[-.`~!@$%^*()={}|?\w]+@([\w.]{2,})+?\.[a-zA-Z]{2,9}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod('checkTrim', function(value, element){
           return value.trim().length > 0;
       });
   
       $.validator.addMethod("checkMin", function (value, element) {
           return parseFloat(value) >= parseFloat(minAmount);
       }, "Jumlah harus sama atau di atas rentang minimum 50.");
   
       $.validator.addMethod("checkMax", function (value, element) {
           return parseFloat(value) <= parseFloat(maxAmount);
       }, "Batas Maksimal Rp. 50000 per transaksi");
   
       jQuery.validator.addMethod("LocalBankSixDigit", function(value, element, param) {
           var val = $(param).val();
           if (/^(\d{6})?$/i.test(val)) return true;
           else return false;
       }, 'Old account number shall not be less than 6 digit.');
   
       jQuery.validator.addMethod("Zerodecimal", function(value, element, param) {
           var val = $(param).val();
           if (val.indexOf('.') != -1 && val.length > 2) {
               if (/^\d+\.{0,1}[0]{1,2}$/.test(val)) return true;
               else return false;
           } else {
               return true;
           }
       }, 'Jumlah dalam titik desimal diperbolehkan dengan 00 saja.');
   
       $.validator.setDefaults({
           errorClass: "error",
           ignore: ":hidden",
           onsubmit: true,
           onkeyup: function (element) { this.element(element); },
           onfocusout: function (element) { this.element(element); },
           showErrors: function () { this.defaultShowErrors(); },
           errorPlacement: function (error, element) { error.insertAfter($(element)); }
       });
   
       function isVNDSpecialBank() {
           var currCode = "IDR";
   
           if (currCode == "VND") {
               var strVal = $("input[name='depositMethod']:checked").next().text();
               if (strVal == "Ngân hàng Ngoại Thương" || strVal == "Ngân hàng Kỹ Thương Việt Nam" || strVal == "Vietcom Bank" || strVal == "Techcom Bank" || strVal == "Ngân hàng Sài Gòn Thương Tín" || strVal == "Sacom Bank" || strVal == "Ngân hàng Ngoại Thương (Vietcombank)" || strVal == "Ngân hàng Kỹ Thương Việt Nam (Techcombank)" || strVal == "Ngân hàng Sài Gòn Thương Tín (Sacombank)" || strVal == "Nếu bạn không tìm thấy tài khoản ngân hàng sử dụng để gửi tiền, vui lòng điền vào 6 chữ số cuối của số tài khoản." || strVal == "If you cannot find the bank account you used for the deposit, please fill in the last 6 digits of the bank account number.") {
                   return true;
               }
           }
           if (currCode == "CNY" || currCode == "RMB" || currCode == "IDR") {
               return true;
           }
           return false;
       };
   
       var formValidator = form.validate();
</script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/clipboard.min.js"></script>
<script>
   $(document).ready(function () {
       var clipboard = new Clipboard('.btn');
       var alertMessage = " ".trim();
       var depositStatus = "";
       var memCurr = "IDR";
       var overload = "False";
       var Step3To2 = 'Langkah 2. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       var Step3To1 = 'Langkah 1. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       var originalChangeStepMessage = 'Langkah 3. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       $("#steps").val(0);
       if (overload.toLowerCase() == "true") {
           Page.message("Pilihan deposit yang telah anda pilih saat ini akan memerlukan waktu proses sekitar 60 menit. Apakah anda ingin mencoba metode deposit lainnya?");
       }
   
       if ($("input[name='depositMethod']").length < 1) {
           $('.deposit-step-title').text(Step3To1);
       }
   
       if (alertMessage != null && alertMessage != "") {
           Page.message(alertMessage);
       }
   
       if (depositStatus != null && depositStatus != "") {
           setInterval(function () {
               GenerateCountDown();
           }, 1000);
           $('#after-Deposit').css("display", "block");
           $('#before-Deposit').css("display", "none");
           ga('send', 'event', 'Deposit_IDR' , 'Localbank', 'Transaction', '');
       } else {
           $('#after-Deposit').css("display", "none");
           $('#before-Deposit').css("display", "block");
       }
   
       
   
   
       $('#ddlHours, #ddlMinutes, #ddlSeconds').change(function () {
           if ($('#ddlHours').val() === '' || $('#ddlMinutes').val() === '' || $('#ddlSeconds').val() === '')
               $('.timeErrorMessage').show();
           else
               $('.timeErrorMessage').hide();
       });
   
       $("input[name='depositMethod']").change(function () {
           var flag = true;
           //$("#SelectedMerchant").val($("input[name='depositMethod']:checked").next().text().trim());
           $("#SelectedMerchant").val($("input[name='depositMethod']:checked").next().attr('data-bankName'));
           if ($("input[name='depositMethod']:checked").val() == "9999") {
               $('#stepTwo').css("display", "none");
               $('#VNDChangeStep b').text(Step3To2);
               $(".hide_LastFive_VND").show();
               $(".hide_Ref_VND").show();
               $(".hide_LastFive_THB").show();
               flag = false;
           } else {
               $('#stepTwo').css("display", "block");
               $('#VNDChangeStep b').text(originalChangeStepMessage);
               $(".hide_LastFive_VND").hide();
               $(".hide_LastFive_THB").hide();
           }
           if ($('#GenerateDetails').parent().is(":hidden")) {
               if (flag) {
                   $('.account-details').toggle("slow");
                   $('#GenerateDetails').parent().show();
               }
           }
           if ($('#ConfirmDetails').parent().is(":hidden")) {
               $('#ConfirmDetails').parent().show();
               $('.transactionDetails').toggle("slow");
           }
           $('#stepThreeTick').removeClass("icon icon-tick-circle-button");
           $("#steps").val(0);
           $('#stepOneTick').addClass("icon icon-tick-circle-button");
       });
   
       // to determine if the user is using Internet Explorer (IE)
       function isIE() {
           ua = navigator.userAgent;
           /* MSIE used to detect old browsers and Trident used to newer ones*/
           var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
   
           return is_ie;
       }
   
       $("#fileAttachment").change(function (e) {
           $('#fileAttachment-error').css("display", "none");
           $('#fileAttachment-error').html("");
           if (e.target.files) {
               var files = e.originalEvent.target.files;
               var n = files[0].name,
               s = files[0].size,
               t = files[0].type;
   
               if (t != "image/jpeg" && t != "image/gif" && t != "image/bmp" && t != "image/png" && t != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && t != "application/msword" && t != "application/pdf") {
                   $('#fileAttachment-error').html('Hanya mengijinkan gambar, dokumen dan format pdf dalam slip bank anda.');
                   $('#fileAttachment-error').css("display", "block");
                   // if the user is using Internet Explorer (IE)
                   if (isIE()) {
                       $('#fileAttachment').after($(this).clone(true)).remove();
                   } else {
                       $('#fileAttachment').val('');
                   }
                   
                   $('#fileName').val('');
                   return;
               }
               else if (s > 2097152) {
                   $('#fileAttachment-error').html('File tidak dapat lebih besar dari 2MB');
                   $('#fileAttachment-error').css("display", "block");
                   // if the user is using Internet Explorer (IE)
                   if (isIE()) {
                       $('#fileAttachment').after($(this).clone(true)).remove();
                   } else {
                       $('#fileAttachment').val('');
                   }
                   $('#fileName').val('');
                   return;
               }
           }
               //<= IE9
           else {
               var files = e.target.value;
               var n = files.replace(/^.*[\\\/]/, '')
               t = files.substr((files.lastIndexOf('.') + 1));
   
               if (t != "jpg" && t != "jpeg" && t != "gif" && t != "bmp" && t != "png" && t != "doc" && t != "docx" && t != "pdf") {
                   $('#fileAttachment-error').html('Hanya mengijinkan gambar, dokumen dan format pdf dalam slip bank anda.');
                   $('#fileAttachment-error').css("display", "block");
                   $('#fileAttachment').val('');
                   $('#fileName').val('');
                   return;
               }
           }
           $('#fileName').val($('#fileAttachment')[0].files[0].name);
       });
   
       Payment.LocalBank();
       Payment.LocalBankResubmit();
       if ("id" == "zh") {
           $('#txtAccountHolderName').attr("placeholder", "请用中文输入正确全名");
       }
   
       if (memCurr.toLowerCase() == "vnd" || memCurr.toLowerCase() == "idr") {
           $("#txtAmount").addClass("amount_vnd_idr");
       } else {
           $("#txtAmount").removeClass("amount_vnd_idr");
       }
   
       $('#backToDeposit').on("click", function () {
           location.href = "#";
       })
       $('#txtAmount').on('change',
           function () {
               if (this.value !== "" &&
                   this.value !== 0 &&
                   $(this).valid()) {
                   $("#btnReload1").click();
               }
           }
       );
   
       $('#ddlTargetWallet').on('change',
           function () {
               $("#btnReload").click();
           }
       );
   
       var step = $("#steps").val();
   
       if (step >= 1) {
           $('#GenerateDetails').click();
       }
   
       if (step >= 2) {
           $('#ConfirmDetails').click();
       }
   
   });
   
   function GenerateCountDown() {
       var elementCountDown = $('#ChaseCountDown');
       if (typeof elementCountDown != "undefined" && elementCountDown != null && typeof elementCountDown.html() != "undefined") {
           var countDownTime = elementCountDown.html().split(":");
           var countMinutes = countDownTime[0];
           var countSeconds = countDownTime[1];
           var setTime = new Date().setMinutes(countMinutes, countSeconds);
           var substractSecond = new Date(setTime.valueOf() - 1000);
           if (countMinutes == "00" && countSeconds == "00") {
               $('#CountdownDesc').html("Transaksi anda sedang di proses, harap menunggu!");
                   $('#chaseDepositButton').html("Tanya Status");
                   $('#chaseDepositButton').attr("onclick", "Page.popUpLiveChat()");
               } else {
                   elementCountDown.html((substractSecond.getMinutes() < 10 ? '0' : '') + substractSecond.getMinutes() + ":" + (substractSecond.getSeconds() < 10 ? '0' : '') + substractSecond.getSeconds());
               }
           }
       }
   
       function chaseDepositMessage() {
           Page.message("Permintaan anda sedang kami proses, harap menunggu!");
   }
   
   $(function () {
       $('#dateDeposit').fdatepicker({
           format: 'dd/mm/yyyy',
           endDate: new Date(),
           disableDblClickSelection: true,
           leftArrow: '<<',
           rightArrow: '>>',
           closeIcon: 'X',
           closeButton: true,
           keyboardNavigation: false
       });
   
   });
</script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.signalR-2.3.0.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}Assets/Scripts/ext/pnotify.custom.min.js"></script>
<script src="{{APP_ASSETS}}signalr/hubs.js"></script>
<script type="text/javascript">
   var boLang = "id-id";
   
   $('#mobile-notification-received').on("click", function () {
       $('.side-panel-notification-setting').empty();
       $('.notifyNoDisplay').text("0");
       $('.notifyNoDisplay').hide();
       window.open('#', '_blank');
   });
   
   var stack_bottomright = { "dir1": "up", "dir2": "left", "spacing1": 0, "spacing2": 0 };;
   
   function notifyMe(msg, iconClass, activePages) {
       PNotify.desktop.permission();
       var pNotify;
       if (Helper.IsDesktopApps === "true" && (Helper.IsWindows7 === "true" || Helper.IsWindows8 === "true" || Helper.IsWindows8Dot1 === "true" || Helper.IsWindows10 === "true")) {
           pNotify = (new PNotify({
               title: 'FUN88',
               icon: 'notificationIcon icon ' + iconClass,
               text: msg,
               addclass: "stack-bottomright",
               stack: stack_bottomright,
               delay: 30000,
               desktop: {
                   icon: '/Assets/images/General/fun88_notification_logo.png'
               },
               buttons: {
                   closer: true
               }
           }));
       } else {
           pNotify = (new PNotify({
               title: 'FUN88',
               icon: 'notificationIcon icon ' + iconClass,
               text: msg,
               desktop: {
                   desktop: true,
                   tag: msg,
                   fallback: true,
                   icon: '/Assets/images/General/fun88_notification_logo.png'
               },
               addclass: "stack-bottomright",
               stack: stack_bottomright,
               delay: 30000,
               history: false,
               buttons: {
                   closer: true
               }
           }));
       }
   
       getLatestNotificationData(activePages).then(function (result) {
           var notificationData = result.split(',');
   
           pNotify.get().click(function (e) {
               PNotify.removeAll();
               pNotify.remove();
               if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
               setTimeout(function () {
                   window.open(notificationData[0], "_blank");
               }, 1000);
           });
   
           var rptPushNotification = $('#templateHolder > .side-panel-setting-notification-content-table').eq(0).clone(true);
           $(rptPushNotification).attr('id', notificationData[1]);
           $(rptPushNotification).find(".notification-child-text").html(msg);
           $(rptPushNotification).find(".notification-child-icon-holder").removeClass("icon-notification-announcement").addClass(iconClass);
           $('.side-panel-setting-notification-content-wrapper').prepend($(rptPushNotification));
       });
   }
   
   function getLatestNotificationData(activePages) {
       return new Promise(function(resolve, reject) {
           $.ajax({
               type: "GET",
               url: "#",
               data: { firstLoad: "true" },
               success: function (data) {
                   if (typeof data != "undefined") {
                       if (data != null) {
                           var notificationArr = data.broadCastList;
   
                           if (notificationArr != null) {
                               var notificationId = notificationArr[0].Id.split('_');
   
                               var notificationRedirectionLink = activePages == "announcement"
                               ? "#" + notificationArr[0].Category + "&id=" + notificationId[1]
                               : "#" + notificationArr[0].Category + "&id=" + notificationId[1];
   
                               var notificationSidePanelId = notificationArr[0].Category + '_' + notificationArr[0].Id;
   
                               resolve(notificationRedirectionLink + ',' + notificationSidePanelId);
                           }
                       }
                   }
               }
           });
       });
   }
   
   function isNewNotificationSupported() {
       if (!window.Notification || !Notification.requestPermission)
           return false;
       if (Notification.permission == 'granted')
           throw new Error('You must only call this \*before\* calling Notification.requestPermission(), otherwise this feature detect would bug the user with an actual notification!');
       try {
           new Notification('');
       } catch (e) {
           if (e.name == 'TypeError')
               return false;
       }
       return true;
   }
   function addNewNotificationValue() {
       var notificationNumber = Number($('.notifyNoDisplay').first().text()) + 1;
       $(".notifyNoDisplay").text(notificationNumber);
       $(".notifyNoDisplayMobile").text(notificationNumber);
       $('.notifyNoDisplay').show();
   }
   $(function () {
       if ($.connection.hub && $.connection.hub.state === $.signalR.connectionState.reconnecting) {
           var chats = $.connection.chatHub;
           chats.stop();
           setTimeout(function (){
               chats.start();
           },
           5000);
       }
       if ($.connection.hub && $.connection.hub.state === $.signalR.connectionState.disconnected) {
           var chat = $.connection.chatHub;
           chat.client.broadcastLiveMatchMessage = function (message) {
               var data = JSON.parse(message);
   
               if (data !== null) {
                   $.each(data, function (i, match) {
                       var team = $(".home-sport-team")[i];
                       var location = $(".home-sport-location-time")[i];
                       var objDate = new Date(match.MatchStartTime);
   
                       team.innerHTML = match.HomeTeamName + " vs " + match.AwayTeamName;
   
                       location.innerHTML = match.league + " " + objDate.format("MMM dd, hh:mm tt ");
                   });
               }
           };
   
           chat.client.broadcastMessage = function (message, language) {
               try {
                   if (language.toLowerCase() === boLang) {
                       addNewNotificationValue();
                       if (!Helper.IsMobile) {
                           notifyMe(message, 'icon-notification-announcement', 'announcement');
                       }
   
                   }
               } catch (err) {
   
               }
           };
   
           chat.client.privateMessage = function (message, category) {
               try {
                   addNewNotificationValue();
                   if (!Helper.IsMobile) {
                       $.ajax({
                           type: "GET",
                           url: "/Services/BroadcastMessageService.ashx",
                           data: { Message: message, Category: category },
                           success: function (data) {
                               var broadcastMessage = data.broadCastList;
                               notifyMe(broadcastMessage[0].Message, broadcastMessage[0].IconClass, 'notification');
                           }
                       });
                   }
               } catch (err) {
   
               }
           };
   
           chat.client.refreshPromotion = function () {
               try {
                   if (document.location.pathname.indexOf('/promo') >= 0) {
                       FillUpPromotionPage();
                   }
               } catch (err) {
   
               }
           };
       }
   
       $.connection.hub.start();
   });
</script>
@endsection