@extends('main.base_head_foot')
@section('title', $title)
@section('css') 

<link href="{{APP_ASSETS}}bundles/css/payment.css" rel="stylesheet"/>


@endsection
@section('content')
<div class="content-Wrapper">
   <div class="bank_top_panel">
      <input type="hidden" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$totalBalance" id="totalBalance" />
      <div class="bank_top_title">
         <h3><b>Bank Saya</b></h3>
      </div>
      <div class="bank_top_summary">
         <h6>Ringkasan Akun</h6>
      </div>
      <div class="ui_seperator"></div>
      <div class="row modify-row-width balance_panel" style="margin: 15px 0;">
         <div class="columns large-3 account_summary_div" data-equalizer>
            <div class="account_summary_outer">
               <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                  <span style="margin-bottom: 0"><b class="balance_type">TOTAL SALDO</b></span>
               </div>
               <div class="columns large-12 small-7" data-equalizer-watch>
                  <div class="walletAmountPlaceHolder TotalBalanceAmount">
                     <b id="totalBalanceAmt" class="l_font">3066.90</b>
                  </div>
               </div>
            </div>
         </div>
         <div class="columns large-3 account_summary_div" data-equalizer>
            <div class="account_summary_outer">
               <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                  <span style="margin-bottom: 0"><b class="balance_type">AKUN UTAMA</b></span>
               </div>
               <div class="columns large-12 small-7" data-equalizer-watch>
                  <div class="walletAmountPlaceHolder TotalWalletAmount">
                     <b class="m_font" id="mainAccountBalanceAmt">1066.90</b>
                  </div>
               </div>
            </div>
         </div>
         <div class="columns large-6" style="padding: 0;" data-equalizer>
            <div style="display: block">
               <div class="account_summary_wallet large-6">
                  <div class="columns large-12 small-5 account_summary" data-equalizer-watch>
                     <span style="margin-bottom: 0"><b class="balance_type">Dompet Pilihan: <span class="preferColorBar" style="vertical-align:text-bottom;margin:0 5px;"></span>AKUN UTAMA</b></span>
                  </div>
                  <div class="columns large-12 small-7" data-equalizer-watch>
                     <div class="walletAmountPlaceHolder TotalPreferAmount">
                        <b class="m_font" id="mainWalletBalanceAmt">500.00</b>
                     </div>
                  </div>
               </div>
               <div class="account_summary_deposit large-5">
                  <a href="#" class="depositSelfExlusion">
                  <span>
                  <b style="font-size: 16px;color: #000;">Deposit</b>
                  </span>
                  </a>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="ui_seperator"></div>
   <div id="financeTypeSelection" class="row modify-row-width large-up-6 medium-up-6 small-up-6" style="text-align: center;" data-equalizer>
      <a href="#" id="wallet" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-my-account bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Akun Saya</b>
            </span>
         </div>
      </a>
      <a href="#" id="deposit" class="depositSelfExlusion columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_deposit bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Deposit</b>
            </span>
         </div>
      </a>
      <a href="#" id="transfer" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_transfer bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Transfer</b>
            </span>
         </div>
      </a>
      <a href="#" id="withdrawal" class="columns column-block financeType withdrawalSelfExlusion" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank_withdrawal bank-icon withdrawalSelfExlusion" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Penarikan</b>
            </span>
         </div>
      </a>
      <a href="#" id="bankdetail" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-fun88_bank bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Perincian Bank</b>
            </span>
         </div>
      </a>
      <a href="#" id="history" class="columns column-block financeType" data-equalizer-watch>
         <div style="margin:10px auto">
            <span class="icon icon-history bank-icon" style="font-size:25px;"></span>
            <span class="methodName">
            <b>Riwayat</b>
            </span>
         </div>
      </a>
   </div>
   <div id="paymentMethodSlider" class="row modify-row-width" style="border-bottom: 1px solid #dcdcdc; text-align: left;" data-equalizer>
      <a href="#" style="display: inline-block; text-align: center" data-equalizer-watch>
         <div class="deposit-methods" data-page="localbank" data-index="0">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Finance/visa.png" class="Deposit_localbank" style="float: left; display: none; width: 25%" />
            <span><b>Bank Lokal</b></span> 
         </div>
      </a>
      <a href="#" style="display: inline-block; text-align: center" data-equalizer-watch>
         <div class="deposit-methods" data-page="help2pay" data-index="1">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Finance/visa.png" class="Deposit_help2pay" style="float: left; display: none; width: 25%" />
            <span><b>Help 2 Pay</b></span> 
         </div>
      </a>
      <a href="#" style="display: inline-block; text-align: center" data-equalizer-watch>
         <div class="deposit-methods" data-page="xpay" data-index="2">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Finance/visa.png" class="Deposit_xpay" style="float: left; display: none; width: 25%" />
            <span><b>FunPay</b></span> 
         </div>
      </a>
      <a href="#" style="display: inline-block; text-align: center" data-equalizer-watch>
         <div class="deposit-methods" data-page="quickpay" data-index="3">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Finance/visa.png" class="Deposit_quickpay" style="float: left; display: none; width: 25%" />
            <span><b>Quick Pay</b></span> 
         </div>
      </a>
      <a href="/#" style="display: inline-block; text-align: center" data-equalizer-watch>
         <div class="deposit-methods" data-page="cashcard" data-index="4">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Finance/visa.png" class="Deposit_cashcard" style="float: left; display: none; width: 25%" />
            <span><b>Cash Card</b></span> 
         </div>
      </a>
   </div>
   <div class="helppayment">
   </div>
   <br />
   <br />
   <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$hidblackbox" type="hidden" id="hidblackbox" />
   <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$hidE2" type="hidden" id="hidE2" />
   <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$newOld" type="hidden" id="newOld" value="0" />
   <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$HidResubmitSetting" type="hidden" id="HidResubmitSetting" />
   <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$SelectedMerchant" type="hidden" id="SelectedMerchant" />
   <div class="row payment-Content">
      <div class="large-12 columns form-wrapper" id="before-Deposit">
         <div class="Rules">
            <span>Menerima semua jenis bank transfer, deposit/transfer via ATM, melalui loket bank dan mobile transfer.</span><br />
            <span>Masa waktu normal persetujuan sekitar 30 menit</span><br />
            <span>Jika anda memiliki masalah apapun, jangan ragu untuk menghubungi para petugas layanan pelanggan online kami.</span><br />
            <br />
            <span>Sekali deposit <span class="min-max">Minimum：Rp 50，Maksimum：Rp 50,000 per transaksi.</span></span>
         </div>
         <div id="stepOne" class="large-12 deposit-step">
            <div class="process-status"><span id="stepOneTick"></span></div>
            <div class="deposit-step-title"><b>Langkah 1. Pilih Metode</b><span id="help" onclick="window.open('#','_blank')" title="Butuh Bantuan?
               Langkah 1: Pilih Bank
               Langkah 2: Klik untuk mengupdate Detail Akun Bank
               Copy Detail yang tertera dan deposit ke Akun Bank Kami.
               Langkah 3: Klik untuk mengkonfirmasi deposit yang telah anda lakukan.
               Jika anda masih mempunyai pertanyaan, Klik untuk detail lebih lanjut.">Butuh Bantuan?</span></div>
            <div class="account-details-desc"><b>Silahkan untuk deposit pada rekening yang benar di bawah ini. Jika tidak, maka permintaan deposit anda akan mengalami keterlambatan dalam proses.</b></div>
            <div class="depositMethodOptions">
               <input type="radio" name="depositMethod" id="62105" value="62105" />
               <label for="62105" data-bankName="Commerce International Merchant Bankers">
               Commerce International Merchant Bankers (CIMB)
               </label>
            </div>
            <div class="depositMethodOptions">
               <input type="radio" name="depositMethod" id="66104" value="66104" />
               <label for="66104" data-bankName="Bank Negara Indonesia">
               Bank Negara Indonesia (BNI)
               </label>
            </div>
            <div class="depositMethodOptions">
               <input type="radio" name="depositMethod" id="68719" value="68719" />
               <label for="68719" data-bankName="Bank Rakyat Indonesia">
               Bank Rakyat Indonesia (BRI)
               </label>
            </div>
            <div class="depositMethodOptions">
               <input type="radio" name="depositMethod" id="69543" value="69543" />
               <label for="69543" data-bankName="Bank Central Asia">
               Bank Central Asia (BCA)
               </label>
            </div>
            <div class="depositMethodOptions">
               <input type="radio" name="depositMethod" id="9999" value="9999" />
               <label for="9999" data-bankName="Apabila tidak menemukan pilihan bank yang anda gunakan untuk deposit , mohon lampirkan 6 digit terakhir dari nomor rekening yang anda gunakan untuk deposit.">
               Apabila tidak menemukan pilihan bank yang anda gunakan untuk deposit , mohon lampirkan 6 digit terakhir dari nomor rekening yang anda gunakan untuk deposit.
               </label>
            </div>
         </div>
         <div id="stepTwo" class="large-12 deposit-step">
            <div class="process-status"><span id="stepTwoTick"></span></div>
            <div class="deposit-step-title"><b>Langkah 2. Tampilkan rincian akun Anda</b></div>
            <div class="account-details">
               <div class="account-details-desc-2"><b>Dibawah ini adalah informasi transfer anda. Gunakan bank lokal atau transfer tunai menggunakan akun bank online.</b>
                  <br/><b style="color:red;">Harap hubungi Customer Service di Live Chat untuk Nomor Rekening.</b>
               </div>
               <table class="account-details-table">
                  <tr>
                     <td><span><b>Nama Bank</b></span></td>
                     <td><span id="mercBankName"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercBankName"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
                  <tr>
                     <td><span><b>Nama Akun</b></span></td>
                     <td><span id="mercAccountName"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercAccountName"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
                  <tr>
                     <td><span><b>Nomor Akun</b></span></td>
                     <td><span id="mercAccountNumber"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercAccountNumber"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
                  <tr>
                     <td><span><b>Propinsi</b></span></td>
                     <td><span id="mercProvince"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercProvince"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
                  <tr>
                     <td><span><b>Kota</b></span></td>
                     <td><span id="mercCity"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercCity"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
                  <tr>
                     <td><span><b>Cabang Bank</b></span></td>
                     <td><span id="mercBranch"></span></td>
                     <td><span class="btn" data-clipboard-target="#mercBranch"><span class="icon icon-tick-circle-button" style="display: none; margin-right: 5px"></span><span class="no-copy">Copy</span></span></td>
                  </tr>
               </table>
            </div>
            <div class="ProceedButton"><span id="GenerateDetails"><b>TAMPILKAN RINCIAN AKUN</b></span></div>
         </div>
         <div class="large-12 deposit-step">
            <div class="process-status"><span id="stepThreeTick"></span></div>
            <div class="deposit-step-title" id="VNDChangeStep"><b>Langkah 3. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.</b></div>
            <div class="transactionDetails large-6 form-wrapper">
               <div id="amountRow" class="large-12 row">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Jumlah :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$txtAmount" type="number" id="txtAmount" class="resetRequired" onblur="PaymentCommonFunction.TrimSpace();" oncopy="return false;" onpaste="return false" />
                        <span class="amount_IDR" style="display: none;">,000</span>
                        <span class="mandatory">*</span>
                     </div>
                     <label id="txtAmount-error" class="error diverror amount_Error_IDR" for="txtAmount"></label>
                     <div style="display: none" class="show_IDR"><span>กรุณากรอกจำนวนเงินฝากไม่รวมค่าธรรมเนียม</span></div>
                  </div>
               </div>
               <div class="large-12 row hide_LastFive_IDR">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Nomor Akun Bank Deposito<br/>(6 Nomor Terakhir) :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$txtLastDigit" type="number" id="txtLastDigit" class="resetRequired" />
                        <span class="mandatory">*</span>
                     </div>
                     <label id="txtLastDigit-error" class="error diverror" for="txtLastDigit"></label>
                  </div>
               </div>
               <div id="refNoRow" class="large-12 row hide_Ref_IDR">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Nomor Referensi :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$txtRefNumber" type="text" id="txtRefNumber" class="resetRequired" maxlength="50" />
                        <span class="mandatory">*</span>
                     </div>
                     <label id="txtRefNumber-error" class="error diverror" for="txtRefNumber"></label>
                     <div id="RefNoRemark_Vietcom"><span>Silahkan isi nomor referensi dengan 16 angka yang diberikan oleh sistem bank untuk transaksi yang sukses<br/>Contoh : 20xxxxxxxxxxxxxx</span></div>
                     <div id="RefNoRemark_Techcom"><span>Harap isi nomor referensi yang diberikan oleh sistem bank untuk transaksi sukses.<br/>Contoh : FT/xxxxx/xxxxxxxxx</span></div>
                     <div id="RefNoRemark_Sacom"><span>Silahkan isi nomor referensi yang disediakan oleh sistem perbankan anda supaya transaksi berhasil.<br/>
                        Contoh: ATXXXXXXXXXX \ XXX atau FTXXXXXXXXXX \ XXX atau TTXXXXXXXXXX \ XXX</span>
                     </div>
                     <div id="RefNoRemark_Other"><span>Please fill-up if only you’re deposited into SAVOM, TECHOM, and VIETCOM. <br/>
                        Example: Successful transaction reference number at Sacombank, Techcombank and Vietcombank is provided by the Bank with the following format. <br />
                        Sacombank  :  ATXXXXXXXXXX\XXX  or FTXXXXXXXXXX\XXX or TTXXXXXXXXXX\XXX <br />
                        Techcombank :  FT/xxxxx/xxxxxxxxx <br />
                        Vietcombank  :  20xxxxxxxxxxxxxx</span>
                     </div>
                  </div>
               </div>
               <div class="large-12 row">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Tanggal Disetor :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$dateDeposit" type="text" id="dateDeposit" maxlength="10" readonly="" style="cursor: pointer" />
                        <span class="mandatory">*</span>
                     </div>
                     <label id="dateDeposit-error" class="error diverror" for="dateDeposit"></label>
                     <div style="display: none" class="show_IDR"><span>กรุณาระบุวันที่ทำธุรกรรมฝากเงินของท่านจากไอคอนปฏิทิน</span></div>
                  </div>
               </div>
               <div class="large-12 row" id="timeRow">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Waktu Disetor :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlHours" id="ddlHours">
                           <option value=""></option>
                           <option value="00">00</option>
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12">12</option>
                           <option value="13">13</option>
                           <option value="14">14</option>
                           <option value="15">15</option>
                           <option value="16">16</option>
                           <option value="17">17</option>
                           <option value="18">18</option>
                           <option value="19">19</option>
                           <option value="20">20</option>
                           <option value="21">21</option>
                           <option value="22">22</option>
                           <option value="23">23</option>
                        </select>
                        <div style="margin: auto; display: inline-block">: </div>
                        <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlMinutes" id="ddlMinutes">
                           <option value=""></option>
                           <option value="00">00</option>
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12">12</option>
                           <option value="13">13</option>
                           <option value="14">14</option>
                           <option value="15">15</option>
                           <option value="16">16</option>
                           <option value="17">17</option>
                           <option value="18">18</option>
                           <option value="19">19</option>
                           <option value="20">20</option>
                           <option value="21">21</option>
                           <option value="22">22</option>
                           <option value="23">23</option>
                           <option value="24">24</option>
                           <option value="25">25</option>
                           <option value="26">26</option>
                           <option value="27">27</option>
                           <option value="28">28</option>
                           <option value="29">29</option>
                           <option value="30">30</option>
                           <option value="31">31</option>
                           <option value="32">32</option>
                           <option value="33">33</option>
                           <option value="34">34</option>
                           <option value="35">35</option>
                           <option value="36">36</option>
                           <option value="37">37</option>
                           <option value="38">38</option>
                           <option value="39">39</option>
                           <option value="40">40</option>
                           <option value="41">41</option>
                           <option value="42">42</option>
                           <option value="43">43</option>
                           <option value="44">44</option>
                           <option value="45">45</option>
                           <option value="46">46</option>
                           <option value="47">47</option>
                           <option value="48">48</option>
                           <option value="49">49</option>
                           <option value="50">50</option>
                           <option value="51">51</option>
                           <option value="52">52</option>
                           <option value="53">53</option>
                           <option value="54">54</option>
                           <option value="55">55</option>
                           <option value="56">56</option>
                           <option value="57">57</option>
                           <option value="58">58</option>
                           <option value="59">59</option>
                        </select>
                        <div style="margin: auto; display: inline-block">: </div>
                        <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlSeconds" id="ddlSeconds">
                           <option value=""></option>
                           <option value="00">00</option>
                           <option value="01">01</option>
                           <option value="02">02</option>
                           <option value="03">03</option>
                           <option value="04">04</option>
                           <option value="05">05</option>
                           <option value="06">06</option>
                           <option value="07">07</option>
                           <option value="08">08</option>
                           <option value="09">09</option>
                           <option value="10">10</option>
                           <option value="11">11</option>
                           <option value="12">12</option>
                           <option value="13">13</option>
                           <option value="14">14</option>
                           <option value="15">15</option>
                           <option value="16">16</option>
                           <option value="17">17</option>
                           <option value="18">18</option>
                           <option value="19">19</option>
                           <option value="20">20</option>
                           <option value="21">21</option>
                           <option value="22">22</option>
                           <option value="23">23</option>
                           <option value="24">24</option>
                           <option value="25">25</option>
                           <option value="26">26</option>
                           <option value="27">27</option>
                           <option value="28">28</option>
                           <option value="29">29</option>
                           <option value="30">30</option>
                           <option value="31">31</option>
                           <option value="32">32</option>
                           <option value="33">33</option>
                           <option value="34">34</option>
                           <option value="35">35</option>
                           <option value="36">36</option>
                           <option value="37">37</option>
                           <option value="38">38</option>
                           <option value="39">39</option>
                           <option value="40">40</option>
                           <option value="41">41</option>
                           <option value="42">42</option>
                           <option value="43">43</option>
                           <option value="44">44</option>
                           <option value="45">45</option>
                           <option value="46">46</option>
                           <option value="47">47</option>
                           <option value="48">48</option>
                           <option value="49">49</option>
                           <option value="50">50</option>
                           <option value="51">51</option>
                           <option value="52">52</option>
                           <option value="53">53</option>
                           <option value="54">54</option>
                           <option value="55">55</option>
                           <option value="56">56</option>
                           <option value="57">57</option>
                           <option value="58">58</option>
                           <option value="59">59</option>
                        </select>
                        <span class="mandatory">*</span>
                     </div>
                     <div style="display: none" class="timeErrorMessage diverror"><span>Please enter your time of deposit into FUN88 account. <br/>For example: If your time of deposit is at 10:30 AM, then enter 10 : 30 : 00</span></div>
                     <div style="display: none" class="show_IDR"><span>กรุณาระบุเวลาฝากเงินให้ถูกต้อง เพื่อให้ยอดเงินฝากของคุณอนุมัติได้อย่างรวดเร็ว</span></div>
                  </div>
               </div>
               <div class="large-12 row show_Bank_IDR" style="display: none">
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <span style="margin-left: .9375rem; color: red;">Silahkan mengisi langsung transfer dana sukses kedalam bank akun Fun88 anda.</span>
                     </div>
                  </div>
               </div>
               <div id="atttachmentRow" class="large-12 row hide_Attachment_IDR">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Slip Transfer Bank :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$fileAttachment" type="file" id="fileAttachment" class="show-for-sr resetRequired" style="height: 0; display:none" />
                     <input type="text" id="fileName" class="large-8 column" style="width: 67%; background-color: white" disabled="disabled" />
                     <label for="fileAttachment" id="btn_attachment" class="button" style="width: 30%">Telusuri</label>
                  </div>
                  <div class="column large-3 medium-4 form-field">
                  </div>
                  <div id="fileAttachment-wrapper" class="column large-9 medium-8 form-field">
                     <label id="fileAttachment-error" class="error diverror"></label>
                     <div style="display: none" class="show_IDR"><span>กรุณาแนบสลิปหากธนาคารที่ท่านทำรายการฝากกำลังอยู่ระหว่างปรับปรุงเพื่อการดำเนินการที่รวดเร็วขึ้น</span></div>
                  </div>
               </div>
               <div id="bankTypeRow" class="large-12 row hide_IDR">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Jenis Deposit :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlBankType" id="ddlBankType">
                        <option value="IB">Ke Bank Lain</option>
                        <option value="IBT">Sesama Bank</option>
                        <option value="ATM">ATM</option>
                        <option value="MB">Mobile Banking</option>
                     </select>
                  </div>
               </div>
               <div id="btnNewField" class="large-12 row" style="display:none">
                  <div class="column large-3 medium-4 form-field">
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <span id="btnNew">Tambahkan Akun Baru</span>
                  </div>
               </div>
               <div id="btnPreferredField" class="large-12 row" style="margin-bottom: 40px">
                  <div class="column large-3 medium-4 form-field">
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <span id="btnPreferred">Akun Pilihan</span>
                  </div>
               </div>
               <div id="preferredAccount" style="display:none">
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                        <span><b>Akun Pilihan :</b></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlMemberAccount" id="ddlMemberAccount">
                        </select>
                        <div style="display: none" class="show_IDR"><span>กรุณาระบุบัญชีของท่านที่ใช้โอนเงิน</span></div>
                     </div>
                  </div>
               </div>
               <div id="newAccount">
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                        <span><b>Nama Lengkap :</b></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <div class="input-field">
                           <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$txtNewAccountHolderName" type="text" id="txtNewAccountHolderName" class="resetRequired" maxlength="50" onchange="PaymentCommonFunction.FrontNEndTrim(this)" style="margin-bottom:0;" />
                           <span class="mandatory">*</span>
                        </div>
                        <label id="txtNewAccountHolderName-error" class="error diverror" for="txtNewAccountHolderName"></label>
                     </div>
                  </div>
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                        <span><b>Nama Bank :</b></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <div class="input-field">
                           <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlNewAccountBankName" id="ddlNewAccountBankName">
                              <option value="Mandiri">Mandiri</option>
                              <option value="Bank Central Asia(BCA)">Bank Central Asia(BCA)</option>
                              <option value="Bank Rakyat Indonesia(BRI)">Bank Rakyat Indonesia(BRI)</option>
                              <option value="Bank Negara Indonesia(BNI)">Bank Negara Indonesia(BNI)</option>
                              <option value="0">Bank lain</option>
                           </select>
                           <span class="mandatory">*</span>
                        </div>
                     </div>
                  </div>
                  <div class="large-12 row" id="otherBank" style="display: none">
                     <div class="column large-3 medium-4 form-field">
                        <span></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$otherBankName" type="text" id="otherBankName" class="resetRequired" />
                        <label id="otherBankName-error" class="error diverror" for="otherBankName"></label>
                     </div>
                  </div>
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                        <span><b>No. Rekening Bank :</b></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <div class="input-field">
                           <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$txtNewAccountNumber" type="text" id="txtNewAccountNumber" class="resetRequired" maxlength="20" onchange="PaymentCommonFunction.FrontNEndTrim(this)" />
                           <span class="mandatory">*</span>
                        </div>
                        <label id="txtNewAccountNumber-error" class="error diverror" for="txtNewAccountNumber"></label>
                     </div>
                  </div>
                  <div class="large-12 row">
                     <div class="column large-3 medium-4 form-field">
                        <span></span>
                     </div>
                     <div class="column large-9 medium-8 form-field">
                        <input name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$chkRmb" type="checkbox" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_chkRmb" cientidmode="static" class="large-2 column" style="width: 20px; height: 40px; padding: 0; margin: 0;" />
                        <label for="chkRmb" class="large-10 column" style="width: auto; line-height: 40px; padding: 0; margin: 0 0 0 15px; float: left;">Ingat perincian bank saya</label>
                     </div>
                  </div>
               </div>
               <div class="large-12 row">
                  <div class="column large-3 medium-4 form-field">
                     <span><b>Ke Akun :</b></span>
                  </div>
                  <div class="column large-9 medium-8 form-field">
                     <div class="input-field">
                        <select name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$ddlTargetWallet" id="ddlTargetWallet" tabindex="17" class="white">
                           <option selected="selected" value="MAIN">AKUN UTAMA</option>
                           <option value="SBT">OLAHRAGA FUN88</option>
                           <option value="SP">OLAHRAGA ONEWORKS</option>
                           <option value="IPSB">OLAHRAGA IM</option>
                           <option value="LD">LIVE CASINO</option>
                           <option value="IPK">POKER</option>
                           <option value="AG">ROYAL PALACE</option>
                           <option value="SLOT">PERMAINAN</option>
                           <option value="PT">PT PERMAINAN</option>
                        </select>
                        <span class="mandatory" style="vertical-align: super;">*</span>
                     </div>
                  </div>
               </div>
               <div id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_updatePanel1">
               </div>
               <div class="large-12 row">
                  <div id="upBonus">
                     <input type="image" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$btnReload" id="btnReload" src="{{APP_ASSETS}}Assets/images/General/refreshbutton.png" style="height:20px;width:20px;display: none" />
                     <input type="image" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$btnReload1" id="btnReload1" src="{{APP_ASSETS}}Assets/images/General/refreshbutton.png" style="height:20px;width:20px;display: none" />
                     <div style="text-align: center;">
                        <div id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_upProgress" style="display:none;">
                           <img alt="" src="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/General/loading.gif">
                        </div>
                     </div>
                     <input type="hidden" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$BonusListing$rblBonusId" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_BonusListing_rblBonusId" value="0" />
                  </div>
               </div>
               <div class="large-12 row" style="margin: 40px 0; padding: 0;">
                  <input type="submit" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$submit" value="Ajukan" onclick="return PaymentCommonFunction.LocalBankSubmit();" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_submit" class="BtnSubmit button-Blue" />
                  <input type="submit" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$cancel" value="Batal" onclick="return PaymentCommonFunction.LocalBankCancel();" id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BodyContentPlaceHolderFinance_BodyContentPlaceHolderDeposit_cancel" class="BtnCancel button-White" />
               </div>
            </div>
            <div class="ProceedButton"><span id="ConfirmDetails"><b>KONFIRMASI TRANSFER ANDA</b></span></div>
         </div>
         <input type="hidden" name="ctl00$ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderFinance$BodyContentPlaceHolderDeposit$steps" id="steps" value="0" />
      </div>
      <div class="large-12 columns" id="after-Deposit" style="margin-top: 30px">
         <div class="row" style="text-align: center; margin: auto">
            <span id="successTick" class="icon icon-tick-circle-button"></span>
         </div>
         <div class="row" style="text-align: center; margin: auto;">
            <div>
               <span class="LB_inProgress">Transaksi Sedang di Proses</span>
            </div>
            <div style="margin-bottom: 20px" id="CountdownDesc"><span style="font-size: 16px; color: #2c2c2c;"><b>Transaksi anda akan segera kami proses dalam waktu <span id="ChaseCountDown">10:00</span>, harap menunggu.</b></span></div>
            <div style="margin: 30px auto 60px">
               <span id="chaseDepositButton" onclick="chaseDepositMessage()">Kilat</span>
            </div>
            <div>
               <span style="font-size: 16px; color: #2c2c2c;">Anda bisa melihat status transaksi di halaman <a style="color: #3f97ff" href="#"> History.</a>.</span>
            </div>
            <div style="margin-bottom: 40px"><span id="backToDeposit" style="cursor: pointer; font-size: 14px; color: #3f97ff">Klik untuk kembali ke halaman deposit bank lokal</span></div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-3.1.1.min.js"></script>
<script src="{{APP_ASSETS}}jquery.main/main.js"></script>
<script src="{{APP_ASSETS}}js.main/main.js"></script>
<script src="{{APP_ASSETS}}bundles/js/require.js"></script>
<script src="{{APP_ASSETS}}bundles/js/module.js"></script>
<script src="{{APP_ASSETS}}bundles/js/base.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/DatePicker/foundation-datepicker.min.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/wallets.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/payment.js" type="text/javascript"></script> 
<script src="{{APP_ASSETS}}Assets/Scripts/ext/iCheck.js" type="text/javascript" ></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>    
<script src="{{APP_ASSETS}}bundles/js/validation.js"></script>
<script type="text/javascript">
   var form = $("#baseForm");
   var txtAmount = $("#txtAmount");
   var txtRefNumber = $("#txtRefNumber");
   var txtOldAccountNumberLastSix = $("#txt_OldAccountNumber");
   var txtLastDigit = $("#txtLastDigit");
   var txtAccountHolderName = $("#txtAccountHolderName");
   var txtNewAccountHolderName = $("#txtNewAccountHolderName");
   var txtCNYAccountHolderName = $("#txtCNYAccountHolderName");
   var txtAccountHolderName = $("#txtAccountHolderName");
   var otherBankName = $("#otherBankName");
   var txtNewAccountNumber = $("#txtNewAccountNumber");
   var txtYeePayCardNumber = $("#txtCardNumber");
   var txtYeePayPassword = $("#txtPassword");
   var txtNewAccountNumber = $("#txtNewAccountNumber");
   var txtName = $("#txtName");
   var txtCardNumber = $("#txtCardNumber");
   var txtCVV = $("#txtCVV");
   var txtAstroNumber = $("#txtAstroNumber");
   var txtAstroSafePin = $("#txtAstroSafePin");
   var txtBitCardNumber = $("#txtCardSerialNumber");
   var txtBitPassword = $("#txtPassword");
   var txtCashCardNumber = $("#txtCashCardNumber");
   var txtCashCardPW = $("#txtCashCardPW");
   var txtIBanqUserID = $("#txtUserID");
   var txtIBanqPassword = $("#txtPassword");
   var txtMBEmail = $("#txtEmail");
   var minAmount = "50";
   var maxAmount = "50000";
   var fileName = $('#fileName');
   var hoursField = $('#ddlHours');
   var minField = $('#ddlMinutes');
   var secondField = $('#ddlSeconds');
       //var txtCaptcha = $("#txtCaptcha");
       var selectDepositTargetWallet = $("#ddlTargetWallet");
   
       $.validator.addMethod("noWhiteSpace", function (value) {
           return value.indexOf(" ") === -1;
       });
   
       $.validator.addMethod("regex", function (value, element, regexp) {
           return new RegExp(regexp).test(value);
       });
   
       $.validator.addMethod("isNotZeroAmount", function (value) {
           value = parseFloat(value);
           return !isNaN(value) && value > 0;
       });
   
       $.validator.addMethod("maxTrasnfer", function (value) {
           value = parseFloat(value);
           return value >= 0.01 && value <= 1000000;
       });
   
       $.validator.addMethod("CheckRequired", function (value) {
           if ("False".toLowerCase() == "true") {
               return true;
           } else {
               return (value.length > 0);
           }
       });
   
       $.validator.addMethod("CheckRegex", function (value, element, regexp) {
           if ("IDR" == "VND") {
               return true;
           } else {
               return new RegExp(regexp).test(value);
           }
       });
   
       $.validator.addMethod("CheckSpecialRegex", function (value, element, regexp) {
           if($("#newOld").val() != "0"){
               return true;
           }else{
               return new RegExp(regexp).test(value);
           }
       });
   
       $.validator.addMethod("MaxLengthInput", function (value, element, maxlength) {
           return value.length <= maxlength;
       });
   
       $.validator.addMethod("AWcnRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{16,20}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("AWccRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{3,5}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("APcnRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{16}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod("APccRegex", function (value, element) {
           return this.optional(element) || /^[0-9]{4}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod('email', function (value, element) {
           return this.optional(element) || /^\w+[-.`~!@$%^*()={}|?\w]+@([\w.]{2,})+?\.[a-zA-Z]{2,9}$/i.test(value);
       }, 'Format yang salah.');
   
       $.validator.addMethod('checkTrim', function(value, element){
           return value.trim().length > 0;
       });
   
       $.validator.addMethod("checkMin", function (value, element) {
           return parseFloat(value) >= parseFloat(minAmount);
       }, "Jumlah harus sama atau di atas rentang minimum 50.");
   
       $.validator.addMethod("checkMax", function (value, element) {
           return parseFloat(value) <= parseFloat(maxAmount);
       }, "Batas Maksimal Rp. 50000 per transaksi");
   
       jQuery.validator.addMethod("LocalBankSixDigit", function(value, element, param) {
           var val = $(param).val();
           if (/^(\d{6})?$/i.test(val)) return true;
           else return false;
       }, 'Old account number shall not be less than 6 digit.');
   
       jQuery.validator.addMethod("Zerodecimal", function(value, element, param) {
           var val = $(param).val();
           if (val.indexOf('.') != -1 && val.length > 2) {
               if (/^\d+\.{0,1}[0]{1,2}$/.test(val)) return true;
               else return false;
           } else {
               return true;
           }
       }, 'Jumlah dalam titik desimal diperbolehkan dengan 00 saja.');
   
       $.validator.setDefaults({
           errorClass: "error",
           ignore: ":hidden",
           onsubmit: true,
           onkeyup: function (element) { this.element(element); },
           onfocusout: function (element) { this.element(element); },
           showErrors: function () { this.defaultShowErrors(); },
           errorPlacement: function (error, element) { error.insertAfter($(element)); }
       });
   
       function isVNDSpecialBank() {
           var currCode = "IDR";
   
           if (currCode == "VND") {
               var strVal = $("input[name='depositMethod']:checked").next().text();
               if (strVal == "Ngân hàng Ngoại Thương" || strVal == "Ngân hàng Kỹ Thương Việt Nam" || strVal == "Vietcom Bank" || strVal == "Techcom Bank" || strVal == "Ngân hàng Sài Gòn Thương Tín" || strVal == "Sacom Bank" || strVal == "Ngân hàng Ngoại Thương (Vietcombank)" || strVal == "Ngân hàng Kỹ Thương Việt Nam (Techcombank)" || strVal == "Ngân hàng Sài Gòn Thương Tín (Sacombank)" || strVal == "Nếu bạn không tìm thấy tài khoản ngân hàng sử dụng để gửi tiền, vui lòng điền vào 6 chữ số cuối của số tài khoản." || strVal == "If you cannot find the bank account you used for the deposit, please fill in the last 6 digits of the bank account number.") {
                   return true;
               }
           }
           if (currCode == "CNY" || currCode == "RMB" || currCode == "IDR") {
               return true;
           }
           return false;
       };
   
       var formValidator = form.validate();
</script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/clipboard.min.js"></script>
<script>
   $(document).ready(function () {
       var clipboard = new Clipboard('.btn');
       var alertMessage = " ".trim();
       var depositStatus = "";
       var memCurr = "IDR";
       var overload = "False";
       var Step3To2 = 'Langkah 2. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       var Step3To1 = 'Langkah 1. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       var originalChangeStepMessage = 'Langkah 3. Transfer Sukses? Klik Dibawah ini untuk Konfirmasi.';
       $("#steps").val(0);
       if (overload.toLowerCase() == "true") {
           Page.message("Pilihan deposit yang telah anda pilih saat ini akan memerlukan waktu proses sekitar 60 menit. Apakah anda ingin mencoba metode deposit lainnya?");
       }
   
       if ($("input[name='depositMethod']").length < 1) {
           $('.deposit-step-title').text(Step3To1);
       }
   
       if (alertMessage != null && alertMessage != "") {
           Page.message(alertMessage);
       }
   
       if (depositStatus != null && depositStatus != "") {
           setInterval(function () {
               GenerateCountDown();
           }, 1000);
           $('#after-Deposit').css("display", "block");
           $('#before-Deposit').css("display", "none");
           ga('send', 'event', 'Deposit_IDR' , 'Localbank', 'Transaction', '');
       } else {
           $('#after-Deposit').css("display", "none");
           $('#before-Deposit').css("display", "block");
       }
   
       
   
   
       $('#ddlHours, #ddlMinutes, #ddlSeconds').change(function () {
           if ($('#ddlHours').val() === '' || $('#ddlMinutes').val() === '' || $('#ddlSeconds').val() === '')
               $('.timeErrorMessage').show();
           else
               $('.timeErrorMessage').hide();
       });
   
       $("input[name='depositMethod']").change(function () {
           var flag = true;
           //$("#SelectedMerchant").val($("input[name='depositMethod']:checked").next().text().trim());
           $("#SelectedMerchant").val($("input[name='depositMethod']:checked").next().attr('data-bankName'));
           if ($("input[name='depositMethod']:checked").val() == "9999") {
               $('#stepTwo').css("display", "none");
               $('#VNDChangeStep b').text(Step3To2);
               $(".hide_LastFive_VND").show();
               $(".hide_Ref_VND").show();
               $(".hide_LastFive_THB").show();
               flag = false;
           } else {
               $('#stepTwo').css("display", "block");
               $('#VNDChangeStep b').text(originalChangeStepMessage);
               $(".hide_LastFive_VND").hide();
               $(".hide_LastFive_THB").hide();
           }
           if ($('#GenerateDetails').parent().is(":hidden")) {
               if (flag) {
                   $('.account-details').toggle("slow");
                   $('#GenerateDetails').parent().show();
               }
           }
           if ($('#ConfirmDetails').parent().is(":hidden")) {
               $('#ConfirmDetails').parent().show();
               $('.transactionDetails').toggle("slow");
           }
           $('#stepThreeTick').removeClass("icon icon-tick-circle-button");
           $("#steps").val(0);
           $('#stepOneTick').addClass("icon icon-tick-circle-button");
       });
   
       // to determine if the user is using Internet Explorer (IE)
       function isIE() {
           ua = navigator.userAgent;
           /* MSIE used to detect old browsers and Trident used to newer ones*/
           var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
   
           return is_ie;
       }
   
       $("#fileAttachment").change(function (e) {
           $('#fileAttachment-error').css("display", "none");
           $('#fileAttachment-error').html("");
           if (e.target.files) {
               var files = e.originalEvent.target.files;
               var n = files[0].name,
               s = files[0].size,
               t = files[0].type;
   
               if (t != "image/jpeg" && t != "image/gif" && t != "image/bmp" && t != "image/png" && t != "application/vnd.openxmlformats-officedocument.wordprocessingml.document" && t != "application/msword" && t != "application/pdf") {
                   $('#fileAttachment-error').html('Hanya mengijinkan gambar, dokumen dan format pdf dalam slip bank anda.');
                   $('#fileAttachment-error').css("display", "block");
                   // if the user is using Internet Explorer (IE)
                   if (isIE()) {
                       $('#fileAttachment').after($(this).clone(true)).remove();
                   } else {
                       $('#fileAttachment').val('');
                   }
                   
                   $('#fileName').val('');
                   return;
               }
               else if (s > 2097152) {
                   $('#fileAttachment-error').html('File tidak dapat lebih besar dari 2MB');
                   $('#fileAttachment-error').css("display", "block");
                   // if the user is using Internet Explorer (IE)
                   if (isIE()) {
                       $('#fileAttachment').after($(this).clone(true)).remove();
                   } else {
                       $('#fileAttachment').val('');
                   }
                   $('#fileName').val('');
                   return;
               }
           }
               //<= IE9
           else {
               var files = e.target.value;
               var n = files.replace(/^.*[\\\/]/, '')
               t = files.substr((files.lastIndexOf('.') + 1));
   
               if (t != "jpg" && t != "jpeg" && t != "gif" && t != "bmp" && t != "png" && t != "doc" && t != "docx" && t != "pdf") {
                   $('#fileAttachment-error').html('Hanya mengijinkan gambar, dokumen dan format pdf dalam slip bank anda.');
                   $('#fileAttachment-error').css("display", "block");
                   $('#fileAttachment').val('');
                   $('#fileName').val('');
                   return;
               }
           }
           $('#fileName').val($('#fileAttachment')[0].files[0].name);
       });
   
       Payment.LocalBank();
       Payment.LocalBankResubmit();
       if ("id" == "zh") {
           $('#txtAccountHolderName').attr("placeholder", "请用中文输入正确全名");
       }
   
       if (memCurr.toLowerCase() == "vnd" || memCurr.toLowerCase() == "idr") {
           $("#txtAmount").addClass("amount_vnd_idr");
       } else {
           $("#txtAmount").removeClass("amount_vnd_idr");
       }
   
       $('#backToDeposit').on("click", function () {
           location.href = "#";
       })
       $('#txtAmount').on('change',
           function () {
               if (this.value !== "" &&
                   this.value !== 0 &&
                   $(this).valid()) {
                   $("#btnReload1").click();
               }
           }
       );
   
       $('#ddlTargetWallet').on('change',
           function () {
               $("#btnReload").click();
           }
       );
   
       var step = $("#steps").val();
   
       if (step >= 1) {
           $('#GenerateDetails').click();
       }
   
       if (step >= 2) {
           $('#ConfirmDetails').click();
       }
   
   });
   
   function GenerateCountDown() {
       var elementCountDown = $('#ChaseCountDown');
       if (typeof elementCountDown != "undefined" && elementCountDown != null && typeof elementCountDown.html() != "undefined") {
           var countDownTime = elementCountDown.html().split(":");
           var countMinutes = countDownTime[0];
           var countSeconds = countDownTime[1];
           var setTime = new Date().setMinutes(countMinutes, countSeconds);
           var substractSecond = new Date(setTime.valueOf() - 1000);
           if (countMinutes == "00" && countSeconds == "00") {
               $('#CountdownDesc').html("Transaksi anda sedang di proses, harap menunggu!");
                   $('#chaseDepositButton').html("Tanya Status");
                   $('#chaseDepositButton').attr("onclick", "Page.popUpLiveChat()");
               } else {
                   elementCountDown.html((substractSecond.getMinutes() < 10 ? '0' : '') + substractSecond.getMinutes() + ":" + (substractSecond.getSeconds() < 10 ? '0' : '') + substractSecond.getSeconds());
               }
           }
       }
   
       function chaseDepositMessage() {
           Page.message("Permintaan anda sedang kami proses, harap menunggu!");
   }
   
   $(function () {
       $('#dateDeposit').fdatepicker({
           format: 'dd/mm/yyyy',
           endDate: new Date(),
           disableDblClickSelection: true,
           leftArrow: '<<',
           rightArrow: '>>',
           closeIcon: 'X',
           closeButton: true,
           keyboardNavigation: false
       });
   
   });
</script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.signalR-2.3.0.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}Assets/Scripts/ext/pnotify.custom.min.js"></script>
<script src="{{APP_ASSETS}}signalr/hubs.js"></script>
<script type="text/javascript">
   var boLang = "id-id";
   
   $('#mobile-notification-received').on("click", function () {
       $('.side-panel-notification-setting').empty();
       $('.notifyNoDisplay').text("0");
       $('.notifyNoDisplay').hide();
       window.open('#', '_blank');
   });
   
   var stack_bottomright = { "dir1": "up", "dir2": "left", "spacing1": 0, "spacing2": 0 };;
   
   function notifyMe(msg, iconClass, activePages) {
       PNotify.desktop.permission();
       var pNotify;
       if (Helper.IsDesktopApps === "true" && (Helper.IsWindows7 === "true" || Helper.IsWindows8 === "true" || Helper.IsWindows8Dot1 === "true" || Helper.IsWindows10 === "true")) {
           pNotify = (new PNotify({
               title: 'FUN88',
               icon: 'notificationIcon icon ' + iconClass,
               text: msg,
               addclass: "stack-bottomright",
               stack: stack_bottomright,
               delay: 30000,
               desktop: {
                   icon: '/Assets/images/General/fun88_notification_logo.png'
               },
               buttons: {
                   closer: true
               }
           }));
       } else {
           pNotify = (new PNotify({
               title: 'FUN88',
               icon: 'notificationIcon icon ' + iconClass,
               text: msg,
               desktop: {
                   desktop: true,
                   tag: msg,
                   fallback: true,
                   icon: '/Assets/images/General/fun88_notification_logo.png'
               },
               addclass: "stack-bottomright",
               stack: stack_bottomright,
               delay: 30000,
               history: false,
               buttons: {
                   closer: true
               }
           }));
       }
   
       getLatestNotificationData(activePages).then(function (result) {
           var notificationData = result.split(',');
   
           pNotify.get().click(function (e) {
               PNotify.removeAll();
               pNotify.remove();
               if ($('.ui-pnotify-closer, .ui-pnotify-sticker, .ui-pnotify-closer *, .ui-pnotify-sticker *').is(e.target)) return;
               setTimeout(function () {
                   window.open(notificationData[0], "_blank");
               }, 1000);
           });
   
           var rptPushNotification = $('#templateHolder > .side-panel-setting-notification-content-table').eq(0).clone(true);
           $(rptPushNotification).attr('id', notificationData[1]);
           $(rptPushNotification).find(".notification-child-text").html(msg);
           $(rptPushNotification).find(".notification-child-icon-holder").removeClass("icon-notification-announcement").addClass(iconClass);
           $('.side-panel-setting-notification-content-wrapper').prepend($(rptPushNotification));
       });
   }
   
   function getLatestNotificationData(activePages) {
       return new Promise(function(resolve, reject) {
           $.ajax({
               type: "GET",
               url: "#",
               data: { firstLoad: "true" },
               success: function (data) {
                   if (typeof data != "undefined") {
                       if (data != null) {
                           var notificationArr = data.broadCastList;
   
                           if (notificationArr != null) {
                               var notificationId = notificationArr[0].Id.split('_');
   
                               var notificationRedirectionLink = activePages == "announcement"
                               ? "#" + notificationArr[0].Category + "&id=" + notificationId[1]
                               : "#" + notificationArr[0].Category + "&id=" + notificationId[1];
   
                               var notificationSidePanelId = notificationArr[0].Category + '_' + notificationArr[0].Id;
   
                               resolve(notificationRedirectionLink + ',' + notificationSidePanelId);
                           }
                       }
                   }
               }
           });
       });
   }
   
   function isNewNotificationSupported() {
       if (!window.Notification || !Notification.requestPermission)
           return false;
       if (Notification.permission == 'granted')
           throw new Error('You must only call this \*before\* calling Notification.requestPermission(), otherwise this feature detect would bug the user with an actual notification!');
       try {
           new Notification('');
       } catch (e) {
           if (e.name == 'TypeError')
               return false;
       }
       return true;
   }
   function addNewNotificationValue() {
       var notificationNumber = Number($('.notifyNoDisplay').first().text()) + 1;
       $(".notifyNoDisplay").text(notificationNumber);
       $(".notifyNoDisplayMobile").text(notificationNumber);
       $('.notifyNoDisplay').show();
   }
   $(function () {
       if ($.connection.hub && $.connection.hub.state === $.signalR.connectionState.reconnecting) {
           var chats = $.connection.chatHub;
           chats.stop();
           setTimeout(function (){
               chats.start();
           },
           5000);
       }
       if ($.connection.hub && $.connection.hub.state === $.signalR.connectionState.disconnected) {
           var chat = $.connection.chatHub;
           chat.client.broadcastLiveMatchMessage = function (message) {
               var data = JSON.parse(message);
   
               if (data !== null) {
                   $.each(data, function (i, match) {
                       var team = $(".home-sport-team")[i];
                       var location = $(".home-sport-location-time")[i];
                       var objDate = new Date(match.MatchStartTime);
   
                       team.innerHTML = match.HomeTeamName + " vs " + match.AwayTeamName;
   
                       location.innerHTML = match.league + " " + objDate.format("MMM dd, hh:mm tt ");
                   });
               }
           };
   
           chat.client.broadcastMessage = function (message, language) {
               try {
                   if (language.toLowerCase() === boLang) {
                       addNewNotificationValue();
                       if (!Helper.IsMobile) {
                           notifyMe(message, 'icon-notification-announcement', 'announcement');
                       }
   
                   }
               } catch (err) {
   
               }
           };
   
           chat.client.privateMessage = function (message, category) {
               try {
                   addNewNotificationValue();
                   if (!Helper.IsMobile) {
                       $.ajax({
                           type: "GET",
                           url: "/Services/BroadcastMessageService.ashx",
                           data: { Message: message, Category: category },
                           success: function (data) {
                               var broadcastMessage = data.broadCastList;
                               notifyMe(broadcastMessage[0].Message, broadcastMessage[0].IconClass, 'notification');
                           }
                       });
                   }
               } catch (err) {
   
               }
           };
   
           chat.client.refreshPromotion = function () {
               try {
                   if (document.location.pathname.indexOf('/promo') >= 0) {
                       FillUpPromotionPage();
                   }
               } catch (err) {
   
               }
           };
       }
   
       $.connection.hub.start();
   });
</script>
@endsection