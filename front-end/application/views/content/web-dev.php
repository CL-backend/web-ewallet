@extends('main.base_head_foot')
@section('title', $title)
@section('content')



         <div id="site-content" class="site-content">
            
            <div id="content-body" class="content-body">
               <div class="content-body-inner wrap">
                     <!-- The main content -->
                     <main id="main-content" class="main-content" itemprop="mainContentOfPage">
                        <div class="main-content-inner">             <div class="content">
            <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section mask vc_custom_1536718432033 vc_section-has-fill" style="padding: 50px;"><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1540432280435"><div class="wpb_column vc_column_container vc_col-sm-7"><div class="vc_column-inner"><div class="wpb_wrapper"><div style="font-size: 3rem;color: #ffffff;text-align: left" class="vc_custom_heading delay-1 custom-hd wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1536309433909" ></div><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>
<h2 style="font-size: 6rem;color: #ffffff;line-height: 0.9;text-align: left" class="vc_custom_heading custom-hd delay-2 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" ></h2><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_10 vc_sep_border_width_5 vc_sep_pos_align_left vc_separator_no_text vc_sep_color_white vc_custom_1540432371541  vc_custom_1540432371541 delay-4 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" >
</div>
   <div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1540432167152 delay-3" >
      <div class="wpb_wrapper">
         

      </div>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-5"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div></section><div class="vc_row-full-width vc_clearfix"></div><section id="overview" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1536315102680"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="wpb_widgetised_column wpb_content_element">
      <div class="wpb_wrapper">
         
         <div id="nav_menu-7" class="widget-odd widget-last widget-first widget-1 fixed-menu widget widget_nav_menu"><div class="menu-services-container"><ul id="menu-services" class="menu"><li id="menu-item-1191" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1191"><a href="#overview" data-ps2id-api="true">Overview</a></li>
<li id="menu-item-1193" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1193"><a href="#solutions" data-ps2id-api="true">What We Do</a></li>
<li id="menu-item-1200" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1200"><a href="#site-footer" data-ps2id-api="true">Contact us</a></li>
</ul></div></div>
      </div>
   </div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 80px" ><span class="vc_empty_space_inner"></span></div>
<h6 style="font-size: 10px;color: #627792;text-align: center" class="vc_custom_heading style1" >What's The Difference?</h6><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="text-align: center;color: #BE2026" class="vc_custom_heading" >Web Design</h3><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>

   <div class="wpb_text_column wpb_content_element " >
      <div class="wpb_wrapper">
         <p style="text-align: justify;color: #000;">Web design encompasses many different skills and disciplines in the production and maintenance of websites. The different areas of web design include web graphic design; interface design; authoring, including standardised code and proprietary software; user experience design; and search engine optimization. </p>

      </div>
   </div>
   <div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="text-align: center;color: #BE2026" class="vc_custom_heading" >Web Development</h3><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>

   <div class="wpb_text_column wpb_content_element " >
      <div class="wpb_wrapper">
         <p style="text-align: justify;color: #000;">Web development is the work involved in developing a web site for the Internet or an intranet. Web development can range from developing a simple single static page of plain text to complex web-based internet applications electronic businesses, and social network services. </p>

      </div>
   </div>
<div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner">

   <div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322833940"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/08/royal-2995_51c8d05c-efe9-46d9-9a17-4b4e8fccfaad.svg" alt="Years experience" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="30" data-speed="1000">0</span><span class="counter-suffix">+</span>
      </h3>
   <div class="counter-title">Years experience</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322962219"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/diploma-2983_a6bb0b64-dbc1-431e-ac00-a83597982a0.svg" alt="Certified experts" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="250" data-speed="1000">0</span><span class="counter-suffix"></span>
      </h3>
   <div class="counter-title">Certified experts</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322997713"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/08/user-group-2874_87351201-7b45-463a-9b86-0ebd80aa8fa0.svg" alt="End user satisfaction" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="100" data-speed="1000">0</span><span class="counter-suffix">%</span>
      </h3>
   <div class="counter-title">End user satisfaction</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536323036950"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/world-map-826_e5eaf722-e834-484a-b7a6-0b1f4ab98ee.svg" alt="Global reach" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="7" data-speed="1000">0</span><span class="counter-suffix">countries</span>
      </h3>
   <div class="counter-title">Global reach</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536323065309"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/headphones-user-5252_ca8ffcdb-4e36-4baa-8e93-4fb725476d7.svg" alt="Service desk" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="365" data-speed="1000">0</span><span class="counter-suffix">24/7</span>
      </h3>
   <div class="counter-title">Service desk</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 10px" ><span class="vc_empty_space_inner"></span></div>

   <div class="wpb_text_column wpb_content_element " >
      <div class="wpb_wrapper">
         <p style="text-align: center; font-size: 15px;">Our Managed IT services will help you succeed. <a class="dot" href="{{site_url()}}/test/about">Let’s get started</a></p>

      </div>
   </div>
</div></div></div></div></section><div class="vc_row-full-width vc_clearfix"></div><section id="solutions" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1540522045425 vc_section-has-fill"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="color: #ffffff;text-align: center" class="vc_custom_heading" >What We Do</h3><div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
<ul class="iconlist iconlist iconlist-icon-small columns-2 gap-50"><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/router-1807_67aa302b-3a94-46a7-aa3d-66b8928a87d7.svg" alt="router-1807_67aa302b-3a94-46a7-aa3d-66b8928a87d7" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">Website and Web Applications</span></h4><p><span style="color: #9b9aad">With deep insights into your business, we deliver attractive websites which enhance existing channels, and create new ones to your target market. Our solutions include corporate websites, branding websites and various web applications.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/telephone-operator-4682_c9489618-836b-47ec-8489-e15f613cb10c.svg" alt="telephone-operator-4682_c9489618-836b-47ec-8489-e15f613cb10c" /></div>

   <div class="iconlist-item-content"><h4><span style="color: #ffffff">e-Commerce</span></h4><p><span style="color: #9b9aad">The amount of trade on internet is growing faster than ever in Singapore and globally. Based on your business size and specific needs, we provide the most suitable and cost-effective e-commerce solutions which create pleasant shopping experience for your customers.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}/wp-content/uploads/2018/09/computer-network-1878_39828809-88f9-48e1-9a76-61c99401ec99.svg" alt="computer-network-1878_39828809-88f9-48e1-9a76-61c99401ec99" /></div>

      <div class="iconlist-item-content"><h4><span style="color: #ffffff">Enterprise Software Solutions</span></h4><p><span style="color: #9b9aad">We deliver software solutions for various industries including government sectors, finance, manufacturing, health care and logistics. These solutions facilitate efficient information, material, financial, human resource and customer relationship management for corporations.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/settings-server-1872_2e41baf2-8789-4215-b430-db35c3899936.svg" alt="settings-server-1872_2e41baf2-8789-4215-b430-db35c3899936" /></div>

         <div class="iconlist-item-content"><h4><span style="color: #ffffff">Mobile Applications</span></h4><p><span style="color: #9b9aad">The use of mobile applications has become increasingly prevalent. We develop applications for mobile devices (phone and tablet) across different platforms (Android, Windows and Apple iOS).</span></div></div></li>
         </ul></div></div></div></div>
      </section><div class="vc_row-full-width vc_clearfix"></div>




      <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1536034816290"><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper">



<div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="text-align: center;color: #BE2026;" class="vc_custom_heading" >Cypherlabs Client</h3>
<div class="vc_empty_space"   style="height: 40px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid wpb_animate_when_almost_visible wpb_bounceIn bounceIn vc_column-gap-35 vc_row-o-content-middle vc_row-flex"><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://user.com.sg/wp-content/uploads/2017/07/2.jpg" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://www.user.com.sg/wp-content/uploads/2015/08/11.jpg" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://www.user.com.sg/wp-content/uploads/2015/08/govtech.png" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://www.user.com.sg/wp-content/uploads/2015/08/33.jpg" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://www.user.com.sg/wp-content/uploads/2015/08/44.png" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2 vc_col-xs-4"><div class="vc_column-inner"><div class="wpb_wrapper">
   <div  class="wpb_single_image wpb_content_element vc_align_center">

      <figure class="wpb_wrapper vc_figure">
         <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://www.user.com.sg/wp-content/uploads/2015/08/yokogawa.png" class="vc_single_image-img attachment-full" alt="" /></div>
      </figure>
   </div>
</div></div></div></div></section>
<div class="vc_row-full-width vc_clearfix"></div>



<section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section fix-overflow wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1540353749121"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner"><div class="wpb_wrapper"><h6 style="font-size: 10px;color: #627792;text-align: center" class="vc_custom_heading style1" >Testimonials</h6><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="text-align: center;color: #BE2026;" class="vc_custom_heading" >What our customers say</h3><div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><!-- BEGIN: .elements-carousel -->
<div class="elements-carousel  " data-config="{&quot;items&quot;:3,&quot;itemsTablet&quot;:[768,2],&quot;itemsMobile&quot;:[479,1],&quot;autoPlay&quot;:false,&quot;stopOnHover&quot;:true,&quot;mouseDrag&quot;:true,&quot;touchDrag&quot;:true,&quot;responsive&quot;:true,&quot;scrollPerPage&quot;:true,&quot;slideSpeed&quot;:200,&quot;paginationSpeed&quot;:200,&quot;rewindSpeed&quot;:200,&quot;navigation&quot;:false,&quot;rewindNav&quot;:false,&quot;pagination&quot;:true,&quot;paginationNumbers&quot;:false,&quot;dragBeforeAnimFinish&quot;:true,&quot;addClassActive&quot;:true,&quot;autoHeight&quot;:true,&quot;navigationText&quot;:[&quot;Previous&quot;,&quot;Next&quot;],&quot;itemsScaleUp&quot;:true}">
   <div class="elements-carousel-wrap">
      
   <div class="testimonial   vc_custom_1539848935838 has-image">
      <div class="testimonial-wrap">
         <div class="testimonial-content">
            
      <div class="testimonial-image">
         <img src="{{APP_ASSETS}}wp-content/uploads/2018/09/j-c.svg" alt="John H. Bedard, Jr" />
      </div>
               <div class="testimonial-text"><p>&#8220;I sleep easier at night knowing the NanoSoft team is in my corner. Supporting my business and keeping my systems in Tip-Top shape&#8221;</p>
</div>
         </div>

         <div class="author-info">
            <h6 class="author-name">John H. Bedard, Jr</h6>
            <span class="subtitle">Office Manager</span>       </div>
      </div>
   </div>

   <div class="testimonial   vc_custom_1539848941825 has-image">
      <div class="testimonial-wrap">
         <div class="testimonial-content">
            
      <div class="testimonial-image">
         <img src="{{APP_ASSETS}}wp-content/uploads/2018/09/box-c.svg" alt="John Doe" />
      </div>
               <div class="testimonial-text"><p>“NanoSoft are always accommodating our diverse needs and we feel like they are a part of our Company rather than an external supplier”</p>
</div>
         </div>

         <div class="author-info">
            <h6 class="author-name">John Doe</h6>
            <span class="subtitle">CEO</span> <span class="divider">-</span> <span class="company">Envato</span>        </div>
      </div>
   </div>

   <div class="testimonial   vc_custom_1539848946651 has-image">
      <div class="testimonial-wrap">
         <div class="testimonial-content">
            
      <div class="testimonial-image">
         <img src="{{APP_ASSETS}}wp-content/uploads/2018/09/k-c.svg" alt="John H. Bedard, Jr" />
      </div>
               <div class="testimonial-text"><p>“Being a managed services client has improved our pptime, increased our productivity and systematized our software updates”</p>
</div>
         </div>

         <div class="author-info">
            <h6 class="author-name">John H. Bedard, Jr</h6>
            <span class="subtitle">Chris Ellison</span> <span class="divider">-</span> <span class="company">CEO and Co-founder</span>       </div>
      </div>
   </div>

   <div class="testimonial   vc_custom_1539848952548 has-image">
      <div class="testimonial-wrap">
         <div class="testimonial-content">
            
      <div class="testimonial-image">
         <img src="{{APP_ASSETS}}wp-content/uploads/2018/09/fu-c.svg" alt="John H. Bedard, Jr" />
      </div>
               <div class="testimonial-text"><p>&#8220;I sleep easier at night knowing the NanoSoft team is in my corner. Supporting my business and keeping my systems in Tip-Top shape&#8221;</p>
</div>
         </div>

         <div class="author-info">
            <h6 class="author-name">John H. Bedard, Jr</h6>
            <span class="subtitle">Office Manager</span>       </div>
      </div>
   </div>
   </div>
</div>
<!-- END: .elements-carousel -->
</div></div></div></div></section><div class="vc_row-full-width vc_clearfix"></div>

<br>
<br>
<section class="vc_section wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1537850404651"><div class="vc_row wpb_row vc_row-fluid"><div class="delay-1 wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><h6 style="font-size: 10px;color: #627792;text-align: center" class="vc_custom_heading style1" >Pricing and Plan</h6><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<!-- <h3 style="text-align: center" class="vc_custom_heading" >1 monthly fee for all IT services. No costly surprises</h3> --><div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
<div class="pricing-table three-columns ">
      <div class="price-column ">
         <div class="column-container">
            <div class="plan">
               <h4>Standard Web Design</h4>
               <span>Designed for businesses with basic IT requirements</span>
            </div> 
            <div class="price"><span>$800 ++</span> /month 
               <div class="des">Pricing includes coverage for users</div></div>
            <ul class="features">
               <li><span>All careBasic services include:</span>
               </li><li>24/7 system monitoring</li>
               <li>Security management</li>
               <li>Patch management</li>
               <li>Remote support</li>
            </ul>
            <!-- <div class="cta"><a class="button" href="#">Start free trial</a></div> --></div></div>

      <div class="price-column highlight"><div class="popular">Most Popular</div><div class="column-container"><div class="plan"><h4>carePlus</h4><span>Designed for businesses looking to eliminate costly break/fix IT services</span></div> <div class="price"><span>$2000</span> /month <div class="des">Pricing includes coverage for users</div></div><ul class="features"><li><span>All carePlus services include:</span></li><li>Preventive maintenance</li><li>Asset management</li><li>Secure cloud backup</li><li>Server/Network support</li></ul><!-- <div class="cta"><a class="button" href="#">Start free trial</a></div> --></div></div>

      <div class="price-column "><div class="column-container"><div class="plan"><h4>Customized Web Development</h4><span>A fully comprehensive plan for any business size or needs.</span></div> <div class="price"><span>$5000++</span> /month <div class="des">Pricing includes coverage for users</div></div><ul class="features"><li><span>All carePro services include:</span></li><li>Reporting</li><li>Vendor management</li><li>Virtual CIO (vCIO)</li><li>Workstation support</li></ul><!-- <div class="cta"><a class="button" href="#">Start free trial</a></div> --></div></div>

      </div></div></div></div></div></section>

      <div class="vc_row-full-width vc_clearfix"></div>




<div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div>
<div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner"><div class="wpb_wrapper"><div role="form" class="wpcf7" id="wpcf7-f4-p239-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="http://demo.linethemes.com/nanosoft/contact-us/#wpcf7-f4-p239-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="4">
<input type="hidden" name="_wpcf7_version" value="5.0.5">
<input type="hidden" name="_wpcf7_locale" value="en_US">
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f4-p239-o1">
<input type="hidden" name="_wpcf7_container_post" value="239">
</div>
<div class="contact">
<div class="row">
   <div class="columns columns-12"><h4 align="center" style="padding-bottom: 10px;">Contact Cypherlabs Team To Get the Fastest Responses</h4></div>
<div class="columns columns-6"><label>Full name * <span class="wpcf7-form-control-wrap text-228"><input type="text" name="text-228" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="e.g., John Doe"></span></label></div>
<div class="columns columns-6"><label>Organization * <span class="wpcf7-form-control-wrap text-228"><input type="text" name="text-228" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Company Name"></span></label></div>
</div>
<div class="row">
<div class="columns columns-6"><label>Work email address * <span class="wpcf7-form-control-wrap email-322"><input type="email" name="email-322" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" placeholder="name@company.com"></span></label></div>
<div class="columns columns-6"><label>Phone number <span class="wpcf7-form-control-wrap tel-744"><input type="tel" name="tel-744" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-tel wpcf7-validates-as-tel" aria-invalid="false" placeholder="Full Number (incl. prefix)"></span></label></div>
</div>
<div class="row">
<div class="columns columns-6"><label>Company size * <span class="wpcf7-form-control-wrap menu-570"><select name="menu-570" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="Please Select">Please Select</option><option value="1 (freelancer)">1 (freelancer)</option><option value="2-19">2-19</option><option value="20-49">20-49</option><option value="50+">50+</option></select></span></label></div>
<div class="columns columns-6"><label>What is your inquiry about? * <span class="wpcf7-form-control-wrap menu-570"><select name="menu-570" class="wpcf7-form-control wpcf7-select wpcf7-validates-as-required" aria-required="true" aria-invalid="false"><option value="Please Select">Please Select</option><option value="General Information Request">General Information Request</option><option value="Partner Relations">Partner Relations</option><option value="Careers">Careers</option><option value="Software Licencing">Software Licencing</option><option value="I need help">I need help</option></select></span></label></div>
</div>
<div class="row">
<div class="columns columns-12"><label>How we can help you? * <span class="wpcf7-form-control-wrap textarea-431"><textarea name="textarea-431" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required" aria-required="true" aria-invalid="false" placeholder="Let us know what you need."></textarea></span></label></div>
</div>
<div class="row">
<div class="columns columns-12"><input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit primary"><span class="ajax-loader"></span></div>
</div>
</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div></div></div></div>


                     </div>
         <!-- /.content -->

                                       </div>
                     <!-- /.main-content-inner -->
                  </main>
                  <!-- /.main-content -->

                     
                  </div>
               <!-- /.content-body-inner -->
            </div>
            <!-- /.content-body -->       
         </div>
         <!-- /.site-content -->





@endsection

@section('css')

   <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1536718432033{background: url(http://35.197.138.252/cypherlabs.com/assets/images/header-webdesign.jpg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1536315102680{padding-bottom: 60px !important;}.vc_custom_1540522045425{padding-top: 80{{APP_ASSETS}}px !important;padding-bottom: 30px !important;background-color: #181223 !important;}.vc_custom_1536330643232{padding-top: 80px !important;padding-bottom: 80px !important;}.vc_custom_1536308212411{background: #fc766a url(http://demo.linethemes.com/nanosoft/wp-content/uploads/2018/08/job12-279-chim-01065.jpg?id=1062) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1536309193694{padding-top: 80px !important;}.vc_custom_1540432280435{padding-top: 120px !important;padding-bottom: 100px !important;}.vc_custom_1536309433909{margin-left: 3px !important;}.vc_custom_1540432371541{margin-bottom: 50px !important;margin-left: 5px !important;padding-top: 15px !important;}.vc_custom_1540432167152{margin-left: 3px !important;}.vc_custom_1536322833940{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536322962219{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536322997713{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536323036950{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536323065309{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536205294665{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205301947{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205308214{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205314909{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205321845{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536285924597{padding-top: 120px !important;padding-bottom: 120px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
@endsection