@extends('main.base_head_foot')
@section('title', $title)
@section('content')



         <div id="site-content" class="site-content">
            
            <div id="content-body" class="content-body">
               <div class="content-body-inner wrap">
                     <!-- The main content -->
                     <main id="main-content" class="main-content" itemprop="mainContentOfPage">
                        <div class="main-content-inner">             <div class="content">
            <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section mask vc_custom_1536718432033 vc_section-has-fill" style="padding: 50px;"><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1540432280435"><div class="wpb_column vc_column_container vc_col-sm-7"><div class="vc_column-inner"><div class="wpb_wrapper"><div style="font-size: 3rem;color: #ffffff;text-align: left" class="vc_custom_heading delay-1 custom-hd wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1536309433909" ></div><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>
<h2 style="font-size: 6rem;color: #ffffff;line-height: 0.9;text-align: left" class="vc_custom_heading custom-hd delay-2 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" ></h2><div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_10 vc_sep_border_width_5 vc_sep_pos_align_left vc_separator_no_text vc_sep_color_white vc_custom_1540432371541  vc_custom_1540432371541 delay-4 wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp" >
</div>
   <div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1540432167152 delay-3" >
      <div class="wpb_wrapper">
        

      </div>
   </div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-5"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div></section><div class="vc_row-full-width vc_clearfix"></div><section id="overview" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1536315102680"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs"><div class="vc_column-inner"><div class="wpb_wrapper"><div  class="wpb_widgetised_column wpb_content_element">
      <div class="wpb_wrapper">
         
         <div id="nav_menu-7" class="widget-odd widget-last widget-first widget-1 fixed-menu widget widget_nav_menu"><div class="menu-services-container"><ul id="menu-services" class="menu"><li id="menu-item-1191" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1191"><a href="#overview" data-ps2id-api="true">Overview</a></li>
<li id="menu-item-1193" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1193"><a href="#solutions" data-ps2id-api="true">What We Do</a></li>
<li id="menu-item-1200" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1200"><a href="#site-footer" data-ps2id-api="true">Contact us</a></li>
</ul></div></div>
      </div>
   </div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 80px" ><span class="vc_empty_space_inner"></span></div>
<h6 style="font-size: 10px;color: #000;text-align: center" class="vc_custom_heading style1" >overview</h6><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="text-align: center;color: #BE2026" class="vc_custom_heading" >What is UX Research?</h3><div class="vc_empty_space"   style="height: 20px" ><span class="vc_empty_space_inner"></span></div>

   <div class="wpb_text_column wpb_content_element " >
      <div class="wpb_wrapper">
         <p style="text-align: justify;color: #000;">UX (user experience) research is the systematic investigation of users and their requirements, in order to add context and insight into the process of designing the user experience. UX research employs a variety of techniques, tools, and methodologies to reach conclusions, determine facts, and uncover problems, thereby revealing valuable information which can be fed into the design process.</p>

      </div>
   </div>
<div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-2"><div class="vc_column-inner"><div class="wpb_wrapper"></div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322833940"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/08/royal-2995_51c8d05c-efe9-46d9-9a17-4b4e8fccfaad.svg" alt="Years experience" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="30" data-speed="1000">0</span><span class="counter-suffix">+</span>
      </h3>
   <div class="counter-title">Years experience</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322962219"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/diploma-2983_a6bb0b64-dbc1-431e-ac00-a83597982a0.svg" alt="Certified experts" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="250" data-speed="1000">0</span><span class="counter-suffix"></span>
      </h3>
   <div class="counter-title">Certified experts</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536322997713"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/08/user-group-2874_87351201-7b45-463a-9b86-0ebd80aa8fa0.svg" alt="End user satisfaction" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="100" data-speed="1000">0</span><span class="counter-suffix">%</span>
      </h3>
   <div class="counter-title">End user satisfaction</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536323036950"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/world-map-826_e5eaf722-e834-484a-b7a6-0b1f4ab98ee.svg" alt="Global reach" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="7" data-speed="1000">0</span><span class="counter-suffix">countries</span>
      </h3>
   <div class="counter-title">Global reach</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-1/5 vc_col-xs-6"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="counter aligncenter  vc_custom_1536323065309"><div class="counter-image"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/headphones-user-5252_ca8ffcdb-4e36-4baa-8e93-4fb725476d7.svg" alt="Service desk" /></div> <div class="counter-detail">
      <h3 class="counter-content">
         <span class="counter-prefix"></span><span class="counter-value" data-from="0" data-to="365" data-speed="1000">0</span><span class="counter-suffix">24/7</span>
      </h3>
   <div class="counter-title">Service desk</div></div></div><div class="vc_empty_space"   style="height: 30px" ><span class="vc_empty_space_inner"></span></div>
</div></div></div></div><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 10px" ><span class="vc_empty_space_inner"></span></div>

   <div class="wpb_text_column wpb_content_element " >
      <div class="wpb_wrapper">
         <p style="text-align: center; font-size: 15px;">Our Managed IT services will help you succeed. <a class="dot" href="{{site_url()}}/test/about">Let’s get started</a></p>

      </div>
   </div>
</div></div></div></div></section>

<div class="vc_row-full-width vc_clearfix"></div><section id="solutions" data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section wpb_animate_when_almost_visible wpb_fadeInUp fadeInUp vc_custom_1540522045425 vc_section-has-fill"><div class="vc_row wpb_row vc_row-fluid"><div class="wpb_column vc_column_container vc_col-sm-12"><div class="vc_column-inner"><div class="wpb_wrapper"><div class="vc_empty_space"   style="height: 6px" ><span class="vc_empty_space_inner"></span></div>
<h3 style="color: #ffffff;text-align: center" class="vc_custom_heading" >What We Do</h3><div class="vc_empty_space"   style="height: 60px" ><span class="vc_empty_space_inner"></span></div>
<ul class="iconlist iconlist iconlist-icon-small columns-2 gap-50"><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/router-1807_67aa302b-3a94-46a7-aa3d-66b8928a87d7.svg" alt="router-1807_67aa302b-3a94-46a7-aa3d-66b8928a87d7" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">User Interviews</span></h4><p><span style="color: #9b9aad">User interviews are structured conversations with your current and potential users. By talking with the users, we gather insights into their requirements and preferences about products.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/telephone-operator-4682_c9489618-836b-47ec-8489-e15f613cb10c.svg" alt="telephone-operator-4682_c9489618-836b-47ec-8489-e15f613cb10c" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">Contextual Inquiry</span></h4><p><span style="color: #9b9aad">In contextual inquiry, we observe users when they interact with products. This approach uncovers customer behaviors and the real-life problems they face when working with the products.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}/wp-content/uploads/2018/09/computer-network-1878_39828809-88f9-48e1-9a76-61c99401ec99.svg" alt="computer-network-1878_39828809-88f9-48e1-9a76-61c99401ec99" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">Usability Testing</span></h4><p><span style="color: #9b9aad">In usability testing, customers perform a series of tasks with interactive products. We observe customer behaviours, and detect usability issues of the products. We also make effective suggestions on how to address these issues and improve the current design.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/settings-server-1872_2e41baf2-8789-4215-b430-db35c3899936.svg" alt="settings-server-1872_2e41baf2-8789-4215-b430-db35c3899936" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">Surveys</span></h4><p><span style="color: #9b9aad">Surveys are quantitative techniques to understand users behaviors and needs. Current and potential users are approached via phone, face-to-face and online questionnaires.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/source-code-1754_2b435bd8-ce76-4910-8137-7d07a3557fa3.svg" alt="source-code-1754_2b435bd8-ce76-4910-8137-7d07a3557fa3" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">Focus Group</span></h4><p><span style="color: #9b9aad">Focus groups bring together all stakeholders and facilitate deep discussions with them. A focus group enables a good understanding on different personal experiences among users.</span></div></div></li><li><div ><div class="iconlist-item-icon"><img src="{{APP_ASSETS}}wp-content/uploads/2018/09/add-image-5030_dcf585b8-8f3d-48ad-8579-a4ad56d14ba6.svg" alt="add-image-5030_dcf585b8-8f3d-48ad-8579-a4ad56d14ba6" /></div><div class="iconlist-item-content"><h4><span style="color: #ffffff">A/B and Multivariate Testing</span></h4><p><span style="color: #9b9aad">A/B testing and multivariate testing are also conducted online, especially for website and mobile application design evaluations. These tests are effective tools to achieve the optimal design for your products. By manipulating the design elements, the profile resulting in the optimal user experience is determined.</span></div></div></li></ul></div></div></div></div>

</section><div class="vc_row-full-width vc_clearfix"></div>


                     </div>
         <!-- /.content -->

                                       </div>
                     <!-- /.main-content-inner -->
                  </main>
                  <!-- /.main-content -->

                     
                  </div>
               <!-- /.content-body-inner -->
            </div>
            <!-- /.content-body -->       
         </div>
         <!-- /.site-content -->





@endsection

@section('css')

   <style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1536718432033{background: url(http://35.197.138.252/cypherlabs.com/assets/images/header-research.jpg) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1536315102680{padding-bottom: 60px !important;}.vc_custom_1540522045425{padding-top: 80{{APP_ASSETS}}px !important;padding-bottom: 30px !important;background-color: #181223 !important;}.vc_custom_1536330643232{padding-top: 80px !important;padding-bottom: 80px !important;}.vc_custom_1536308212411{background: #fc766a url(http://demo.linethemes.com/nanosoft/wp-content/uploads/2018/08/job12-279-chim-01065.jpg?id=1062) !important;background-position: center !important;background-repeat: no-repeat !important;background-size: cover !important;}.vc_custom_1536309193694{padding-top: 80px !important;}.vc_custom_1540432280435{padding-top: 120px !important;padding-bottom: 100px !important;}.vc_custom_1536309433909{margin-left: 3px !important;}.vc_custom_1540432371541{margin-bottom: 50px !important;margin-left: 5px !important;padding-top: 15px !important;}.vc_custom_1540432167152{margin-left: 3px !important;}.vc_custom_1536322833940{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536322962219{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536322997713{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536323036950{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536323065309{padding-top: 15% !important;padding-right: 10% !important;padding-bottom: 15% !important;padding-left: 10% !important;background-color: #f3f6f9 !important;border-radius: 5px !important;}.vc_custom_1536205294665{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205301947{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205308214{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205314909{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536205321845{border-top-width: 7px !important;border-top-color: #ff4b36 !important;border-top-style: solid !important;}.vc_custom_1536285924597{padding-top: 120px !important;padding-bottom: 120px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>
@endsection