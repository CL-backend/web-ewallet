@extends('main.base_head_foot')
@section('title', $title) 
@section('css')
 
@endsection
@section('content')
<div class="large-12 row expanded">
   <div class="slot-banner-Wrapper">
      <div class="slot-banner-slider">
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-1.png'); background-position-x: 46%!important" data-url=''
               title=""
               data-promoid='13443'
               data-action='3' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span style="color: #25aae1;">INSTANT SIGN UP <span>2 MILLION</span><br /></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid='13443'
                  data-action='3' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='banner-Button en'>GET BONUS</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-2.png'); background-position-x: 40%!important" data-url=''
               title=""
               data-promoid=''
               data-action='0' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Slot best brand MGS\nGrab iPhone XS Max &amp; Freebets everyday \nMILLIONS worth of prizes up for grabs!\nButton：Grab Now&quot;}" data-sheets-userformat="{&quot;2&quot;:515,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,14281427],&quot;12&quot;:0}"><span style="color: #25aae1;">1% REBATE</span><br /><span style="color: #25aae1;">4% CASHBACK</span></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">DUO PLUS GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid=''
                  data-action='0' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='en'>APPLY</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-3.png'); background-position-x: 24%!important" data-url=''
               title=""
               data-promoid='13422'
               data-action='3' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span style="color: #25aae1;">DAILY BONUS&nbsp;<span>3 MILLION</span><br /></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid='13422'
                  data-action='3' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='banner-Button en'>GET BONUS</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
      </div>
      <div class="slot-banner-arrow"></div>
      <div class="slot-banner-paging"></div>
      <div id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BannerContentPlaceHolderGame_SlotBannerControl_rcntWinnerPlaceHolder" class="recentWinnerHolder" style="display:none">
         <div style="position: relative;">
            <div class="recentWinnerText">
               <span>Latest Winners</span>
            </div>
            <div class="recentWinnerPlayLists">
            </div>
         </div>
      </div>
   </div>
   <div class="large-12 medium-12 small-12 row expanded" style="position: relative; z-index: 0; background-color: white;">
      <div class="large-12 medium-12 small-12 columns show-for-large" style="border-bottom: 1px solid #cacaca;">
         <div class="large-2 medium-2 columns">
            <div class="totalGamesCount">
               <span class="totalSlotsNumbers"></span>
               <span style="text-transform: uppercase;">&nbsp;Slots</span>
            </div>
         </div>
         <div class="large-10 medium-10 columns gameBoardSelectionList">
            <ul class="gameDetailsSelectionUl">
               <li class="gameFilteringName active gamefilter" data-mixitup-control="" data-type="myBoard">
                  Game Board
               </li>
               <li style="padding-right: 0; cursor: default;">Sort By :
               </li>
               <li class="gameFilteringName bold-black-text selection gamefilter secondaryFiltering" data-mixitup-control="" data-type="recent:asc">LAST PLAYED GAME
               </li>
               <li style="padding-left: 0;" class="gameFilteringName bold-black-text selection gamefilter secondaryFiltering recommendGame" data-mixitup-control="" data-type="recommended:asc order:asc">RECOMMENDED
               </li>
               <li style="padding-left: 0;" class="gameFilteringName bold-black-text selection newGameFilter gamefilter secondaryFiltering" data-mixitup-control="" data-type="new:asc">New Games
               </li>
               <li style="padding-left: 0;" class="gameFilteringName bold-black-text selection favGameFilter gamefilter secondaryFiltering" data-mixitup-control="" data-type="fav:asc">My Favorites
               </li>
               <li style="padding-left: 0;">
                  <div class="nameSortingIcon default">
                     <a class="mobileAlphabetSorting defaultAlphabetSorting" data-order="name:asc">A-Z
                     </a>
                  </div>
               </li>
            </ul>
            <div class="filter-group search gameSearchField">
               <div style="position: relative;">
                  <input type="text" class="gameSearchBox"
                     placeholder="Search" />
                  <div id="search-button-close" style="position: absolute; top: 5px; right: 5px; display: none; cursor: pointer;">
                     <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/General/close_button.png" style="height: 20px;" width="20" height="20"/>
                  </div>
               </div>
            </div>
            <div style="position: absolute; right: 6%; top: 27%;">
               <div class="icon icon-select_list imagePopulateView gameMasterIcon" data-view="grid" data-grid-clicked="false">
               </div>
               <div class="icon icon-details_symbol imagePopulateView gameMasterIcon" data-view="list" data-list-clicked="false" style="left: 40px;">
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="large-12 medium-12 small-12 row expanded" id="resultFilteringHolder">
      <!-- MOBILE FILTERING AND SORTING-->
      <div class="hide-for-large medium-12 columns mobileFilterSortSection">
         <div class="large-12 medium-12 small-12 row expanded" style="min-height: 900px;">
            <div class="filterHeaderMobile">
               <span>Filter & Sort</span>
               <span class="icon icon icon-close_btn closeButtonFilterPanel"></span>
            </div>
            <div class="small-12 row expanded" style="margin-top: 30px;">
               <span class="mobileTitleFilter">Sort By :</span>
            </div>
            <div class="small-12 row expanded" id="mobile-sort-ddl-holder">
               <div id="titleSorting">
                  Please select
               </div>
               <div class="icon icon-filter_expand">
               </div>
               <ul id="mobile-sort-ddl">
                  <li class="mobileRadioBoxWrap" data-mixitup-control="" data-type="new:asc">New Games</li>
                  <li class="mobileRadioBoxWrap" data-mixitup-control="" data-type="fav:asc">My Favorites</li>
                  <li class="mobileRadioBoxWrap" data-mixitup-control="" data-type="recommended:asc order:asc">RECOMMENDED</li>
                  <li class="mobileRadioBoxWrap" data-mixitup-control="" data-type="recent:asc">Last Played Game</li>
               </ul>
            </div>
            <div class="small-12 row expanded" style="position: relative;">
               <div style="position: relative;">
                  <span class="mobileTitleFilter">Filter :</span>
               </div>
               <div style="position: relative;" class="mobile-filter-group search">
                  <input type="text" class="gameSearchBox" style="width: 90%; border-radius: 3px; margin-left: auto; margin-right: auto;"
                     placeholder="Search" />
                  <div id="mobile-search-button-close" style="position: absolute; top: 5px; right: 55px; display: none; cursor: pointer;">
                     <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/General/close_button.png" style="height: 20px;" width="20" height="20" />
                  </div>
               </div>
            </div>
            <div class="small-12 row expanded" style="position: relative; padding: 0 10px; color: #808080;">
               <div class="gameFilterTitle">
                  VENDOR
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".MGSQF" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".MGSQF" class="chkBoxText upperCaseLetter" style="color: #808080;">Microgaming</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".SWF" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".SWF" class="chkBoxText upperCaseLetter" style="color: #808080;">Skywind</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".TG" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".TG" class="chkBoxText upperCaseLetter" style="color: #808080;">Pragmatic Play</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".LX" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".LX" class="chkBoxText upperCaseLetter" style="color: #808080;">Laxino</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".BSG" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".BSG" class="chkBoxText upperCaseLetter" style="color: #808080;">Betsoft</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".PT" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".PT" class="chkBoxText upperCaseLetter" style="color: #808080;">Play Tech</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobile" value=".CQG" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".CQG" class="chkBoxText upperCaseLetter" style="color: #808080;">CQ9</span>
               </div>
               <fieldset class="mobile-filter-group">
                  <input type="hidden" name="mobileChkBox" value="" />
               </fieldset>
            </div>
            <div class="small-12 row expanded" style="position: relative; padding: 0 10px; margin-top: 10px; color: #808080;">
               <div class="gameFilterTitle">
                  GAME TYPE
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".hotgames" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".hotgames" class="chkBoxText upperCaseLetter" style="color: #808080;">Hot Games</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".multiplayer" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".multiplayer" class="chkBoxText upperCaseLetter" style="color: #808080;">Multiplayer</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".jackpot" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".jackpot" class="chkBoxText upperCaseLetter" style="color: #808080;">Jackpot</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".fishinggame" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".fishinggame" class="chkBoxText upperCaseLetter" style="color: #808080;">Fishing Game</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".tablegame" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".tablegame" class="chkBoxText upperCaseLetter" style="color: #808080;">Table Game</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameType" value=".othersgame" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".othersgame" class="chkBoxText upperCaseLetter" style="color: #808080;">Other</span>
               </div>
               <fieldset class="mobile-filter-group">
                  <input type="hidden" name="mobileChkBox" value="" />
               </fieldset>
            </div>
            <div class="small-12 row expanded" style="position: relative; padding: 0 10px; margin-top: 10px;">
               <div class="gameFilterTitle">
                  Game Features
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameFeatures" value=".freespin" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".freespin" class="chkBoxText upperCaseLetter" style="color: #808080;">Free Spins</span>
               </div>
               <div class="small-6 column mobile-filter-group end" style="height: 35px;">
                  <input type="checkbox" name="mobileGameFeatures" value=".bonusround" class="chkBoxGameProductGroup chkBoxFilterResult">
                  <span data-value=".bonusround" class="chkBoxText upperCaseLetter" style="color: #808080;">Game Bonus</span>
               </div>
               <fieldset class="mobile-filter-group">
                  <input type="hidden" name="mobileChkBox" value="" />
               </fieldset>
            </div>
            <div class="small-12 row expanded" style="position: relative; padding: 0 20px; margin-top: 10px; color: #808080;">
               <div style="margin-bottom: 10px;">
                  Paylines
               </div>
               <fieldset class="mobile-filter-group payLineValue">
                  <input type="text" id="mobileCombinePayLine" name="payLineRangeName" value="" />
               </fieldset>
               <span id="mobilefromAmount"></span><span>&nbsp;-&nbsp;</span><span id="mobiletoAmount"></span>
               <input type="text" id="mobilepayLineRange" name="payLineRangeName" value="" />
            </div>
         </div>
         <div class="small-12 row expanded mobile-btn-selection-holder">
            <div class="small-8 columns mobileFilterBtn" style="width: 65%;">
               <span>Filter</span>
            </div>
            <div class="small-4 columns mobileClearFilterBtn">
               <span>Clear</span>
            </div>
         </div>
      </div>
      <!-- END MOBILE FILTERING AND SORTING-->
      <div class="large-2 medium-12 columns filterSectionHolder show-for-large">
         <div style="width: 100%;">
            <span class="filterTitle show-for-large">Filter</span><a class="clearFilterGames">Clear Options</a>
         </div>
         <span class="filterTitle hide-for-large">Filter</span>
         <div class="large-12 medium-12 small-12 row expanded">
            <div class="medium-12 small-12 row expanded">
               <div class="large-12 medium-6 small-6 columns no-padding">
                  <div id="slotVendorDropDownList" class="ddlListGames">
                     <div class="filterVendorTitleName">VENDOR</div>
                     <div class="icon icon-fun88_arrow_down_triangle slotDropDownListSmallArrowButton"></div>
                  </div>
                  <div id="slotVendorList" class="ddlListChilds">
                     <ul style="list-style: none; list-style-type: none; margin-left: 5px;" id="chkBoxListProviders" class="slotFilterUnorderedList filter-group checkboxes">
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".MGSQF" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".MGSQF" class="chkBoxText chkBoxVendorText">Microgaming</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".SWF" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".SWF" class="chkBoxText chkBoxVendorText">Skywind</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".TG" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".TG" class="chkBoxText chkBoxVendorText">Pragmatic Play</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".LX" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".LX" class="chkBoxText chkBoxVendorText">Laxino</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".BSG" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".BSG" class="chkBoxText chkBoxVendorText">Betsoft</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".PT" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".PT" class="chkBoxText chkBoxVendorText">Play Tech</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".CQG" class="deksChecker chkBoxGameVendor chkBoxFilterResult">
                           <span data-value=".CQG" class="chkBoxText chkBoxVendorText">CQ9</span>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="large-12 medium-6 small-6 columns no-padding">
                  <div id="slotGameTypeDropDownList" class="ddlListGames">
                     <div class="filterGameTypeName" style="position: absolute; left: 0;">
                        GAME TYPE
                     </div>
                     <div class="icon icon-fun88_arrow_down_triangle slotDropDownListSmallArrowButton"></div>
                  </div>
                  <div id="slotGameType" class="ddlListChilds">
                     <ul style="list-style: none; list-style-type: none; margin-left: 5px;" id="chkBoxListGameProductGroup" class="slotFilterUnorderedList filter-group checkboxes">
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".hotgames" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".hotgames" class="chkBoxText upperCaseLetter">Hot Games</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".multiplayer" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".multiplayer" class="chkBoxText upperCaseLetter">Multiplayer</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".jackpot" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".jackpot" class="chkBoxText upperCaseLetter">Jackpot</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".fishinggame" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".fishinggame" class="chkBoxText upperCaseLetter">Fishing Game</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".tablegame" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".tablegame" class="chkBoxText upperCaseLetter">Table Game</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".othersgame" class="deksChecker chkBoxGameProductGroup filter-group chkBoxFilterResult">
                           <span data-value=".othersgame" class="chkBoxText upperCaseLetter">Other</span>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="medium-12 row expanded" id="slot-Feature-Bar">
               <div class="large-12 medium-6 small-6 columns no-padding">
                  <div id="slotGameFeaturesDropDownList" class="ddlListGames">
                     <div class="filterGameFeatures">
                        Game Features
                     </div>
                     <div class="icon icon-fun88_arrow_down_triangle slotDropDownListSmallArrowButton"></div>
                  </div>
                  <div id="slotGameFeatures" class="ddlListChilds">
                     <ul style="list-style: none; list-style-type: none; margin-left: 5px;" id="chkBoxListGameFeaturesList" class="slotFilterUnorderedList filter-group checkboxes">
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".freespin" class="deksChecker chkBoxGameFeatures filter-group chkBoxFilterResult">
                           <span data-value=".freespin" class="chkBoxText">Free Spins</span>
                        </li>
                        <li class="checkBoxListWrap">
                           <input type="checkbox" value=".bonusround" class="deksChecker chkBoxGameFeatures filter-group chkBoxFilterResult">
                           <span data-value=".bonusround" class="chkBoxText">Game Bonus</span>
                        </li>
                     </ul>
                  </div>
               </div>
               <div class="large-12 medium-6 small-6 columns no-padding">
                  <div id="slotPayLineDropDownList" class="ddlListGames">
                     <div class="filterGameLine">
                        Paylines
                     </div>
                     <div class="icon icon-fun88_arrow_down_triangle slotDropDownListSmallArrowButton"></div>
                  </div>
                  <div id="slotPayLineFeatures" style="width: 100%;" class="ddlListChilds">
                     <fieldset class="filter-group payLineValue">
                        <input type="text" id="combinePayLine" name="payLineRangeName" value="" />
                     </fieldset>
                     <span id="fromAmount"></span><span>&nbsp;-&nbsp;</span><span id="toAmount"></span>
                     <input type="text" id="payLineRange" name="payLineRangeName" value="" />
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="large-10 medium-12 columns gameholder">
         <div class="medium-12 small-12 hide-for-large filterPatternsHolder row" style="margin-top: 10px;">
            <div class="medium-4 small-5 columns no-padding">
               <div class="medium-12 small-12 columns no-padding" id="mobileFilterButton" style="margin-left: 15px; position: relative; cursor: pointer; text-align: center; max-width: 125px;">
                  <div class="sortAndFilterBtn">
                     Filter & Sort
                  </div>
               </div>
            </div>
            <div class="medium-4 small-4 columns no-padding gamesNumberMobileHolder en" style="text-align: center;">
               <span class="totalSlotsNumbers" style="display: inline-block; vertical-align: middle;"></span>
               <span style="text-transform: uppercase; display: inline-block; vertical-align: middle;">&nbsp;Slots</span>
            </div>
         </div>
         <hr class="hide-for-large" style="margin: 0.75rem 0 0 auto;" />
         <div class="small-12 searchFilteringHolder">
         </div>
         <div class="large-12 medium-12 small-12 row expanded">
            <div id="searchFunctionPlaceHolder">
               <input type="text" id="gameInput" placeholder="Search" style="border-radius: 5px;">
               <div class="searchResults" style="text-align: left; margin-bottom: 15px; display: none;">
                  <span style="font-style: italic;">Searchkeyword&nbsp;&nbsp;</span>:&nbsp;<span style="font-weight: bolder" id="gameNameResults"></span><br>
                  <span style="font-style: italic;">Search Result&nbsp;&nbsp;</span>:&nbsp;<span style="font-weight: bolder" id="totalGameCounts"></span><span id="gamesMainCategory">&nbsp;</span>
               </div>
               <div class="loadingSpan" id="gameSearch" style="text-align: left; margin-bottom: 15px; display: none;">
                  Searching<span>.</span><span>.</span><span>.</span>
               </div>
            </div>
            <section id="noGameFoundSection" class="show-for-large" style="display: none;">
               <div id="onlyRolaGames">
                  <div id="leftGirlHolder" class="no-game-found-girl-holder">
                     <img data-src="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/banner/slot/funambassador/fun88-gamesearch-leftgirl.jpg" class="img-responsive lazyload" />
                  </div>
                  <div id="logoHolder">
                     <img data-src="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/banner/slot/funambassador/fun88-logo.jpg" class="lazyload" />
                  </div>
                  <div id="rightGirlHolder" class="no-game-found-girl-holder">
                     <img data-src="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/banner/slot/funambassador/fun88-gamesearch-rightgirl.jpg" class="img-responsive lazyload" />
                  </div>
                  <div class="text-no-game-found" data-option="nogamesfound">
                     <span> Sorry, No results found. <br /> Try our recommended game for you! </ span>
                  </div>
                  <div class="text-no-game-found" data-option="nofavoritegame">
                     <span>You have not yet added slot favorite games. <br/>Click on <span class="icon icon-fun88_bookmark_heart_checked"></span> icon to save games in your Favorirte list.<br/>Try out our recommended games just for you!</span>
                  </div>
                  <div class="text-no-game-found" data-option="nolastplayedgame">
                     <span>You have not yet played slot game. <br/>Try out our recommended games just for you!</span>
                  </div>
               </div>
            </section>
            <div class="mix-target MGSQF th cn vn id desktopgameparent islive issoftlaunch isdemoplayavailable mgsqfslot videoslots slots column large-3 medium-3 small-6 end slotGamePanel slotGamePanelBoxWrap gridViewSlotHolder" data-fav="1" data-recent="1" data-recommended="1" data-new="0" data-order="99999" data-view="grid" data-bo="" data-payline="20" data-game-id="1905" data-name="icy gems permata dingin" style="">
               <div class="game-overlay grid">
                  <img onclick="currentGameType ='slot'; Games.EnterGame('MGSQF','1905');" class="slotGameImage lazyloaded" data-src="https://www.enhuoyea11.net//Assets/images/Games/Slots/MGSQF/IcyGems.jpg" data-hover="https://www.enhuoyea11.net//Assets/images/Games/Slots/MGSQF/IcyGems.gif" width="294" height="181" src="https://www.enhuoyea11.net//Assets/images/Games/Slots/MGSQF/IcyGems.jpg">
                  <div class="playNowIconOverlay" onclick="currentGameType ='slot'; Games.EnterGame('MGSQF','1905');" style="display: none;">
                     <div style="position: relative;">
                        <div class="gameh-play-f" style="opacity: 0;"><img class="w20 spinner" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/bg_O_2.png" alt="loader" width="65" height="64"></div>
                        <div class="gameh-centerwrap"><img width="79" height="80" class="gameh-centerplay w100p" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/play.png" alt="play" style="opacity: 0;"></div>
                     </div>
                  </div>
                  <div class="progressiveJackpotHolder hide" style="display: block;"><span class="icon icon-fun88_bookmark_star_checked"></span><span class="jackpotCurrencySymbol jackpotValueStyle"></span>&nbsp;<span class="jackpotAmount jackpotValueStyle" id="" data-jp-incre="" data-jp-incre-period=""></span></div>
                  <div class="icon icon-fun88_bookmark_heart_unchecked slot-fav-icon inactive-fav-icon" onclick="Games.InsertFavGames('1905');" style="color: rgb(255, 255, 255);"></div>
                  <div class="icon_positioning">
                     <div class="VendorLogo allProducts mgsqfProducts">MG</div>
                  </div>
               </div>
               <div class="playNowButtonList btnLangen" onclick="currentGameType ='slot'; Games.EnterGame('MGSQF','1905');">Play Now</div>
               <div class="tryNowButtonList DemoFree freeSloten inactive" onclick="Games.EnterDemoGame('MGSQF','1905');">Try Now</div>
               <div class="freePlayHolder small-12 row hide-for-large" style="margin-top: 5px; display: block;" onclick="Games.EnterDemoGame('MGSQF','1905');">
                  <p class="demoPlayBtn DemoFree inactive">Try For Free</p>
               </div>
               <div class="small-12 row gameNameInfoWrap gameNamePositioningGrid">
                  <span class="newGameTag show">New Games</span><span class="gamesName">Icy Gems</span>
                  <div class="freePlayHolder show-for-large" style="position: absolute; bottom: 3px; right: 0" onclick="Games.EnterDemoGame('MGSQF','1905');">
                     <p class="demoPlayBtn DemoFree inactive">Try For Free</p>
                  </div>
               </div>
               <div class="large-12 medium-12 row gameSlotInfoWrapper"><span class="gameCategoryTextHolder"><span class="gamesTypeName">Video Slots,&nbsp;</span></span><span class="productProvider" style="float: left;">Microgaming</span></div>
            </div>
            <div class="mix-target TG islive issoftlaunch videoslots slots tgpslot th mobilegameparent cn vn id desktopgameparent iosmobile androidmobile column large-3 medium-3 small-6 end slotGamePanel slotGamePanelBoxWrap gridViewSlotHolder" data-fav="1" data-recent="1" data-recommended="1" data-new="1" data-order="99999" data-view="grid" data-bo="" data-payline="0" data-game-id="1898" data-name="mustang gold emas mustang" style="">
               <div class="game-overlay grid">
                  <img onclick="currentGameType ='slot'; Games.EnterGame('TG','1898');" class="slotGameImage lazyloaded" data-src="https://www.enhuoyea11.net//Assets/images/Games/Slots/TG/MustangGold.jpg" data-hover="https://www.enhuoyea11.net//Assets/images/Games/Slots/TG/MustangGold.gif" width="294" height="181" src="https://www.enhuoyea11.net//Assets/images/Games/Slots/TG/MustangGold.jpg">
                  <div class="playNowIconOverlay" onclick="currentGameType ='slot'; Games.EnterGame('TG','1898');" style="display: none;">
                     <div style="position: relative;">
                        <div class="gameh-play-f" style="opacity: 0;"><img class="w20 spinner" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/bg_O_2.png" alt="loader" width="65" height="64"></div>
                        <div class="gameh-centerwrap"><img width="79" height="80" class="gameh-centerplay w100p" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/play.png" alt="play" style="opacity: 0;"></div>
                     </div>
                  </div>
                  <div class="progressiveJackpotHolder hide" style="display: block;"><span class="icon icon-fun88_bookmark_star_checked"></span><span class="jackpotCurrencySymbol jackpotValueStyle"></span>&nbsp;<span class="jackpotAmount jackpotValueStyle" id="" data-jp-incre="" data-jp-incre-period=""></span></div>
                  <div class="icon icon-fun88_bookmark_heart_unchecked slot-fav-icon inactive-fav-icon" onclick="Games.InsertFavGames('1898');" style="color: rgb(255, 255, 255);"></div>
                  <div class="icon_positioning">
                     <div class="VendorLogo allProducts tgProducts">PP</div>
                  </div>
               </div>
               <div class="playNowButtonList btnLangen" onclick="currentGameType ='slot'; Games.EnterGame('TG','1898');">Play Now</div>
               <div class="tryNowButtonList inactive NotDemoFree freeSloten" onclick="Games.EnterDemoGame('TG','1898');">Try Now</div>
               <div class="freePlayHolder small-12 row hide-for-large" style="margin-top: 5px; display: block;" onclick="Games.EnterDemoGame('TG','1898');">
                  <p class="demoPlayBtn NotDemoFree">Try For Free</p>
               </div>
               <div class="small-12 row gameNameInfoWrap gameNamePositioningGrid">
                  <span class="newGameTag hide">New Games</span><span class="gamesName">Mustang Gold</span>
                  <div class="freePlayHolder show-for-large" style="position: absolute; bottom: 3px; right: 0" onclick="Games.EnterDemoGame('TG','1898');">
                     <p class="demoPlayBtn NotDemoFree">Try For Free</p>
                  </div>
               </div>
               <div class="large-12 medium-12 row gameSlotInfoWrapper"><span class="gameCategoryTextHolder"><span class="gamesTypeName">Video Slots,&nbsp;</span></span><span class="productProvider" style="float: left;">Pragmatic Play</span></div>
            </div>
            <div class="mix-target CQG islive issoftlaunch videoslots slots cqgslot th mobilegameparent cn vn id desktopgameparent iosmobile androidmobile column large-3 medium-3 small-6 end slotGamePanel slotGamePanelBoxWrap gridViewSlotHolder" data-fav="1" data-recent="1" data-recommended="1" data-new="1" data-order="99999" data-view="grid" data-bo="" data-payline="0" data-game-id="1897" data-name="jungle party pesta hutan" style="">
               <div class="game-overlay grid">
                  <img onclick="currentGameType ='slot'; Games.EnterGame('CQG','1897');" class="slotGameImage lazyloaded" data-src="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/JungleParty.jpg" data-hover="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/JungleParty.gif" width="294" height="181" src="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/JungleParty.jpg">
                  <div class="playNowIconOverlay" onclick="currentGameType ='slot'; Games.EnterGame('CQG','1897');" style="display: none;">
                     <div style="position: relative;">
                        <div class="gameh-play-f" style="opacity: 0;"><img class="w20 spinner" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/bg_O_2.png" alt="loader" width="65" height="64"></div>
                        <div class="gameh-centerwrap"><img width="79" height="80" class="gameh-centerplay w100p" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/play.png" alt="play" style="opacity: 0;"></div>
                     </div>
                  </div>
                  <div class="progressiveJackpotHolder hide" style="display: block;"><span class="icon icon-fun88_bookmark_star_checked"></span><span class="jackpotCurrencySymbol jackpotValueStyle"></span>&nbsp;<span class="jackpotAmount jackpotValueStyle" id="" data-jp-incre="" data-jp-incre-period=""></span></div>
                  <div class="icon icon-fun88_bookmark_heart_unchecked slot-fav-icon inactive-fav-icon" onclick="Games.InsertFavGames('1897');" style="color: rgb(255, 255, 255);"></div>
                  <div class="icon_positioning">
                     <div class="VendorLogo allProducts cqgProducts">CQ9</div>
                  </div>
               </div>
               <div class="playNowButtonList btnLangen" onclick="currentGameType ='slot'; Games.EnterGame('CQG','1897');">Play Now</div>
               <div class="tryNowButtonList inactive NotDemoFree freeSloten" onclick="Games.EnterDemoGame('CQG','1897');">Try Now</div>
               <div class="freePlayHolder small-12 row hide-for-large" style="margin-top: 5px; display: block;" onclick="Games.EnterDemoGame('CQG','1897');">
                  <p class="demoPlayBtn NotDemoFree">Try For Free</p>
               </div>
               <div class="small-12 row gameNameInfoWrap gameNamePositioningGrid">
                  <span class="newGameTag hide">New Games</span><span class="gamesName">Jungle Party</span>
                  <div class="freePlayHolder show-for-large" style="position: absolute; bottom: 3px; right: 0" onclick="Games.EnterDemoGame('CQG','1897');">
                     <p class="demoPlayBtn NotDemoFree">Try For Free</p>
                  </div>
               </div>
               <div class="large-12 medium-12 row gameSlotInfoWrapper"><span class="gameCategoryTextHolder"><span class="gamesTypeName">Video Slots,&nbsp;</span></span><span class="productProvider" style="float: left;">CQ9</span></div>
            </div>
            <div class="mix-target CQG islive issoftlaunch videoslots slots cqgslot th vn cn id desktopgameparent iosmobile androidmobile mobilegameparent column large-3 medium-3 small-6 end slotGamePanel slotGamePanelBoxWrap gridViewSlotHolder" data-fav="1" data-recent="1" data-recommended="1" data-new="1" data-order="99999" data-view="grid" data-bo="" data-payline="0" data-game-id="1896" data-name="fa cai shen fa cai shen" style="">
               <div class="game-overlay grid">
                  <img onclick="currentGameType ='slot'; Games.EnterGame('CQG','1896');" class="slotGameImage lazyloaded" data-src="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/FaCaiShen.jpg" data-hover="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/FaCaiShen.gif" width="294" height="181" src="https://www.enhuoyea11.net//Assets/images/Games/Slots/CQG/FaCaiShen.jpg">
                  <div class="playNowIconOverlay" onclick="currentGameType ='slot'; Games.EnterGame('CQG','1896');" style="display: none;">
                     <div style="position: relative;">
                        <div class="gameh-play-f" style="opacity: 0;"><img class="w20 spinner" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/bg_O_2.png" alt="loader" width="65" height="64"></div>
                        <div class="gameh-centerwrap"><img width="79" height="80" class="gameh-centerplay w100p" src="https://www.enhuoyea11.net//Assets/images/Games/Icon/play.png" alt="play" style="opacity: 0;"></div>
                     </div>
                  </div>
                  <div class="progressiveJackpotHolder hide" style="display: block;"><span class="icon icon-fun88_bookmark_star_checked"></span><span class="jackpotCurrencySymbol jackpotValueStyle"></span>&nbsp;<span class="jackpotAmount jackpotValueStyle" id="" data-jp-incre="" data-jp-incre-period=""></span></div>
                  <div class="icon icon-fun88_bookmark_heart_unchecked slot-fav-icon inactive-fav-icon" onclick="Games.InsertFavGames('1896');" style="color: rgb(255, 255, 255);"></div>
                  <div class="icon_positioning">
                     <div class="VendorLogo allProducts cqgProducts">CQ9</div>
                  </div>
               </div>
               <div class="playNowButtonList btnLangen" onclick="currentGameType ='slot'; Games.EnterGame('CQG','1896');">Play Now</div>
               <div class="tryNowButtonList inactive NotDemoFree freeSloten" onclick="Games.EnterDemoGame('CQG','1896');">Try Now</div>
               <div class="freePlayHolder small-12 row hide-for-large" style="margin-top: 5px; display: block;" onclick="Games.EnterDemoGame('CQG','1896');">
                  <p class="demoPlayBtn NotDemoFree">Try For Free</p>
               </div>
               <div class="small-12 row gameNameInfoWrap gameNamePositioningGrid">
                  <span class="newGameTag hide">New Games</span><span class="gamesName">Fa Cai Shen</span>
                  <div class="freePlayHolder show-for-large" style="position: absolute; bottom: 3px; right: 0" onclick="Games.EnterDemoGame('CQG','1896');">
                     <p class="demoPlayBtn NotDemoFree">Try For Free</p>
                  </div>
               </div>
               <div class="large-12 medium-12 row gameSlotInfoWrapper"><span class="gameCategoryTextHolder"><span class="gamesTypeName">Video Slots,&nbsp;</span></span><span class="productProvider" style="float: left;">CQ9</span></div>
            </div>
            <div style="display: none">
               <div class="small-12 columns" id="lessRecentlyPlayedGameFound">
                  <span>You have <span class="lastPlayedGameCount">0</span> last played games.<br/>Try out more games recommended just for you!</span>
               </div>
               <div class="small-12 columns" id="favPlayedGame">
                  <span> You Have Entered Game <span class = "favPlayedGameCount"> 0 </ span> Go to Favorites List. </ span>
               </div>
            </div>
            <div id="firstLoadPanel" class="twenty-five-padding-top" style="display: block;">
               <div class="row expanded slotholder show">
                  <div class="large-12 medium-12 small-12 row expanded">
                     <div class="large-10 medium-10 small-8 columns slotCategoryHolder">
                        <span class="dark-grey-bold-text" style="text-transform: uppercase;">LAST PLAYED GAME</span>
                     </div>
                     <div class="large-2 medium-2 small-4 columns viewAllSpanWording en">
                        <span class="viewAllBtn" data-mixitup-control="" data-type="recent:asc">View All</span>
                     </div>
                  </div>
                  <div id="lastPlayedGamesHolder" class="myBoardHolder"></div>
               </div>
               <div class="row expanded slotholder show">
                  <div class="large-12 medium-12 small-12 row expanded">
                     <div class="large-10 medium-10 small-8 columns slotCategoryHolder">
                        <span class="dark-grey-bold-text" style="text-transform: uppercase;">Recommended For You</span>
                     </div>
                     <div class="large-2 medium-2 small-4 columns viewAllSpanWording en">
                        <span class="viewAllBtn" data-mixitup-control="" data-type="recommended:asc order:asc">View All</span>
                     </div>
                  </div>
                  <div id="recommendedGamesHolder" class="myBoardHolder"></div>
               </div>
               <div class="row expanded slotholder show">
                  <div class="large-12 medium-12 small-12 row expanded">
                     <div class="large-10 medium-10 small-8 columns slotCategoryHolder">
                        <span class="dark-grey-bold-text">New Games</span>
                     </div>
                     <div class="large-2 medium-2 small-4 columns viewAllSpanWording en">
                        <span class="viewAllBtn" data-mixitup-control="" data-type="new:asc">View All</span>
                     </div>
                  </div>
                  <div id="newGamesHolder" class="myBoardHolder"></div>
               </div>
               <div class="row expanded slotholder show">
                  <div class="large-12 medium-12 small-12 row expanded">
                     <div class="large-10 medium-10 small-8 columns slotCategoryHolder">
                        <span class="dark-grey-bold-text">My Favorites</span>
                     </div>
                     <div class="large-2 medium-2 small-4 columns viewAllSpanWording en">
                        <span class="viewAllBtn" data-mixitup-control="" data-type="fav:asc">View All</span>
                     </div>
                  </div>
                  <div id="myFavoritesHolder" class="myBoardHolder"></div>
               </div>
               <div class="row expanded slotholder show" style="position: relative;">
                  <div class="large-12 medium-12 small-12 row expanded">
                     <div class="large-10 medium-10 small-4 small-8 columns slotCategoryHolder">
                        <span class="dark-grey-bold-text" style="text-transform: uppercase;">JACKPOT</span>
                     </div>
                     <div class="totalJackpotDivHolder large-12 show-for-large">
                        <div class="jackpotCombineHolder">
                           <span class="icon icon-fun88_bookmark_star_checked" style="float: left; padding-top: 6px;"></span><span class="totalJackpotAmountHolder jackpotPrizeWording" style="float: left;">Jackpot Prize Name&nbsp;&nbsp;</span>
                           <div id="desktopSlotJp" class="jackpotTotalAmount totalJackpotAmountHolder"></div>
                        </div>
                     </div>
                     <div class="totalJackpotDivHolder small-12 hide-for-large">
                        <div class="jackpotCombineHolder">
                           <span class="icon icon-fun88_bookmark_star_checked" style="float: left; padding-top: 6px;"></span><span class="totalJackpotAmountHolder jackpotPrizeWording" style="float: left;">Jackpot Prize Name&nbsp;&nbsp;</span>
                        </div>
                        <div id="mobileSlotJp" class="jackpotTotalAmount totalJackpotAmountHolder"></div>
                     </div>
                     <div class="large-2 medium-2 small-4 columns viewAllSpanWording en">
                        <span class="viewAllBtn jackpotBtn" data-mixitup-control="" data-type="jackpot">View All</span>
                     </div>
                  </div>
                  <div id="jackpotGameHolder" class="myBoardHolder"></div>
               </div>
            </div>
            <div style="padding-top: 5px;" id="allSlotGamesHolder" class="unifiedSlotGameHolder">
               <input type="hidden" name="ctl00$ctl00$ctl00$BodyContentPlaceHolderBase$BodyContentPlaceHolderSite$BodyContentPlaceHolderGame$CurrentGameType" id="CurrentGameType" value="slot" />
               <div id="slotContainer" class="row expanded slotsGamesList default" data-view="grid">
               </div>
            </div>
         </div>
         <div class="loading-gif">
            <img src='{{APP_ASSETS}}/www.enhuoyea11.net/Assets/images/General/loading.gif' width="100" height="100" />
         </div>
      </div>
   </div>
   <div id="feelingLuckyHolder" class="column medium-3 small-6 end slotGamePanel large-3 slotGamePanelBoxWrap hide" data-view="grid" data-bo="">
      <div class="feelLuckyGamePanel large-12 medium-12 small-12 row expanded en">
         <div class="large-3 medium-3 small-3 column feelLuckyImageHolder">
            <img src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/feel_lucky_dice_icon.png" class="img-responsive iconLucky" />
         </div>
         <div class="large-9 medium-9 small-9 column">
            <div class="feelLuckyText en">FEELING LUCKY TODAY?</div>
            <div class="feelLuckyButton en" id="feelLuckySpin">I Feel Lucky</div>
         </div>
      </div>
      <div class="feelLuckyGamePanel large-12 medium-12 small-12 row expanded" data-id="download">
         <div class="large-3 medium-3 small-3 column feelLuckyImageHolder en">
            <img src="{{APP_ASSETS}}images/feel_lucky_slot_icon.png" class="img-responsive iconLuckyDownload" />
         </div>
         <div class="large-9 medium-9 small-9 column">
            <div class="feelLuckyText casinoLucky en">Slots For Desktop</div>
            <div class="feelLuckyButton en" id="feelLuckyDownload">
               <div class="icon icon-down_btn" style="float: left;"></div>
               <div class="downloadFeelLuckyText">
                  Download
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script src="{{APP_ASSETS}}js.main/main.js"></script>
<script src="{{APP_ASSETS}}bundles/js/require.js"></script>
<script src="{{APP_ASSETS}}bundles/js/module.js"></script>
<script src="{{APP_ASSETS}}bundles/js/base.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/unifiedproduct.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/iCheck.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/ion.rangeSlider.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/games.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/filter-product.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/lazysizes.min.js"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.mixitup.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery.easing-1.3.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/wallets.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/int/disable-scroll.js" type="text/javascript"></script>
<script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>
<script type="text/javascript">
   $('.slot-banner-slider').slick({
       lazyLoad: 'ondemand',
       slidesToShow: 1,
       fade: true,
       cssEase: 'linear',
       draggable: false,
       arrows: false,
       slidesToScroll: 1,
       autoplay: true,
       autoplaySpeed: 2000,
       appendArrows: $(".slot-banner-arrow"),
       dots: true,
       appendDots: $(".slot-banner-paging"),
       customPaging: function (slider, i) {
           return '<span class="icon icon-fun88_pagination_radio_unchecked inactivedot"></span><span class="icon icon-fun88_pagination_radio_checked activedot"></span>';
       }
   });
   
</script>
@endsection