@extends('main.base_head_foot')
@section('title', $title)
@section('css') 
       
@endsection
@section('content')
<div class="slot-banner-Wrapper">
      <div class="slot-banner-slider">
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-1.png'); background-position-x: 46%!important" data-url=''
               title=""
               data-promoid='13443'
               data-action='3' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span style="color: #25aae1;">INSTANT SIGN UP <span>2 MILLION</span><br /></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid='13443'
                  data-action='3' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='banner-Button en'>GET BONUS</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-2.png'); background-position-x: 40%!important" data-url=''
               title=""
               data-promoid=''
               data-action='0' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span data-sheets-value="{&quot;1&quot;:2,&quot;2&quot;:&quot;Slot best brand MGS\nGrab iPhone XS Max &amp; Freebets everyday \nMILLIONS worth of prizes up for grabs!\nButton：Grab Now&quot;}" data-sheets-userformat="{&quot;2&quot;:515,&quot;3&quot;:[null,0],&quot;4&quot;:[null,2,14281427],&quot;12&quot;:0}"><span style="color: #25aae1;">1% REBATE</span><br /><span style="color: #25aae1;">4% CASHBACK</span></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">DUO PLUS GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid=''
                  data-action='0' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='en'>APPLY</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
         <div style="position: relative">
            <div class="gamebannerImage" style="cursor:pointer;background: url('{{APP_ASSETS}}images/banner-game-3.png'); background-position-x: 24%!important" data-url=''
               title=""
               data-promoid='13422'
               data-action='3' onclick="bannerAction(this); return false;">
               <!-- <div class="gamebannerContent" style="text-align: left">
                  <span class="gamebannerTitle"><span style="color: #25aae1;">DAILY BONUS&nbsp;<span>3 MILLION</span><br /></span></span>
                  <span class="gamebannerDesc"><span style="color: #25aae1;">GAMES</span></span>
                  <span class="gamebannerMiniDesc"></span>
                  <div data-url=''
                  data-promoid='13422'
                  data-action='3' onclick="bannerAction(this); return false;" class="Display-Banner-Button">
                  <h5 class='banner-Button en'>GET BONUS</h5>
                  
                  </div>
                  </div> -->
            </div>
         </div>
      </div>
      <div class="slot-banner-arrow"></div>
      <div class="slot-banner-paging"></div>
      <div id="BodyContentPlaceHolderBase_BodyContentPlaceHolderSite_BannerContentPlaceHolderGame_SlotBannerControl_rcntWinnerPlaceHolder" class="recentWinnerHolder" style="display:none">
         <div style="position: relative;">
            <div class="recentWinnerText">
               <span>Latest Winners</span>
            </div>
            <div class="recentWinnerPlayLists">
            </div>
         </div>
      </div>
   </div>
   <section class="content-Wrapper">
      <hr class="hr">
      <hr class="hr show-for-large">
      <div class="row expanded" id="SpecialPromo">
         <div class="large-6 small-12 column">
            <div id="featuredPromoCont" class="row collapse expanded card">
               <div class="specialBigTitle mobileFeatureCardHeader">Featured</div>
               <div class="hidden specialPromoComingSoon" style="display: none;">
                  Stay Tuned
               </div>
               <div class="specialPromoCont hidden" style="display: block;">
                  <div class="divInline large-2 column small-3 show-for-large">
                     <span class="icon promotion-tab-title-logo icon-promo_special"></span>
                  </div>
                  <div class="divInline divSec large-6 column small-12">
                     <span class="tncUrl specialPromoTitle" data-url="#" style="pointer-events: unset;cursor: pointer;">100% LIFE INSURANCE NEW MEMBER</span>
                     <br>
                     <span class="icon icon-clock"></span>
                     <span class="expireTime specialPromoTime" data-time="30 Dec 2018 23:59:00 GMT +08">3d 20h 58m</span>
                  </div>
                  <div class="divInline divThird large-4 column">
                     <span class="specialPromoBonusId" data-contentid="14083" data-bonus-type="Bonus" data-status="Grouping"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="NavigateToBonus(14083); return false;">Apply Now</span></span>
                     <br class="show-for-large">
                     <span class="tncUrl specialPromoReadMore" data-url="#">More Details</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="large-6 small-12 column show-for-large" id="divEndPromoSoon">
            <div id="endingPromoCont" class="row collapse expanded">
               <div class="specialBigTitle">Ending Soon</div>
               <div class="hidden specialPromoComingSoon" style="display: none;">
                  Stay Tuned
               </div>
               <div id="EndingSoon" class="hidden specialPromoCont" style="display: block;">
                  <div class="divInline large-2 column small-3">
                     <span class="icon promotion-tab-title-logo icon-promo_special"></span>
                  </div>
                  <div class="divInline divSec large-6 column small-12">
                     <span class="tncUrl specialPromoTitle" data-url="#" style="pointer-events: unset;cursor: pointer;">100% INSTANT SIGN UP SPORTSBOOK FUN88 (NEW)</span>
                     <br>
                     <span class="icon icon-clock"></span>
                     <span class="expireTime specialPromoTime" data-time="30 Dec 2018 23:59:00 GMT +08">3d 20h 58m</span>
                  </div>
                  <div class="divInline divThird large-4 column">
                     <span class="specialPromoBonusId" data-contentid="13905" data-bonus-type="Bonus" data-status="Grouping"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="NavigateToBonus(13905); return false;">Apply Now</span></span>
                     <br class="show-for-large">
                     <span class="tncUrl specialPromoReadMore" data-url="#">More Details</span>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="rows" id="myPromoContainer">
            <div id="myPromo" class="small-3 large-2 columns promoOptionBtn en promoSelected">Promotion</div>
            <div id="myPromoHistory" class="small-4 large-2 columns promoOptionBtn en">Promotion History</div>
         </div>
         <div id="container" style="position: relative;" class="promoRunningList">
            <br>
            <div class="row expanded collapse ddlSelectionHolder">
               <div class="large-5 small-12 columns" id="promoCategoryHolder">
                  <div class="large-3 small-5 columns promoHeader selectionField large-5 small-3" id="promoCat" style="cursor: pointer;">
                     <span>NEW MEMBERS</span>
                  </div>
                  <div class="large-5 small-5 columns promoHeader large-pull-3 selectionField" id="promoVersType" style="cursor: pointer; position: relative;">
                     <span>Promotion Type</span>
                  </div>
               </div>
               <div id="promoSecondCatList" class="en">
                  <div data-version="">ALL</div>
                  <div data-version="bonuspromotion">Bonus</div>
                  <div data-version="rebatepromotion">Rebate</div>
                  <div data-version="manualpromotion">Special</div>
                  <div data-version="otherspromotion">Others</div>
               </div>
               <div id="promoCatList" class="en">
                  <div data-type="">ALL</div>
                  <div data-type="newmember">NEW MEMBERS</div>
                  <div data-type="sportsbook">SPORTSBOOK</div>
                  <div data-type="livecasino">LIVE CASINO</div>
                  <div data-type="games">GAMES</div>
                  <div data-type="sponsorship">Sponsorship<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">HOT</sup></div>
                  <div data-type="poker">POKER</div>
                  <div data-type="exclusive">EXCLUSIVE</div>
                  <div data-type="vip">VIP</div>
                  <div data-type="woturnover">Without TO</div>
               </div>
               <div class="large-3 show-for-large columns promoHistoryInvi promoHeader" style="display: none;">Last 90 Days</div>
               <div class="large-3 show-for-large columns promoDateColumn promoHeader"><span class="promoNonHistory" style="display: inline;">Expires</span><span class="promoHistory" style="display: none;">Time</span></div>
               <div class="large-3 show-for-large columns end promoHeader promoProgressOrStatus end">Progress/Status</div>
            </div>
            <br>
                        <div class="promotions row expanded collapse" data-url="#" data-id="8012" data-status="Available">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="https://www.enhuoyea11.net//ContentFilesP4/13905/ID-EN-13905.html">100% INSTANT SIGN UP SPORTSBOOK FUN88 (NEW) </span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Grouping">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(8012); return false;">Apply Now</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="https://www.enhuoyea11.net//ContentFilesP4/13905/ID-EN-13905.html">More Details</span></div>
            </div>
            <div class="promotions row expanded collapse" data-url="#" data-id="1162" data-status="Available">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="#">100% INSTANT SIGN UP GAMES BONUS </span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Grouping">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(1162); return false;">Apply Now</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
            </div>
            <div class="promolistholder row expanded collapse" data-status="Grouping" style="position:relative;">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl" data-url="#">30% 100% &amp; 150% BONUS SIGN UP LIVE CASINO ANYTIME WD</span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Grouping">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column moreGrpingPromo" data-contentid="14081"><span>Promo List</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
               <div style="display:none;" class="promotions row expanded collapse" data-url="#" data-id="118029" data-status="Available">
                  <div class="large-5 small-7 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl groupingTncUrl" data-url="#">30% 100% &amp; 150% BONUS SIGN UP LIVE CASINO ANYTIME WD</span></div>
                  <div class="large-push-3 large-1 small-5 column promoButtonAlign end"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118029); return false;">Apply Now</span></div>
                  <div class="large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large"></div>
               </div>
               <div style="display:none;" class="promotions row expanded collapse" data-url="#" data-id="118027" data-status="Available">
                  <div class="large-5 small-7 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl groupingTncUrl" data-url="#">30% 100% &amp; 150% BONUS SIGN UP LIVE CASINO ANYTIME WD</span></div>
                  <div class="large-push-3 large-1 small-5 column promoButtonAlign end"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118027); return false;">Apply Now</span></div>
                  <div class="large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large"></div>
               </div>
               <div style="display:none;" class="promotions row expanded collapse" data-url="#" data-id="118028" data-status="Available">
                  <div class="large-5 small-7 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl groupingTncUrl" data-url="#">30% 100% &amp; 150% BONUS SIGN UP LIVE CASINO ANYTIME WD</span></div>
                  <div class="large-push-3 large-1 small-5 column promoButtonAlign end"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118028); return false;">Apply Now</span></div>
                  <div class="large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large"></div>
               </div>
            </div>
            <div class="promolistholder row expanded collapse" data-status="Grouping" style="position:relative;">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl" data-url="#">100% &amp; 150% SIGN UP SPORTSBOOK ONEWORKS ANYTIME WD</span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Grouping">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column moreGrpingPromo" data-contentid="14082"><span>Promo List</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
               <div style="display:none;" class="promotions row expanded collapse" data-url="#" data-id="118031" data-status="Available">
                  <div class="large-5 small-7 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl groupingTncUrl" data-url="#">100% &amp; 150% SIGN UP SPORTSBOOK ONEWORKS ANYTIME WD</span></div>
                  <div class="large-push-3 large-1 small-5 column promoButtonAlign end"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118031); return false;">Apply Now</span></div>
                  <div class="large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large"></div>
               </div>
               <div style="display:none;" class="promotions row expanded collapse" data-url="#" data-id="118030" data-status="Available">
                  <div class="large-5 small-7 column title"><span class="show-for-large icon icon-promo_special"></span><span class="tncUrl groupingTncUrl" data-url="#">100% &amp; 150% SIGN UP SPORTSBOOK ONEWORKS ANYTIME WD</span></div>
                  <div class="large-push-3 large-1 small-5 column promoButtonAlign end"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118030); return false;">Apply Now</span></div>
                  <div class="large-1 small-2 column promoMoreInfoBtn expandDetail show-for-large"></div>
               </div>
            </div>
            <div class="promotions row expanded collapse" data-url="#" data-id="118032" data-status="Available">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="#">100% LIFE INSURANCE NEW MEMBER </span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Grouping">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="OpenTransferWithBonus(118032); return false;">Apply Now</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
            </div>
            <div class="promotions row expanded collapse" data-url="#" data-id="13938" data-status="Eligible" data-runningrebate="false">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="#">JERSEY VAGANZA</span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="30 Dec 2018 23:59:00 GMT +08" data-status="Eligible">3d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"><span class="linkDeposit button-Green promoBtn" data-close="" onclick="ManualPromo(13938); return false;">Apply Now</span></div>
               <div class="large-1 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
            </div>
            <div class="promotions row expanded collapse" data-url="#" data-id="null" data-status="" data-runningrebate="false">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="#">WELCOME FREEBIES</span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="31 Dec 2018 23:59:00 GMT +08" data-status="">4d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"></div>
               <div class="large-1 large-push-3 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
            </div>
            <div class="promotions row expanded collapse" data-url="#" data-id="null" data-status="" data-runningrebate="false">
               <div class="large-5 small-12 column title"><span class="show-for-large icon icon-promo_special"></span><span style="" class="tncUrl" data-url="#">Keep your Fun88 profile up to date!</span></div>
               <span class="large-3 small-12 column clockHolder">
                  <span class="icon icon-clock promoClock"></span>
                  <div class="expireTime" data-time="31 Dec 2018 23:59:00 GMT +08" data-status="">4d 20h 58m</div>
               </span>
               <div class="large-3 small-5 column promoButtonAlign"></div>
               <div class="large-1 large-push-3 small-7 column promoMoreInfoBtn end"><span class="detailsTncUrl promoDetails" data-url="#">More Details</span></div>
            </div>
         </div>
      </div>
   </section>
   
        @endsection
        @section('javascript')
        <script src="{{APP_ASSETS}}Assets/Scripts/ext/jquery-3.1.1.min.js"></script>
        <script src="{{APP_ASSETS}}bundles/js/require.js"></script>
        <script src="{{APP_ASSETS}}bundles/js/module.js"></script>
        <script src="{{APP_ASSETS}}bundles/js/base.js"></script>
        <script src="{{APP_ASSETS}}jquery.main/main.js"></script>
        <script src="{{APP_ASSETS}}js.main/main.js"></script>
        <script src="{{APP_ASSETS}}Assets/VIP/JS/external/progressbar.js"></script>
        <script src="{{APP_ASSETS}}Assets/Scripts/int/promoCountdown.js"></script>
        <script type="text/javascript">
          var activeCls = "promoSelected";
          docReady(function () {
            $(".promotion-border.header-menu-Item").addClass("active");
            $('#promoCat').addClass("selectionField");
            $('.mobile-navigation-tr[data-navigation="promotion"]').addClass("active");
         });
      </script>
      <script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>
      <script>
       $('.slot-banner-slider').slick({
         slidesToScroll: 1,
         autoplay: true,
         autoplaySpeed: 2000,
         appendArrows: $(".slot-banner-arrow"),
         dots: true,
         arrows: false,
         appendDots: $(".slot-banner-paging"),
         customPaging: function (slider, i) {
           return '<span class="icon icon-fun88_pagination_radio_unchecked inactivedot"></span><span class="icon icon-fun88_pagination_radio_checked activedot"></span>';
        }
     });
  </script>
  <script type="text/javascript">
    $(Document).scroll(function () {
      var y = $(this).scrollTop();
      if (y > 470) {
        $('.HelperPromo').fadeOut(100);
     } else {
        $('.HelperPromo').fadeIn(100);
     }
  });
</script>
<script>
 $(document).ready(function () {
   $("#myPromo").addClass(activeCls);
   GoToAllPage();
   $('#promoSecondCatList div[data-version=' + queryStringPromoVal("category") + ']').click().change();
   function queryStringPromoVal(key) {
     var paramIndexVal = location.search.split("&");
     for (var x = 0; x < paramIndexVal.length ; x++) {
       if (paramIndexVal[x].indexOf(key) > -1) {
         return paramIndexVal[x].split('=')[1];
      }
   }
}
});
</script>
<script>
 $(document).ready(function(){
   $('.mobile-sub-menu .hideBullet .NewMember').addClass('active');
});
</script>
        @endsection