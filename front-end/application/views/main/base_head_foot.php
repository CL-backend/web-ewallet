@php  
defined('BASEPATH') OR exit('No direct script access allowed');
@endphp
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
   <head>
      <link rel="shortcut icon" href="{{APP_ASSETS}}favicon.ico" />
      <meta charset="utf-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=Edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <link href="{{APP_ASSETS}}bundles/css/baseCss.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}bundles/css/siteCss.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}Assets/Css/ext/ion.rangeSlider.skinHTML5.css" rel="stylesheet" type="text/css" />
      <link href="{{APP_ASSETS}}bundles/css/bannerCss.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}bundles/css/generalCss.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}css/flag-icon.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}css.main/main.css" rel="stylesheet"/>
      <link href="{{APP_ASSETS}}Assets/Css/ext/CheckBoxBlueSkin.css" rel="stylesheet" type="text/css" />
      <link href="{{APP_ASSETS}}Assets/Css/ext/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
      <link href="{{APP_ASSETS}}Assets/Css/ext/slick.css" rel="stylesheet" type="text/css" />
      <link href="{{APP_ASSETS}}Assets/Css/int/game.css" rel="stylesheet" type="text/css" />
      <link href='{{APP_ASSETS}}Assets/Css/int/registration.css' rel='stylesheet' type='text/css' />
      <link rel="stylesheet" href="{{APP_ASSETS}}Assets/Css/ext/jquery-ui.css">
      <style type="text/css">
.dummy_page {
  height: 200px;
  width: 100%;
  background-color: #fff;
  text-align: center;
  box-sizing: border-box;
  padding: 60px 0px;
}
/* STYLES SPECIFIC TO FOOTER  */
.footer {
  width: 100%;
  position: relative;
  height: auto;
  background-color: #0747A6;
}
.footer .col {
  width: 190px;
  height: auto;
  float: left;
  box-sizing: border-box;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  padding: 0px 20px 20px 20px;
}
.footer .col h1 {
  margin: 0;
  padding: 0;
  font-family: inherit;
  font-size: 14px;
  line-height: 17px;
  padding: 20px 0px 5px 0px;
  color: #fff;
  font-weight: bold;
  text-transform: uppercase;
  letter-spacing: 0.250em;
}
.footer .col ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
.footer .col ul li {
  color: #fff;
  font-size: 14px;
  font-family: inherit;
  font-weight: bold;
  padding: 5px 0px 5px 0px;
  cursor: pointer;
  transition: .2s;
  -webkit-transition: .2s;
  -moz-transition: .2s;
}
.social ul li {
  display: inline-block;
  padding-right: 5px !important;
}

.footer .col ul li:hover {
  color: #2199e8;
  transition: .1s;
  -webkit-transition: .1s;
  -moz-transition: .1s;
}
.clearfix {
  clear: both;
}
@media only screen and (min-width: 1280px) {
  .contain {
    width: 1200px;
    margin: 0 auto;
}
}
@media only screen and (max-width: 1139px) {
  .contain .social {
    width: 1000px;
    display: block;
}
.social h1 {
    margin: 0px;
}
}
@media only screen and (max-width: 950px) {
  .footer .col {
    width: 33%;
}
.footer .col h1 {
    font-size: 14px;
}
.footer .col ul li {
    font-size: 13px;
}
}
@media only screen and (max-width: 500px) {
    .footer .col {
      width: 50%;
  }
  .footer .col h1 {
      font-size: 14px;
  }
  .footer .col ul li {
      font-size: 13px;
  }
}
@media only screen and (max-width: 340px) {
  .footer .col {
    width: 100%;
}
}
</style>
      @section('css')
      @show 
      <title>@yield('title')</title>
      <meta name="description" content="E-WALLET Indonesia - the leading online betting booker. Best & trusted betting promotions. Register now and claim New Member Bonus!" />
      
   </head>
   <body class="en">
      <div id="css-async-cover" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: white; z-index: 99999; text-align: center;">
         <div class="Baseloading"></div>
      </div>
      <div id="PageLoad" style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #dcdcdc; opacity: .5; z-index: 99999; text-align: center; display: none">
         <div class="Baseloading"></div>
      </div>
      <form method="post" action="#" id="baseForm">
         <div class="aspNetHidden">
            <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="" />
         </div>
         <div class="aspNetHidden">
            <input type="hidden" name="__VIEWSTATEGENERATOR" id="__VIEWSTATEGENERATOR" value="" />
            <input type="hidden" name="__EVENTVALIDATION" id="__EVENTVALIDATION" value="" />
         </div>
         <div id="overlayBase" onclick="offCanvas.TriggerCloseMenu();" style="display: none;">
         </div>
         ﻿
         <header id="Navbartop">
            <div id="modal-OuterNotification" data-toggler data-animate="slide-in-down slide-out-up" class="callout secondary">
               <div id="modal-InnerNotification">
                  <div id="modal-InnerNotification-img">
                     <img class="fun-logo ImageLazy" data-original="{{APP_ASSETS}}Assets/images/Fun88QRlogo.png" src="" width="70" />
                  </div>
                  <div class="modal-InnerNotification-text-container">
                     <div class="modal-InnerNotification-text-title">Always update with the latest news and promotions</div>
                     <div class="modal-InnerNotification-text-description">
                        E-WALLET always wants to provide the best playing experience. Click & apos; Agree & apos; to get notifications from us.
                     </div>
                  </div>
                  <div class="modal-InnerNotification-button-container">
                     <div id="notf-decline" class="notfBtn">Decline</div>
                     <div id="notf-allow" class="notfBtn">Agree</div>
                  </div>
               </div>
            </div>
            <ul style="position: relative;">
               <li>
                  <div class="header-hamburger hide-for-large" onclick="offCanvas.TriggerMobileMenu();">
                     <span class="icon icon-Menu"></span>
                  </div>
                  <a class="fun-header-logo-sea" href='{{site_url()}}/works'>
                  <span>E-WALLET</span>
                  </a>
               </li>
               <li class="header-middle-wrapper IntroSteps Intro_Menus">
                  <div class="header-menu-Item show-for-large sportsbook-border" data-target="header-sportsbook-menu">
                     <a href='{{site_url()}}/works'>
                        <h7>SPORTS<SUP STYLE='COLOR:RED;POSITION:RELATIVE;Z-INDEX:9999; MARGIN-LEFT: 1%;FONT-WEIGHT:700;TEXT-TRANSFORM:UPPERCASE;'>HOT</SUP></h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large livecasino-border" data-target="header-casino-menu">
                     <a href="{{site_url()}}/about">
                        <h7>CASINO</h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large"  data-target="header-pt-slot-menu">
                     <a href="{{site_url()}}/about">
                        <h7>PT SLOT</h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large slot-border" data-target="header-slot-menu" >
                     <a href='{{site_url()}}/about'>
                        <h7>SLOT</h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large keno-border">
                     <a href='{{site_url()}}/about'>
                        <h7>POKER</h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large promotion-border" data-announcement="" data-target="header-promotion-menu" data-grabbed="0">
                     <a href="{{base_url()}}">
                        <h7>PROMOTION<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">Hot</sup></h7>
                     </a>
                  </div>
                  <div class="header-menu-Item show-for-large more-border" id="header-more" style="position: relative" data-target="header-more-menu" data-announcement="" data-grabbed="0">
                     <a>
                        <h7>MORE</h7>
                     </a>
                  </div>
               </li>
               <li class="header-right-wrapper IntroSteps Intro_LoginBar">
                  <div class="show-for-large login-view">
                     <div class="loginItem">
                        <a class="btnLogin" id="btnHeaderLogin" data-toggle="modal-PopupLogin" data-target="popupHeader">LOGIN</a>
                     </div>
                     <div class="loginItem">
                        <a href="{{site_url()}}/contact" class="linkJoinNow button-Red en">
                           <h7>JOIN NOW</h7>
                        </a>
                     </div>
                  </div>
                  <div class="hide-for-large login-view">
                     <div style="display: inline-block">
                        <a class="btnLogin en" onclick="offCanvas.TriggerMobileNavi();">LOGIN</a>
                     </div>
                     <div class="loginItem">
                        <a href="#" class="linkJoinNow button-Red en">
                           <h7>JOIN NOW</h7>
                        </a>
                     </div>
                  </div>
               </li>
            </ul>
            <div class="show-for-large header-bullet-menu right">
               <span class="icon icon-bullet_dots desktop-bullet-menu"></span>
               <span class="icon icon-close_btn desktop-bullet-menu-close" style="font-size: 20px;" onclick="sidePanel.CloseSidePanel();"></span>
            </div>
         </header>
         <div id="leftSideBannerContainer" style="display: none;">
            <div style="position: relative">
               <div class="closeSideBannerBtn" onclick="closeSideBanner()"></div>
               <a id="sideBannerPromo">
               <img id="imgSideBanner" />
               </a>
               <div id="imgSideBannerReminder" style="display: none;">
                  <span id="CloseRouletteOnce" onclick="SetSideBannerCookieTemporarily()">&#8250; 我 暂时 不想 看到 你</span>
                  <br />
                  <br />
                  <span id="CloseRouletteToday" onclick="SetSideBannerCookieForTomorrow();">&#8250; 我 今天 不想 看到 你 了</span>
               </div>
            </div>
         </div>
         
         <div class="site-content-wrapper">         
            @yield('content')
         </div>
         </div>

   

         <footer id="allFooter">
                      <div class="footer-section-wrapper footer-icon-wrapper">
               <div class="row collapse expanded ">
                  <div class="columns large-2 footer-small-overwrite">
                     <ul class="footer-icon">
                        <li>
                           <h8>Licenses:</h8>
                        </li>
                        <li>
                           <a href="#" target="_blank" rel="noopener">
                           <span class="IOMLicenses FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="IOM"></span>
                           </a>
                        </li>
                        <li>
                           <a href="#" target="_blank" rel="noopener">
                           <span class="FirstCagayanLicenses FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="First Cagayan"></span>
                           </a>
                        </li>
                        <li>
                           <span class="PagcorLicenses FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="PT games are deployed by PT TransPacific under the authority of PAGCOR"></span>
                        </li>
                     </ul>
                  </div>
                  <div class="columns large-6 footer-small-overwrite">
                     <ul class="footer-icon">
                        <li>
                           <h8>International Payment:</h8>
                        </li>
                        <li>
                           <span class="ChinaUnionPayment FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="UnionPay"></span>
                        </li>
                        <li>
                           <span class="FastBankPayment FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="Fastbank Transfer"></span>
                        </li>
                        <li>
                           <span class="BankWirePayment FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="Bankwire"></span>
                        </li>
                        <li>
                           <span class="SkrillPayment FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="Skrill"></span>
                        </li>
                        <li>
                           <span class="Help2PayPayment FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="Help2Pay"></span>
                        </li>
                     </ul>
                  </div>
                  <div class="columns large-2 footer-small-overwrite-half left">
                     <ul class="footer-icon">
                        <li>
                           <h8>Security:</h8>
                        </li>
                        <li>
                           <span class="IovationSecurity FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="Iovation"></span>
                        </li>
                     </ul>
                  </div>
                  <div class="columns large-2 footer-small-overwrite-half right hidden-footer">
                     <ul class="footer-icon">
                        <li>
                           <h8>Responsibility:</h8>
                        </li>
                        <li>
                           <a href="#" target="_blank" rel="noopener">
                           <span class="GameCareResponsibility FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png"></span>
                           </a>
                        </li>
                        <li>
                           <a href="#" target="_blank"  rel="noopener">
                           <span class="AboveAgeResponsibility FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="18+"></span>
                           </a>
                        </li>
                     </ul>
                  </div>
                  <div class="columns large-2 footer-small-overwrite">
                     <ul class="footer-icon float-right">
                        <li>
                           <a href="#" target="_blank"  rel="noopener">
                           <span class="AboveAgeResponsibility FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="18+"></span>
                           </a>
                        </li>
                        <li>
                           <a href="#" target="_blank" rel="noopener">
                           <span class="GameCareResponsibility FooterIcon lazy" data-original="{{APP_ASSETS}}images/footer.png" title="GameCare"></span>
                           </a>
                        </li>
                        <li>
                           <h8>Responsibility:</h8>
                        </li>
                     </ul>
                  </div>
               </div>
            </div>
            <section class="footer-content-Wrapper content-Wrapper">
               <div class="row expanded collapse footer-row">
                  <div class="column large-4">
                     <div class="footer-section-wrapper">
                        <span class="footer-section-title">Partner</span>
                     </div>
                     <div class="row expanded collapse footer-partner-wrapper slider7">
                        <div class="column small-6">
                           <div class="sponsor-newcastle" title="EPL Newcastle United Official Shirt Sponsor">
                              <a href="#" target="_blank" style="display: block" rel="noopener">
                                 <div class="footer-partner newcastle lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                                 <div class="partner-description">
                                    <table class="sponsor-table">
                                       <tbody>
                                          <tr>
                                             <td>
                                                <span class="sponsor-wording">Official Shirt Sponsor</span>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><span class="sponsor-wording">Newcastle United FC</span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="column small-6">
                           <div class="sponsor-tottenham" title="EPL Tottenham Hotspur Official Asian Betting Partner">
                              <a href="#" target="_blank" style="display: block" rel="noopener">
                                 <div class="footer-partner tottenham lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                                 <div class="partner-description">
                                    <table class="sponsor-table">
                                       <tbody>
                                          <tr>
                                             <td>
                                                <span class="sponsor-wording">Official Asian Betting Partner</span>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><span class="sponsor-wording">Tottenham Hotspur FC</span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="column small-6">
                           <div class="sponsor-newcastle" title="EPL Newcastle United Official Shirt Sponsor">
                              <a href="#" target="_blank" style="display: block" rel="noopener">
                                 <div class="footer-partner newcastle lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                                 <div class="partner-description">
                                    <table class="sponsor-table">
                                       <tbody>
                                          <tr>
                                             <td>
                                                <span class="sponsor-wording">Official Shirt Sponsor</span>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><span class="sponsor-wording">Newcastle United FC</span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </a>
                           </div>
                        </div>
                        <div class="column small-6">
                           <div class="sponsor-tottenham" title="EPL Tottenham Hotspur Official Asian Betting Partner">
                              <a href="#" target="_blank" style="display: block" rel="noopener">
                                 <div class="footer-partner tottenham lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                                 <div class="partner-description">
                                    <table class="sponsor-table">
                                       <tbody>
                                          <tr>
                                             <td>
                                                <span class="sponsor-wording">Official Asian Betting Partner</span>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td><span class="sponsor-wording">Tottenham Hotspur FC</span></td>
                                          </tr>
                                       </tbody>
                                    </table>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="column large-4 show-for-large">
                     <div class="footer-section-wrapper">
                        <span class="footer-section-title">Platform</span>
                     </div>
                     <div class="row expanded collapse" style="padding: 0 10px;">
                        <div class="1">
                        <div class="column large-4">
                           <div class="footer-gaming-platform bti lazy" data-original="{{APP_ASSETS}}images/footer_icon.png"  style="background-image: none;"></div>
                        </div>
                        <div class="column large-4">
                           <div class="footer-gaming-platform ebet lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                        </div>
                        <div class="column large-4">
                           <div class="footer-gaming-platform microgaming lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                        </div>
                        
                        <div class="column large-4">
                           <div class="footer-gaming-platform onework lazy" data-original="{{APP_ASSETS}}images/footer_icon.png"  style="background-image: none;"></div>
                        </div>
                        </div>
                        <div class="2">
                        <div class="column large-4">
                           <div class="footer-gaming-platform asiagaming lazy" data-original="{{APP_ASSETS}}images/footer_icon.png" style="background-image: none;"></div>
                        </div>
                        <div class="column large-4">
                           <div class="footer-gaming-platform golddeluxe lazy" data-original="{{APP_ASSETS}}images/footer_icon.png"  style="background-image: none;"></div>
                        </div>
                        <div class="column large-4">
                           <div class="footer-gaming-platform im lazy" data-original="{{APP_ASSETS}}images/footer_icon.png"  style="background-image: none;"></div>
                        </div>
                        <div class="column large-4">
                           <div class="footer-gaming-platform playtech lazy" data-original="{{APP_ASSETS}}images/footer_icon.png"  style="background-image: none;"></div>
                        </div>
                        </div>
                        <div class="column large-4">
                        </div>
                     </div>
                  </div>
                  <div class="column large-4 show-for-large">
                     <div class="footer-section-wrapper">
                        <span class="footer-section-title">Ambassador</span>
                     </div>
                     <div class="row expanded collapse" style="padding: 0 50px;">
                        <div class="column large-5" style="position: relative">
                           <div class="footer-ambassador robbie-sign lazy ambassador-en-robbie" data-original="{{APP_ASSETS}}images/footer-brand-ambassador_EN.png" style="background-image: none;"></div>
                        </div>
                        <div class="column large-5" style="position: relative">
                           <div class="footer-ambassador steve_nash-sign lazy ambassador-en-steve" data-original="{{APP_ASSETS}}images/footer-brand-ambassador_EN.png" style="background-image: none;"></div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>


                        <!-- FOOTER START -->
                        <div class="footer">
                          <div class="contain" style="width: auto;margin: 0 auto;display: table;">
                             <div class="col">
                               <h1>Games</h1>
                               <ul>
                                 <li>Sportsbook</li>
                                 <li>Live Casino</li>
                                 <li>Games</li>
                                 <li>Playtech Slots</li>
                              </ul>
                           </div>
                        <div class="col">
                         <h1>Promotion</h1>
                         <ul>
                           <li>E-Wallet Reward</li>
                           <li>Refer friends</li>
                           <li>E-Wallet Bliss</li>
                           <li>Download Center</li>
                           <li>Help</li>
                        </ul>
                     </div>
                     <div class="col">
                      <h1>More</h1>
                      <ul>
                        <li>About E-Wallet</li>
                        <li>Affiliate</li>
                        <li>Rules & Regulations</li>
                        <li>Responsible Gambling</li>
                        <li>Privacy Policy</li>
                        <li>Term and Conditions</li>
                     </ul>
                  </div>
               <div class="col social">
                <h1>Social</h1>
                <ul>
                  <li><div class="footer-social-media facebook lazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/footer/footer-social-media.png" title="Like FUN88 on Facebook" style="background-image: url(&quot;{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/footer/footer-social-media.png&quot;);"></div></li>
                  <li><div class="footer-social-media twitter lazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/footer/footer-social-media.png" title="Follow FUN88 on Twitter" style="background-image: url(&quot;{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/footer/footer-social-media.png&quot;);"></div></li>
               </ul>
            </div>
            <div class="clearfix"></div>
            </div>
            </div>
            <!-- END OF FOOTER -->
            <section class="footer-section-link-wrapper">
               <div>  
               </div>
               <div class="footer-content-container" >
               </div>
               <div class="row expanded collapse" style="padding: 0px 0;">
                  <div class="column small-12">
                     <p style="margin: 0; color: white; text-align: center; font-size: 11px;">
                        E-Wallet is operated by Welton Holdings Limited with its registered office at 1st Floor Bourne Concourse, Peel Street, Ramsey, Isle of Man IM8 1JJ.
                     </p>
                     <p style="margin: 0; color: white; text-align: center; font-size: 11px;">
                        Regulated & Licensed in the British Isles since 17th July 2009 by the Isle of Man Gambling Supervision Commission: http://www.gov.im/gambling
                     </p>
                  </div>
               </div>
            </section>
         </footer>
         <div id="mobileFooter" style="width: 100%; position: fixed; bottom: 0; z-index: 3;" class="hide-for-large floatingPanel">
            <table style="margin: 0; background-color: #0747A6; border: 0; height: 46px; table-layout: fixed;">
               <tbody style="margin: 0; background-color: #0747A6; border: 0; text-align: center; color: white; font-size: 22px;">
                  <tr>
                     <td style="padding: 0 0 15px 0; width:20%;">
                        <a href='#'>
                        <span style="color: white;" class="icon icon-Home"></span>
                        <span style="color: white; font-size: 9px; display: block; line-height: 0.7;">Home</span>
                        </a>
                     </td>
                     <td style="padding: 0 0 15px 0; width:20%;">
                        <a href="#">
                        <span class="icon icon-sign_up" style="color: white;"></span>
                        <span style="color: white; font-size: 9px; display: block; line-height: 0.7;">JOIN NOW</span>
                        </a>
                     </td>
                     <td style="padding: 0 0 15px 0; width:20%;">
                        <a href="#" target="_blank" rel="noopener">
                        <span class="icon icon icon-download_cloud IntroSteps Intro_MobileDownload" style="color: white;"></span>
                        <span style="color: white; font-size: 9px; display: block; line-height: 0.7;">Download Center</span>
                        </a>
                     </td>
                     <td style="padding: 0 0 15px 0; width:20%;">
                        <div onclick="Page.popUpLiveChat()">
                           <span style="cursor: pointer" class="icon icon-LivecChat"></span>
                           <span style="color: white; font-size: 9px; display: block; line-height: 0.7;">Live Chat</span>
                        </div>
                     </td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="side-panel-wrapper show-for-large">
            <div>
               <div>
                  <table class="side-panel-table IntroSteps Intro_SideBar">
                     <tbody>
                        <tr>
                           <td class="has-tip left" data-tooltip data-h-offset="25" data-hover-delay="0" data-fade-in-duration="0" data-fade-out-duration="0" data-hover-delay="0" data-fade-in-duration="0" data-fade-out-duration="0" aria-haspopup="true" title='Download Center'>
                              <a class="icon icon-download_cloud" href='#' target="_blank" rel="noopener"><span class="icon icon-Download"></span></a>
                           </td>
                        </tr>
                        <tr>
                           <td class="has-tip left" data-tooltip data-h-offset="25" data-hover-delay="0" data-fade-in-duration="0" data-fade-out-duration="0" aria-haspopup="true" title='Bank Info' onclick="sidePanel.SettingToggle(this);" data-target="side-panel-bank-status-setting">
                              <span class="icon icon-bank_status"></span>
                           </td>
                        </tr>
                        <tr>
                           <td class="has-tip left" data-tooltip data-h-offset="25" data-hover-delay="0" data-fade-in-duration="0" data-fade-out-duration="0" aria-haspopup="true" title='Help Center'>
                              <a target="_blank" class="icon" href='#' rel="noopener"><span class="icon icon-help_center"></span></a>
                           </td>
                        </tr>
                        <tr>
                           <td class="has-tip left" data-tooltip data-h-offset="25" data-hover-delay="0" data-fade-in-duration="0" data-fade-out-duration="0" aria-haspopup="true" title='Live Chat'>
                              <span class="icon icon-livechat_II " onclick="Page.popUpLiveChat()"></span>
                           </td>
                        </tr>
                     </tbody>
                  </table>
                  <div class="side-panel-content-wrapper">
                     <div class="side-panel-bank-setting side-panel-setting-content-wrapper">
                        <table class="side-panel-setting-content-table">
                           <tbody>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-myaccount" href="#" target="_blank">
                                    <span class="icon icon-bank_status2"></span>
                                    My Account
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-deposit depositSelfExlusion" href="#"> 
                                    <span class="icon icon-fun88_bank_deposit"></span>
                                    Deposit
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-transfer" href="#" target="_blank">
                                    <span class="icon icon-fun88_bank_transfer"></span>
                                    Transfer
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-withdrawal withdrawalSelfExlusion" href="#" target="_blank">
                                    <span class="icon icon-fun88_bank_withdrawal"></span>
                                    Withdrawal
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-bankinfo" href="#" target="_blank">
                                    <span class="icon icon-fun88_bank"></span>
                                    Bank Details
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-transactionhistory" href="#" target="_blank">
                                    <span class="icon icon-history"></span>
                                    History
                                    </a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="side-panel-notification-setting side-panel-setting-notification-content-wrapper">
                     </div>
                     <div id="templateHolder" style="display: none;">
                        <table class="side-panel-setting-notification-content-table">
                           <tbody>
                              <tr>
                                 <td class="notification-table-look">
                                    <div>
                                       <div class="icon icon-notification-announcement notification-child-icon-holder">
                                       </div>
                                       <div class="notification-child-text">
                                       </div>
                                    </div>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="side-panel-account-setting side-panel-setting-content-wrapper">
                        <table class="side-panel-setting-content-table">
                           <tbody>
                              <tr>
                                 <td><a class="side-navi-target-summary" href="#" target="_blank">
                                    <span class="icon icon-account_summary"></span>
                                    Summary
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td><a class="side-navi-target-updateprofile" href="#" target="_blank">
                                    <span class="icon icon-update_profile"></span>
                                    Update Profile
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-changepassword" href="#" target="_blank">
                                    <span class="icon icon-password_setting"></span>
                                    Change Password 
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-ptchangepassword" href="#" target="_blank">
                                    <span class="icon icon-password_setting"></span>
                                    Change Password PT 
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-announcement" href="#" target="_blank">
                                    <span class="icon icon-fun88_nortify"></span>
                                    Notifications
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-bet-record" href="#" target="_blank">
                                    <span class="icon icon-bet-record"></span>
                                    Bet Record
                                    </a>
                                 </td>
                              </tr>
                              <tr>
                                 <td>
                                    <a class="side-navi-target-self-exclusion" href="#" target="_blank">
                                    <span class="icon icon-self-exclusion"></span>
                                    Self Exclusion 
                                    </a>
                                 </td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                     <div class="side-panel-bank-status-setting side-panel-setting-content-wrapper">
                        <table class="side-panel-setting-content-table" id="side-panel-bank-status-body">
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="subMenu-Wrapper">
            <div class="row expanded header-menu-content" id="header-sportsbook-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul>
                     <li>
                        <div>
                           <a class="gameSelfExlusion vendor-hover sportsbookHover blue" data-producttype="sportsbook" href='#' data-content-target="Fun88Sports" onclick="CheckSportsbookQuickTransfer('SBT','redirect',this.href); return false;">
                           <span class="vendor-hover-title">
                           E-WALLET SPORTS
                           <sup style='color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;'>New!</sup>
                           </span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="gameSelfExlusion vendor-hover sportsbookHover" data-producttype="sportsbook" href='#' data-content-target="SP" onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">
                           <span class="vendor-hover-title">ONEWORKS SPORTS</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="gameSelfExlusion vendor-hover sportsbookHover" data-producttype="sportsbook" href='#' data-content-target="IPSB" onclick="CheckSportsbookQuickTransfer('IPSB','redirect',this.href); return false;">
                           <span class="vendor-hover-title">IM SPORTS</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
                  <ul>
                     <li>
                        <div>
                           <a class="other-hover" target="_blank" href='#'>
                           <span class="vendor-hover-title">SPORTS STATISTICS</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="other-hover" target="_blank" href='#'>SOCCER FACTS<span class="icon icon-fun88_arrow_thick_right"></span></a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory">
                  <div class="vendor-Content sportsbook firstChild SP">
                     <span class="header-vendor-tab-title">POPULAR SPORTS</span>
                     <ul class="vendor-Content sportsbook firstChild SP">
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">SOCCER</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">BASKETBALL</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">NUMBER GAME</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">VIRTUAL SPORTS</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','redirect',this.href); return false;">E-SPORTS</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SP','popup',this.href); return false;">COLOSSUS BETS</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content sportsbook Fun88Sports">
                     <span class="header-vendor-tab-title">POPULAR SPORTS</span>
                     <ul class="vendor-Content sportsbook firstChild Fun88Sports">
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('SBT','redirect',this.href); return false;">SOCCER</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content sportsbook IPSB">
                     <span class="header-vendor-tab-title">POPULAR SPORTS</span>
                     <ul class="vendor-Content sportsbook IPSB">
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('IPSB','redirect',this.href); return false;">SOCCER</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('IPSB','redirect',this.href); return false;">BASKETBALL</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('IPSB','redirect',this.href); return false;">VIRTUAL SOCCER</a></li>
                        <li><a class="game-link" href='#' onclick="CheckSportsbookQuickTransfer('IPSB','redirect',this.href); return false;">VIRTUAL BASKETBALL</a></li>
                     </ul>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image">
                  <p id="BodyContentPlaceHolderBase_SubHeaderControl_SportHappening" class="header-vendor-tab-title header-vendor-center-content">Next Match</p>
                  <div style="margin-top: 10px; text-align: center;">
                     <div style="position: relative">
                        <div style="position: absolute; width: 100%; top: 45%;">
                           <a id="header-sport-url-btn" target="_blank" href='#' data-sbt="#" data-sp="#" data-ipsb='#' class="header-sport-play-button" style="display: none; color: white; background-color: #33c85d; padding: 5px 20px; border-radius: 5px;">BET </a>
                        </div>
                        <img id="header-sportbook-livematch-image" class="header-vendor-center-content" src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Sportsbook/nav_fun88sports.jpg" />
                     </div>
                     <p id="BodyContentPlaceHolderBase_SubHeaderControl_matchTeamDetail" class="header-vendor-tab-title sport-next-Match header-vendor-center-content" style="margin: 15px auto 0;"></p>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Announcement">
                  <span class="header-vendor-tab-title">Announcement </span>
                  <div class="specialAnnouncementList">
                     <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px" class="announcementHolderList newsoption-4"></ul>
                  </div>
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-casino-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul id="casinoCategory" data-required="false">
                     <li>
                        <div>
                           <a class="vendor-hover livecasinoHover casino-game-tab blue" data-producttype="livecasino" data-content-target="ebt" style="text-transform: uppercase">
                           <span class="vendor-hover-title">E PALACE<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">HOT</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover livecasinoHover casino-game-tab" data-producttype="livecasino" data-content-target="gdl" style="text-transform: uppercase">
                           <span class="vendor-hover-title">Imperial Palace</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover livecasinoHover casino-game-tab" data-producttype="livecasino" data-content-target="ag" style="text-transform: uppercase">
                           <span class="vendor-hover-title">Royal Palace</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover livecasinoHover casino-game-tab" data-producttype="livecasino" data-content-target="abt" style="text-transform: uppercase">
                           <span class="vendor-hover-title">Dragon Palace</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover livecasinoHover casino-game-tab" data-producttype="livecasino" data-content-target="nle" style="text-transform: uppercase">
                           <span class="vendor-hover-title">Happy Palace<span> </span></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory" id="casinoGame">
                  <span class="header-vendor-tab-title">GAMES</span>
                  <ul class="vendor-Content livecasino abt">
                     <li><a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt" style="display: none;">Download Center</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;750&quot;)">VIP Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;751&quot;)">BidMe Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;749&quot;)">Bonus Deluxe Poker</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;752&quot;)">Sicbo</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;754&quot;)">Roulette</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;ABT&quot;,&quot;753&quot;)">Dragon Tiger</a></li>
                  </ul>
                  <ul class="vendor-Content livecasino nle">
                     <li><a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt" style="display: none;">Download Center</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;NLE&quot;,&quot;1280&quot;)">No commission baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;NLE&quot;,&quot;1279&quot;)">Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;NLE&quot;,&quot;1281&quot;)">Sicbo</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;NLE&quot;,&quot;1282&quot;)">Roulette</a></li>
                  </ul>
                  <ul class="vendor-Content livecasino ebt" style="display: block;">
                     <li><a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt" style="display: none;">Download Center</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1831&quot;)">Roulette</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1830&quot;)">Sicbo</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1829&quot;)">Dragon Tiger</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1828&quot;)">Speed Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1827&quot;)">VIP Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1826&quot;)">Non-Commission Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;EBT&quot;,&quot;1825&quot;)">Baccarat</a></li>
                  </ul>
                  <ul class="vendor-Content livecasino gdl">
                     <li><a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt" style="display: none;">Download Center</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;GDL&quot;,&quot;779&quot;)">Interactive Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;GDL&quot;,&quot;707&quot;)">3D Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;GDL&quot;,&quot;776&quot;)">Sic Bo</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;GDL&quot;,&quot;717&quot;)">Roulette</a></li>
                  </ul>
                  <ul class="vendor-Content livecasino ag">
                     <li><a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt" style="display: none;">Download Center</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;748&quot;)">Bid Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;719&quot;)">6 Card Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;721&quot;)">Quick Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;1862&quot;)">European Baccarat</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;730&quot;)">Multiplay</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;732&quot;)">Black Jack</a></li>
                     <li><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame(&quot;AG&quot;,&quot;1863&quot;)">European Blackjack</a></li>
                     <a class="livecasino-game-tab game-link" target="_blank" href="#" data-vendor="pt">Download Center</a>
                  </ul>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image slot-category" style="cursor: default;">
                  <p class="header-vendor-tab-title header-vendor-center-content">RECOMMENDED</p>
                  <div class="header-vendor-center-content" style="margin-top: 10px;">
                     <div class="game-overlay game-wrapper-home" style="position: relative;" onclick="Games.EnterGame('EBT', '1831');">
                        <div class="playNowIconOverlay">
                           <div class="gameh-play-f">
                              <img class="w20 spinner lazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/bg_O_2.png" alt="loader" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC">
                           </div>
                           <div class="gameh-centerwrap">
                              <img width="79" height="80" class="gameh-centerplay w100p playLazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/play.png" alt="play" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC">
                           </div>
                        </div>
                        <div class="slot-image-on-header"><img width="294" height="181" onerror="this.src='{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/Games/Icon/coming_soon.gif';" class="header-vendor-center-content header-slot-game-image-service Loaded" src="{{APP_ASSETS}}images/EBT_Roulette.jpg" data-ori="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/Games/LiveCasino/EBT/EBT_Roulette.jpg"></div>
                        <div class="gameLaunch slot-play-button-header"></div>
                        <div class="slot-demo-button-header" style="position: absolute; top: 55%; left: 37%;"></div>
                     </div>
                     <div style="float: left;">
                        <div class="game-type-header">Live Casino</div>
                        <p class="header-vendor-tab-title" style="margin-bottom: 0; width: 350px;"><span class="game-name-header">Roulette</span><span style="font-size: 12px; color: #3f97ff; margin-left: 15px;" class="games-vendor-name-header">E PALACE</span></p>
                     </div>
                  </div>
                  <div class="header-vendor-center-content LB" style="margin-top: 10px;">
                     <div class="game-overlay game-wrapper-home" style="position: relative;" onclick="Games.EnterGame('EBT', '1831');">
                        <div class="playNowIconOverlay">
                           <div class="gameh-play-f">
                              <img class="w20 spinner lazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/Games/Icon/bg_O_2.png" alt="loader" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC">
                           </div>
                           <div class="gameh-centerwrap">
                              <img width="79" height="80" class="gameh-centerplay w100p playLazy" data-original="{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/Games/Icon/play.png" alt="play" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC">
                           </div>
                        </div>
                        <div class="slot-image-on-header"></div>
                        <div class="gameLaunch slot-play-button-header"></div>
                        <div class="slot-demo-button-header" style="position: absolute; top: 55%; left: 37%;"></div>
                     </div>
                     <div style="float: left;">
                        <div class="game-type-header"></div>
                        <p class="header-vendor-tab-title" style="margin-bottom: 0; width: 350px;"><span class="game-name-header"></span><span style="font-size: 12px; color: #3f97ff; margin-left: 15px;" class="games-vendor-name-header"></span></p>
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Announcement">
                  <span class="header-vendor-tab-title">Announcement </span>
                  <div class="specialAnnouncementList">
                     <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px" class="announcementHolderList newsoption-5"></ul>
                  </div>
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-pt-slot-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul id="header-vendor-wrapper">
                     <li>
                        <div>
                           <a class="vendor-hover ptslotHover header-slot-vendor blue" data-producttype="ptslot" data-content-target-seo="playtech" data-content-target="pt">
                           <span class="vendor-hover-title">PT SLOT</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory">
                  <span class="header-vendor-tab-title">GAMES</span>
                  <ul class="vendor-Content ptslot firstChild pt">
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="recent" data-content-target-seo="playtech" data-vendor="mgsqf">LAST PLAYED GAME</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="new" data-content-target-seo="playtech" data-vendor="mgsqf">New Games</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="recommended" data-content-target-seo="playtech" data-vendor="mgsqf">RECOMMENDED</a></li>
                     <li><a class="slot-game-tab  game-link" data-mixitup-control="" data-slot-target="fav" data-content-target-seo="playtech" data-vendor="mgsqf">My Favorites</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="HotGames" data-content-target-seo="playtech" data-vendor="mgsqf">Hot Games</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Multiplayer" data-content-target-seo="playtech" data-vendor="mgsqf">Multiplayer</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Jackpot" data-content-target-seo="playtech" data-vendor="mgsqf">Jackpot</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Freespin" data-content-target-seo="playtech" data-vendor="mgsqf">Free Spins</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="BonusRound" data-content-target-seo="playtech" data-vendor="mgsqf">Game Bonus</a></li>
                     <li><a class="downloadlink" target="_blank" href="#" data-vendor="pt">Download Center</a></li>
                  </ul>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image slot-category" style="cursor: default;">
                  <p class="header-vendor-tab-title header-vendor-center-content">RECOMMENDED</p>
                  <div class="header-vendor-center-content" style="margin-top: 10px;">
                     <div class="game-overlay game-wrapper-home" style="position: relative;" onclick="Games.EnterGame('PT', '1234');">
                        <div class="playNowIconOverlay">
                           <div class="gameh-play-f">
                              <img class="w20 spinner lazy" data-original="{{APP_ASSETS}}images/bg_O_2.png" alt="loader" src="{{APP_ASSETS}}images/bg_O_2.png">
                           </div>
                           <div class="gameh-centerwrap">
                              <img width="79" height="80" class="gameh-centerplay w100p playLazy" data-original="{{APP_ASSETS}}images/play.png" alt="play" src="{{APP_ASSETS}}images/play.png">
                           </div>
                        </div>
                        <div class="slot-image-on-header"><img width="294" height="181" onerror="this.src='{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/Games/Icon/coming_soon.gif';" class="header-vendor-center-content header-slot-game-image-service Loaded" src="{{APP_ASSETS}}images/RiRiShengCai.jpg" data-ori="{{APP_ASSETS}}images/RiRiShengCai.jpg" data-hover="{{APP_ASSETS}}images/RiRiShengCai.gif"></div>
                        <div class="gameLaunch slot-play-button-header"></div>
                        <div class="slot-demo-button-header" style="position: absolute; top: 55%; left: 37%;"></div>
                     </div>
                     <div style="float: left;">
                        <div class="game-type-header">Slots</div>
                        <p class="header-vendor-tab-title" style="margin-bottom: 0; width: 350px;"><span class="game-name-header">Ri Ri Sheng Cai</span><span style="font-size: 12px; color: #3f97ff; margin-left: 15px;" class="games-vendor-name-header">Play Tech</span></p>
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Announcement" style="position: relative;">
                  <span class="header-vendor-tab-title">Announcement </span>
                  <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px">
                     <li style="padding-bottom: 10px; position: relative;"><span class="icon icon-fun88_bookmark_star_checked"></span><span style="font-weight: normal; color: #3f97ff; font-size: 14px;">Jackpot Prize Name</span>
                     </li>
                     <li id="headerPtControlJackpot">
                        <div id="headerPtJp" class="jackpotTotalAmount" style="position: relative; float: left;">
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -31px;" class="jodometer_decimal_1" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Decimal 2"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -88.1944px;" class="jodometer_decimal_0" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Decimal 1"></div>
                           <div class="icon SlotJackpotFrame comma" style="width: 10px;">
                              <div style="background:url({{APP_ASSETS}}images/jackpotNumberPic.png) no-repeat center bottom;background-position: 0 -413px;" class="dotSeperator"></div>
                           </div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -143.228px;" class="jodometer_en jodometer_integer_0" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 1"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -128.026px;" class="jodometer_en jodometer_integer_1" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 2"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -340.468px;" class="jodometer_en jodometer_integer_2" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 3"></div>
                           <div class="icon SlotJackpotFrame comma" style="width: 6px;">
                              <div style="background:url({{APP_ASSETS}}images/jackpotNumberPic.png) no-repeat center bottom;" class="commaSeperator"></div>
                           </div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position: absolute; right: 6px; top: -279px;" class="jodometer_en jodometer_integer_3" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 4"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-93px;" class="jodometer_en jodometer_integer_4" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 5"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-31px;" class="jodometer_en jodometer_integer_5" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 6"></div>
                           <div class="icon SlotJackpotFrame comma" style="width: 6px;">
                              <div style="background:url({{APP_ASSETS}}images/jackpotNumberPic.png) no-repeat center bottom;" class="commaSeperator"></div>
                           </div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-186px;" class="jodometer_en jodometer_integer_6" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 7"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-217px;" class="jodometer_en jodometer_integer_7" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 8"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-124px;" class="jodometer_en jodometer_integer_8" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 9"></div>
                           <div class="icon SlotJackpotFrame comma" style="width: 6px;">
                              <div style="background:url({{APP_ASSETS}}images/jackpotNumberPic.png) no-repeat center bottom;" class="commaSeperator"></div>
                           </div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-31px;" class="jodometer_en jodometer_integer_9" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 10"></div>
                           <div class="icon icon-frame SlotJackpotFrame"><img style="position:absolute; right:6px; top:-279px;" class="jodometer_en jodometer_integer_10" src="{{APP_ASSETS}}images/jackpotNumberPic.png" alt="Integer 11"></div>
                        </div>
                     </li>
                  </ul>
                  <div class="specialAnnouncementList" style="margin-top: 50px; position: absolute;">
                     <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px" class="announcementHolderList newsoption-9"></ul>
                  </div>
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-slot-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul id="slotCategory" data-required="false">
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor blue" data-producttype="slot" data-content-target="mgsqf" style="text-transform: uppercase" data-content-target-seo="microgaming">
                           <span class="vendor-hover-title">Microgaming<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">HOT</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="swf" style="text-transform: uppercase" data-content-target-seo="skywind">
                           <span class="vendor-hover-title">Skywind</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="tg" style="text-transform: uppercase" data-content-target-seo="pragmaticplay">
                           <span class="vendor-hover-title">Pragmatic Play</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="lx" style="text-transform: uppercase" data-content-target-seo="laxino">
                           <span class="vendor-hover-title">Laxino</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="bsg" style="text-transform: uppercase" data-content-target-seo="betsoft">
                           <span class="vendor-hover-title">Betsoft</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="pt" style="text-transform: uppercase" data-content-target-seo="playtech">
                           <span class="vendor-hover-title">Play Tech</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover header-slot-vendor" data-producttype="slot" data-content-target="cqg" style="text-transform: uppercase" data-content-target-seo="cq9">
                           <span class="vendor-hover-title">CQ9</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover slotHover" data-producttype="slot" href="#" target="_blank">
                           <span class="vendor-hover-title">LEADER BOARD</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory">
                  <span class="header-vendor-tab-title">GAMES</span>
                  <ul class="vendor-Content slot firstChild mgsqf tg lx bsg iwg swf pt cqg tgp_slot">
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="recent" data-content-target-seo="cq9" data-vendor="cq9">LAST PLAYED GAME</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="new" data-content-target-seo="cq9" data-vendor="cq9">New Games</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="recommended" data-content-target-seo="cq9" data-vendor="cq9">RECOMMENDED</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="fav" data-content-target-seo="cq9" data-vendor="cq9">My Favorites</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="hotgames" data-content-target-seo="cq9" data-vendor="cq9">Hot Games</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Multiplayer" data-content-target-seo="cq9" data-vendor="cq9">Multiplayer</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Jackpot" data-content-target-seo="cq9" data-vendor="cq9">Jackpot</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="TableGame" data-content-target-seo="cq9" data-vendor="cq9">Table Game</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="OthersGame" data-content-target-seo="cq9" data-vendor="cq9">Other</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="Freespin" data-content-target-seo="cq9" data-vendor="cq9">Free Spins</a></li>
                     <li><a class="slot-game-tab game-link" data-mixitup-control="" data-slot-target="BonusRound" data-content-target-seo="cq9" data-vendor="cq9">Game Bonus</a></li>
                     <li><a class="downloadlink" target="_blank" href="#" data-vendor="cq9">Download Center</a></li>
                  </ul>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image slot-category" style="cursor: default;">
                  <p class="header-vendor-tab-title header-vendor-center-content">RECOMMENDED</p>
                  <div class="header-vendor-center-content" style="margin-top: 10px;">
                     <div class="game-overlay game-wrapper-home" style="position: relative;" onclick="Games.EnterGame('LX', '286');">
                        <div class="playNowIconOverlay">
                           <div class="gameh-play-f">
                              <img class="w20 spinner lazy" data-original="{{APP_ASSETS}}images/bg_O_2.png" alt="loader" src="{{APP_ASSETS}}images/bg_O_2.png">
                           </div>
                           <div class="gameh-centerwrap">
                              <img width="79" height="80" class="gameh-centerplay w100p playLazy" data-original="{{APP_ASSETS}}images/play.png" alt="play" src="{{APP_ASSETS}}images/play.png">
                           </div>
                        </div>
                        <div class="slot-image-on-header"><img width="294" height="181" onerror="this.src='{{APP_ASSETS}}www.enhuoyea11.net//Assets/images/Games/Icon/coming_soon.gif';" class="header-vendor-center-content header-slot-game-image-service Loaded" src="{{APP_ASSETS}}images/magic-stone.jpg" data-ori="{{APP_ASSETS}}images/magic-stone.jpg" data-hover="{{APP_ASSETS}}images/magic-stone.gif"></div>
                        <div class="gameLaunch slot-play-button-header"></div>
                        <div class="slot-demo-button-header" style="position: absolute; top: 55%; left: 37%;"></div>
                     </div>
                     <div style="float: left;">
                        <div class="game-type-header">Video Slots</div>
                        <p class="header-vendor-tab-title" style="margin-bottom: 0; width: 350px;"><span class="game-name-header">Magic Stone</span><span style="font-size: 12px; color: #3f97ff; margin-left: 15px;" class="games-vendor-name-header">Laxino</span></p>
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Announcement" style="position: relative;">
                  <span class="header-vendor-tab-title">Announcement </span>
                  <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px">
                     <li style="padding-bottom: 10px; position: relative;"><span class="icon icon-fun88_bookmark_star_checked"></span><span style="font-weight: normal; color: #3f97ff; font-size: 14px;">Jackpot Prize Name</span>
                     </li>
                     <li id="headerControlJackpot">
                        <div id="headerJp" class="jackpotTotalAmount" style="position: relative; float: left;">
                        </div>
                     </li>
                  </ul>
                  <div class="specialAnnouncementList" style="margin-top: 50px; position: absolute;">
                     <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px" class="announcementHolderList newsoption-10"></ul>
                  </div>
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-keno-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul>
                     <li>
                        <div>
                           <a class="vendor-hover kenoHover blue" data-producttype="keno" href#' data-content-target="SLC">
                           <span class="vendor-hover-title"><span class="vendor-hover-title">LOTTERY</span></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory">
                  <span class="header-vendor-tab-title">VENDOR</span>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image">
                  <p class="header-vendor-tab-title header-vendor-center-content">NEXT DRAWING STARTS IN</p>
                  <div class="header-vendor-center-content" style="margin-top: 10px;">
                  </div>
                  <div class="header-vendor-center-content" style="margin-top: 10px;">
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Announcement">
                  <span class="header-vendor-tab-title">Announcement </span>
                  <div class="specialAnnouncementList">
                     <ul style="list-style: none; list-style-type: none; margin: 0px; font-size: 14px; margin-top: 10px" class="announcementHolderList newsoption-7"></ul>
                  </div>
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-more-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul>
                     <li>
                        <div>
                           <a class="vendor-hover blue moreHover" data-producttype="more" href='#' data-content-target="MoreFishing" target="_blank">
                           <span class="vendor-hover-title">FISHING GAME <sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">Hot</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover blue moreHover" data-producttype="more" href='#' data-content-target="WorldCup" target="_blank">
                           <span class="vendor-hover-title">SPONSORSHIP<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">New!</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover blue moreHover" data-producttype="more" data-content-target="MoreBliss" target="_blank" href='#'>
                           <span class="vendor-hover-title">E-WALLET ANGELS</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover moreHover" data-producttype="more" data-content-target="MoreFriend" href='#' target="_blank">
                           <span class="vendor-hover-title">REFER-A-FRIEND</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover moreHover" data-producttype="more" data-content-target="MoreAffiliate" href='#' target="_blank">
                           <span class="vendor-hover-title">AFFILIATE</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover moreHover" data-producttype="more" data-content-target="MoreReward" onclick="Rewards.syncLogin(true);" href="#" target="_blank">
                           <span class="vendor-hover-title">E-WALLET REWARD</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover moreHover" data-producttype="more" data-content-target="MoreDownload" href='#' target="_blank">
                           <span class="vendor-hover-title">DOWNLOAD CENTER</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory" style="width: 330px">
                  <div class="vendor-Content more MoreFishing">
                     <span class="header-vendor-tab-title">Fishing Game</span>
                     <div class="vendor-Content more firstChild MoreFishing">
                        <ul style="list-style: none; list-style-type: none; margin: 15px 0 0 0">
                           <li style="padding: 3px 0"><a class="game-link" href="javascript:void(0)" onclick="Games.EnterGame('PT', '695');">PT FISH CASH</a></li>
                        </ul>
                     </div>
                  </div>
                  <div class="vendor-Content more WorldCup">
                     <span class="header-vendor-tab-title">Sponsorship</span>
                     <div style="margin-top: 15px" class="text-white">
                        Proving its commitment to provide the best and the greatest to its members, E-WALLET proudly sponsors English Premier League teams, sports legends, and influencers in the entertainment industry. Get to know more our partners, the most exclusive promotions, and our milestones through the years!
                     </div>
                  </div>
                  <div class="vendor-Content more MoreVip">
                     <span class="header-vendor-tab-title">VIP Info</span>
                     <div style="margin-top: 15px" class="text-white">
                        Be one of our E-WALLET high-rollers and enjoy an unparalleled gaming experience with a personal touch! Our exclusive VIP program is designed to meet your needs and desires. Our VIP will enjoy more frequent, more exciting, and more rewarding priviledges!
                     </div>
                  </div>
                  <div class="vendor-Content more firstChild MoreBliss">
                     <span class="header-vendor-tab-title">About E-WALLET Angels</span>
                     <div style="margin-top: 15px" class="text-white">
                        Enjoy E-WALLET’s very own news and entertainment portal. Be up to date with the latest news on sports and entertainment. And watch out for the hottest girls in Asia! Exclusive games and promotions up for grabs!
                     </div>
                  </div>
                  <div class="vendor-Content more MoreFriend">
                     <span class="header-vendor-tab-title" class="text-white">Share the Fun</span>
                     <div style="margin-top: 15px">
                        Share the fun with your friends while earning big! Refer friend to E-WALLET and enjoy our rewards to you and your friend. Introduce more friends for higher Free Bets, plus cool gadgets!
                     </div>
                  </div>
                  <div class="vendor-Content more MoreAffiliate">
                     <span class="header-vendor-tab-title">Affiliate Info</span>
                     <div style="margin-top: 15px" class="text-white">
                        Affiliate program is strategic cooperation relationship between you and E-WALLET. Grow with us as one of our partners, gain returns and dividends in accordance with the stakes your referred players have laid down. Register, promote and earn!
                     </div>
                  </div>
                  <div class="vendor-Content more MoreReward">
                     <span class="header-vendor-tab-title">About E-WALLET Rewards</span>
                     <div style="margin-top: 15px" class="text-white">
                        You’re always winning at E-WALLET! With our Rewards Program, redeem luxury, lifestyle, electronics, free bets and more! Don’t miss out on lucky draws and tournaments!
                     </div>
                  </div>
                  <div class="vendor-Content more MoreDownload">
                     <span class="header-vendor-tab-title">E-WALLET Download Center</span>
                     <div style="margin-top: 15px" class="text-white">
                        At E-WALLET, we value your convenience. No need for browsers, install all your needed gaming access. Convenient gaming experience on the go with our downloadable apps, tools, live chat and more!
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image">
                  <p id="BodyContentPlaceHolderBase_SubHeaderControl_P1" class="header-vendor-tab-title header-vendor-center-content"></p>
                  <div style="margin-top: 10px; text-align: center;">
                     <div style="position: relative; cursor: pointer;">
                        <a id="game_image_redirect" href='#' target="_blank">
                        <img id="header-fishing-game-image" class="header-vendor-center-content" src="" data-src="{{APP_ASSETS}}more-tab-banner.jpg" />
                        </a>
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper">
               </div>
            </div>
            <div class="row expanded header-menu-content" id="header-promotion-menu">
               <div class="column large-2 vendor-wrapper">
                  <ul>
                     <li>
                        <div>
                           <a class="vendor-hover blue promoHover" data-producttype="promo" href='#' data-content-target="DailyDeals" target="_blank">
                           <span class="vendor-hover-title">DAILY DEALS<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">Hot</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" href='#' data-content-target="NewMember" target="_blank">
                           <span class="vendor-hover-title">NEW MEMBERS<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">New!</sup></span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" href='#' data-content-target="Sportsbook" target="_blank">
                           <span class="vendor-hover-title">SPORTSBOOK</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="LiveCasino" target="_blank" href='#'>
                           <span class="vendor-hover-title">LIVE CASINO</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="Games" target="_blank" href='#'>
                           <span class="vendor-hover-title">GAMES</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="HeaderKeno" href='#' target="_blank">
                           <span class="vendor-hover-title">POKER</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>       
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="BrandSponsorship" href='#' target="_blank">
                           <span class="vendor-hover-title">Sponsorship</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="Exclusive" onclick="Rewards.syncLogin(true);" href='#' target="_blank">
                           <span class="vendor-hover-title">EXCLUSIVE</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                     <li>
                        <div>
                           <a class="vendor-hover promoHover" data-producttype="promo" data-content-target="HeaderVip" href='#' target="_blank">
                           <span class="vendor-hover-title">VIP</span>
                           <span class="icon icon-fun88_arrow_thick_right"></span>
                           </a>
                        </div>
                     </li>
                  </ul>
               </div>
               <div class="column large-2 vendor-Content-Wrapper vendor-Content-GameCategory" style="width: 330px">
                  <div class="vendor-Content promo firstChild DailyDeals">
                     <span class="header-vendor-tab-title">DAILY DEALS</span>
                     <ul class="vendor-Content promo firstChild DailyDeals">
                        <li><a class="game-link" target="_blank" href='#'>Today's Deals </a></li>
                        <li><a class="game-link" target="_blank" href='#'>History </a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo NewMember">
                     <span class="header-vendor-tab-title">NEW MEMBERS</span>
                     <ul class="vendor-Content promo firstChild NewMember">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo Sportsbook">
                     <span class="header-vendor-tab-title">SPORTSBOOK</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo LiveCasino">
                     <span class="header-vendor-tab-title">LIVE CASINO</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo Games">
                     <span class="header-vendor-tab-title">GAMES</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo HeaderKeno">
                     <span class="header-vendor-tab-title">LOTTERY</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo BrandSponsorship">
                     <span class="header-vendor-tab-title">Sponsorship</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo Exclusive">
                     <span class="header-vendor-tab-title">EXCLUSIVE</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
                  <div class="vendor-Content promo HeaderVip">
                     <span class="header-vendor-tab-title">E-WALLET VIP</span>
                     <ul class="vendor-Content promo firstChild DailyDeals NewMember Sportsbook LiveCasino Games HeaderKeno BrandSponsorship Exclusive HeaderVip">
                        <li><a class="game-link" target="_blank" href='#'>Bonus</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Rebate</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Special</a></li>
                        <li><a class="game-link" target="_blank" href='#'>Others</a></li>
                     </ul>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper Image">
                  <p id="BodyContentPlaceHolderBase_SubHeaderControl_P2" class="header-vendor-tab-title header-vendor-center-content"></p>
                  <div style="margin-top: 10px; text-align: center;">
                     <div style="position: relative; cursor: pointer;">
                        <a id="promo_image_redirect" href="#" target="_blank">
                        <img id="promo-image" class="header-vendor-center-content" src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/homecontrol/header/Promotion.jpg" data-src="{{APP_ASSETS}}www.enhuoyea11.net/Assets/images/homecontrol/header/Promotion.jpg" />
                        </a>
                     </div>
                  </div>
               </div>
               <div class="column large-4 vendor-Content-Wrapper">
               </div>
            </div>
         </div>
         <div class="mobile-menu">
            <div>
               <div class="header-mobile-menu-wrapper-left">
                  <div class="mobile-header-wrapper">
                     <div class="mobile-header-panel-close">
                        <span class="icon icon-close_btn" onclick="offCanvas.TriggerCloseMenu()"></span>
                     </div>
                     <div class="mobile-header-logo">

                        <span style="color: white;text-size:18px;">E-WALLET</span>

                     </div>
                  </div>
                  <div>
                     <table class="header-mobile-menu-table-wrapper IntroSteps Intro_MobileHeaderMenu">
                        <tbody>
                           <tr class="mobile-navigation-tr" data-navigation="sportsbook">
                              <td><a href="#"><span class="icon icon-fun88_sports"></span></a></td>
                              <td class="row">
                                 <a href="#" class="small-10 columns">
                                 SPORTS
                                 <sup style='color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;'>Hot</sup>
                                 </a>
                                 <span class="large-font float-right small-2 MobileMenuParent" id="SportsBook-RightIcon">+</span>
                              </td>
                           </tr>
                           <tr class="SportsBook-Child mobile-sub-menu">
                              <td colspan="2">
                                 <ul class="hideBullet">
                                    <li>
                                       <div>
                                          <a class="gameSelfExlusion black sportsbookHover SP" target="_blank" href='#' onclick="CheckSportsbookQuickTransfer('sbt','redirect',this.href); return false;">
                                          <span class="vendor-hover-title">
                                          E-WALLET SPORTS
                                          <sup style='color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;'>New!</sup>
                                          </span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div>
                                          <a class="gameSelfExlusion black sportsbookHover SP" target="_blank" href='#' onclick="CheckSportsbookQuickTransfer('sp','redirect',this.href); return false;">
                                          <span class="vendor-hover-title">ONEWORKS SPORTS&nbsp</span>
                                          </a>
                                       </div>
                                    </li>
                                    <li>
                                       <div>
                                          <a class="gameSelfExlusion black sportsbookHover IPSB" href='#' onclick="CheckSportsbookQuickTransfer('ipsb','redirect',this.href); return false;">
                                          <span class="vendor-hover-title">IM SPORTS&nbsp</span>
                                          </a>
                                       </div>
                                    </li>
                                 </ul>
                              </td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="livecasino">
                              <td><a href="#"><span class="icon icon-live_casino"></span></a></td>
                              <td class="row">
                                 <a href="#" class="small-10 columns">CASINO</a>
                                 <span class="large-font small-2 float-right MobileMenuParent" id="LiveCasino-RightIcon">+</span>
                              </td>
                           </tr>
                           <tr class="LiveCasino-Child mobile-sub-menu">
                              <td colspan="2">
                                 <ul id="casinoMobileCategory" class="hideBullet" data-required="true">
                                    <li>
                                       <div>
                                          <a class="mobile-font livecasinoHover casino-game-tab" data-producttype="livecasino" style="text-transform: uppercase">
                                          <span class="vendor-hover-title"></span>
                                          </a>
                                       </div>
                                    </li>
                                 </ul>
                              </td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="ptslot" id="ptSlot">
                              <td><a id="ptSlotUrl1" href='#'><span class="icon icon-slot"></span></a></td>
                              <td><a id="ptSlotUrl2" href='#'>PT SLOT</a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="slot">
                              <td><a href='#'><span class="icon icon-slot"></span></a></td>
                              <td class="row">
                                 <a href='#' class="small-10 columns">SLOT</a>
                                 <span class="large-font float-right small-2 MobileMenuParent" id="Slot-RightIcon">+</span>
                              </td>
                           </tr>
                           <tr class="Slot-Child mobile-sub-menu">
                              <td colspan="2">
                                 <ul id="slotMobileCategory" class="hideBullet" data-required="true">
                                    <li>
                                       <div>
                                          <a class="mobile-font slotHover header-slot-vendor" data-producttype="slot" style="text-transform: uppercase">
                                          <span class="vendor-hover-title"></span>
                                          </a>
                                       </div>
                                    </li>
                                 </ul>
                              </td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="poker">
                              <td><a href='#'><span class="icon icon-Keno"></span></a></td>
                              <td><a href='#'>POKER</a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="promotion">
                              <td><a href='#'><span class="icon icon-Promo"></span></a></td>
                              <td class="row">
                                 <a href='#' class="small-10 columns">PROMOTION<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">Hot</sup></a>
                                 <span class="large-font float-right small-2 MobileMenuParent" id="Promotion-RightIcon">+</span>
                              </td>
                           </tr>
                           <tr class="Promotion-Child mobile-sub-menu">
                              <td colspan="2">
                                 <div class="hideBullet left">
                                    <a href='#' class="mobieSidePromoFont DailyDeals">
                                       <div data-type="dailydeals" class="PromoFont">DAILY DEALS<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">Hot</sup></div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont NewMember">
                                       <div data-type="newmember" class="PromoFont">NEW MEMBERS</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont Sportsbook">
                                       <div data-type="sportsbook" class="PromoFont">SPORTSBOOK</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont LiveCasino">
                                       <div data-type="livecasino" class="PromoFont">LIVE CASINO</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont Games">
                                       <div data-type="games" class="PromoFont">GAMES</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont sponsorship">
                                       <div data-type="sponsorship" class="PromoFont">Sponsorship<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">HOT</sup></div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont Poker">
                                       <div data-type="poker" class="PromoFont">POKER</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont exclusive">
                                       <div data-type="exclusive" class="PromoFont">EXCLUSIVE</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont vip">
                                       <div data-type="vip" class="PromoFont">VIP</div>
                                    </a>
                                    <a href='#' class="mobieSidePromoFont woturnover">
                                       <div data-type="woturnover" class="PromoFont">Without TO</div>
                                    </a>
                                 </div>
                              </td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="fishing">
                              <td><a href='#'><span class="icon icon-fishing_game"></span></a></td>
                              <td class="row">
                                 <a href='#' class="small-10 columns">FISHING GAME</a>
                                 <span class="large-font float-right small-2 MobileMenuParent" id="Fishing-RightIcon">+</span>
                              </td>
                           </tr>
                           <tr class="Fishing-Child mobile-sub-menu">
                              <td colspan="2">
                                 <ul class="hideBullet" id="fishing-menu-list">
                                    <li style="padding: 6px 0; font-weight: bold"><a class="game-link vendor-hover" href="javascript:void(0)" data-content-target-fishing="PT" onclick="Games.EnterGame('PT', '695');">PT FISH CASH</a></li>
                                 </ul>
                              </td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="bliss">
                              <td><a href='#' target="_blank"><span class="icon icon-worldcup-icon"></span></a></td>
                              <td><a href='#' target="_blank">SPONSORSHIP<sup style="color:red;position:relative;z-index:9999; margin-left: 1%;font-weight:700;text-transform:uppercase;">New!</sup></a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="bliss">
                              <td><a href='#' target="_blank"><span class="icon icon-bliss"></span></a></td>
                              <td><a href='#' target="_blank">E-WALLET ANGELS</a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="introafriend">
                              <td><a href='#'><span class="icon icon-referral"></span></a></td>
                              <td><a href='#'>REFER-A-FRIEND</a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="affiliate">
                              <td><a href='#' target="_blank"><span class="icon icon-affiliate"></span></a></td>
                              <td><a href='#' target="_blank">AFFILIATE</a></td>
                           </tr>
                           <tr class="mobile-navigation-tr" data-navigation="reward">
                              <td><a onclick="Rewards.syncLogin(true);" href="#" target="_blank"><span class="icon icon-reward"></span></a></td>
                              <td><a onclick="Rewards.syncLogin(true);" href="#" target="_blank">E-WALLET REWARD</a></td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
         <div class="mobile-navi">
            <div>
               <div class="header-mobile-menu-wrapper-right">
                  <div class="mobile-header-wrapper">
                     <p class="header-mobile-menu-login" style="display: inline-block">
                        LOGIN
                     </p>
                     <div class="mobile-header-panel-close" style="float: right">
                        <span class="icon icon-close_btn" style="color: black" onclick="offCanvas.TriggerCloseMenu()"></span>
                     </div>
                  </div>
                  <div class="mobile-header-wrapper" style="height: auto; padding-top: 0; border-bottom: 1px solid #dcdcdc;">
                     <div class="inputloginWrapper">
                        <input name="" type="text" maxlength="20" id="txtMobileUsername" autocomplete="off" autocorrect="off" autocapitalize="none" placeholder="Username" />
                     </div>
                     <div class="inputloginWrapper">
                        <input name="" type="password" maxlength="20" id="txtMobilePassword" autocomplete="off" autocorrect="off" autocapitalize="none" placeholder="Password" />
                     </div>
                     <div class="inputloginWrapper">
                        <a id="mobileLogin" class="btnMobileLogin" onclick="Member.LoginViaMobile();">LOGIN</a>
                     </div>
                     <div class="inputloginWrapper">
                        <a class="btmMobileRegister" href="#">JOIN NOW</a>
                     </div>
                     <div class="inputLinkWrapper">
                        <a href='#'>Forget ? </a>
                        <span>|  </span>
                        <a onclick="Page.popUpLiveChat()">Contact Us</a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="mobile-navi-table-wrapper IntroSteps Intro_MobileSideBar">
               <table class="mobile-navi-table">
                  <tbody>
                     <tr onclick="sidePanel.MobileSettingToggle(this);" class="mobile-navi-action" data-target="mobile-navi-bank-status-extend">
                        <td>
                           <span class="icon icon-bank_status navi-icon"></span>
                        </td>
                        <td>
                           <span class="navi-title">Bank Info</span>
                        </td>
                        <td><span class="icon icon-fun88_arrow_down_triangle mobile-navi-down-arrow"></span>
                           <span class="icon icon-fun88_arrow_up_triangle mobile-navi-up-arrow"></span>
                        </td>
                     </tr>
                     <tr>
                        <td colspan="3" class="navi-content-wrapper">
                           <div class="mobile-navi-bank-status-extend mobile-navi-extend">
                              <ul id="mobile-bank-status-view" data-open="bankStatusMessage">
                              </ul>
                           </div>
                        </td>
                     </tr>
                     <tr class="mobile-navi-action">
                        <td>
                           <span class="icon icon-help_center navi-icon"></span>
                        </td>
                        <td>
                           <span class="navi-title">
                           <a href='#'>
                           Help Center
                           </a>
                           </span>
                        </td>
                        <td></td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <section id="bankStatusMessage" class="Modal-Bank-Status-Wrapper" data-reveal style="display: none; width: 800px !important">
            <div class="Modal-Header-Wrapper">
               <span class="Modal-Header-Title">
               Offline Bank Info
               </span>
               <span class="icon icon-close-circle-button Modal-Bank-Status-Close-Button" data-close></span>
            </div>
            <div class="Modal-Content-Wrapper">
               <div class="row expanded" id="banks-status-mobile-view-detail">
               </div>
            </div>
         </section>
         <div id="modal-SmsVerification" class="reveal full modal-Container" data-reveal="" style="z-index: 4;"></div>
         <div id="modal-FunLineQRScan" class="reveal full modal-Container" data-reveal="" style="z-index: 4;"></div>
         <div id="modal-tutotrial" class="reveal full modal-Container" data-reveal="" style="z-index: 4;">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title">Installation Guide</span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <div class="modal-content-message">
                     <p id="InstallatioSsubtitle">Play at E-WALLET on your iOS device in 6 EASY STEPS:</p>
                     <ol>
                        <li>If you have the previous version of the E-WALLET App, please uninstall the application.</li>
                        <li>Scan QR code</li>
                        <li>After downloading, click on “Install”. It will run automatically in the background.</li>
                        <li>Go to SETTINGS > GENERAL > PROFILES & DEVICE MANAGEMENT</li>
                        <li>Click on “Guangzhou Juhuang Network Technology”, then click on “Trust”.</li>
                        <li>You’re all set to play at E-WALLET!</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div id="modal-ebt-tutotrial" class="reveal full modal-Container" data-reveal="" style="z-index: 4;">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title">Installation Guide</span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <div class="modal-content-message">
                     <p id="InstallatioSsubtitle">Play at E-WALLET on your iOS device in 6 EASY STEPS:</p>
                     <ol>
                        <li>If you have the previous version of the E-WALLET EBET App, please uninstall the application.</li>
                        <li>Scan QR code</li>
                        <li>After downloading, click on “Install”. It will run automatically in the background.</li>
                        <li>Go to SETTINGS > GENERAL > PROFILES & DEVICE MANAGEMENT</li>
                        <li>Click on “TIRABIT,OOO”, then click on “Trust”.</li>
                        <li>You’re all set to play at E-WALLET!</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div id="modal-ebetApp-tutotrial" class="reveal full modal-Container" data-reveal="" style="z-index: 4;">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title">User Guide</span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <div class="modal-content-message">
                     <u>Login Guide</u>
                     <ol style="list-style-type: none">
                        <li>Please add f8 in front of username when login using app</li>
                        <br/>
                        <li>E.g.</li>
                        <li>E-WALLET username:  abcd1234</li>
                        <li>ebet app login username: f8abcd1234</li>
                        <li>*password will be same as E-WALLET account</li>
                     </ol>
                     <br/>
                     <u>Deposit Guide</u>
                     <ol>
                        <li>Click on the (+) button on the top left of the screen.</li>
                        <li>User will be directed to E-WALLET deposit page by clicking the (+) button.</li>
                        <li>User may login and performs transaction.</li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
         <div id="modal-Roulette" class="reveal full modal-Container" data-reveal="" style="z-index: 4; padding: 0;" data-multiple-opened="true"></div>
         <div id="modal-Roulette-Message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  <span class="prizesWon"></span>
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message text-alignment-roulette" style="font-size: 18px;">
                  </p>
               </div>
               <div class="modal-button roulette">
                  <span class="modal-button-message lightblue" data-close>Setuju</span>
                  <span class="modal-button-message green functionButton">Submit</span>
               </div>
            </div>
         </div>
         <div id="modal-Angpao-Message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header" style="padding: 10px 30px 10px 10px">
                  <span class="modal-header-title" id="modal-header-title-fix" style="padding: 10px 30px 10px 0px; word-wrap: break-word; height: 65px">
                  Angpao Rain Luck             
                  </span>
                  <span class="prizesWon"></span>
                  <span class="modal-header-title" id="modal-header-title-custom" style="padding: 10px 30px 10px 0px; word-wrap: break-word"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p id="angpaoContent" class="modal-content-message text-alignment-roulette" style="font-size: 18px;">
                  </p>
               </div>
               <div class="modal-button roulette">
                  <span class="modal-button-message lightblue" data-close>Setuju</span>
                  <span class="modal-button-message green functionButton">Submit</span>
               </div>
            </div>
         </div>
         <div id="modal-message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue closeButton" data-close>Close</span>
                  <span class="modal-button-message green functionButton">Submit</span>
               </div>
            </div>
         </div>
         <!--Closed Account Modal -->
         <div id="modal-closed-acc" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Permanently Close Account
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <div class="row">
                     <div style="padding: 5px !important; text-align: center!important">
                        <span class="icon icon-warning" style="font-size: 120px;"></span>
                     </div>
                  </div>
                  <div class="row" style="padding-bottom: 20px;">
                     <div style="padding: 5px !important">
                        <div style="height: inherit !important; padding-left: 2%; padding-right: 2%">
                           <p class="modal-content-accClosed-message">
                              Your account has been closed in alignment with account management policy. For more information, learn more about <span id="linktxt1" onclick="ClosedAccRedirection();">Account Policy</span> or please contact our <span id="linktxt2" onclick="Page.popUpLiveChat();">Customer Service</span>.
                           </p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row" style="padding-bottom: 20px;">
                  <div class="columns medium-6 small-5">
                     <a id="livechatbtnEN" onclick="Page.popUpLiveChat();">Live Chat</a>
                  </div>
                  <div class="columns medium-6 small-5">
                     <a id="findoutmorebtnEN" data-close="modal-closed-acc" onclick="ClosedAccRedirection();">Learn More</a>
                  </div>
               </div>
            </div>
         </div>
         <!--Closed Account Modal (end) -->
         <div id="modal-UpdateFullName" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true" data-options="closeOnBackgroundClick:false; closeOnEsc:false;">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message green functionButton">Submit</span>
               </div>
            </div>
         </div>
         <div id="modal-slc-mobile-message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue" data-close>NO</span>
                  <span class="modal-button-message green functionButton">YES</span>
               </div>
            </div>
         </div>
         <div id="modal-bonus-message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" class="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" class="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue" data-close>Close</span>
                  <span class="modal-button-message green functionButton">Submit</span>
               </div>
            </div>
         </div>
         <div id="modal-emergency-message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper emergency-wrapper">
               <div class="modal-header">
                  <span class="modal-emergency-header-title" id="modal-emergency-header-title-fix">
                  Info
                  </span>
                  <span class="modal-emergency-header-title en" id="modal-emergency-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue" data-close>Close</span>
                  <span class="modal-button-message green functionButton">Submit</span>
                  <label class="doNotShowCheckBoxLabel">
                  <input class="doNotShowChkBox" type='checkbox' />
                  <span class="chkBoxText" style="color: black; margin-left: 27px;">Do not show this again</span>
                  </label>
               </div>
            </div>
         </div>
         <div id="modal-pt-message" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-pt-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-pt-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue functionButton">Close</span>
               </div>
            </div>
         </div>
         <div id="modal-specialCancel" class="reveal full modal-Container" data-reveal="">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title">Title
                  </span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue functionButton">Close</span>
               </div>
            </div>
         </div>
         <div id="modal-depositImgSlider" class="reveal full modal-Container" data-reveal=""></div>
         <!-- Modal Confirm -->
         <div id="modal-confirm" class="reveal full modal-Container" data-reveal="">
            <div class="wrapper">
               <div style="display: inline-block; width: 100%; padding: 5px 5px 0">
                  <span class="icon-general-icon_close" data-close="modal-confirm" style="cursor: pointer; font-weight: bolder; font-size: 14px; float: right; padding: 5px"></span>
               </div>
               <div class="row" style="padding-bottom: 10px;">
                  <div class="columns medium-4 hide-for-small-only" style="padding: 5px !important">
                     <span class="icon-finance_exclamationpoint modalicon" style="font-size: 100px"></span>
                  </div>
                  <div class="columns medium-8" style="padding: 5px !important">
                     <div class="row collapse">
                        <div class="columns  small-2 show-for-small-only">
                           <span class="icon-finance_exclamationpoint modalicon" style="font-size: 35px"></span>
                        </div>
                        <div class="column small-10 medium-12">
                           <header class="modal-title" style="display: inline-block">Info</header>
                        </div>
                     </div>
                     <article style="height: inherit !important"></article>
                     <nav class="modal-actions">
                        <button type="button" class="modal-button reveal-close-button" data-close="modal-confirm">Close</button>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <!-- Modal Bonus -->
         <div id="modal-bonus" class="reveal large modal-Container" data-reveal="" style="overflow: scroll;">
            <div class="wrapper">
               <div class="row align-right ">
                  <div class="columns shrink" style="padding: 5px;">
                     <span class="icon-general-icon_close" data-close="modal-bonus" style="cursor: pointer; font-weight: bolder; font-size: 14px; float: right"></span>
                  </div>
               </div>
               <article></article>
            </div>
         </div>
         <div id="modal-QRPLoginDetail" class="reveal full modal-Container" data-reveal="" data-options="closeOnBackgroundClick:false; closeOnEsc: false;"></div>
         <div id="modal-TitanLoginDetail" class="reveal full modal-Container" data-reveal="" data-options="closeOnBackgroundClick:false; closeOnEsc: false;"></div>
         <!-- Modal pt register-->
         <div id="modal-ptRegister" class="reveal full modal-Container" data-reveal=""></div>
         <!-- Modal poker register-->
         <div id="modal-pokerRegister" class="reveal full modal-Container" data-reveal=""></div>
         <!-- unfinishedgame modal -->
         <div id="modal-unfinishedgame" class="reveal full modal-Container" data-reveal="">
            <div id="unfinishGamePopup">
               <div class="cboxClose" data-close></div>
               <div class="form-group">
                  <div class="row expanded" style="padding-top: 20px;">
                     <div class="column large-5 medium-5 small-12" id="imageUnfinishGameHolder">
                        <img data-original="{{APP_ASSETS}}images/fun88_cs_angel.png" style="max-height: 530px;" class="lazy unfinish" />
                     </div>
                     <div class="column large-7 medium-7 small-12 end">
                        <div class="titleContainer">
                           <p id="unfinishGameTitleText">
                              <span class = "goldTxt"> Dear Members, </ span> <br> You still have unfinished games, <br> Bonus will be accepted after all the games are over. </ p>
                        </div>
                        <div id="BonusSeekerGameList" class="column large-7 medium-7 small-12" style="display: block; min-height: 48px; max-height: none; height: 335px;">
                           <div class="bsPlatformHeader">
                              <div class="bsPlatform active "
                                 data-platform="desktopGame">
                                 Desktop Game (<span id="desktopGameCounts"></span>)
                              </div>
                              <div class="bsPlatform "
                                 data-platform="mobileGame">
                                 Mobile Games  (<span id="mobileGamesCount"></span>)
                              </div>
                           </div>
                           <div id="bsGameContainer">
                              <div style="position: relative; height: 140px;" id="gameItemHolder">
                              </div>
                           </div>
                           <div style="position: absolute; display: none;" class="mobileGame Remark">Please Login From Mobile to complete the game </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- Popup Login -->
         <div id="modal-PopupLogin" class="reveal full modal-Container" data-reveal="">
            <div class="wrapper">
               <div class="small-12 column" id="popupHeader">
                  <div class="small-12 column">
                     <span style="display: block; padding-right: 25px;">Please login or register</span>
                     <button class="close-button" data-close aria-label="Close modal" type="button">
                     <span aria-hidden="true" class="icon icon-close-circle-button"></span>
                     </button>
                  </div>
               </div>
               <div class="small-12 column" style="padding-top: 25px;">
                  <div class="small-3 column" style="padding-right: 0 !important;">
                     <h7>Username</h7>
                  </div>
                  <div class="small-9 column loginField">
                     <input name="" type="text" maxlength="20" id="txtPopUpUsername" autocomplete="off" autocorrect="off" autocapitalize="none" />
                  </div>
               </div>
               <div class="small-12 column">
                  <div class="small-3 column" style="padding-right: 0 !important;">
                     <h7>Password</h7>
                  </div>
                  <div class="small-9 column loginField">
                     <input name="" type="password" maxlength="20" id="txtPopUpPassword" autocomplete="off" autocorrect="off" autocapitalize="none" />
                  </div>
               </div>
               <div class="small-12 column">
                  <div class="medium-3 column">
                     <span>&nbsp;&nbsp;</span>
                  </div>
                  <div class="medium-4 small-6 column">
                     <h7>
                        <div id="btnPopupLogin" style="background-color:#FFBB33;color:#ffffff; border-radius: 5px;display:block; height: 35px; width: 120px;text-align: center !important;padding-top:8px;cursor:pointer;">
                           LOG IN
                        </div>
                     </h7>
                  </div>
                  <div class="medium-4 small-6 column" style="padding-left: 0 !important;">
                     <h7>
                        <div id="btnPopupSingup" onclick="window.open('#')" style="background-color:#C82333;color:#ffffff; border-radius: 5px;display:block; height: 35px; width: 120px;text-align: center !important;padding-top:8px;cursor:pointer;float:right;">
                           SIGN UP
                        </div>
                     </h7>
                  </div>
               </div>
               <div class="small-12 column" style="padding-bottom: 15px; padding-top: 10px;">
                  <div class="medium-3 column">
                  </div>
                  <div class="medium-9 column">
                     <h8><a class="forgotpassword" style="font-size: 13px;" href='#'>FORGOT THE PASSWORD ?</a></h8>
                  </div>
               </div>
            </div>
         </div>
         <!-- Game Launch Transfer -->
         <div id="modal-GameTransfer" class="reveal full modal-Container" data-reveal=""></div>
         <!-- SosBonus -->
         <div id="modal-sosBonus" class="reveal tiny modal-Container" data-reveal="">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  SOS BONUS
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content" style="overflow-y: scroll">
                  <p class="modal-content-message">
                  </p>
               </div>
            </div>
         </div>
         <!-- ApplyBefore -->
         <div id="modal-applyBefore" class="reveal full modal-Container" data-reveal="">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                     This offer has not yet started
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue" data-close>Close</span>
               </div>
            </div>
         </div>
         <div id="modal-PlayerPreference" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true"></div>
         <div id="modal-LanguageRegion" class="reveal medium modal-Container" data-reveal=""></div>
         <div id="modal-ApplyNow" class="reveal medium modalContainer" data-reveal></div>
         <div id="modal-promo" class="reveal large modalContainer" data-reveal></div>
         <div id="modal-EmailVerification" class="reveal full modal-Container" data-reveal="">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title"></span>
                  <span class="icon icon-close-circle-button Modal-Close-Button" data-close></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message lightblue" data-close>Skip</span>
                  <span class="modal-button-message green functionButton functionButtonEmail"></span>
               </div>
            </div>
         </div>
         <div id="modal-GameTransferMessage" class="reveal full modalContainer" data-reveal="">
            <div class="wrapper">
               <div style="display: inline-block; width: 100%; padding: 5px 5px 0">
                  <span class="icon-general-icon_close" data-close="modal-GameTransferMessage" style="cursor: pointer; font-weight: bolder; font-size: 14px; float: right; padding: 5px"></span>
               </div>
               <div class="row" style="padding-bottom: 10px;">
                  <div class="columns medium-12">
                     <div class="row collapse">
                        <div class="columns small-2 show-for-small-only">
                           <span class="icon-finance_help modalicon" style="font-size: 35px"></span>
                        </div>
                        <div class="column small-10 medium-12">
                           <div class="modal-title" style="display: inline-block">Info</div>
                        </div>
                     </div>
                     <article style="height: inherit !important"></article>
                     <nav class="modal-actions" style="margin-top: 20px;">
                        <button type="button" class="modal-button reveal-close-button" data-close="modal-GameTransferMessage" onclick="">Close</button>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
         <div id="modal-message-withdrawal-verification" class="reveal full modal-Container" data-reveal="" data-multiple-opened="true">
            <div class="modal-wrapper small">
               <div class="modal-header">
                  <span class="modal-header-title" id="modal-header-title-fix">
                  Info
                  </span>
                  <span class="modal-header-title" id="modal-header-title-custom"></span>
               </div>
               <div class="modal-content">
                  <p class="modal-content-message">
                  </p>
               </div>
               <div class="modal-button">
                  <span class="modal-button-message green functionButton">Update NOW!</span>
               </div>
            </div>
         </div>
         <div id="modal-Intro" class="reveal full modal-Container" data-reveal="" style="z-index: 4;"></div>
         <div id="modal-SelfExclusion" class="reveal full modal-Container" data-reveal=""></div>
      </form>
      <input name="" type="hidden" id="hidblackbox" />
      <input name="" type="hidden" id="hidE2" />
      @yield('javascript')
      <script type="text/javascript">
        $(document).ready(function(){
         $("#BasicPointer").click(function(){
          $("#BasicInput").toggle();
        });
       });
     </script>
     <script type="text/javascript">
        $(document).ready(function(){
         $("#AccountInput").hide();
         $("#AccountPointer").click(function(){
          $("#AccountInput").toggle();
        });
       });
     </script>
     <script src="{{APP_ASSETS}}register/main.js"></script>
     <script src="{{APP_ASSETS}}dist/js/select2.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
          $('.js-example-basic-single').select2();
      });
      </script>
      <script type="text/javascript">
      $(document).ready(function() {
          $('.select23').select2();
      });
      </script>
      <script src="{{APP_ASSETS}}Assets/Scripts/ext/slick.js"></script>
      <script>
         $('.slider7').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
        });
         $('.1').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
        });
         $('.2').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 1000,
            arrows: false,
        });

     </script>
     @section('javascript2')
     @show
   </body>
</html>