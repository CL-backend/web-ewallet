<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define("APP_ASSETS", base_url('assets/'));
define('APP_NAME', "E Wallet");

function send_email($email_to,$data,$function)
{
	$ci =& get_instance();
	$ci->load->library('MyPHPMailer');

	$fromEmail = "cs.optimus.c@gmail.com";
	$mail = new PHPMailer();
    
    if($function=='register')
    $mail->Body = $ci->load->view('email_template', $data , TRUE);
	if($function=='verify')
	$mail->Body = $ci->load->view('email_verify', $data , TRUE);
	if($function=='transaction')
	$mail->Body = $ci->load->view('email_transaction', $data , TRUE);	

    $mail->IsSMTP();   // we are going to use SMTP
    $mail->SMTPAuth   = true; // enabled SMTP authentication
// prefix for secure protocol to connect to the server
    $mail->Host       = "smtp.gmail.com";      // setting GMail as our SMTP server
    $mail->SMTPSecure = 'tls';
	$mail->Port = 587;                  // SMTP port to connect to GMail
    $mail->Username   = $fromEmail;  // alamat email kamu
    $mail->Password   = "cypherlabs";            // password GMail
    $mail->SetFrom('cs.optimus.c@gmail.com', 'Management');  //Siapa yg mengirim email
    $mail->Subject    = "Welcome To My Application";
    $mail->IsHTML(true);
    $toEmail = $email_to; // siapa yg menerima email ini
    $mail->AddAddress($toEmail);
    if(!$mail->Send()) {
    	//echo "Eror: ".$mail->ErrorInfo;
    } else {
    	//echo "Email berhasil dikirim";
    }
}

function convertDate ($date) 
    { 
        // convert date and time to seconds 
        $sec = strtotime($date); 
  
        // convert seconds into a specific format 
        $date = date("M d, Y : H:i", $sec); 
    
        // print final date and time 
        return $date; 
    } 
    
function dateDiff ($d1, $d2) {
	return round(abs(strtotime($d1) - strtotime($d2))/86400);
}

function ref_id($id){
	$const=1111111111;
	$const +=$id; //ID nya
	$randnum = rand($const,9999999999);
	return $randnum;
}


function version()
{
	$ci =& get_instance();
	$query = $ci->db->query('select value from setting order by id desc');
	if($query->num_rows() > 0) {
		return $query->row()->value;
	}else{
		return false;
	}
}

function value_balance()
{
	$ci =& get_instance();
	$id = $ci->user_data->id;
	$rows = $ci->db->query("select a.*, b.* from tb_wallet a join tb_balance_type b on a.balance_name = b.key_name where a.user_id='$id'");
	if($rows->num_rows() > 0) 
	{
		$data = array();
		foreach ($rows->result() as $key) {
			$data[$key->key_name] = $key->value;
		}
		return $data;
	}else{
		$data = array(
				"main_wallet"=>0,
	        	"ysb_sport"=>0,
	        	"casino_live"=>0
        	);
		return $data;
	}
	
}

function total_balance()
{
	$ci =& get_instance();
	$id = $ci->user_data->id;
	$rows = $ci->db->query("select sum(value) as total from tb_wallet where user_id='$id'");
	if(!empty($rows->row()->total)) 
	{
		$balance = $rows->row()->total;
		return $balance;
	}else{
		$balance = 0;
		return $balance;
	}
	
}

function currency()
{
	$ci =& get_instance();
	$country = $ci->user_data->country;

	if($country=='Indonesia') {
		return "IDR";
	}else{
		return 'USD';
	}
}

function balance_type()
{
	$ci =& get_instance();
	$query = $ci->db->query("select * from tb_balance_type ");
	foreach ($query->result() as $key) {
		$data [] = array(
				'key'=>$key->key_name,
				'icon'=>$key->icon,
				'name'=>$key->name
				);
	}

	return $data;
}

function convert_country($id)
{
	$ci =& get_instance();
	$query = $ci->db->query("select nicename from country where iso='$id' ");
	return $query->row()->nicename;
}

function get_bank($bank){
	$ci =& get_instance();
	$query = $ci->db->query("select * from bank_account where bank='$bank' ");
	$dt = $query->row();
	return $dt;
}

function array_non_empty_items($input)
{
		    // If it is an element, then just return it
	if (!is_array($input)) {
		return $input;
	}
	unset($input['id']);
	unset($input['parent_id']);
	if (!array_key_exists("has_sub",$input))
    {
  		unset($input['game']);
    }
		
	

	$non_empty_items = array();

	foreach ($input as $key => $value) {
		$non_empty_items[$key] = array_non_empty_items($value);
	}
	return $non_empty_items;
}

function get_deposit()
{	
	$ci =& get_instance();
	$id = $ci->user_data->id;

	$query = $ci->db->query("select a.*,b.status from tb_transaction_history a join tb_deposit b on a.id_type=b.id where a.member_code = '$id' and a.type='deposit' and b.status=0 ");
	if($query->num_rows()>0){
		foreach ($query->result() as $key) {
			if($key->status==0){
				$status = "Pending";
				$status_message = "Waitting Transfer";
			}

			$data [] = array('transaction_id'=>$key->id,
							'status'=>$status,
							'status_message'=>$status_message,
							'amount'=>$key->amount,
							'currency'=>$key->currency);
		}
	}else{
		$data = array();
	}
	return $data;
}

function get_trans($id)
{
	$ci =& get_instance();
	$query = $ci->db->query("select id_type,type from tb_transaction_history where id='$id' ");
	$dt = $query->row();
	return $dt;
}

function makeTree($data = [], $parent_key = 'parent_id', $node_name = 'childs',  $parent_code = 0)
{
	$key ='id';
	$parent_ids = array_column($data, $parent_key);
	$result = [];
	$i=0;
	foreach ($data as $k=>$v) {
		if($v[$parent_key] == $parent_code){
			$i=+1;
			$result[$v[$key]] = $v;
			
				$result[$v[$key]][$node_name] = makeTree($data, $parent_key, $node_name, $v[$key]);
		}
		if($i==0){
			$node_name ='game';
		}

	}
	return array_values ($result);
}

function create_menu()
{
	$ci =& get_instance();
	$query = $ci->db->query('select * from tb_menu where status="menu" ');
	 	foreach ($query->result() as $key ) {

	 		if($key->has_sub==1)
	 			$sub = true;
	 		else if($key->has_sub==0){
	 			$sub = false;
	 		}
	 		
	 	 	$data [] = array(
	 	 				'id'=>$key->id,
	 	 				'parent_id'=>$key->parent_id,
	 	 				'name'=>$key->name,
	 	 				'icon'=>$key->icon,
	 	 				'has_sub'=>$sub
	 	 			);
	 	 }

	 	$query_parent = $ci->db->query('select * from tb_menu where status="parent" ');
	 	foreach ($query_parent->result() as $key ) {

	 		if($key->has_sub==1)
	 			$sub = true;
	 		else
	 			$sub = false;
	 		
	 	 	$data [] = array(
	 	 				'id'=>$key->id,
	 	 				'parent_id'=>$key->parent_id,
	 	 				'sub_name'=>$key->name,
	 	 				'icon'=>$key->icon,
	 	 				'has_sub'=>$sub
	 	 			);
	 	}

	 	$query_child = $ci->db->query('select * from tb_menu where status="child" ');
	 	foreach ($query_child->result() as $key ) {

	 		if($key->has_sub==1)
	 			$sub = true;
	 		else
	 			$sub = '';
	 		
	 	 	$data [] = array(
	 	 				'id'=>$key->id,
	 	 				'parent_id'=>$key->parent_id,
	 	 				'name'=>$key->name,
	 	 				'cover_image'=>$key->icon,
	 	 				'link_url'=>$key->link_url
	 	 			);
	 	}

	 	return $data;
}
?>