<?php

class AUTHORIZATION
{
    public static function validateTimestamp($token)
    {
        $CI =& get_instance();
        $token = self::validateToken($token);
        if ((date('d/m/Y H:i:s') < date('d/m/Y H:i:s', $token->expired))) {
            $data = array('id'=>$token->id,'email'=>$token->email,'date_expired'=>date('m/d/Y H:i:s', $token->expired),'date_now'=>date('m/d/Y H:i:s'));
            return $data;
        }
        return false;
    }

    public static function validateToken($token)
    {
        $CI =& get_instance();
        return JWT::decode($token, $CI->config->item('jwt_key'));
    }

    public static function generateToken($data)
    {
        $CI =& get_instance();
        return JWT::encode($data, $CI->config->item('jwt_key'));
    }

}