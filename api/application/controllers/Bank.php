<?php
	
	defined("BASEPATH") OR exit('No direct script access allowed');

	class Bank extends AUTH_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if(!$this->cek()){
		        $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
	        }
		}

		public function index_get()
		{
			$query = $this->db->get('tb_bank_list');
			foreach ($query->result() as $key) {
				$result [] = array(
							'code'=>$key->code,
							'name'=>$key->name,
							'logo'=>$key->logo
						);
			}

			$response = array(
							'status'=>true,
							'message'=>'success',
							'version'=>version(),
							'data'=>$result
						);

			$this->response($response);
		}
	}


?>