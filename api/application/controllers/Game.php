<?php
	
	defined("BASEPATH") OR exit('No direct script access allowed');

	class Game extends AUTH_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if(!$this->cek()){
		        $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
	        }
		}

		public function login_get($gameId = false)
		{
			if(!$gameId){
				return $this->response(['status' => false, 'message' => 'Invalid request.'], 502);	
			}
			
			$game = $this->db->get_where('tb_game_service', array('gameID' => $gameId));
			if($game->num_rows() <= 0 ){
				return $this->response(['status' => false, 'message' => "Ivalid Game ID"], 502);
			}
			
			$this->load->library('prs_service');
			
			$user = $this->user_data;
			
			$payload = [
				'gameId'	=> $gameId,
				'ip'		=> $this->input->ip_address(),
				'username'	=> $user->username,
				'lang'		=> 'en',
				'isMobile'	=> true			
			];
			
			$loginGame = $this->prs_service->gameLogin($payload);
			if($loginGame){
				$data = [
					'status'	=> true,
					'gameUrl'	=> $this->prs_service->response_data,
					'messagge'	=> "Login token successfully retrieved."
				];
				$this->response($data);
			}else{
				
				return $this->response(['status' => false, 'message' => $this->prs_service->errorMessage], 502);
			}
			
			
		}
		
			
			
	}


?>