<?php
	


	class Banner extends AUTH_Controller
	{
		public function __construct(){
			parent::__construct();

	        if(!$this->cek()){
		        $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
	        }

		}

		public function promote_get(){
			
			  	$sql = $this->db->get('tb_banner');
			  	$version = array('version'=>version());

			  	foreach ($sql->result() as $key) {
			  		$data [] = array(
			  						'id'=>$key->id,
			  						'Banner Url'=>$key->banner_url,
			  						'Promotion Link'=>$key->promotion_link
			  					);
			  	}
			  	$merge = array_merge($data,$version);

			  	if($data){
				    $response = array(
					  				'status'=>true,
					  				'message'=>'success',
					  				'data'=>$merge
							    );
				}else{
					$response = array(
					  				'status'=>false,
					  				'message'=>'data is empty'
							    );
				}
			
						
				$this->response($response);
		}
		
	

	}

?>