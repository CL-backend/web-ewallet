<?php
	
	defined('BASEPATH') OR exit ('No direct script access allwed'); 

	class Banner extends AUTH_Controller
	{
		function __construct()
		{
			parent::__construct();
			
		}

		public function index_get()
		{
			$sql = $this->db->get('tb_banner');
			$version = version();

			foreach ($sql->result() as $key) {
			  	$data [] = array(
			  					'id'=>$key->id,
			  					'banner_url'=>$key->banner_url,
			  					'promotion_link'=>$key->promotion_link
			  				);
			  	}
			  	
			  	if($data){
				    $response = array(
					  				'status'=>true,
					  				'message'=>'success',
					  				'version'=>$version,
					  				'data'=>$data
							    );
				}else{
					$response = array(
					  				'status'=>false,
					  				'message'=>'data is empty'
							    );
				}
			
						
				$this->response($response);
		}

		public function create_post()
		{
			$input = json_decode(file_get_contents('php://input'),true);
          	$insert = $this->db->insert('tb_banner',$input);
          	if($insert){
          		$response = array(
					  				'status'=>true,
					  				'message'=>'success added'
							    );
          	}else{
          		$response = array(
					  				'status'=>false,
					  				'message'=>'unsuccess added'
							    );
          	}
          	$this->response($response);
		}
	}
?>