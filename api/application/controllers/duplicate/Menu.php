<?php

class Menu extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_get()
	{
		$result = makeTree(create_menu(),'parent_id', 'sub_menu',  0);
		$hash = array_non_empty_items($result);

		$query = $this->db->query('select * from tb_popular_game');
			foreach ($query->result() as $key ) {
				$data [] = array('name'=>$key->name,'cover_image'=>$key->cover_image,'link_url'=>$key->link_url);
			}

		$response = array(
				'status'=>true,
				'message'=>'success',
				'version'=>version(),
				'menu_list'=> $hash,
				'popular_game'=>$data
		);

		$this->response($response);
	}
}

?>