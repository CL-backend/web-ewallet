<?php

class Game_List extends AUTH_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index_get()
	{
		$query = $this->db->query('select * from tb_popular_game');
		foreach ($query->result() as $key ) {
			$data []= array('name'=>$key->name,'cover_image'=>$key->cover_image,'link_url'=>$key->link_url);
		}

		$response = array(
				'status'=>true,
				'message'=>'success',
				'version'=>version(),
				'popular_game'=>$data
		);

		$this->response($response);
	}

	public function create_post()
	{
		$input = json_decode(file_get_contents('php://input'),true);
        $insert = $this->db->insert('tb_popular_game',$input);
          	if($insert){
          		$response = array(
					  				'status'=>true,
					  				'message'=>'success added'
							    );
          	}else{
          		$response = array(
					  				'status'=>false,
					  				'message'=>'unsuccess added'
							    );
          	}
          	$this->response($response);
	}
}

?>