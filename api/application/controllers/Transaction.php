<?php
	
	defined("BASEPATH") OR exit('No direct script access allowed');

	class Transaction extends AUTH_Controller
	{
		public function __construct()
		{
			parent::__construct();
			if(!$this->cek())
			{
		        $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
	        }
		}

		public function deposit_post()
		{
			$input = json_decode(file_get_contents("php://input"),true);
			
			$this->form_validation->set_data($input);
			$value = array(
						   "member_code" =>$this->user_data->id,
						   "currency" =>currency(),
						   "payment_method" =>$input["payment_method"],
						   "bank_name"=>$input["bank_name"],
						   "amount"=>$input["amount"],
						   "submit_date"=>Date('Y-m-d H:i:s'),
						   "account_bank"=>$input["account_bank"],
						   "account_name"=>$input["account_name"],
						   "id_referal"=>ref_id($this->user_data->id)
					);

			$this->form_validation->set_rules($this->rules());

			if($this->form_validation->run()==true)
			{
				$transaction_value = array(
						   "member_code" =>$this->user_data->id,
						   "wallet" =>'Main Wallet',
						   "trans_type" =>$input["payment_method"],
						   "amount"=>$input["amount"],
						   "type"=>"Debet",
						   "date"=>Date('Y-m-d'),
						   "comments"=>'Deposit (Main Wallet)'
					);
				$insert_transaction = $this->db->insert('tb_transaction',$transaction_value);
				$insert_wallet = $this->db->insert('tb_wallet',['user_id'=>$this->user_data->id,'balance_name'=>'main_wallet','value'=>$input["amount"]]);

				$insert = $this->db->insert('tb_deposit',$value);
				if($insert)
				{
					$dt = get_bank($input['bank_name']);
					$id = $this->user_data->id;
					$dt_deposit = $this->db->query("select * from tb_deposit where member_code='$id' order by id desc limit 1 ");
					$rows = $dt_deposit->row();
					$fee = 0;
					$data = array(
							'bank_name'=>$dt->bank,
							'account_number'=>$dt->account_number,
							'account_name'=>$dt->account_name,
							'fee'=>0,
							'amount'=>$input['amount'],
							'total_amount'=>($input['amount']+$fee),
							'id_ref'=>$rows->id_referal,
							'id'=>$this->user_data->id
						);
					
					$response = array(
							'status'=>true,
							'message'=>'success',
							'version'=>version(),
							'data'=>$data
						);
				}else
				{
					$response = array(
							'status'=>false,
							'message'=>'data cannot be saved'
						);
				}
			}else
			{
				$response = array(
							'status'=>false,
							'message'=>validation_errors()
						);
			}

			$this->response($response);
		}

		public function transaction_history_get()
		{
			$start_date = $this->get('start_date');
			$end_date = $this->get('end_date');
			$diff=dateDiff($start_date,$end_date);
			
			

			if ($end_date < $start_date)
			{
				$response = array(
							'status'=>false,
							'message'=>'end date must be greater than or equal to start date',
							'version'=>version(),
							'data'=>[]
						);
			}else if($diff>30){
				$response = array(
							'status'=>false,
							'message'=>'Maximum data is 30 days',
							'version'=>version(),
							'data'=>[]
						);
			}else{
				$id_user = $this->user_data->id;
				$query = $this->db->query("SELECT * from tb_transaction_history where member_code='$id_user' and date_transaction BETWEEN '$start_date' and '$end_date' ");
				if($query->num_rows()<= 0){
					$data = array();
				}else{
					foreach ($query->result() as $row) {
						if($row->status==0){
							$status = "Pending";
							$status_message = "Pending Transfer";
						}else if($row->status==1){
							$status = "Success";
							$status_message = "Success";
						}
						else{
							$status = "Rejected";
							$status_message = "Rejected By Admin";
						}
						$data [] = array(
										'transaction_id'=>$row->id,
										'type'=>$row->type,
										'status'=>$status,
										'status_message'=>$status_message,
										'currency'=>$row->currency,
										'amount'=>$row->amount,
										'date'=>$row->date_transaction
									);
					}
				}
				$response = array(
							'status'=>true,
							'message'=>'success',
							'version'=>version(),
							'data'=>$data
						);
			}

			
			$this->response($response);
		}

		public function detail_transaction_get()
		{
			
			$id_transaction = $this->get('id_transaction');
			$trans = get_trans($id_transaction);

			if($trans->type=='deposit')
			{
				$query = $this->db->query("select a.*, b.id as id_transaction from tb_deposit a join tb_transaction_history b on a.id=b.id_type where b.id='$id_transaction' ");
				$rows = $query->row();
				if($rows->status==0)
					$status = "Pending";
				else if($rows->status==1)
					$status = "Success";
				else
					$status = "Rejected";

				$data = array(
							'transaction_id'=>$rows->id_transaction,
							'type'=>$trans->type,
							'amount'=>$rows->amount,
							'status'=>$status,
							'deposit_method'=>$rows->payment_method,
							'bank'=>$rows->bank_name,
							'date'=>$rows->submit_date
						);
			}else if($trans->type=='transfer')
			{
				$query = $this->db->query("select a.*, b.id as id_transaction from tb_transfer a join tb_transaction_history b on a.id=b.id_type where b.id='$id_transaction' ");
				$rows = $query->row();
				if($rows->fund_out_status==0)
					$status = "Pending";
				else if($rows->fund_out_status==1)
					$status = "Success";
				else
					$status = "Rejected";

				$data = array(
							'transaction_id'=>$rows->id_transaction,
							'type'=>$trans->type,
							'amount'=>$rows->amount,
							'status'=>$status,
							'transfer_from'=>$rows->source_wallet,
							'transfer_to'=>$rows->target_wallet,
							'date'=>$rows->transfer_date
						);
			}else if($trans->type=='withdraw')
			{
				$query = $this->db->query("select a.*, b.id as id_transaction from tb_withdraw a join tb_transaction_history b on a.id=b.id_type where b.id='$id_transaction' ");
				$rows = $query->row();
				if($rows->status==0)
					$status = "Pending";
				else if($rows->status==1)
					$status = "Success";
				else
					$status = "Rejected";

				$data = array(
							'transaction_id'=>$rows->id_transaction,
							'type'=>$trans->type,
							'amount'=>$rows->amount,
							'status'=>$status,
							'bank'=>$rows->bank_name,
							'account_number'=>$rows->account_number,
							'account_name'=>$rows->account_name,
							'date'=>$rows->submit_date
						);
			}
			$response = array(
							'status'=>true,
							'message'=>'success',
							'version'=>version(),
							'data'=>$data
						);

			$this->response($response);
		}

		public function recent_get()
		{
			$id = $this->user_data->id;
			$query = $this->db->query("select id,wallet,amount,trans_type, type, date, comments from tb_transaction where member_code ='$id' limit 50");
			$data = array(array( "id"=>1,
					  "wallet"=>"Main Wallet",
					  "amount" => 200000,
					  "trans_type" => "transfer",
					  "type" => "debet", // Kredit
					  "date"=> "2019/02/01",
					  "ket" => "Transfer (Main Wallet -> YSB Wallet)"
					),array("id"=>1,
  "wallet"=>"Main Wallet",
  "amount" => 200000,
  "trans_type" => "transfer",
  "type" => "kredit", // Kredit
  "date"=> "2019/02/01",
  "ket" => "Deposit (Main Wallet)"));
			// foreach ($query->result() as $key) {

			// // $data [] = array('id'=>$key->id,
			// // 				 'wallet'=>$key->wallet,
			// // 				 'amount'=>$key->amount,
			// // 				 'trans_type'=>$key->trans_type,
			// // 				 'type'=>$key->type,
			// // 				 'date'=>$key->date,
			// // 				 'ket'=>$key->comments
			// // 				);
			// }

			$response = array(
							'status'=>true,
							'message'=>'success',
							'version'=>version(),
							'data'=>$data
						);

			$this->response($response);
		}

		public function rules()
		{
			$config =array(
	             
						array(
							'field'=>'payment_method',
							'label'=>'Payment Method',
							'rules'=>'required'
						),
						array(
							'field'=>'bank_name',
							'label'=>'Bank Name',
							'rules'=>'trim|required'
						),
						array(
							'field'=>'amount',
							'label'=>'Amount',
							'rules'=>'trim|required'
						),
					
						array(
							'field'=>'account_bank',
							'label'=>'Account Bank',
							'rules'=>'trim|required'
						),array(
							'field'=>'account_name',
							'label'=>'Account Name',
							'rules'=>'required'
						)
    		);
    		return $config;
		}


	}

?>