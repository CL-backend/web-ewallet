<?php

	class Menu extends AUTH_Controller
	{
		public function __construct(){
			parent::__construct();

	        if(!$this->cek()){
		        $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
	        }

		}

		public function list_get()
		{
			$Q_menu = $this->db->query('select * from tb_menu');
			foreach ($Q_menu->result() as $key) {
				if(!empty($key->sub_name)){
					$has_sub = true;
					$query_sub = $this->db->query('select * from tb_list_menu');
					foreach ($query_sub->result() as $key2) {
						if($key->sub_name===$key2->sub_name){
							$query_sub_game = $this->db->query('select * from tb_menu_game');
							foreach ($query_sub_game->result() as $key3) {
								if($key2->sub_game===$key3->sub_menu){
									$game [] =array(
													'name'=>$key3->name,
													'cover_image'=>$key3->cover_image,
													'link_url'=>$key3->link_url
												);

								}else{
									unset($game);
								}

							}
							$sub [] = array(
										'sub_name'=>$key2->sub_name,
										'sub_icon'=>$key2->sub_icon,
										'has_sub'=>$has_sub,
										'game'=>$game
									); 
						}else{
							
							$sub [] = array(
											'sub_name'=>$key2->sub_name,
											'icon'=>$key2->sub_icon
										); 	
						}
						
					}
					$menu [] = array(
								'name'=>$key->name, 
								'icon_menu'=>$key->icon_menu,
								'has_sub'=>$has_sub,
								'sub_menu'=>$sub
							);
					
				}else{
					$menu [] = array(
								'name'=>$key->name, 
								'icon_menu'=>$key->icon_menu
							);
				}
			}

			$popular_game = $this->db->query('select * from tb_popular_game');
			foreach ($popular_game->result() as $key) {
				$pop_game [] = array(
									'name'=>$key->name,
									'cover_image'=>$key->cover_image,
									'link_url'=>$key->link_url
								);
			}

			

				$response = array(
							'status'=>true,
					  		'message'=>'success',
					  		'version'=>version(),
					  		'data'=>array(
					  		'menu_list'=> $menu,
					  		'popular_game'=>$pop_game)
						);

			$this->response($response);
		}
	}

?>