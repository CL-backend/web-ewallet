<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    // require APPPATH . '/libraries/REST_Controller.php';
    // use Restserver\Libraries\REST_Controller;

    class Auth extends AUTH_Controller
    {
        function __construct()
        {
            parent::__construct();

        }

        public function login_post() 
        {

            
            $invalidLogin = ['invalid' => $this->input->post('email')];
            $tokenID = $this->post('tokenID');

            $input = json_decode(file_get_contents('php://input'),true);
            $this->form_validation->set_data($input);
            $this->form_validation->set_rules('email','Email', 'required|valid_email');
            $this->form_validation->set_rules('password','Password', 'required|min_length[8]');

            if($this->form_validation->run()==false){  
                    $response = array(
                                    'status'=>FALSE,
                                    'message'=>validation_errors()
                                );
            }else{
                $login = $this->aauth->login($input['email'], $input['password']);
                if($login)
                {
                    $user = $this->aauth->get_user();
                   
                    $data_token = array(
                                 "id"=>$user->id,
                                 "email"=>$user->email, 
                                 "username"=>$user->username,
                                 "firstname"=>$user->first_name, 
                                 "lastname"=>$user->last_name, 
                                 "country"=>$user->country, 
                                 "birthdate"=>$user->birthdate, 
                                 "phone_number"=>$user->phone_number,
                                 "expired"=>strtotime('+1 Month') 
                                 );


                    $response = array(
                                    'status'=>TRUE,
                                    'message'=>'success loggedin',
                                    'data'=>array(
                                        'tokenID'=>$tokenID,
                                        'token'=>AUTHORIZATION::generateToken($data_token),
                                        'data'=>$data_token
                                    )
                                );

                }else{
                    $response = array(
                                    'status'=>FALSE,
                                    'message'=>$this->aauth->errors[0]
                                );
                }
            }
                $this->response($response);

        }


        function register_post() 
        {
        

            $input = json_decode(file_get_contents('php://input'),true);
            $this->form_validation->set_data($input);
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email'
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'trim|required|min_length[6]'
                ),
                array(
                    'field' => 'username',
                    'label' => 'Username',
                    'rules' => 'trim|required|min_length[6]|callback_valid_username'
                ),
                array(
                    'field' => 'firstname',
                    'label' => 'firstname',
                    'rules' => 'required|alpha'
                ),
              
                array(
                    'field' => 'country',
                    'label' => 'country',
                    'rules' => 'required'
                ),array(
                    'field' => 'birthdate',
                    'label' => 'birthdate',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'confirm_password',
                    'label' => 'confirm_password',
                    'rules' => 'trim|required|min_length[6]'
                ),array(
                    'field' => 'phone_number',
                    'label' => 'phone number',
                    'rules' => 'trim|required|min_length[8]'
                )
                 
            );
            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == TRUE){


                if($input['confirm_password']==$input['password'])
                {
                        
                        $filter_data = array(
                            'first_name'=>$input['firstname'],
                            'username'=>$input['username'],
                            'last_name'=>$input['lastname'],
                            'country'=>convert_country($input['country']),
                            'birthdate'=>$input['birthdate'],
                            'phone_number'=>$input['phone_number']
                        );
                    

                    $insert = $this->aauth->create_user($input['email'],$input['username'], $input['password'] ,$filter_data);
                
                    if($insert)
                    {
                        $data = array(
                            'firstname'=> $input['firstname'],
                            'lastname'=> $input['lastname'],
                            'email'=>$input['email'],
                            'country'=>convert_country($input['country'])
                        );
                        send_email($input['email'],$data,'register');
                        $response = array(
                            'status'=>TRUE,
                            'message'=>"success"
                        );
                    }else{
                        $response = array(
                                    'status'=>FALSE,
                                    'message'=> $this->aauth->errors[0]
                        );
                    }
                }else{
                    $response = array(
                            'status'=>FALSE,
                            'message'=>"Password and password confirmation are different"
                    );
                    
                }
            }else{
                $response = array(
                            'status'=>FALSE,
                            'message'=>validation_errors()
                    );
            }

            $this->response($response);
        }

         public function updatePassword_post()
        {
            $input = json_decode(file_get_contents('php://input'),true);
            $this->form_validation->set_data($input);
            $config = array(
                array(
                    'field' => 'oldPass',
                    'label' => 'Old Password',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'newPass',
                    'label' => 'New Password',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'confirmPass',
                    'label' => 'Confirm Password',
                    'rules' => 'trim|required|matches[newPass]'
                ));

            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == TRUE){
                if($this->aauth->check_password($this->aauth->get_user()->id, $input['oldPass'], $this->aauth->get_user()->pass)){
                    $new_pass   = $this->aauth->hash_password($input['newPass'],0);
                    $updated    = $this->db->update('tb_member',['pass'=> $new_pass], array('id'=> $input['id']));

                    if($updated){
                         $response = array(
                            'status'=>true,
                            'message'=>'Success, password has been changed'
                        );
                       
                    }else{
                      
                        $response = array(
                            'status'=>false,
                            'message'=>$this->aauth->errors[0]
                        );
                        
                    }
                    $this->response($response);
                    

                }else{
                     $response = array(
                            'status'=>false,
                            'message'=>'old password not match, try again'
                        );
                    
                }
                    $this->response($response);
            }else{
                 $response = array(
                            'status'=>false,
                            'message'=>validation_errors()
                    );
                    $this->response($response);
            }

        }

        public function email_verify_post()
        {
            $input = json_decode(file_get_contents("php://input"), true);
            $this->form_validation->set_data($input);

            //parameter input
            $config = array(
                            array(
                                  'field' => 'email',
                                  'label' => 'Email',
                                  'rules' => 'trim|required|valid_email'
                            )
                        );


            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == TRUE){
                $this->db->select('*');
                $this->db->from('tb_member');
                $this->db->where('email', $input['email']);
                $user = $this->db->get();

                if($user->num_rows() > 0)
                {
                    $now =  strtotime('now');

                    if($user->row()->forgot_exp != "" && $user->row()->forgot_exp >= $now){
                        $ver_code = $user->row()->verification_code;
                        $request_time = date('d F Y H:i:s', strtotime($user->row()->forgot_time));
                    }else{
                        $ver_code = mt_rand(10000,99999);
                        $request_time = date("Y-m-d H:i:s",strtotime("+5 minutes", $now ));
                        $data = [
                            'forgot_exp' => strtotime('+5 minutes'),
                            'verification_code' => $ver_code,
                            'forgot_time'   => $request_time
                        ];
                    
                        
                        $this->db->update('tb_member',$data,['id' => $user->row()->id]);
                    }

                    $body_data = array(
                                        'email'=>$user->row()->email,
                                        'firstname'=>$user->row()->first_name,
                                        'lastname'=>$user->row()->last_name,
                                        'ver_code'=>$ver_code,
                                        'time_req'=>date('d F Y H:i:s', strtotime($request_time))
                                      );
                    send_email($input['email'],$body_data,'verify');

                    return $this->response(array('status'=>true, 'message'=>"Success, verification code has been sent to your email", 'email'=>$user->row()->email), 200);
                }

                return $this->response(array('status'=>false, 'message'=> "Not valid email, Please use registered email."), 502);
                            
            }
            
            return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
        }

        public function verification_code_post()
        {
            $input = json_decode(file_get_contents("php://input"), true);
            $this->form_validation->set_data($input);

            //parameter input
            $config = array(
                            array(
                                  'field' => 'email',
                                  'label' => 'Email',
                                  'rules' => 'trim|required|valid_email'
                            ),
                            array(
                                  'field' => 'verification_code',
                                  'label' => 'Verification Code',
                                  'rules' => 'trim|required'
                            )
                        );

            $this->form_validation->set_rules($config);

            if($this->form_validation->run()==TRUE)
            {
                $this->db->select('*');
                $this->db->from('tb_member');
                $this->db->where('email', $input['email']);
                $user = $this->db->get();

                if($user->num_rows() > 0)
                {
                    $now = strtotime('now');
                    $user = $user->row();
                    $data = array('id'=>$user->id,'email'=>$user->email);

                    if($input['verification_code'] == $user->verification_code)
                    {
                        if($user->forgot_exp > $now){
                            return $this->response(array('status'=>true, 'message'=>"Success, verification code is valid.", 'data' => $data), 200);
                        }

                        return $this->response(array('status'=>false, 'message'=> "Verification code is not valid."), 502); 
                    }
                    return $this->response(array('status'=>$user->email, 'message'=> "Verification code is not valid."), 502);     
                }
                return $this->response(array('status'=>false, 'message'=> "Not valid email, Please use registered email."), 502);
            }
            return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
        }

        public function reset_password_post(){
            
            $input = json_decode(file_get_contents("php://input"), true);
            $this->form_validation->set_data($input);

            $config = array(
                            array(
                                  'field' => 'id',
                                  'label' => 'ID',
                                  'rules' => 'trim|required'
                            ),
                            array(
                                  'field' => 'password',
                                  'label' => 'Password',
                                  'rules' => 'trim|required'
                            ),
                            array(
                                  'field' => 'confirm_password',
                                  'label' => 'Confirm Password',
                                  'rules' => 'trim|required|matches[password]'
                            )
                        );
            $this->form_validation->set_rules($config);

            if($this->form_validation->run() == TRUE){
                
                $this->db->select('*');
                $this->db->from('tb_member');
                $this->db->where('id', $input['id']);
                $user = $this->db->get();

                if($user->num_rows() >0 ){
                    $new_pass   = $this->aauth->hash_password($input['password'], 0);
                    
                    $update     = $this->db->update('tb_member',['pass'=> $new_pass,'forgot_exp'=> "", 'verification_code'=> ""], ['id'=> $input['id']]);
                    if($update){
                        return $this->response(array('status'=>true, 'message'=>"Success, password has been reset."), 200);
                    }
                    return $this->response(array('status'=>false, 'message'=>"Failed. Something has error please try again."), 502);
                }
                return $this->response(array('status'=>false, 'message'=>"Failed. invalid users"), 502);
            }
            
            return $this->response(array('status'=>false, 'message'=>strip_tags(validation_errors())), 502);
        }

        public function valid_username($username)
        {
            $username = trim($username);
            $regex_lowercase = '/[a-z]/';
            $regex_uppercase = '/[A-Z]/';
            $regex_number = '/[0-9]/';
            
            if (empty($username))
            {
                $this->form_validation->set_message('valid_username', 'The {field} field is required.');
                return FALSE;
            }
           

            if (preg_match_all($regex_number, $username) < 1)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field must have at least one number.');
                return FALSE;
            }

            if (strlen($username) < 6)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field must be at least 6 characters in length.');
                return FALSE;
            }
            
            if (strlen($username) > 32)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field cannot exceed 32 characters in length.');
                return FALSE;
            }
            return TRUE;
        }
    }
?>