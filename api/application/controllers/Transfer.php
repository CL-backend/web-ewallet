<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Transfer extends AUTH_Controller
	{
		public function __construct()
		{
			parent::__construct();
			
			if(!$this->cek()){
	    		return false;
	    	}
	    	
		}

		public function cross_post()
		{
			$input = json_decode(file_get_contents("php://input"),true);
			
			$id = $this->user_data->id;
			$cek_walllet_post = $input['from_wallet'];
			$transfer_fund = $input['transfer_fund'];
			$target_wallet = $input['target_wallet'];

			$value_balance_wallet = $this->cek_balance_wallet_request($cek_walllet_post,$id);
			if(is_null($value_balance_wallet) || $value_balance_wallet==0 )
			{
				$response = array(
								'status'=>false,
								'message'=>'Sorry your balance in '.$input['from_wallet'].' is zero / 0'
							);
				
			}else if($value_balance_wallet<$transfer_fund)
			{
				$response = array(
								'status'=>false,
								'message'=>'your balance in '.$input['from_wallet'].' not enough , transfer funds must be less than or equal to the balance'
							);
			}else{
					if($this->cross_transfer($cek_walllet_post,$transfer_fund,$target_wallet,$id) != false )
					{
						$transaction_value = array(
							   "member_code" =>$id,
							   "wallet" =>'Main Wallet',
							   "trans_type" =>'transfer',
							   "amount"=>$transfer_fund,
							   "type"=>"Debet",
							   "date"=>Date('Y-m-d'),
							   "comments"=>"Transfer (".$cek_walllet_post." => ".$target_wallet.")"
						);
						$insert_transaction = $this->db->insert('tb_transaction',$transaction_value);

						$email_body = array(
							   "email" =>$this->user_data->email,
							   "firstname" =>$this->user_data->first_name,
							   "lastname" =>$this->user_data->last_name,
							   "date" =>Date('Y-m-d'),
							   "amount"=>$transfer_fund,
							   "from"=>$cek_walllet_post,
							   "to"=>$target_wallet,
							   "type"=>"Debet"
						);
						send_email($this->user_data->email,$email_body,'transaction');
						$response = array(
									'status'=>true,
									'message'=>'sucess'
								);
					}else{
						$response = array(
									'status'=>false,
									'message'=>'error query'
								);
					}
					$this->response($response);
			}

			$this->response($response);
		}

		public function cek_balance_wallet_request($wallet,$id)
		{
			$this->db->select('value');
			$balance = $this->db->get_where('tb_wallet', array(
															'user_id' => $id,
															'balance_name'=>$wallet
														));
			$value = $balance->row()->value;

			return $value;
		}

		public function cross_transfer($from_wallet,$funds,$target_wallet,$id)
		{
			$sql = $this->db->query(" UPDATE tb_wallet 
									  SET value = CASE WHEN balance_name = '$from_wallet' THEN value-$funds ELSE value END,
									  	  value = CASE WHEN balance_name = '$target_wallet' THEN value+$funds ELSE value END 
									      WHERE user_id ='$id'
									");
			return $sql;
		}
	}

?>