<?php
    
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Profile extends AUTH_Controller
    {
         function __construct()
        {
            parent::__construct();
            if(!$this->cek()){
               $this->response(array('status' => false, 'message'=>"Unauthorised"),502);
            }
        }

        public function index_get()
        {
            $user = $this->user_data;
            $data = array(
                        "email"=>$user->email,
                        "username"=>$user->username,
                        "firstname"=>$user->first_name,
                        "lastname"=>$user->last_name,
                        "country"=>$user->country,
                        "birthdate"=>$user->birthdate,
                        "phone_number"=>$user->phone_number
                    );

            $response = array(
                            "status"=>true,
                            "message"=>"success",
                            "version"=>version(),
                            "data"=>$data,
                            "currency"=>currency(),
                            "total_balance"=>total_balance(),
                            "balance_type"=>balance_type(),
                            "balance"=>value_balance(),
                            "pending_deposit"=>get_deposit()
                        );
             $this->response($response);
        }

        public function edit_post()
        {
            $input = json_decode(file_get_contents('php://input'),true);
            $this->form_validation->set_data($input);
            $config = array(
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'trim|required|valid_email'
                ),
                array(
                    'field' => 'firstname',
                    'label' => 'First name',
                    'rules' => 'required|alpha'
                ),
                array(
                    'field' => 'username',
                    'label' => 'Username',
                    'rules' => 'trim|required|min_length[6]|callback_valid_username'
                ),
                array(
                    'field' => 'country',
                    'label' => 'Country',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'birthdate',
                    'label' => 'Birthdate',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'phone_number',
                    'label' => 'Phone number',
                    'rules' => 'trim|required|min_length[8]'
                )
            );

            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == TRUE)
            {
                $profile_data = array(
                            'email'=>$input['email'],
                            'first_name'=>$input['firstname'],
                            'username'=>$input['username'],
                            'last_name'=>$input['lastname'],
                            'country'=>convert_country($input['country']),
                            'birthdate'=>$input['birthdate'],
                            'phone_number'=>$input['phone_number']
                        );
                $update = $this->db->update('tb_member', $profile_data, ['id'=>$this->user_data->id] );
                

                if($update)
                {

                    $user = $this->db->get_where('tb_member',['id'=>$this->user_data->id]);
                    $row = $user->row();

                    $data = array(
                                "email"=>$row->email,
                                "username"=>$row->username,
                                "firstname"=>$row->first_name,
                                "lastname"=>$row->last_name,
                                "country"=>$row->country,
                                "birthdate"=>$row->birthdate,
                                "phone_number"=>$row->phone_number
                            );

                    $response = array(
                                "status"=>true,
                                "message"=>"success update",
                                "version"=>version(),
                                "data"=>$data
                             );
                }
            }else
            {
                $response = array(
                                'status'=>FALSE,
                                'message'=>validation_errors()
                            );
            }
            $this->response($response);
        }

         public function updatePassword_post()
        {
            $input = json_decode(file_get_contents('php://input'),true);
            $this->form_validation->set_data($input);
            $config = array(
                array(
                    'field' => 'oldPass',
                    'label' => 'Old Password',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'newPass',
                    'label' => 'New Password',
                    'rules' => 'trim|required'
                ),
                array(
                    'field' => 'confirmPass',
                    'label' => 'Confirm Password',
                    'rules' => 'trim|required|matches[newPass]'
                ));

            $this->form_validation->set_rules($config);
            if($this->form_validation->run() == TRUE){
                if($this->aauth->check_password($this->user_data->id, $input['oldPass'], $this->user_data->pass)){
                    $new_pass   = $this->aauth->hash_password($input['newPass'],0);
                    $updated    = $this->db->update('tb_member',['pass'=> $new_pass], array('id'=> $this->user_data->id));

                    if($updated){
                         $response = array(
                            'status'=>true,
                            'message'=>'Success, password has been updated'
                        );
                       
                    }else{
                      
                        $response = array(
                            'status'=>false,
                            'message'=>$this->aauth->errors[0]
                        );
                        
                    }
                    $this->response($response);
                   
                }else{
                     $response = array(
                            'status'=>false,
                            'message'=>'old password not match, try again'
                        );
                    
                }
                    $this->response($response);
            }else{
                    $response = array(
                            'status'=>false,
                            'message'=>validation_errors()
                    );
                    $this->response($response);
            }
        }


         public function valid_username($username)
        {
            $username = trim($username);
            $regex_lowercase = '/[a-z]/';
            $regex_uppercase = '/[A-Z]/';
            $regex_number = '/[0-9]/';
            
            if (empty($username))
            {
                $this->form_validation->set_message('valid_username', 'The {field} field is required.');
                return FALSE;
            }
           

            if (preg_match_all($regex_number, $username) < 1)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field must have at least one number.');
                return FALSE;
            }

            if (strlen($username) < 6)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field must be at least 6 characters in length.');
                return FALSE;
            }
            
            if (strlen($username) > 32)
            {
                $this->form_validation->set_message('valid_username', 'The {field} field cannot exceed 32 characters in length.');
                return FALSE;
            }
            return TRUE;
        }

    }

?>
