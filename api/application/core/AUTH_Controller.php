<?php
	
	require APPPATH . '/libraries/REST_Controller.php';

	use Restserver\Libraries\REST_Controller;


	class AUTH_Controller extends Rest_Controller{
         public $user_data = '';

		 public function __construct($authorize = false) {
            parent::__construct();
            
        }

		public function cek(){
        $headers = array_change_key_case(getallheaders(),CASE_LOWER);
        if (array_key_exists('authorization', $headers) && !empty($headers['authorization'])) {
            //TODO: Change 'token_timeout' in application\config\jwt.php
            $decodedToken = AUTHORIZATION::validateTimestamp($headers['authorization']);

            // return response if token is not valid
            if(AUTHORIZATION::validateToken($headers['authorization'])!=true){
                 $response = array(
                            "status"=>false,
                            "message"=>"invalid token");

                $this->response($response, Rest_Controller::HTTP_UNAUTHORIZED);
            }else if ($decodedToken != true) {
                $response = array(
                            "status"=>false,
                            "message"=>"sesion_expired");

                $this->response($response, Rest_Controller::HTTP_UNAUTHORIZED);
            }

            else{
                $login = $this->aauth->login_fast($decodedToken['id']);
                if($login){
                    $this->user_data = $this->aauth->get_user();
                    return true;
                }else{
                    $response = array(
                            "status"=>false,
                            "message"=>$this->user_data->errors[0]
                        );
                    $this->response($response,Rest_Controller::HTTP_UNAUTHORIZED);
                    return false;
                }
                
            }
        }else{
            $response = array(
                            "status"=>false,
                            "message"=>"Unauthorized");
            $this->response($response, Rest_Controller::HTTP_UNAUTHORIZED);        
         }
            
    	}

	}

?>