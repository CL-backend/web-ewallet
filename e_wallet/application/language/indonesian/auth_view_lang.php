<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** TEMPLATE VIEW */

$lang ['login_button']      = "Masuk";
$lang ['remember_me']       = "Ingatkan Saya";
$lang ['forgot_password']   = "Lupa password?"; 
$lang ['have_account']      = "Belum punya akun?";
$lang ['create_account']    = "Buat akun disini";
$lang ['atau']              = "atau";
$lang ['join']              = "Bergabung dengan %s";

/** LOGIN */
$lang ['login_title']       = "Masuk akun anda";
$lang ['username_field']    = "Username";
$lang ['email_field']       = "Email";
$lang ['password_field']    = "Password";

/** REGISTER */
$lang ['register_button']   = "DAFTAR";
$lang ['has_been_regis']    = "Sudah punya akun?";
$lang ['login_here']        = "Silahkan masuk disini";
$lang ['full_name']         = "Nama Lengkap";
$lang ['phone_number']      = "Nomor Handphone";
$lang ['address']           = "Alamat";
$lang ['email']             = "Alamat Email";
$lang ['confirm_pass']      = "Ulangi password";
$lang ['register_title']    = "Daftar Sekarang";
$lang ['register_sub_titles']= "Masukkan Informasi pribadi anda";