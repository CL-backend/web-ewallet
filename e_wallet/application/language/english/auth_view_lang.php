<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** TEMPLATE VIEW */

$lang ['login_button']      = "Masuk";
$lang ['remember_me']       = "Ingatkan Saya";
$lang ['forgot_password']   = "Don't have account yet?"; 
$lang ['have_account']      = "Belum punya akun?";
$lang ['create_account']    = "Buat akun disini";
$lang ['atau']              = "atau";

/** LOGIN */
$lang ['login_title']       = "Login to your account";
$lang ['username_field']    = "Username";
$lang ['email_field']       = "Email";
$lang ['password_field']    = "Password";

/** REGISTER */
$lang ['register_button']   = "Register";
$lang ['has_been_regis']    = "Don't have account yet?";
$lang ['login_here']        = "Silahkan masuk disini";
$lang ['full_name']         = "Nama Lengkap";
$lang ['address']           = "Alamat";
$lang ['email']             = "Alamat Email";
$lang ['register_title']    = "Daftar Sekarang";
$lang ['register_sub_titles']= "Masukkan Informasi pribadi anda";