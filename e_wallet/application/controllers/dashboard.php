<?php

	class Dashboard extends CI_Controller{
		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/Authentication');
			}
		}
		public function index(){
			$data = ['title' => APP_NAME];
			return view("content.dashboard", $data);
		}


	}
?>