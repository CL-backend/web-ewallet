<?php

	class Notice extends CI_Controller{
		private $title;

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}
		

		public function index(){
			$data  = array('title'=>$this->title.APP_NAME);
					
			
			return view('content.page_notice',$data);
		}

		public function get_notice(){
			$query = $this->db->query("select * from tb_notice");
			
			echo json_encode(['data'=>$query->result()]);
		}

		public function insert(){
				$config ['upload_path'] = './upload/notice/';
				$config ['allowed_types'] = "gif|jpg|png|jpeg";
				$config ['overwrite'] = TRUE;
				$config ['file_name'] = $_FILES['file']['name'];
				$config ['max_size'] = 100; // Can be set to particular file size , here it is 2 MB(2048 Kb)
				$config ['max_height'] = 768;
				$config ['max_width'] = 1024;
					
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('file'))
				{

					$titles = $this->input->post('title');
					$description = $this->input->post('description');
					$image_file = $this->input->post('image_file');
					
					$data_insert = array(
								'title'=>$titles,
								 'description'=>$description,
								 'image_file'=>$_FILES['file']['name']
								);
					$insert = $this->db->insert('tb_notice',$data_insert);
					if($this->db->affected_rows() > 0){
						return redirect('/index.php/Notice');
					}else{
						$data ['massage'] = 'Cannot Update Data';
						return view('content.page_notice',$data);
					}

				}else{
					$data ['title'] = $this->title.APP_NAME;
					$data ['massage'] = $this->upload->display_errors();
					return view('content.page_profile_get',$data);
				}
		}

		public function get_data($id){
			$query = $this->db->query("select * from tb_notice where id = '$id' ");
			echo json_encode($query->row());
		}
		public function get_link($id){
			$query = $this->db->query("select * from tb_notice where id = '$id' ");
			return $query->row()->image_file;
		}

		public function edit($id){
				$config ['upload_path'] = './upload/notice/';
				$config ['allowed_types'] = "gif|jpg|png|jpeg";
				$config ['overwrite'] = TRUE;
				$config ['file_name'] = $_FILES['file']['name'];
				$config ['max_size'] = 100; // Can be set to particular file size , here it is 2 MB(2048 Kb)
				$config ['max_height'] = 768;
				$config ['max_width'] = 1024;
					
				unlink('./upload/notice/'.$this->get_link($id));

				$this->load->library('upload', $config);
				if($this->upload->do_upload('file'))
				{
					$titles = $this->input->post('title');
					$description = $this->input->post('description');
					$image_file = $_FILES['file']['name'];
					
					$query = $this->db->query("update tb_notice set title='$titles', description='$description', image_file='$image_file' where id = '$id' ");
				if($this->db->affected_rows() > 0){
							return redirect('/index.php/Notice');
						}else{
							$data ['massage'] = 'Cannot Update Data';
							return view('content.page_notice',$data);
						}

					}else{
						$data ['title'] = $this->title.APP_NAME;
						$data ['massage'] = $this->upload->display_errors();
						return view('content.page_notice',$data);
					}
			
		}

	}
?>