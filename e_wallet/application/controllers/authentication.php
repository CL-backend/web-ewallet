<?php

	class Authentication extends CI_Controller{
		private $title ='Login';

		public function __construct(){
			parent::__construct();
				
			
		}

		public function index(){
			if(!$this->aauth->is_loggedin()){
					$data = [
					'title'	    => "Login | ".APP_NAME,
					'message' =>""
					];
					
					return view('content.page_login',$data);
				}else{
					redirect('index.php/dashboard');
					
				}
			
		}

		public function page_register(){
			$data = [
				'title'=>"Page Register",
				'message'=>""
			];

			return view('content.page_register',$data);
		}

		public function login(){
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('password','Password','trim|required|min_length[8]');
			
			if($this->form_validation->run() != false){
				$login = $this->aauth->login($this->input->post('email'), $this->input->post('password'));
			
				 if($login){
				 	redirect('index.php/dashboard');		 	
				 }else{
				 	$data = array('message'=>$this->aauth->errors[0],
						'title'=>$this->title);
					view('content.page_login',$data);
				 }
			}else{
				

				$data = array('message'=>validation_errors(),
						'title'=>$this->title);
				view('content.page_login',$data);
			}
			
		}

		public function register(){

			$this->form_validation->set_rules('first_name','First Name','trim|required');
			$this->form_validation->set_rules('last_name','Last Name','trim|required');
			$this->form_validation->set_rules('account_type','Account Type','trim|required');
			$this->form_validation->set_rules('email','Email','trim|required');
			$this->form_validation->set_rules('password','Password','trim|required|min_length[8]');

			if($this->form_validation->run() == true){
				$data = array('first_name'=>$this->input->post('first_name'),
							 'last_name'=>$this->input->post('last_name'),
							 'account_type'=>$this->input->post('account_type'));
				$insert = $this->aauth->create_user($this->input->post('email'), $this->input->post('password') ,$data);
				
				if($insert){
					return redirect('/index.php/authentication');
				}else{
					$res ['message']   = $this->aauth->errors[0];
					$res ['title']   = "Page Register";
                	return view('content.page_register',$res);
				}
			}else {
	            $res ['message']   = validation_errors();
	            $res ['title']   = "Page Register";

	            return view('content.page_register',$res);
	        }
		}

		public function logout(){
			
			$this->aauth->logout();
			redirect('index.php/authentication');
		}

	}
?>