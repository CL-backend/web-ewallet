<?php

	class Games extends CI_Controller{

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/Authentication');
			}
			$this->load->library('prs_service');

		}
		public function index(){
			
			
			$data = ['title' => APP_NAME];

			return view("content.games_services", $data);
		}

		public function data(){
			$query = $this->db->get('tb_game_service');

			echo json_encode(['data'=>$query->result()]);
		}

		public function sync_act()
		{

			$status = $this->prs_service->getServiceList();

			if($status==true){
				$this->db->truncate('tb_game_service');
				$response = $this->prs_service->response_data;	
				$filters = json_decode(json_encode($response),true);
				
				foreach($filters as $obj){
				   $data  = array(
				   					'gameId'=>$obj['gameId'],
				   					'gameName'=>$obj['gameName'],
				   					'walletId'=>$obj['walletId'],
				   					'isActive'=>$obj['isActive']);
				   $this->db->insert('tb_game_service',$data);
				}
				 
				 redirect('index.php/games');
			}
			
		}

		public function by_id($id)
		{
			$query = $this->db->query("select * from tb_game_service where gameID = '$id' ");
			echo json_encode($query->row());
		}

		public function get_icon($id){
			$query = $this->db->query("select * from tb_game_service where gameID = '$id' ");
			return $query->row()->icon;
		}

		public function edit($id)
		{
			$path_icon = parse_url($this->get_icon($id), PHP_URL_PATH); 
			$banner_icon = basename($path_icon);
			$config ['upload_path'] = './upload/';
			$config ['allowed_types'] = "gif|jpg|png|jpeg";
			$config ['overwrite'] = TRUE;
			$config ['max_size'] = 500; 
			$config ['max_height'] = 200;
			$config ['max_width'] = 300;
			
			$this->load->library('upload', $config);

			if($this->upload->do_upload('icon'))
			{
				$gameName = $this->input->post('game_name');
				$status = $this->input->post('status');

				
				$data_insert = array(
									'gameName'=>$gameName,
									'icon'=>site_url().'upload/'.$_FILES['icon']['name'],
									'isActive'=>$status
								);
				$this->db->where('gameID', $id);
				$update = $this->db->update('tb_game_service', $data_insert);

				if($update){
					return redirect('/index.php/games');
				}else{
					$data  = ['title'=>APP_NAME];
					$data ['massage'] = 'Cannot Update Data';
					echo $this->db->error()['message'].'error';
				}

			}else{
					$data ['title'] = ['title'=>APP_NAME];
					$data ['massage'] = $this->upload->display_errors();
					// return view('content.page_banner',$data);
					echo $this->upload->display_errors();
			}
		}

	}
?>