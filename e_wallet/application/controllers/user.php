<?php

	class User extends CI_Controller{
		private $title = ""; 

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}
		public function index(){
			$data ['title'] = $this->title.APP_NAME;
			$data ['massage'] ="";
			
			return view('content.page_profile',$data);
		}

		public function update_profile(){
			
		

				$config ['upload_path'] = './upload/';
				$config ['allowed_types'] = "gif|jpg|png|jpeg";
				$config ['overwrite'] = TRUE;
				$config ['file_name'] = $_FILES['avatar']['name'];
				$config ['max_size'] = 100; // Can be set to particular file size , here it is 2 MB(2048 Kb)
				$config ['max_height'] = 768;
				$config ['max_width'] = 1024;
					
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('avatar'))
				{
					$data ['massage'] = 'Success Uplpad Avatar';
					$data ['title'] = $this->title.APP_NAME;

					$update_data = array('first_name'=>$this->input->post('first_name'),
										'last_name'=>$this->input->post('last_name'),
										'email'=>$this->input->post('email'),
										'avatar'=>site_url().'upload/'.$_FILES['avatar']['name']);

					$this->db->where('id', get_user_info()->id);					
					$this->db->update('tb_user',$update_data);
					if($this->db->affected_rows() > 0){
						return redirect('/index.php/user');
					}else{
						$data ['massage'] = 'Cannot Update Data';
						return view('content.page_profile',$data);
					}
					
				}
				else
				{
					$data ['title'] = $this->title.APP_NAME;
					$data ['massage'] = $this->upload->display_errors();
					return view('content.page_profile',$data);
				}
		}

		
		public function change_password(){
			$old_password = $this->input->post('old_password');
			$new_password = $this->input->post('new_password');
			$confirm_password = $this->input->post('confirm_password');

			if($new_password==$confirm_password){
				if($this->aauth->check_password($this->aauth->get_user()->id, $old_password, $this->aauth->get_user()->pass)){
					$new_pass 	= $this->aauth->hash_password($new_password,0);
					$updated	= $this->db->update('tb_user',['pass'=> $new_pass], array('id'=> $this->aauth->get_user()->id));

					if($updated){
						$data = array(
							'title'=>$this->title.APP_NAME,
							'massage'	=> "Success, password has been updated"
						);
						
					}else{
						$message=$this->aauth->errors[0];
						$data = array(
							'title'=>$this->title.APP_NAME,
							'massage'	=> $message
						);
					
					}

					return view('content.page_profile',$data);	
				}
			}else{
			
				$data = array(
					'title'=>$this->title.APP_NAME,
					'massage'	=> "Failed, you password doesnt match"
				);	
				
				return view('content.page_profile',$data);
			}
			redirect('index.php/user');
		}
	}

?>