<?php
	class Banner extends CI_Controller
	{
		public function construct()
		{
			parent::__construct();
			if(!$this->aauth->is_loggedin())
			{
				redirect('index.php/authentication');
			}
		}

		public function index()
		{
			$data  = ['title'=>APP_NAME];
			return view('content.page_banner',$data);
		}

		public function create()
		{
			$config ['upload_path'] = './upload/';
			$config ['allowed_types'] = "gif|jpg|png|jpeg";
			$config ['overwrite'] = TRUE;
			$config ['max_size'] = 2048; // Can be set to particular file size , here it is 2 MB(2048 Kb)
			$config ['max_height'] = 768;
			$config ['max_width'] = 1024;
					
				
			$this->load->library('upload', $config);
				if($this->upload->do_upload('file') && $this->upload->do_upload('icon'))
				{
					$data_filter = $this->input->post('datefilter');
					$a = explode(" - ", $data_filter);
					$start_date = $a[0];
					$end_date = $a[1];

					$promotion_title = $this->input->post('promotion_title');
					$promotion_content = $this->input->post('promotion_content');
					$display = $this->input->post('display');
					if($display==''){
						$val_display = 0;
					}else{
						$val_display = 1;
					}
					$kategori = $this->input->post('kategori');

					
					$data_insert = array(
								'start_date'=>$start_date,
								'end_date'=>$end_date,
								'promotion_title'=>$promotion_title,
								 'promotion_content'=>$promotion_content,
								 'banner'=>site_url().'upload/'.$_FILES['file']['name'],
								 'icon'=>site_url().'upload/'.$_FILES['icon']['name'],
								 'status_display'=>$val_display,
								 'kategori'=>$kategori
								);
					$insert = $this->db->insert('tb_banner_be',$data_insert);
					if($this->db->affected_rows() > 0){
						return redirect('/index.php/banner');
					}else{
						$data  = ['title'=>APP_NAME];
						$data ['massage'] = 'Cannot Update Data';
						echo $this->db->error()['message'];
					}

				}else{
					$data ['title'] = ['title'=>APP_NAME];
					$data ['massage'] = $this->upload->display_errors();
					// return view('content.page_banner',$data);
					echo $this->upload->display_errors();
				}
		}

		public function get_data()
		{
			$query = $this->db->get('tb_banner_be');
			foreach ($query->result() as $key) {
				$val [] = array(
									'id'=>$key->id,
									'active_date'=>$key->start_date.'-'.$key->end_date,
									'promotion_title'=>$key->promotion_title,
									'promotion_content'=>$key->promotion_content,
									'banner'=>$key->banner,
									'icon'=>$key->icon,
									'status_display'=>$key->status_display,
									'kategori'=>$key->kategori
								);
			}
			echo json_encode(['data'=>$val]);
		}

		public function by_id($id)
		{
			$query = $this->db->query("select * from tb_banner_be where id = '$id' ");
			echo json_encode($query->row());
		}

		public function get_link($id){
			$query = $this->db->query("select * from tb_banner_be where id = '$id' ");
			return $query->row()->banner;
		}

		public function get_icon($id){
			$query = $this->db->query("select * from tb_banner_be where id = '$id' ");
			return $query->row()->icon;
		}

		public function edit($id)
		{
			$path_banner = parse_url($this->get_link($id), PHP_URL_PATH); 
			$banner_image = basename($path_banner);
			$path_icon = parse_url($this->get_icon($id), PHP_URL_PATH); 
			$banner_icon = basename($path_icon);
			
			$config ['upload_path'] = './upload/';
			$config ['allowed_types'] = "gif|jpg|png|jpeg";
			$config ['overwrite'] = TRUE;
			$config ['max_size'] = 2048; // Can be set to particular file size , here it is 2 MB(2048 Kb)
			$config ['max_height'] = 768;
			$config ['max_width'] = 1024;
					
			
            unlink('./upload/'.$banner_image);
            unlink('./upload/'.$banner_icon);
          
              

			$this->load->library('upload', $config);
				if($this->upload->do_upload('file') && $this->upload->do_upload('icon'))
				{
					$data_filter = $this->input->post('datefilter');
					$a = explode(" - ", $data_filter);
					$start_date = $a[0];
					$end_date = $a[1];

					$promotion_title = $this->input->post('promotion_title');
					$promotion_content = $this->input->post('promotion_content');
					$display = $this->input->post('display');
					if($display==''){
						$val_display = 0;
					}else{
						$val_display = 1;
					}
					$kategori = $this->input->post('kategori');

					
					$data_insert = array(
								'start_date'=>$start_date,
								'end_date'=>$end_date,
								'promotion_title'=>$promotion_title,
								 'promotion_content'=>$promotion_content,
								 'banner'=>site_url().'upload/'.$_FILES['file']['name'],
								 'icon'=>site_url().'upload/'.$_FILES['icon']['name'],
								 'status_display'=>$val_display,
								 'kategori'=>$kategori
								);
					$this->db->where('id', $id);
					$update = $this->db->update('tb_banner_be', $data_insert);
					if($this->db->affected_rows() > 0){
						return redirect('/index.php/banner');
					}else{
						$data  = ['title'=>APP_NAME];
						$data ['massage'] = 'Cannot Update Data';
						echo $this->db->error()['message'];
					}

				}else{
					$data ['title'] = ['title'=>APP_NAME];
					$data ['massage'] = $this->upload->display_errors();
					// return view('content.page_banner',$data);
					echo $this->upload->display_errors();
				}
		}

		public function change_status()
		{
			$id = $this->uri->segment(3);
			$status = $this->uri->segment(4);

			$query = $this->db->query("update tb_banner_be set status_display='$status' where id='$id' ");
			if($query){
				$data['status'] = true;
				$data['massage'] = "Success Update Status";
				$data['context'] = "info";
			}else{
				$data['status'] = false;
				$data['massage'] = "Unsuccess Update Status";
				$data['context'] = "danger";
			}
			
			echo json_encode($data);
		}

		public function delete($id)
		{
			$query = $this->db->delete('tb_banner_be', array('id' => $id));
			if($query)
			{
				$data['status'] = true;
				$data['massage'] = "Data is deleted";
				$data['context'] = "info";
			}else
			{
				$data['status'] = false;
				$data['massage'] = "Unsuccess delete data";
				$data['context'] = "danger";
			}
			
			echo json_encode($data);
		}

	}
?>