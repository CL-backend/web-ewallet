<?php


	class Admin extends CI_Controller{
		private $title = ""; 
		
		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}
		
		public function index(){
			$data  = array('title'=>$this->title.APP_NAME,
							'data'=>$this->get_staff(),
							'message'=>"");
					
			
			return view('content.page_contact_list',$data);
		}

		public function get_staff(){
			$query = $this->db->query("select a.*, b.name from tb_user a join aauth_groups b on a.account_type = b.id  ");
			if($query){
				return $query->result();
			}else{
				return "";
			}
			
		}

		public function create(){
			$password = $this->input->post('password');
			$email = $this->input->post('email');

			$data = array('first_name'=>$this->input->post('first_name'),
							'last_name'=>$this->input->post('last_name'),
							'account_type'=>$this->input->post('account_type'),
							'account_sub_type'=>$this->input->post('sub_account'));


			$insert = $this->aauth->create_user($email,$password,$data);
			if($insert){
					return redirect('/index.php/member');
				}else{
					$data  = array('title'=>$this->title.APP_NAME,
							'data'=>$this->get_staff(),
							'message'=>$this->aauth->errors[0]);
                	return view('content.page_contact_list',$data);
				}

		}


		public function update($id){
			
		

				$config ['upload_path'] = './upload/';
				$config ['allowed_types'] = "gif|jpg|png|jpeg";
				$config ['overwrite'] = TRUE;
				$config ['file_name'] = $_FILES['avatar']['name'];
				$config ['max_size'] = 100; // Can be set to particular file size , here it is 2 MB(2048 Kb)
				$config ['max_height'] = 768;
				$config ['max_width'] = 1024;
					
				
				$this->load->library('upload', $config);
				if($this->upload->do_upload('avatar'))
				{
					$data ['massage'] = 'Success Uplpad Avatar';
					$data ['title'] = $this->title.APP_NAME;

					$update_data = array('first_name'=>$this->input->post('first_name'),
										'last_name'=>$this->input->post('last_name'),
										'email'=>$this->input->post('email'),
										'avatar'=>site_url().'upload/'.$_FILES['avatar']['name']);

					$this->db->where('id', $id);					
					$this->db->update('tb_user',$update_data);
					if($this->db->affected_rows() > 0){
						return redirect('/index.php/member');
					}else{
						$data ['massage'] = 'Cannot Update Data';
						return view('content.page_profile_get',$data);
					}
					
				}
				else
				{
					$data ['title'] = $this->title.APP_NAME;
					$data ['massage'] = $this->upload->display_errors();
					return view('content.page_profile_get',$data);
				}
		}


		public function delete(){
			$id = $this->input->post('id');
			$query = $this->db->delete('tb_user', array('id' => $id));
			return $query;

		}

		public function data_member($id){
			
			$query = $this->db->query("select a.name,b.* from aauth_groups a join tb_user b on a.id=b.account_type where b.id ='$id'");

			$data = array('title'=>$this->title.APP_NAME,
						'massage'=>"",
						'data'=>$query->row());
			return view('content.page_profile_get',$data);
		}
	}

?>