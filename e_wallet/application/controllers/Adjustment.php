<?php

	class Adjustment extends CI_Controller
	{
		public function construct()
		{
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}

		public function index()
		{
			$data = array(
					'title'=>APP_NAME
					);
			return view('content.page_adjustment',$data);
		}

		public function create_adjustment()
		{
			$data = array(
					'title'=>APP_NAME,
					'data'=>$this->data(),
					'code_member'=>$this->member_code()
					);
			return view('content.page_create_adjustment',$data);
		}

		public function adjustment_report()
		{
			$data = array(
					'title'=>APP_NAME
					);
			return view('content.page_adjustment_report',$data);
		}

		public function data_adjustment_report(){		

			$data = array(
					'print'=>"<a href='javascript:void(0)' class='print' onClick='print()'><i class='icon-printer'></a>",
					'currency'=>$this->total()->row()->currency,
					'total_plus'=>($this->total()->row()->total+$this->total_credit()),
					'total_credit'=>$this->total_credit(),
					'total_min'=>($this->total()->row()->total - $this->total_debit()),
					'total_debit'=>$this->total_debit()
					);
			
			echo json_encode(['data'=>array($data)]);
		}

		public function create()
		{
			$data = array(
						'id'=>0,
						'member_code'=>$this->input->post('member_code'),
						'currency'=>$this->input->post('currency'),
						'wallet'=>$this->input->post('wallet'),
						'account_balance'=>$this->input->post('account_balance'),
						'category'=>$this->input->post('category'),
						'adjustment_amount'=>$this->input->post('adjustment_amount'),
						'reference_id'=>$this->input->post('reference_id'),
						'note'=>$this->input->post('note')
					);
			$insert = $this->db->insert('adjustment',$data);
			
			if($insert){

				return redirect('/index.php/Adjustment/create_adjustment');

			}else{

				$data  = array(
							'title'=>"Adjustment Create | ".APP_NAME,
							'data'=>$this->data()
						);
                
                return view('content.page_create_adjustment',$data);
			}

		}

		public function delete()
		{
			$id = $this->input->post('id');
			$query = $this->db->delete('adjustment', array('id' => $id));
			return $query;
		}

		public function update()
		{

		}

		public function data()
		{
			$query = $this->db->query("select * from adjustment");
			return $query;
		}

		public function total()
		{
			$query = $this->db->query("SELECT currency, sum(account_balance) as total FROM `adjustment` group by currency");
			return $query;
		}

		public function total_credit()
		{
			$query = $this->db->query("SELECT currency, sum(adjustment_amount) as total FROM `adjustment`  where category = 'bonus' group by currency ");
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
		}

		public function total_debit()
		{
			$query = $this->db->query("SELECT currency, sum(adjustment_amount) as total FROM `adjustment` where category != 'bonus' group by currency  ");
			if($query->num_rows() > 0){
				return $query->row()->total;
			}else{
				return 0;
			}
			
		}

		public function member_code()
		{
			$query = $this->db->query("SELECT member_code from tb_transaction ");
			return $query;
		}

	}

?>