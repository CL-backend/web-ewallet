<?php
	class Bonus extends CI_Controller{
		
		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}

		public function index(){
			$data = array('title'=>APP_NAME);
			return view('content.page_bonus',$data);
		}

		public function addBonus(){
			$user_id = $this->input->post('user_id');
			$bonus = $this->input->post('bonus');
			$date = $this->input->post('date');
			$bonus_from = $this->input->post('source_bonus');
			$data = array(
				'user_id'=>$user_id,
				'date'=>$date,
				'bonus'=>$bonus,
				'source_bonus'=>$bonus_from
			);

				$insert = $this->db->insert('tb_bonus',$data);
				if($insert){
					
					$data = array(
							'status'=>"Success Add Bonus",
							'context'=>"info"
						);
					echo json_encode($data);
				}else{
					$data = array(
							'status'=>"Failed Add Bonus",
							'context'=>"danger"
						);
					echo json_encode($data);
				}
		}

		public function get_bonus(){
			$query = $this->db->query("select a.user_id, a.date, a.bonus, a.source_bonus , b.username from tb_bonus a join tb_user b on a.user_id=b.id order by a.date desc");
			echo json_encode(['data'=>$query->result()]);
		}


	}
