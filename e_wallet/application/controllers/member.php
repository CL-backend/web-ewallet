<?php
	class Member extends CI_Controller{
		private $title;

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}

		public function index(){

			$data  = array('title'=>$this->title.APP_NAME,'country'=>$this->country());
			return view('content.page_member_list',$data);
		}

		public function country()
		{
			$query = $this->db->get('country');
			return $query->result();
		}

		public function create_member(){
			$full_name= $this->input->post('first_name').' '.$this->input->post('last_name');
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$confirm_password = $this->input->post('confirm_password');
			$country = $this->input->post('country');
			$birthdate = $this->input->post('birthdate');
			$data = array(
				'full_name'=>$full_name,
			 	'country'=>$country,
			 	'birthdate'=>$birthdate,
			 	'first_name'=>$this->input->post('first_name'),
			 	'last_name'=>$this->input->post('last_name'),
			 	'account_type'=>3);
			$insert = $this->aauth->create_user($email,$password,$data);
			if($insert){
					
					$status['status'] = "Success Create Member";
					$status['context'] = "info";
                	
				}else{
					
					$status['status'] = $this->aauth_member->errors[0];
					$status['context'] = "danger";
                	
				}
				echo json_encode($status);
		}

		public function edit($id){
			$data = array(
						'first_name'=>$this->input->post('first_name'),
						'last_name'=>$this->input->post('last_name'),
						'email'=>$this->input->post('email'),
						'pass'=>$this->aauth->hash_password($this->input->post('password'),0),
						'country'=>convert_country($this->input->post('country'))->nicename,
						'birthdate'=>$this->input->post('birthdate'),
						'phone_number'=>"+".convert_country($this->input->post('country'))->phonecode.$this->input->post('phone_number')
					);
			$update = $this->db->update('tb_member', $data , array('id'=>$id) );
			if($update){
				redirect('index.php/Member');
			}
		}

		public function get_member(){
			$query = $this->db->query("select * from tb_member");

			$data = array();
			foreach ($query->result() as $key) {
				$data []= array(
							'id'=>$key->id,
							'full_name'=>$key->first_name.' '.$key->last_name,
							'email'=>$key->email,
							'country'=>$key->country,
							'date_created'=>$key->date_created,
							'balance'=>get_balance($key->id),
							'referal'=>$key->referal,
							'last_activity'=>$key->last_activity,
							'ip_address'=>$key->ip_address,
							'last_login'=>$key->last_login);
			}
			echo json_encode(['data'=>$data]);
		}

		public function get_by($id)
		{
			$query = $this->db->query("select a.*,b.iso from tb_member a join country b on a.country=b.nicename where a.id='$id' ");
			echo json_encode($query->row());
		}

		public function banned($id){
			$query = $this->db->query("update tb_user set banned=1 where id=$id ");
			if($query){
				$status['status'] = "Success Create Member";
				$status['context'] = "info";
			}else{
				$status['status'] = $this->aauth_member->errors[0];
				$status['context'] = "danger";
			}
			return json_encode($status);
		}

		public function delete($id){
			
			$query = $this->db->delete('tb_user', array('id' => $id));
			if($query){
				$status['Success'] = true;
				
			}else{
				$status['Success'] = false;
			}
			
			echo json_encode($status);
		}


	}
?>