<?php
	class Financial extends CI_Controller{

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}
		public function deposit(){
			$data = array('title'=>APP_NAME);

			return view('content.page_deposit',$data);
		}

		public function withdrawal(){
			$data = array('title'=>APP_NAME);

			return view('content.page_withdrawal',$data);
		}

		public function get_deposit(){
			
			$query = $this->db->query("select a.*,b.status from tb_deposit a join tb_transaction b on a.transaction_id=b.id ");
			
			echo json_encode(['data'=>$query->result()]);
		}

		public function approve_deposit($id){
			$update_date = date('Y-m-d H:i:s');
			$query = $this->db->query("update tb_transaction set status='2' where id='$id' ");
			$this->db->query("update tb_deposit set update_date='$update_date' where transaction_id='$id' ");
			
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}

		public function reject_deposit($id){
			$update_date = date('Y-m-d H:i:s');
			$query = $this->db->query("update tb_transaction set status='3' where id='$id' ");
			$this->db->query("update tb_deposit set update_date='$update_date' where transaction_id='$id' ");
			if($query){
				$status['status'] = true;
			}else{
				$status['status'] = false;
			}
			echo json_encode($status);
		}

		public function get_withdrawal(){
			
			$query = $this->db->query("select * from tb_transaction where transaction_type='withdrawal' ");
			
			echo json_encode(['data'=>$query->result()]);
		}

		public function insert_withdrawal(){
			$member_code = $this->input->post('member_code');
			$currency = $this->input->post('currency');
			$payment_method = $this->input->post('payment_method');
			$bank_name = $this->input->post('bank_name');
			$amount = $this->input->post('amount');

			$data_insert = array('member_code'=>$member_code,
						 'currency'=>$currency,
						 'payment_method'=>$payment_method,
						 'bank_name'=>$bank_name,
						 'amount'=>$amount,
						 'submit_date'=>date("Y-m-d"),
						 'transaction_type'=>'withdrawal');
			$insert = $this->db->insert('transaction',$data_insert);
			if($insert){
				$status['status'] = "Successful, the data is saved";
				$status['context'] = "info";
			}else{
				$status['status'] = "Sorry the data is not saved";
				$status['context'] = "danger";
			}

			echo json_encode($status);
			
		}


		public function report()
		{
			$result = $this->db->query('SELECT a.*, b.* from bank_account a join adjustment b on a.member_code=b.member_code');

			$data = array(
				'title'=>APP_NAME,
				'data'=>$result->result()
			);

			return view('content.page_financial_reports',$data);
		}

		// public function change_status(){
		// 	$member_code = $this->input->post('member_code');
		// 	$deposit_id = $this->input->post('deposit_id');
		// 	$status_deposit = $this->input->post('status_deposit');
		// 	$update_date = date('Y-m-d H:i:s');
		// 	if($status_deposit==2){
		// 		$get_amount = $this->db->query("select amount,payment_method from tb_deposit where member_code='$member_code' and id= '$deposit_id' ");
		// 		$row = $get_amount->row();
		// 		$data  = array(
		// 					'member_code'=>$member_code,
		// 					'wallet'=>'Main Wallet',
		// 					'amount'=>$row->amount,
		// 					'trans_type'=>$row->payment_method,
		// 					'type'=>'Credit',
		// 					'date'=>date('Y-m-d'),
		// 					'comments'=>'Deposit (Main Wallet)'
		// 				);
		// 		$insert = $this->db->insert('tb_transaction',$data);
		// 		$insert_wallet = $this->db->insert('tb_wallet',['user_id'=>$member_code,'balance_name'=>'main_wallet','value'=>$row->amount]);
		// 	}
		// 	$update = $this->db->query("update tb_deposit set status='$status_deposit', update_date='$update_date' where member_code='$member_code' and id='$deposit_id' ");
		// 	if($update){
		// 		$status['status'] = "Successful, the data is updated";
		// 		$status['context'] = "info";
		// 	}else{
		// 		$status['status'] = "Sorry the data is not updated";
		// 		$status['context'] = "danger";
		// 	}

		// 	echo json_encode($status);
			
		// }

		



	}
?>