<?php
	class Transfer extends CI_Controller{

		public function __construct(){
			parent::__construct();
			if(!$this->aauth->is_loggedin()){
				redirect('index.php/authentication');
			}
		}

		public function fund_transfer(){
			$data = array('title'=>APP_NAME);

			return view('content.page_transfer_fund',$data);
		}

		public function pending_transfer(){
			$data = array('title'=>APP_NAME);

			return view('content.page_pending_transfer',$data);
		}

		public function get_success_transfer(){
			$query = $this->db->query("select * from tb_transfer where fund_out_status=1 ");
			
			echo json_encode(['data'=>$query->result()]);
		}

		public function get_pending_transfer(){
			$query = $this->db->query("select * from tb_transfer where fund_out_status=0 ");
			
			echo json_encode(['data'=>$query->result()]);
		}
		
	
	}
?>