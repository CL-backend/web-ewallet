<?php
	
	class Bank extends CI_Controller
	{
		public function construct()
		{
			parent::__construct();
			if(!$this->aauth->is_loggedin())
			{
				redirect('index.php/Authentication');
			}
		}

		public function index()
		{
			$data = array(
				'title'=>APP_NAME,
			);

			return view('content.page_member_bank_account',$data);
		}

		public function create()
		{
			$data = array(
				'payment_type'=>$this->input->post('payment_type'),
				'currency_code'=>$this->input->post('currency_code'),
				'member_code'=>$this->input->post('member_code'),
				'account_number'=>$this->input->post('account_number'),
				'account_name'=>$this->input->post('account_name'),
				'bank'=>$this->input->post('bank'),
				'province'=>$this->input->post('province'),
				'city'=>$this->input->post('city'),
				'branch'=>$this->input->post('branch')
			);

			$insert = $this->db->insert('bank_account',$data);

				if($insert)
				{
					
					$data = array(
							'status'=>"Success Add Bank Account",
							'context'=>"info"
						);
					echo json_encode($data);

				}
				else
				{
					$error = $this->db->error();
					$data = array(
							'status'=>"Failed Add Bank Account Becaus ".$error['message'],
							'context'=>"danger"
						);
					echo json_encode($data);
				}
		}

		public function data()
		{
			$query = $this->db->query('select * from bank_account');
			echo json_encode(['data'=>$query->result()]);
		}

		public function banned($id)
		{
			
			$query = $this->db->query("update bank_account set status='0' where id = '$id' ");
			if($query){
				$status['Success'] = true;
				
			}else{
				$status['Success'] = false;
			}
			
			echo json_encode($status);
		}

		public function unbanned($id)
		{
			
			$query = $this->db->query("update bank_account set status='1' where id = '$id' ");
			if($query){
				$status['Success'] = true;
				
			}else{
				$status['Success'] = false;
			}
			
			echo json_encode($status);
		}

	}
?>