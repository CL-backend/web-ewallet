<?php defined('BASEPATH') OR exit('No direct script access allowed');
	

class Prs_service{
	
	
	private $apiKey = "029ce5917cbb";
	private $apiId = "576cf898-3005-11e9-85aa-029ce5917cbb";
	private $iv = "5a4fc9734dcdaaae";
	private $salt = "7723";	
	private $key;
	private $globalPayload = [];
	
	//Encrypt Tools
	private $enc_mode = OPENSSL_RAW_DATA;
	private $enc_module = 'aes-128-cbc';
	
	//default Data
	
	private $agentUser = "bagas01";
	private $agentPassword = "123456";
	private $publicPassword = "optimus123";
	
	public $errorMessage = "";
	private $validation_error = [];
	
	public $response = [];
	
	
	public function __construct(){
		$this->key = $this->apiKey.$this->salt;
		$this->globalPayload = ['apiId' => $this->apiId, 'apiUsername' => $this->agentUser, 'apiPassword' => $this->agentPassword, 'password'=> $this->publicPassword ];
	}
	
	
	/*
	* Module : Create User
	* Payload : Username	
	*/	
	
	public function createUser($payload){
		
		$validation = $this->check_required( $payload, $this->_module()->{__FUNCTION__}->required);
		
		if($validation){
			$params = $this->_generate_payload($payload);
			$request = $this->_run(__FUNCTION__, $params);
			
			if($request->status == 'ok'){
				return true;
			}else{
				$this->errorMessage = $request->message;
				return false;
			}
			
		}else{
			return false;
		}
		
	}
	
	/*
	* Module : GET SERVICE LIST
	* 
	*/	
	
	public function getServiceList(){
		$params 	= $this->_generate_payload();
		$request 	= $this->_run(__FUNCTION__, $params);
		
		if($request->status == 'ok'){
			$this->response_data = $request->data;
			return true;
		}else{
			$this->errorMessage = $request->message;
			return false;
		}
	}
	
	
	private function _run($module, $payload){
	  
	  	$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,$this->_uri($module, $payload));
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,"");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		
		$output = curl_exec($ch);
		
		curl_close ($ch);
		
		$response = json_decode($output);
	
		return json_decode($this->aes_decrypt($response->data));
		
	}
	
	
	
	private function _generate_payload($payload = []){
		
		$data = array_merge($this->globalPayload, $payload);
		$hexData = $this->aes_encrypt($data);
		return $hexData;
		
	}
	
	
	private function _uri($module, $payload){
		
		$endpoint = "http://www.prs188.com/api_service/";
		$endpoint .= $this->_module()->{$module}->endpoint;
		$params  = http_build_query(['apiId'=> $this->apiId, 'data' => $payload]);
		
		return $endpoint.'?'.$params;
	}
	
	private function _module(){
		return (Object)[
			'createUser' => (Object)['endpoint' => 'create_user', 'required' => ['username'=>'']],
			'getServiceList' => (Object) ['endpoint' => 'api_service_listing', 'required' => []]
		];
	}
	
	
	private function check_required( $input, $required ){
		$missing = array();
		foreach ($required as $k=>$v) {
			if (empty($input[$k])) {
				$missing[] = $k;
			} else if (is_array($v)) {
				$missing = array_merge( $this->check_required( $input[$k], $v ), $missing );
			}
		}
		$this->validation_error = $missing;
		$this->errorMessage = 'Please fill require form '. implode(', ', $missing);
		return count($missing) > 0 ? false: true;
	}
	
	private function aes_encrypt($payload){
		
		$crypt = openssl_encrypt(json_encode($payload),$this->enc_module,$this->key, $this->enc_mode,$this->iv);
		$hexacode = bin2hex($crypt);
		return $hexacode;
	}
	
	private function aes_decrypt($stringHex){
		$str = hex2bin($stringHex);
		$payload = openssl_decrypt($str, $this->enc_module, $this->key,$this->enc_mode, $this->iv);
		return $payload;
	}
	
	
}