@extends('main.base_auth')

@section('title', $title)

@section('content')
<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle auth-main">
				<div class="auth-box">
                    <div class="mobile-logo"><a href="index.html"><img src="{{APP_ASSETS}}images/logo-icon.svg" alt="Mplify"></a></div>
                    <div class="auth-left">
                        <div class="left-top">
                            <a href="index.html">
                                <img src="{{APP_ASSETS}}images/logo-icon.svg" alt="Mplify">
                                <span>{{$title}}</span>
                            </a>
                        </div>
                        <div class="left-slider">
                            <img src="{{APP_ASSETS}}images/login/1.jpg" class="img-fluid" alt="">
                        </div>
                    </div>

                    <div class="auth-right">
                     <!--    <div class="right-top">
                            <ul class="list-unstyled clearfix d-flex">
                                <li><a href="index.html"><i class="fa fa-home"></i></a></li>
                                <li><a href="javascript:void(0);">Help</a></li>
                                <li><a href="javascript:void(0);">Contact</a></li>
                            </ul>
                        </div> -->

                        <div class="card">
                            <div class="header">
                                {{$message}}
                                <p class="lead">Create an account </p>
                                
                            </div>
                            <div class="body">
                                <form class="form-auth-small" method="post" action="{{site_url()}}index.php/authentication/register">
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">First Name</label>
                                        <input type="text" class="form-control" name="first_name" placeholder="Your First Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" placeholder="Your Last Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Account Type</label>
                                        <select name="account_type" class="form-control">
                                            <option value="3">Finance</option>
                                            <option value="1">Administrator</option>
                                            <option value="2">Manager</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Your email">
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-password" class="control-label sr-only">Password</label>
                                        <input type="password" class="form-control" name="password" placeholder="Password">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">REGISTER</button>
                                    <div class="bottom">
                                        <span class="helper-text">Already have an account? <a href="{{site_url()}}index.php/authentication/">Login</a></span>
                                    </div>
                                </form>
                                <!-- <div class="separator-linethrough"><span>OR</span></div>
                                <button class="btn btn-signin-social"><i class="fa fa-facebook-official facebook-color"></i> Sign in with Facebook</button>
                                <button class="btn btn-signin-social"><i class="fa fa-twitter twitter-color"></i> Sign in with Twitter</button> -->
                            </div>
                        </div>
                    </div>
				</div>
			</div>
		</div>
	</div>

      @endsection