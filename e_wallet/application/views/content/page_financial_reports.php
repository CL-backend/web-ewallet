@extends('main.base_main')


@section('title',$title)


@section('css')
<style>
        .clock-button {
            background-color: blueviolet;
            padding: 0.5em;
            color: white;
        }
        
        .clock-placement {
            position: fixed;
            /* Fixed/sticky position */
            top: 20px;
            /* Place the button at the top of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
        }
    </style>
@endsection


@section('content')
	
	<div id="main-content">
	
		 	<div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Financial Reports</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Financial Reports</li>
                        </ul>
                    </div>
                </div>
            </div>

             <div class="body">
             
		            <div class="clearfix">
		            	<div class="col-sm-12 col-sm-12">
		                    <div class="card">
		                        <div class="header">
		                            <h2> Financial > Financial Reports</h2>
		                        </div>

		                        <div class="body">

		                        	<div class="form-group row">
									  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
									  	<div class="col-sm-4">
                                        <small>Member Code</small>
                                    	
                                    	<input type="text"  class="input-sm form-control member-code"/>
                                       </div>

                                       <div class="col-sm-4">
                                       	<small>Account Number</small>
                                       		<input type="text"  class="input-sm form-control account-number"/>
                                       </div>

                                        <div class="col-sm-4">
                                       	<small class="input-group-addon range-to">Account Name</small>
                                       		<input type="text"  class="input-sm form-control account-name"/>
                                       </div>


                                     </div>


									 </div>

									<div class="form-group row">
									  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										    <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Currency</small>
	                                   
	                                    		<select class="form-control form-control-xs currency" name="end">
	                                    			<option value="IDR">IDR</option>
	                                    			<option value="USD">USD</option>
	                                    		</select>
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Wallet</small>
	                                   
	                                    		<select class="form-control form-control-xs wallet" name="end">
	                                    			<option value="Main">Main</option>
	                                    			<option value="None">None</option>
	                                    		</select>
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Account Balance</small>
	                                   			<input type="text"  class="input-sm form-control account-balance"/>
	                                        </div>
	                                       

	                                     	</div>
									 </div>

									 <div class="form-group row">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-3">
	                                        	<small>Bank Name</small>
	                                    
	                                    		<select class="form-control form-control-xs bank-name" name="end">
	                                    			<option value="BNI">BNI</option>
	                                    			<option value="BCA">BCA</option>
	                                    		</select>
	                                        </div>
	                                 
									 </div>
									</div>

									 


									 <div class="form-group row float-right">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-12">
	                                        	
	                                        	<button class="btn btn-primary search"><i class="icon-reload"></i> Search</button>
	                                        	<button class="btn btn-primary export"><i class="icon-reload"></i> Export</button>
	                                        </div>
	                                      

	                                     </div>
									 </div>

									 
		               
		                        </div>

		                        <div class="col-xs-12 mr-1 ml-1">
				                    <div class="card">
				                      
				                        
				                            <div class="table-responsive">
				                                <table class="table table-bordered table-hover dataTable table-custom" id="examples">
				                                   <thead class="thead-dark">
				                                        <tr>
				                                        	<th>No</th>
				                                            <th>Member Code</th>
				                                            <th>Account Number</th>
				                                            <th>Account Name</th>
				                                            <th>Currency</th>
				                                            <th>Wallet</th>
				                                            <th>Account Balance</th>
				                                            <th>Bank Name</th>
				                                            <th>Location</th>
				                                           
				                                        </tr>
				                                    </thead>
				                                  	<tbody>
				                                  		@foreach($data as $row)
				                                  		<tr>
				                                  			<td></td>
				                                  			<td>{{$row->member_code}}</td>
				                                  			<td>{{$row->account_number}}</td>
				                                  			<td>{{$row->account_name}}</td>
				                                  			<td>{{$row->currency}}</td>
				                                  			<td>{{$row->wallet}}</td>
				                                  			<td>{{$row->account_balance}}</td>
				                                  			<td>{{$row->bank}}</td>
				                                  			<td>{{$row->province}} , {{$row->city}}, {{$row->branch}}</td>


				                                  		</tr>
				                                  		@endforeach
				                                  	</tbody>
				                                    
				                                </table>
				                            </div>
				                       
				                    </div>
				                </div>

		                    </div>
		              
		                </div>
		           
		
	</div>



@endsection

@section('javascript')
<script type="text/javascript">
$(document).ready(function() {

	$(function() {
	  $('input[name="datefilter"]').daterangepicker({
	    timePicker: true,
	    startDate: moment().startOf('hour'),
	    endDate: moment().startOf('hour').add(32, 'hour'),
	    locale: {
	      format: 'YYYY-MM-DD'
	    }
	  });
	});
	

    var t=$('#examples').DataTable({
    	dom: 'Bfrtip',
        buttons: [
           
            'pdfHtml5',
             orientation: 'landscape',
                pageSize: 'LEGAL',
                title: 'Financial Reports'
        ]

    	}).draw();

    t.on('order.dt search.dt', function () {
	    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        cell.innerHTML = i+1;
	    });
	   
	}).draw();


   $(".search").on("click", function (e) {
	   e.preventDefault();
		search();
	   $('#examples').DataTable().draw();
	});

} );

$(".export").on("click", function() {
    $('#examples').DataTable().button( '.buttons-pdf' ).trigger();
});


	$('.submit').click(function(event){
		var member_code = $('.member_code').val();
		var currency = $('.currencys').children("option:selected").val();
		var payment_method = $('.payment-methods').children("option:selected").val();
		var bank_name = $('.bank_names').children("option:selected").val();
		var amount = $('.amount').val();
					

		jQuery.ajax({
			type:"Post",
			url:"{{site_url()}}index.php/Financial/insert_deposit",
			dataType:'json',
			data:{member_code:member_code,currency:currency,payment_method:payment_method,bank_name:bank_name,amount:amount },

			success:function(res){
				
				
					$("#defaultModal1").modal('hide');
					toastr.remove();
					$context = res.context;
					$positionClass = 'toast-top-center';
					$message = res.status;
	                toastr[$context]($message, '', {
	                    positionClass: $positionClass
	                });
	                $('#examples').DataTable().ajax.reload();
				
			}
		});
	});

function search(){
  $.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			console.log(data);
			var member_code = $('.member-code').val();
			var account_number = $('.account-number').val();
			var account_name = $('.account-name').val();
			var currency = $('.currency').children("option:selected").val();
			var wallet = $('.wallet').children("option:selected").val();
			var account_balance = $('.account-balance').val();
		  	var bank_name = $('.bank-name').children("option:selected").val();
	

		    if(member_code==data[1]){
		    	return true;
		    }if(account_number==data[2]){
		    	return true;
		    }if(account_name==data[3]){
		    	return true;
		    }if(currency==data[4]){
		    	return true;
		    }if(wallet==data[5]){
		    	return true;
		    }if(account_balance==data[6]){
		    	return true;
		    }if(bank_name==data[7]){
		    	return true;
		    }else{
		    	return false;
            }
            
            return false;
		}
	);
}


</script>
@endsection