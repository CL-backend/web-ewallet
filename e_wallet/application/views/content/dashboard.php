@extends('main.base_main')

@section('title', $title)

@section('content')


    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Dashboard</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-12">
                    <div class="card top_report">
                        <div class="row">
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-dollar text-col-blue"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>EARNINGS</h6>
                                            <span class="font700">$22,500</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-blue mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="87"></div>
                                    </div>
                                    <small class="text-muted">19% compared ttotal_withdraw</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-bar-chart-o text-col-green"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>SALES</h6>
                                            <span class="font700">$500</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-green mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="28"></div>
                                    </div>
                                    <small class="text-muted">19% compared to last week</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-shopping-cart text-col-red"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>Orders</h6>
                                            <span class="font700">215</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-red mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="41"></div>
                                    </div>
                                    <small class="text-muted">19% compared to last week</small>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6">
                                <div class="body">
                                    <div class="clearfix">
                                        <div class="float-left">
                                            <i class="fa fa-2x fa-thumbs-up text-col-yellow"></i>
                                        </div>
                                        <div class="number float-right text-right">
                                            <h6>LIKES</h6>
                                            <span class="font700">21,215</span>
                                        </div>
                                    </div>
                                    <div class="progress progress-xs progress-transparent custom-color-yellow mb-0 mt-3">
                                        <div class="progress-bar" data-transitiongoal="75"></div>
                                    </div>
                                    <small class="text-muted">19% compared to last week</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
               
                <div class="col-lg-4 col-md-6 col-sm-6">
                    <div class="card text-center">
                        <div class="header">
                            <h2>Connection</h2>
                        </div>
                        <div class="body pt-0">
                            <div class="row">
                                <div class="col-12 m-b-15">
                                    <h1><i class="icon-user"></i></h1>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h4 class="font-22 text-col-green font-weight-bold">
                                        <small class="font-12 text-col-dark d-block m-b-10">Following</small>
                                        1255
                                    </h4>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <h4 class="font-22 text-col-blue font-weight-bold">
                                        <small class="font-12 text-col-dark d-block m-b-10">Followers</small>
                                        3650
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="col-lg-8 col-md-12">
                    <div class="card Sales_Overview">
                        <div class="header">
                            <h2>Sales Overview</h2>
                            <ul class="header-dropdown">
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>                                
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div id="Sales_Overview" class="ct-chart"></div>
                            <div class="body xl-slategray text-center">
                                <div class="row clearfix">
                                    <div class="col-lg-4 col-md-4">
                                        <h2 class="font700">$15K</h2>
                                        <small>17% <i class="fa fa-level-up text-success"></i>
                                        Total Revenue</small>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <h2 class="font700">$1200</h2>
                                        <small>18% <i class="fa fa-level-down text-danger"></i>
                                        Current Month</small>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <h2 class="font700">$3489</h2>
                                        <small>18% <i class="fa fa-level-up text-success"></i>
                                        This Year</small>
                                    </div>
                                </div>
                            </div>                            
                        </div>
                    </div>
                </div>
               
            </div>

       

            <div class="row clearfix">
          
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="card">
                       <!--  <div class="header">
                            <h2><strong>Marketing</strong> Campaign <small>This Month </small></h2>
                            <ul class="header-dropdown">
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div> -->
                        <!-- <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-5">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <i class="fa fa-facebook fa-2x"></i>
                                            </td>
                                            <td>
                                                <p class="margin-0">Facebook Ads</p>
                                                <span>1.2k Likes, 418 Shares</span>
                                            </td>
                                            <td>
                                                <h6 class="m-b-0">$ 402</h6>
                                                <span class="text-muted">Spent</span>
                                            </td>
                                            <td class="text-right">
                                                <div class="text-success">
                                                    <i class="icon-graph"></i> 23
                                                </div>
                                                <div class="text-muted">up</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-twitter fa-2x"></i>
                                            </td>
                                            <td>
                                                <p class="margin-0">Twitter Ads</p>
                                                <span>1k Likes, 128 Shares</span>
                                            </td>
                                            <td>
                                                <h6 class="m-b-0">$ 489</h6>
                                                <span class="text-muted">Spent</span>
                                            </td>
                                            <td class="text-right">
                                                <div class="text-danger">
                                                    <i class="icon-graph"></i>
                                                    -9
                                                </div>
                                                <div class="text-muted">down</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-instagram fa-2x"></i>
                                            </td>
                                            <td>
                                                <p class="margin-0">Instagram Post</p>
                                                <span>1k Follows, 228 Likes</span>
                                            </td>
                                            <td>
                                                <h6 class="mb-0">$ 718 </h6>
                                                <span class="text-muted">Spent</span>
                                            </td>
                                            <td class="text-right">
                                                <div class=" text-success">
                                                    <i class="icon-graph"></i>
                                                    16
                                                </div>
                                                <div class="text-muted">up</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-linkedin fa-2x"></i>
                                            </td>
                                            <td>
                                                <p class="margin-0">LinkedIn Post</p>
                                                <span>1k Follows, 228 Likes</span>
                                            </td>
                                            <td>
                                                <h6 class="mb-0">$ 768</h6>
                                                <span class="text-muted">Spent</span>
                                            </td>
                                            <td class="text-right">
                                                <div class="text-success">
                                                    <i class="icon-graph"></i>
                                                    27
                                                </div>
                                                <div class="text-muted">up</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div> -->
                </div>                
            </div>
            
        </div>
    </div>
    
</div>
@endsection