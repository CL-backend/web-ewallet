@extends('main.base_main')

@section('title',$title)

@section('css')
<style>
        .clock-button {
            background-color: blueviolet;
            padding: 0.5em;
            color: white;
        }
        
        .clock-placement {
            position: fixed;
            /* Fixed/sticky position */
            top: 20px;
            /* Place the button at the top of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
        }
    </style>
@endsection

@section('content')
	
	<div id="main-content">
	
		 	<div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Withdrawal</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Withdrawal</li>
                        </ul>
                    </div>
                </div>
            </div>

             <div class="body">
             
		            <div class="clearfix">
		            	<div class="col-sm-12 col-sm-12">
		                    <div class="card">
		                        <div class="header">
		                            <h2> Financial > Withdrawal</h2>
		                        </div>

		                        <div class="body">

		                        	<div class="form-group row">
									  <div class="input-daterange input-group col-sm-8" data-provide="datepicker">
									  	<div class="col-sm-8">
                                        <small class="input-group-addon range-to">Date Range</small>
                                    
                                    	<input type="text"  class="input-sm form-control date-range" name="datefilter" />
                                       </div>
                                     </div>
									 </div>

									<div class="form-group row">
									  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										    <div class="col-sm-3">
	                                        	<small>Date Filtered</small>
	                                    
	                                    		<select class="input-sm form-control date-filtered" name="end">
	                                    			<option>Submitted Date</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Currency</small>
	                                   
	                                    		<select class="form-control form-control-xs currency" name="end">
	                                    			<option>IDR</option>
	                                    			<option>USD</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Payment Method</small>
	                                   
	                                    		<select class="input-sm form-control payment-method" name="end">
	                                    			<option>All</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Withdrawal Status</small>
	                                    	
	                                    		<select class="input-sm form-control status-withdrawal" name="end">
	                                    			<option>All</option>
	                                    		</select>
	                                        </div>

	                                     	</div>
									 </div>

									 <div class="form-group row">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-3">
	                                        	<small>Membcer Code</small>
	                                    
	                                    		<input type="text" class="input-sm form-control member-code" name="end">
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Withdrawal ID</small>
	                                   
	                                    		<input type="text" class="input-sm form-control withdrawal-id" name="end">
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Reference ID</small>
	                                   
	                                    		<input type="text" class="input-sm form-control reference-id" name="end">
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Platform</small>
	                                    	
	                                    		<select class="input-sm form-control platform" name="end">
	                                    			<option>All</option>
	                                    		</select>
	                                        </div>

	                                     </div>
									 </div>

									 <div class="form-group row">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-3">
	                                        	<small>Fund in Account</small>
	                                    
	                                    		<select class="input-sm form-control fund-in-account" name="end">
	                                    			<option>None selected</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-3">
	                                        	<small class="input-group-addon range-to">Auto Search</small>
	                                   			<div class="input-group mb-3">
		                                    		<select class="input-sm form-control auto-search" name="end">
		                                    			<option value="60">60 seconds</option>
		                                    			<option value="30">30 seconds</option>
		                                    			<option value="15">15 seconds</option>
		                                    		</select>
		                                    		<div class="input-group-prepend">
		                                                <span class="input-group-text"><input type="checkbox" class="search-time"></span>
		                                            </div>
	                                    		</div>
	                                        </div>
	                            	
	                                     </div>
									 </div>

									  
									 <span class="badge bg-danger text-white" style="width: 20px;height: 20px;"> </span> 
									 <span class="align-top"> > 60 Seconds </span>

									 <span class="badge bg-warning text-white" style="width: 20px;height: 20px;"> </span> 
									 <span class="align-top">> 30 Seconds</span>

									 <span class="badge bg-primary text-white" style="width: 20px;height: 20px;"> </span> 
									 <span class="align-top">> 15 Seconds</span> 

									 <span class="badge bg-info text-white" style="width: 20px;height: 20px;"> </span>
									 <span class="align-top">> 0 Seconds</span>

									 <div class="form-group row float-right">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-12">
	                                        	<button class="btn btn-primary" data-toggle="modal" data-target="#defaultModal1" ><i class="fa fa-plus-circle"></i> Submit Withdrawal</button>
	                                   
	                                        	<button class="btn btn-primary search"><i class="icon-reload"></i> Search</button>
	                                      
	                                        	<button class="btn btn-primary export"><i class="fa fa-download"></i> Export</button>
	                                        </div>
	                                      

	                                     </div>
									 </div>
		               
		                        </div>

		                        <div class="col-xs-12 mr-1 ml-1">
				                    <div class="card">
				                      
				                        
				                            <div class="table-responsive">
				                                <table class="table table-bordered table-hover dataTable table-custom" id="examples">
				                                   <thead class="thead-dark">
				                                        <tr>
				                                            <th>No</th>
				                                            <th>Member Code</th>
				                                            <th>Currency</th>
				                                            <th>Withdrawal ID</th>
				                                            <th>Payment Method</th>
				                                            <th>Payment Gateaway</th>
				                                            <th>Bank Name</th>
				                                            <th>Amount</th>
				                                            <th>Bank Charge</th>
				                                            <th>Status</th>
				                                            <th>Submitted Date</th>
				                                            <th>Updated Date</th>
				                                            <th>Updated By</th>
				                                        </tr>
				                                    </thead>
				                                   
				                                    
				                                </table>
				                            </div>
				                      
				                    </div>
				                </div>

		                    </div>
		              
		                </div>
		           
		
	</div>



	<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Withdrawal</h4>
                </div>
                <div class="modal-body"> 

                                <form class="form-auth-small">
                                    <div class="form-group">
	                                    <label>Member Code</label>
	                                    <input type="text" class="form-control member_code" required="">
	                                </div>   

	                                 <div class="form-group">
	                                    <label>Currency</label>
	                                     <select class="input-sm form-control currencys" name="end">
	                                    	<option value="IDR">IDR</option>
	                                    	<option value="USD">USD</option>
	                                    </select>
	                                </div>     

	                                 <div class="form-group">
	                                 	 <label>Payment Method</label>
	                                    <select class="input-sm form-control payment-methods" name="end">
	                                    	<option value="Bank Transfer">Bank Transfer</option>
	                                    </select>
	                                </div>   

	                                 <div class="form-group">
	                                    <label>Bank Name</label>
	                                     <select class="input-sm form-control bank_names" name="end">
	                                    	<option value="BNI">BNI</option>
	                                    	<option value="BRI">BRI</option>
	                                    </select>
	                                </div>   

	                                 <div class="form-group">
	                                    <label>Amount</label>
	                                    <input type="text" class="form-control amount" required="">
	                                </div>   

            
                    </form>
                        <div class="modal-footer">
                    <input type="submit" class="btn btn-primary submit" value="SAVE CHANGES">
                    <a class="btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
                </div>
                       </div>
            </div>
        </div>
    </div>


  

@endsection

@section('javascript')
<script type="text/javascript">

	
$(document).ready(function() {
	

	$(function() {
	  $('input[name="datefilter"]').daterangepicker({
	    timePicker: true,
	    startDate: moment().startOf('hour'),
	    endDate: moment().startOf('hour').add(32, 'hour'),
	    locale: {
	      format: 'YYYY-MM-DD'
	    }
	  });
	});



   $('#examples').DataTable({
     	
   		// "searching":false,

        "ajax": "{{site_url()}}index.php/Financial/get_withdrawal",
         "columns": [
            { "data": "id" },
            { "data": "member_code" },
            { "data": "currency" },
            { "data": "id" },
            { "data": "payment_method" },
            { "data": "payment_gateaway" },
            { "data": "bank_name" },
            { "data": "amount" },
            { "data": "bank_charge" },
            { "data": "status" },
            { "data": "submit_date" },
            { "data": "update_date" },
            { "data": "update_by" }
        ],
        dom: 'rtip',
        buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title: 'Withdrawal Data'
            }
        	
        ]
    });



	


   $(".search").on("click", function (e) {
	   e.preventDefault();
	   
	   search();
	
	   $('#examples').DataTable().draw();
	});

   
   	$(".search-time").change(function() {
	    if(this.checked) {
	   var time = $('.auto-search').val();
	   var spinHandle = loadingOverlay.activate();
            
	   setTimeout(function(){ 
		
             
        loadingOverlay.cancel(spinHandle);
		search();
	   $('#examples').DataTable().draw();

	        }, (time*1000));
	    }
	});

} );

$(".export").on("click", function() {
    $('#examples').DataTable().button( '.buttons-pdf' ).trigger();
});

	$('.submit').click(function(event){
		var member_code = $('.member_code').val();
		var currency = $('.currencys').children("option:selected").val();
		var payment_method = $('.payment-methods').children("option:selected").val();
		var bank_name = $('.bank_names').children("option:selected").val();
		var amount = $('.amount').val();
					

		jQuery.ajax({
			type:"Post",
			url:"{{site_url()}}index.php/financial/insert_withdrawal",
			dataType:'json',
			data:{member_code:member_code,currency:currency,payment_method:payment_method,bank_name:bank_name,amount:amount },

			success:function(res){
				
				
					$("#defaultModal1").modal('hide');
					toastr.remove();
					$context = res.context;
					$positionClass = 'toast-top-center';
					$message = res.status;
	                toastr[$context]($message, '', {
	                    positionClass: $positionClass
	                });
	                $('#examples').DataTable().ajax.reload();
				
			}
		});
	});


function search(){
       $.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
		   console.log(data);
		   var date_range = $('.date-range').val();
		   var dates = date_range.split(" - ");
		   var start = dates[0];
		   var end = dates[1];

		    var createdAt = data[10] || 0; // Our date column in the table

		    if ((start == "" || end == "") || (moment(createdAt).isSameOrAfter(start) && moment(createdAt).isSameOrBefore(end))) {
		    	console.log("hasil "+ createdAt+" "+start+" "+end);
		      	return true;
		    }if($('.currency').children("option:selected").val()==data[2]){
		    	return true;
		    }if($('.payment-method').children("option:selected").val()==data[3]){
		    	return true;
		    }if($('.status-withdrawal').children("option:selected").val()==data[8]){
		    	return true;
		    }if($('.payment-method').children("option:selected").val()==data[4]){
		    	return true;
		    }if($('.member-code').val()==data[1]){
		    	return true;
		    }if($('.withdrawal-id').val()==data[0]){
		    	return true;
		    // }if($('.reference-id').val()==){
		    // 	return true;
		    // }if($('.platform').children("option:selected").val()==){
		    // 	return true;
		    // }if($('.fund-in-account').children("option:selected").val()==){
		    // 	return true;
		    // }
		}else{
		    
		    	return false;
                  
            }
            return false;
		}
	);
   }

</script>
@endsection


	   								