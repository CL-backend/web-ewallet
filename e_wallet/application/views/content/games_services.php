@extends('main.base_main')

@section('title', $title)

@section('content')


    <div id="main-content">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Games Service List</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Service</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="body">
             
                    <div class="clearfix">
                        <div class="col-sm-12 col-sm-12">
                            <div class="card">

                                <div class="body">

                                    <div class="form-group row">
                                      <div class="input-daterange input-group col-sm-8" data-provide="datepicker">
                                        <div class="col-sm-8">
                                            <form action="{{site_url()}}index.php/games/sync_act">
                                                <input type="submit" class="btn btn-primary" value="Synchronize">
                                            </form>
                                       </div>
                                     </div>
                                     </div>
                                    
                                            <div class="table-responsive">
                                                <table class="table table-bordered table-hover dataTable table-custom" id="myTable" style="width: 100%">
                                                   <thead class="thead-dark">
                                                        <tr>
                                                            <th>Game ID</th>
                                                            <th>Wallet ID</th>
                                                            <th>Game Name</th>
                                                            <th>Icon</th>
                                                            <th>Status</th>
                                                            <th>Option</th>
                                                        </tr>
                                                    </thead>
                                                  
                                                    
                                                </table>
                                            </div>
                                      
                                    </div>
                                </div>
                            </div>
                    </div>
    
            </div>
    
    </div>


    <div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="titles" id="defaultModalLabel">Edit Games Service</h6>
            </div>
            
            <div class="modal-body" > 
                <form class="form-auth-small" enctype="multipart/form-data" method="post" id="form" action="">
                    <div class="form-group">
                        <label for="signup-first" class="control-label sr-only">Game Name</label>
                            <input type="text" class="form-control game_name" name="game_name" placeholder="Game Name" required>
                    </div>

                     <div class="form-group">
                        <label for="signup-email" class="control-label">Icon</label>
                        <input type="file" class="dropify form-control" name="icon" id="icon">
                    </div>   

                    <div class="form-group">
                        <select class="form-control status" id="status" name="status" value="1">
                            <option class="form-check-label" value="0">Not Active</option>
                            <option class="form-check-label" value="1">Active</option>
                        </select>
                    </div>
                            
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-primary submit" value="SAVE CHANGES">
                        <a class="btn-close btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
                    </div>   
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script type="text/javascript">
    var t = $('#myTable').DataTable({
        
        // "searching":false,

        "ajax": "{{site_url()}}index.php/games/data",
         "columns": [
            { "data": "gameID" },
            { "data": "walletID" },
            { "data": "gameName" },
            { "data": "icon",
            "render": function (data, type, row) {
                    if(row.icon!=null)
                        return '<img src="'+data+'">'
                    else
                        return '<span><b>No Icon</b></span>'
                }
            },
            { "data": "isActive",
            "render": function (data, type, row) {
                    
                    if (row.isActive  == 0) {
                        return "<font color='red'><b>Not Active</font></b>";
                    }else if (row.isActive  == 1) {
                        return "<font color='green'><b>Active</font> </b>";
                    }
                }
            },
            {
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, row) {
                            return '<a href="javascript:void(0)" class="badge badge-info edit" data-id="' + row.gameID + '">' +
                            'Edit';
                         }
                },
        ]
    });


    $('#myTable tbody').on('click', 'a.edit', function () {
             // Get the id of selected phone and assign it in a variable called phoneData
        var id = $(this).data().id;
                // Start AJAX function
        $.ajax({
                 // Path for controller function which fetches selected phone data
            url:"{{site_url()}}index.php/games/by_id/"+id,
                    // Method of getting data
            method: "POST",
                    // Data is sent to the server
            dataType:'json',
                    // Callback function that is executed after data is successfully sent and recieved
            success: function(data){
                $('.game_name').val(data['gameName']);
                $('.status').val(data['isActive']).trigger("change");
      
                var drEvent = $('.dropify').dropify();
                // drEvent = drEvent.data('dropify');
                // drEvent.clearElement();
                // drEvent.settings.defaultFile = data['icon'];
                // drEvent.destroy();
                // drEvent.init();
                // $('.dropify#icon').dropify({
                // defaultFile: data['icon'],
                // });

                $("#form").attr("action", "{{site_url()}}index.php/games/edit/"+id);
                $('#defaultModal1').modal('show');
            }
         });
             // End AJAX function
    });

</script>
@endsection