@extends('main.base_main')
@section('title',$title)
@section('content')

  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>User List</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">App</li>
                            <li class="breadcrumb-item active">Contact List</li>
                        </ul>
                    </div>
                </div>
            </div>

   <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Admin / Staff List</h2>
                            <span>{{$message}}</span>
                            <ul class="header-dropdown">
                              <!--   <li><a href="javascript:void(0);" data-toggle="modal" data-target="#addcontact"><i class="icon-plus"></i></a></li> -->
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="#defaultModal1" data-toggle="modal" data-target="#defaultModal1">Add New</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 c_list" id="myTable" width="100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <!-- <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th> -->
                                            <th>First Name</th>                                    
                                            <th>Last Name</th>                                    
                                            <th>Email</th>
                                            <th>Account Type</th>
                                           <!--  <th>Sub Account</th> -->
                                            @if(get_group()=="Administrator")   
                                            <th>Action</th>
                                            @endif
                                        </tr>
                                    </thead>
                                        <tbody>
                                        @foreach($data as $row)
                                        <tr>
                                           <!--  <td style="width: 50px;">
                                            <label class="fancy-checkbox">
                                                    <input class="checkbox-tick" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </td> -->
                                            <td>
                                                
                                                <p class="c_name">{{$row->first_name}}</p>
                                            </td>
                                            <td>
                                                <p class="c_name">{{$row->last_name}}</p>
                                            </td>                                   
                                            <td>
                                                <address><i class="fa fa-envelope-o"></i>{{$row->email}}</address>
                                            </td>
                                            <td>
                                                <address><span class="badge badge-success">{{$row->name}}</span></address>
                                            </td>

                                             <!-- <td>

                                                @if($row->account_sub_type==1)
                                                     <address><span class="badge badge-info">Admin</span></address>
                                                @else
                                                     <address><span class="badge badge-info">Staff</span></address>
                                                @endif

                                               
                                            </td> -->
                                            
                                            @if(get_group()=="Administrator") 
                                            <td>                                           
                                                <a href="{{site_url()}}index.php/Admin/data_member/{{$row->id}}" class="btn btn-edit btn-link btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                                <a href="javascript:void(0)" data-type="{{$row->id}}" class="btn btn-link btn-sm" title="Delete"><i class="fa fa-trash-o text-danger"></i></a>
                                            </td>    
                                            @endif
                                            
                                        </tr>
                                        @endforeach   
                                     
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Add new account</h4>
                </div>
                <div class="modal-body"> 

                                <form class="form-auth-small" method="post" action="{{site_url()}}index.php/Admin/create">
                                    <div class="form-group">
                                        <label for="signup-first" class="control-label sr-only">First Name</label>
                                        <input type="text" class="form-control" name="first_name" placeholder="Your first name" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-last" class="control-label sr-only">Last Name</label>
                                        <input type="text" class="form-control" name="last_name" placeholder="Your last name"  required>
                                    </div>
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Your email"  required>
                                    </div>   
                                    <div class="form-group">
                                        <label for="signup-password" class="control-label sr-only">Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Your password" required="">
                                    </div>            
                                    <div class="form-group">
                                        <label for="signup-account_type" class="control-label sr-only">Account Type</label>
                                        <select class="form-control" name="account_type">
                                            <option value="3">Finance</option>
                                            <option value="1">Administrator</option>
                                            <option value="2">Manager</option>
                                        </select>
                                    </div>
                                            
                                                         
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="SAVE CHANGES">
                    <a class="btn-close btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
                </div>
                    </form>
                       </div>
            </div>
        </div>
    </div>




@endsection

@section('javascript')
<script>

    $(document).ready(function() {
        $('#myTable').DataTable();
    });

     $('button.btn').click(function() {  
            var id = $(this).data('type');   

            swal({
                  title: "Are you sure ?",
                  text: "After you delete it, you won't be able to recover this data !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{site_url()}}index.php/Admin/delete",
                            method: "POST",
                            data: {id: id},
                            success: function (data) {
                                location.reload();
                            }
                        });
                        swal("Your data has been deleted!", {
                          icon: "success",
                        });
                      } else {
                        swal("Your action is canceled !");
                      }
                   

                
                });
     });

    </script>
@endsection
