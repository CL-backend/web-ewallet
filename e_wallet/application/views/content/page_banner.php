@extends('main.base_main')
@section('title',$title)

@section('css')
<style type="text/css">
.dropify-font-upload:before, .dropify-wrapper .dropify-message span.file-icon:before {
    content: '\e800';
}
.dropify-font:before, .dropify-wrapper .dropify-message span.file-icon:before, .dropify-wrapper .dropify-preview .dropify-infos .dropify-infos-inner p.dropify-filename span.file-icon:before, [class*=" dropify-font-"]:before, [class^=dropify-font-]:before {
    font-family: dropify;
    font-style: normal;
    font-size:8px;
    text-decoration: inherit;
    width: 1em;
    text-align: center;
    font-variant: normal;
    text-transform: none;
    line-height: 1em;
}
</style>
@endsection

@section('content')

    <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>Banner List</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">App</li>
                            <li class="breadcrumb-item active">Banner List</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Banner List</h2>
                          
                            <ul class="header-dropdown">
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="#defaultModal1" data-toggle="modal" data-target="#defaultModal1" id="mdl">Add New</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                         <div class="col-xs-12 mr-1 ml-1">
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 c_list" id="myTable" width="100%">
                                    <thead class="thead-dark">
                                        <tr align="center">
                                            <!-- <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th> -->
                                            <th>No</th>
                                            <th>Banner Image</th>                                    
                                            <th>Promotion Title</th>     
                                            <th>Active Date</th>
                                            <th>Status Preview</th>
                                            <th>Option</th>
                                           <!--  <th>Sub Account</th> -->
                                           
                                        </tr>
                                    </thead>
                                        
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="titles" id="defaultModalLabel">Add Banner Promotion</h6>
            </div>
            
            <div class="modal-body" > 
                <form class="form-auth-small" enctype="multipart/form-data" method="post" id="form" action="{{site_url()}}index.php/banner/create">
                    <div class="form-group">
                        <label for="signup-last" class="control-label sr-only">Active Date</label>
                        <input type="text"  class="input-sm form-control date-range" name="datefilter" />
                    </div>
                    <div class="form-group">
                        <label for="signup-email" class="control-label sr-only">Image File</label>
                        <input type="file" class="dropify form-control image_file" name="file" id="file" data-default-file="">
                    </div>   

                    <div class="form-group">
                        <label for="signup-first" class="control-label sr-only">Promotion Title</label>
                            <input type="text" class="form-control promotion_title" name="promotion_title" placeholder="Promotion Title" required>
                    </div>
                    <div class="form-group">
                        <label for="signup-last" class="control-label sr-only">Promotion Content</label>
                            <textarea class="form-control promotion_content" id="promotion_content" name="promotion_content" rows="4"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="signup-last" class="control-label">Category : </label>
                        <select name="kategori" class="kategori">
                            <option value="#">Uncategorized</option>
                            <option value="Game">Game</option>
                        </select>
                    </div>

                     <div class="form-group" style="width: 100px;">
                        <label for="signup-email" class="control-label">Icon (optional)</label>
                        <input type="file" class="dropify form-control" name="icon" id="file" data-default-file="">
                    </div>   

                    <div class="form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1" name="display" value="1">
                        <label class="form-check-label" for="exampleCheck1">Display as home</label>
                    </div>
                            
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-primary submit" value="SAVE CHANGES">
                                     <a class="btn-close btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
                            </div>   
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')
<script>
$(document).ready(function() {

    var t = $('#myTable').DataTable({
        
        // "searching":false,

        "ajax": "{{site_url()}}index.php/Banner/get_data",
         "columns": [
            { "data": "id"},
            {
                render: function (data, type, row) {
                            return '<img src="'+row.banner+'">';
                        }
            },
            { "data": "promotion_title" },
            { "data": "active_date" },
              {
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, row) {
                        if (row['status_display']  == 1) {
                            return 'Active';
                        }else {
                            return 'Inactive';
                        }
                    }

                }, 
                {
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, row) {
                        if (row['status_display']  == 1) {
                            return '<a href="javascript:void(0)" class="badge badge-info edit" data-id="' + row.id + '">' +
                            'Edit' +
                            '</a>'+'<a href="javascript:void(0)" class="badge badge-success detail" data-id="' + row.id + '">' +
                            'Detail' +
                            '</a>'+'<a href="javascript:void(0)" class="badge badge-info status_display" data-statuss="' + row.status_display + '" data-id="' + row.id + '">' +
                                'Inactive</a>'
                                +
                                '<a href="javascript:void(0)" class="badge badge-danger delete" data-id="' + row.id + '">' +
                                'Delete' +
                                '</a>';
                        }else{
                            return '<a href="javascript:void(0)" class="badge badge-info edit" data-id="' + row.id + '">' +
                            'Edit' +
                            '</a>'
                            +'<a href="javascript:void(0)" class="badge badge-success detail" data-id="' + row.id + '">' +
                            'Detail' 
                            +
                            '</a>'+'<a href="javascript:void(0)" class="badge badge-info status_display" data-statuss="' + row.status_display + '" data-id="' + row.id + '">' +
                                'Active</a>'
                                +
                                '<a href="javascript:void(0)" class="badge badge-danger delete" data-id="' + row.id + '">' +
                                'Delete' +
                                '</a>';;
                        }
                    }
                },


        ],
      
    });

     t.on('order.dt search.dt', function () {
        t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        });
    });

    $('#mdl').click(function(){
        $('#defaultModal1').modal().on('shown', function(){
            $('body').css('overflow', 'hidden');
        }).on('hidden', function(){
            $('body').css('overflow', 'auto');
        })
    });

    $(function() {
      $('input[name="datefilter"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
          format: 'YYYY-MM-DD'
        }
      });
    });

    tinymce.init({
        selector: '#promotion_content',
        forced_root_block: ""
    });


    $('#myTable tbody').on('click', 'a.status_display', function () {
        var id = $(this).data().id;
        var status = $(this).data().statuss;
        var urls = '';
        if(status==1)
            urls = "{{site_url()}}index.php/banner/change_status/"+id+"/"+0;
        else
            urls = "{{site_url()}}index.php/banner/change_status/"+id+"/"+1;

        swal({
                  title: "Are you sure ?",
                  text: "After you change it, status preview will change ",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        jQuery.ajax({
                            type:"Post",
                            url:urls,
                            dataType:'json',
                        

                            success:function(res){
                                
                                    toastr.remove();
                                    $context = res.context;
                                    $positionClass = 'toast-top-center';
                                    $massage = res.massage;
                                    toastr[$context]($massage, '', {
                                        positionClass: $positionClass
                                    });
                                    $('#myTable').DataTable().ajax.reload();
                                
                            }

                        });
                        swal(
                            "Your data has been changed!", 
                            {
                                icon: "success",
                            });

                    } else {
                            swal("Your action is canceled !");
                    }
                });
        });

    $('#myTable tbody').on('click', 'a.delete', function () {
            var id = $(this).data().id;
             swal({
                      title: "Are you sure ?",
                      text: "After you delete it, you won't be able to recover this data !",
                      icon: "warning",
                      buttons: true,
                      dangerMode: true,
                    })
                    .then((willDelete) => {
                        if (willDelete) {
            

            jQuery.ajax({
                type:"Post",
                url:"{{site_url()}}index.php/banner/delete/"+id,
                dataType:'json',
                

                success:function(res){
                    toastr.remove();
                    $context = res.context;
                    $positionClass = 'toast-top-center';
                    $massage = res.massage;
                    toastr[$context]($massage, '', {
                        positionClass: $positionClass
                    });
                    $('#myTable').DataTable().ajax.reload();
                }

            });
            swal("Your data has been deleted!", {
                              icon: "success",
                            });
                          } else {
                            swal("Your action is canceled !");
                          }
                          
        });
    });


    $('#myTable tbody').on('click', 'a.detail', function () {
        var tr = $(this).closest('tr');
        var row = $("#myTable").DataTable().row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );


    $('#myTable tbody').on('click', 'a.edit', function () {
             // Get the id of selected phone and assign it in a variable called phoneData
        var id = $(this).data().id;
                // Start AJAX function
        $.ajax({
                 // Path for controller function which fetches selected phone data
            url:"{{site_url()}}index.php/banner/by_id/"+id,
                    // Method of getting data
            method: "POST",
                    // Data is sent to the server
            dataType:'json',
                    // Callback function that is executed after data is successfully sent and recieved
            success: function(data){
                $('.date-range').val(data['start_date']+' - '+data['end_date']);
                $('.promotion_title').val(data['promotion_title']);
                tinymce.get("promotion_content").setContent(data['promotion_content']);
                $('.kategori').val(data['kategori']).change();
                if (data['status_display']==1)
                    {
                        $( "#exampleCheck1").prop('checked', true);
                    }
                    else
                    {
                        $( "#exampleCheck1").prop('checked', false);
                    }
                $("#form").attr("action", "{{site_url()}}index.php/banner/edit/"+id);
                $('#defaultModal1').modal('show');
            }
         });
             // End AJAX function
    });


function format ( val ) {
    // `d` is the original data object for the row
    return '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'+
        '<tr>'+
            '<td>Promotion Content</td>'+
            '<td>'+val.promotion_content+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Category</td>'+
            '<td>'+val.kategori+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Icon</td>'+
            '<td><img src="'+val.icon+'" width="100px"></td>'+
        '</tr>'+
    '</table>';
}


});
</script>
@endsection
