@extends('main.base_main')


@section('title',$title)


@section('css')
<style>
        .clock-button {
            background-color: blueviolet;
            padding: 0.5em;
            color: white;
        }
        
        .clock-placement {
            position: fixed;
            /* Fixed/sticky position */
            top: 20px;
            /* Place the button at the top of the page */
            right: 30px;
            /* Place the button 30px from the right */
            z-index: 99;
            /* Make sure it does not overlap */
        }
    </style>
@endsection


@section('content')
	
	<div id="main-content">
	
		 	<div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Member Bank Account</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Member Bank Account</li>
                        </ul>
                    </div>
                </div>
            </div>

             <div class="body">
             
		            <div class="clearfix">
		            	<div class="col-sm-12 col-sm-12">
		                    <div class="card">
		                        <div class="header">
		                            <h2> Financial > Bank Account > Member Bank Account</h2>
		                        </div>

		                        <div class="body">


									<div class="form-group row">
									  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										    <div class="col-sm-4">
	                                        	<small>Currency</small>
	                                    
	                                    		<select class="input-sm form-control currency" name="end">
	                                    			<option value="usd">USD</option>
	                                    			<option value="idr">IDR</option>
	                                    		</select>
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Member Code</small>
	                                   			<input type="text" class="form-control member-code">
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Payment Type</small>
	                                   
	                                    		<select class="input-sm form-control payment-type" name="end">
	                                    			<option value="Withdrawal">Withdrawal</option>
	                                    			<option value="Deposit"> Deposit</option>
	                                    		</select>
	                                        </div>

	                                       

	                                     	</div>
									 </div>

									 <div class="form-group row">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	 <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Account Name</small>
	                                    		<input type="text" class="form-control account-name">
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Account Number</small>         
	                                    		<input type="text" class="input-sm form-control account-number">
	                                        </div>

	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Active</small>
	                                   			<select class="input-sm form-control active" name="end">
	                                    			<option value="active">Active</option>
	                                    			<option value="inactive">Inactive</option>
	                                    		</select>
	                                        </div>
	                                       

	                                     </div>
									 </div>

									


									 <div class="form-group row float-right">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-12">
	                                        	<button data-toggle="modal" data-target="#defaultModal1" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Create </button>
	                                   
	                                        	<button class="btn btn-primary search"><i class="icon-reload"></i> Search</button>
	                                        </div>
	                                      

	                                     </div>
									 </div>

									 
		               
		                        </div>

		                        <div class="col-xs-12 mr-1 ml-1">
				                    <div class="card">
				                      
				                        
				                            <div class="table-responsive">
				                                <table class="table table-bordered table-hover dataTable table-custom" id="examples">
				                                   <thead class="thead-dark">
				                                        <tr>
				                                            <th>No</th>
				                                            <th>Payment Type</th>
				                                            <th>Currency Code</th>
				                                            <th>Member Code</th>
				                                            <th>Account No</th>
				                                            <th>Account Name</th>
				                                            <th>Bank</th>
				                                            <th>Province</th>
				                                            <th>City</th>
				                                            <th>Branch</th>
				                                            <th>Status</th>
				                                            <th>Function</th>
				                                        </tr>
				                                    </thead>
				                                  
				                                    
				                                </table>
				                            </div>
				                       
				                    </div>
				                </div>

		                    </div>
		              
		                </div>
		           
		
	</div>



	 <div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Create</h4>
                </div>
                <div class="modal-body"> 

                <form class="form-auth-small">

                <div class="row clearfix">
                	<div class="col-md-12">
                    	<div class="card">
                     
                        <div class="body">
                            <div class="row clearfix">

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
	                                    <label>Payment Type</label>
	                                     <select class="input-sm form-control payment-type-input" name="payment-type-input">
	                                    	<option value="Deposit">Deposit</option>
	                                    	<option value="Withdrawal">Withdrawal</option>
	                                    </select>
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">
	                                 <div class="form-group">
	                                    <label>Currency Code</label>
	                                     <select class="input-sm form-control currency-input" name="currency-input">
	                                    	<option value="IDR">IDR</option>
	                                    	<option value="USD">USD</option>
	                                    </select>
	                                </div>   
	                            </div>  

	                            <div class="col-lg-6 col-md-6 col-sm-12">
	                                <div class="form-group">
	                                 	 <label>Member Code</label>
	                                     <input type="text" name="member-code-input" class="form-control member-code-input">
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">
	                                 <div class="form-group">
	                                 	 <label>Account Number</label>
	                                     <input type="text" name="account-number-input" class="form-control account-number-input">
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">
	                                 <div class="form-group">
	                                 	 <label>Account Name</label>
	                                     <input type="text" name="account-name-input" class="form-control account-name-input">
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">

	                                <div class="form-group">
	                                 	 <label>Bank</label>
	                                 	 <select class="input-sm form-control bank-input" name="bank-input">
	                                    	<option value="BNI">BNI</option>
	                                    	<option value="BCA">BCA</option>
	                                    </select>
	                                </div>
	                            </div>
	                            <div class="col-lg-6 col-md-6 col-sm-12">
	                                 <div class="form-group">
	                                 	 <label>Province</label>
	                                 	 <select class="input-sm form-control province-input" name="province-input">
	                                    	<option value="Kepulauan Riau">Kepulauan Riau</option>
	                                    	<option value="Jakarta">Jakarta</option>
	                                    </select>
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">
									 <div class="form-group">
	                                 	 <label>City</label>
	                                 	 <select class="input-sm form-control city-input" name="city-input">
	                                    	<option value="BATAM">BATAM</option>
	                                    	<option value="JAKARTA">JAKARTA</option>
	                                    </select>
	                                </div>   
	                            </div>

	                            <div class="col-lg-6 col-md-6 col-sm-12">
									 <div class="form-group">
	                                 	 <label>Branch</label>
	                                 	 <select class="input-sm form-control branch-input" name="branch-input">
	                                    	<option value="BATAM CENTRE">BATAM CENTRE</option>
	                                    	<option value="JAKARTA PUSAT">JAKARTA PUSAT</option>
	                                    </select>
	                                </div>   
	                            </div>

	                            </div>
	                        </div>
	                    </div>
	                </div>
	            </div>
   
                    </form>
                    <div class="modal-footer">
		                    <input type="submit" class="btn btn-primary submit" value="SAVE CHANGES">
		                    <a class="btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
		                </div>
                        
                       </div>
            </div>
        </div>
    </div>

<span id="clock"></span>

@endsection

@section('javascript')
<script type="text/javascript">


	$(function() {
	  $('input[name="datefilter"]').daterangepicker({
	    timePicker: true,
	    startDate: moment().startOf('hour'),
	    endDate: moment().startOf('hour').add(32, 'hour'),
	    locale: {
	      format: 'YYYY-MM-DD'
	    }
	  });
	});
	
	$('.submit').click(function(event){
        var payment_type = $('.payment-type-input').children("option:selected").val();
        var currency_code = $('.currency-input').children("option:selected").val();
        var member_code = $('.member-code-input').val();
        var account_number = $('.account-number-input').val();    
        var account_name = $('.account-name-input').val();
        var branch = $('.branch-input').children("option:selected").val();
        var bank = $('.bank-input').children("option:selected").val();
        var province = $('.province-input').children("option:selected").val(); 
        var city = $('.city-input').children("option:selected").val();   

        jQuery.ajax({
            type:"Post",
            url:"{{site_url()}}index.php/Bank/create",
            dataType:'json',
            data:{
                payment_type:payment_type,
                currency_code:currency_code,
                member_code:member_code,
                account_number:account_number,
                account_name:account_name,
                bank:bank,
                province:province,
                city:city,
                branch:branch,
            },

            success:function(res){
                    
                
                         $("#defaultModal1").modal('hide');
                        toastr.remove();
                        $context = res.context;
                        $positionClass = 'toast-top-center';
                        $message = res.status;
                        toastr[$context]($message, '', {
                            positionClass: $positionClass
                        });
                        $('#examples').DataTable().ajax.reload();
                              
            }
        });
    });


   var t = $('#examples').DataTable({
     	
   		// "searching":false,

        "ajax": "{{site_url()}}index.php/Bank/data",
         "columns": [
            { "data": "id" },
            { "data": "payment_type" },
            { "data": "currency_code" },
            { "data": "member_code" },
            { "data": "account_number" },
            { "data": "account_name" },
            { "data": "bank" },
            { "data": "province" },
            { "data": "city" },
            { "data": "branch" },
             {
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, row) {
                    
        			if (row["status"] == 0) {
            			return 'Inactive';
            		}
            		else {
            			return 'Active';
            		}
                        
                       
                    }
                },
             {
                    orderable: false,
                    className: 'text-center',
                    render: function (data, type, row) {
                  		if (row["status"] == 0) {
	            			 return '<a href="javascript:void(0)" class="unbanned" data-id="' + row.id + '">' +
                            '<i class="icon-check"><i> ' +
                            '</a>';
	            		}
	            		else {
	            			 return '<a href="javascript:void(0)" class="banned" data-id="' + row.id + '">' +
                            '<i class="icon-ban"><i> ' +
                            '</a>';
	            		}
                       
                       
                    }
                },

        ],
        dom: 'frtip',
        buttons: [{
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL',
                title: 'Bank Account Data'
            }
        	
        ]
    });

    t.on('order.dt search.dt', function () {
	    t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	        cell.innerHTML = i+1;
	    });

	});

   $(".search").on("click", function (e) {
	   e.preventDefault();
		search();
	   $('#examples').DataTable().draw();
	});




$(".export").on("click", function() {
    $('#examples').DataTable().button( '.buttons-pdf' ).trigger();
});


function search(){
  $.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			console.log(data);
			

		    if($('.payment-type').children("option:selected").val()==data[1]){
		    	return true;
		    }if($('.currency').children("option:selected").val()==data[2]){
		    	return true;
		    }if($('.member-code').val()==data[3]){
		    	return true;
		    }if($('.account-number').val()==data[4]){
		    	return true;
		    }if($('.account-name').val()==data[5]){
		    	return true;
		    }if($('.active').children("option:selected").val()==data[10]){
		    	return true;
		    }else{
		    
		    	return false;
                  
            }
            return false;
		}
	);
}

$('#examples tbody').on('click', 'a.banned', function () {
    	var id = $(this).data().id;
         swal({
                  title: "",
                  text: "After you change it ?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
		

		jQuery.ajax({
			type:"Post",
			url:"{{site_url()}}index.php/Bank/banned/"+id,
			dataType:'json',
			

			success:function(res){
				console.log(res);
				if(res.Success==true){
					location.reload();
				}
			}

		});
		swal("Your data has been deleted!", {
                          icon: "success",
                        });
                      } else {
                        swal("Your action is canceled !");
                      }
                      
    } );

} );

$('#examples tbody').on('click', 'a.unbanned', function () {
    	var id = $(this).data().id;
         swal({
                  title: "",
                  text: "After you change it ?",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
		

		jQuery.ajax({
			type:"Post",
			url:"{{site_url()}}index.php/Bank/unbanned/"+id,
			dataType:'json',
			

			success:function(res){
				console.log(res);
				if(res.Success==true){
					location.reload();
				}
			}

		});
		swal("Your data has been deleted!", {
                          icon: "success",
                        });
                      } else {
                        swal("Your action is canceled !");
                      }
                      
    } );

} );

</script>
@endsection