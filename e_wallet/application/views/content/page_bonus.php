@extends('main.base_main')
@section('title',$title)
@section('content')

  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>Bonus List</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">App</li>
                            <li class="breadcrumb-item active">Bonus List</li>
                        </ul>
                    </div>
                </div>
            </div>

   <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Bonus List</h2>
                          
                            <ul class="header-dropdown">
                              <!--   <li><a href="javascript:void(0);" data-toggle="modal" data-target="#addcontact"><i class="icon-plus"></i></a></li> -->
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="#defaultModal1" data-toggle="modal" data-target="#defaultModal1">Add New</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="col-xs-12 mr-1 ml-1">
                        
                            <div class="table-responsive">
                                <table class="table table-hover m-b-0 c_list" id="myTable" width="100%">
                                    <thead class="thead-dark">
                                        <tr>
                                            <!-- <th>
                                                <label class="fancy-checkbox">
                                                    <input class="select-all" type="checkbox" name="checkbox">
                                                    <span></span>
                                                </label>
                                            </th> -->
                                            <th>Member Code</th>
                                            <th>Username</th>                                    
                                            <th>Date</th>                                    
                                            <th>Bonus</th>
                                            <th>Source Bonus</th>
                                           <!--  <th>Sub Account</th> -->
                                           
                                        </tr>
                                    </thead>
                                        
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="titles" id="defaultModalLabel">Add new notice</h4>
                </div>
                <div class="modal-body"> 

                                <form class="form-auth-small">
                                    <div class="form-group">
                                        <label for="signup-first" class="control-label sr-only">Member Code</label>
                                        <input type="text" class="form-control member_code" placeholder="member code" required>
                                    </div>
                                   
                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Date</label>
                                        <input type="date" class="form-control date">
                                    </div>   

                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Bonus</label>
                                        <input type="text" class="form-control bonus" placeholder="bonus value">
                                    </div>   

                                    <div class="form-group">
                                        <label for="signup-email" class="control-label sr-only">Source Bonus</label>
                                        <input type="text" class="form-control source_bonus" placeholder="source bonus">
                                    </div>   
                                  
                                   
                                                         
               
                    </form>
                      <div class="modal-footer">
                            <input type="submit" class="btn btn-primary submit" value="SAVE CHANGES">
                            <a class="btn-close btn btn-primary" data-dismiss="modal" style="color: white">CLOSE</a>
                    </div>   

                       </div>
            </div>
        </div>
    </div>




@endsection

@section('javascript')
<script>

     $('#myTable').DataTable({
        
        // "searching":false,

        "ajax": "{{site_url()}}index.php/Bonus/get_bonus",
         "columns": [
            
            { "data": "user_id" },
            { "data": "username" },
            { "data": "date" },
            { "data": "bonus" },
            { "data": "source_bonus" },
              // {
              //       orderable: false,
              //       className: 'text-right',
              //       render: function (data, type, row) {
                        
              //           return '<a href="javascript:void(0)" class="badge badge-info edit" data-id="' + row.user_id + '">' +
              //               'Edit' +
              //               '</a>'
                        
              //       }
              //   },


        ],
      
    });

    $('.submit').click(function(event){
        var user_id = $('.member_code').val();
        var date = $('.date').val();
        var bonus = $('.bonus').val();
         var source_bonus = $('.source_bonus').val();       

        jQuery.ajax({
            type:"Post",
            url:"{{site_url()}}index.php/Bonus/addBonus",
            dataType:'json',
            data:{
                user_id:user_id,
                bonus:bonus,
                date:date,
                source_bonus:source_bonus,
            },

            success:function(res){
                    
                
                        $("#defaultModal1").modal('hide');
                        toastr.remove();
                        $context = res.context;
                        $positionClass = 'toast-top-center';
                        $message = res.status;
                        toastr[$context]($message, '', {
                            positionClass: $positionClass
                        });
                        $('#myTable').DataTable().ajax.reload();
                              
            }
        });
    });

     // $('#myTable tbody').on('click', 'a.edit', function () {
     //         // Get the id of selected phone and assign it in a variable called phoneData
     //            var id = $(this).data().id;
     //            // Start AJAX function
     //            $.ajax({
     //             // Path for controller function which fetches selected phone data
     //                url:"{{site_url()}}index.php/Notice/get_data/"+id,
     //                // Method of getting data
     //                method: "POST",
     //                // Data is sent to the server
     //                dataType:'json',
     //                // Callback function that is executed after data is successfully sent and recieved
     //                success: function(data){
     //                 // Print the fetched data of the selected phone in the section called #phone_result 
     //                 // within the Bootstrap modal
     //                    console.log(data['title']);
     //                    $('.title').val(data['title']);
     //                    $('.description').html(data['description']);
     //                    var nameImage =  data['image_file'];
     //                    $("#file").attr("data-default-file", nameImage);
     //                    $("#form").attr("action", "{{site_url()}}index.php/Notice/edit/"+id);
     //                    $('#defaultModal1').modal('show');
     //                }
     //         });
     //         // End AJAX function
     //     });

</script>
@endsection
