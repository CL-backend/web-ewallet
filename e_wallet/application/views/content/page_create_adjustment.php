@extends('main.base_main')
@section('title',$title)
@section('content')

  <div id="main-content">
        <div class="container-fluid">
            <div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">                        
                        <h2>Adjustment</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item">App</li>
                            <li class="breadcrumb-item active">Adjustment</li>
                        </ul>
                    </div>
                </div>
            </div>

   <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="header">
                            <h2>Adjustment List</h2>
                            
                            <ul class="header-dropdown">
                              <!--   <li><a href="javascript:void(0);" data-toggle="modal" data-target="#addcontact"><i class="icon-plus"></i></a></li> -->
                                <li> <a href="javascript:void(0);" data-toggle="cardloading" data-loading-effect="pulse"><i class="icon-refresh"></i></a></li>
                                <li><a href="javascript:void(0);" class="full-screen"><i class="icon-size-fullscreen"></i></a></li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"></a>
                                    <ul class="dropdown-menu dropdown-menu-right animated bounceIn">
                                        <li><a href="#defaultModal1" data-toggle="modal" data-target="#defaultModal1">Add New</a></li>
                                        <!-- <li><a href="javascript:void(0);">Another Action</a></li>
                                        <li><a href="javascript:void(0);">Something else</a></li> -->
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                 <table class="table table-bordered table-hover js-basic-example dataTable table-custom">
                                    <thead class="thead-dark">
                                        <tr>
                                            
                                            <th>Member Code</th>                                    
                                            <th>Wallet</th>                                    
                                            <th>Category</th>
                                            <th>Adjustment Amount</th>
                                            <th>Currency</th>
                                            <th>Account Balance</th>
                                            <th>Reference ID</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($data->result() as $row)
                                        <tr>

                                            <td>
                                                <p class="c_name">{{$row->member_code}}</p>
                                            </td> 

                                            <td>
                                               <p class="c_name">{{$row->wallet}}</p>
                                            </td> 

                                            <td>
                                              <p class="c_name"> {{$row->category}}</p>
                                            </td> 

                                            <td>
                                              <p class="c_name">  {{$row->adjustment_amount}}</p>
                                            </td> 

                                            <td>
                                               <p class="c_name">{{$row->currency}}</p>
                                            </td> 

                                            <td>
                                               <p class="c_name">{{$row->account_balance}}</p>
                                            </td> 

                                             <td>
                                               <p class="c_name">{{$row->reference_id}}</p>
                                            </td> 
                                            
                                       
                                           <td>                                           
                                              <!--   <a href="{{site_url()}}index.php/Admin/data_member/{{$row->id}}" class="btn btn-edit btn-link btn-sm" title="Edit"><i class="fa fa-edit"></i></a> -->
                                               <p class="c_name"> <a href="javascript:void(0)" data-type="{{$row->id}}" class="btn delete btn-danger btn-sm" title="Delete">Delete</a></p>
                                            </td> 
                                          
                                        </tr>
                                        @endforeach
                                     
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="defaultModal1" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="title" id="defaultModalLabel">Create</h4>
                     <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <div class="modal-body"> 

        <form class="form-group" method="post" action="{{site_url()}}index.php/Adjustment/create">
            <div class="row clearfix">
                <div class="col-md-12">
                    <div class="card">
                     
                        <div class="body">
                            <div class="row clearfix">

                            
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="phone" class="control-label mr-4">Member Code</label>
                                        <select id="member_code" name="member_code" class="form-control member_code">
                                            <option value=""></option>
                                            @foreach($code_member->result() as $row)
                                                <option value="{{$row->member_code}}">{{$row->member_code}}</option>
                                            @endforeach
                                        </select>
                                   
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="phone-ex" class="control-label mr-4">Currency</label>
                                        <select name="currency" class="form-control Wallet">
                                            <option value="USD">USD</option>
                                            <option value="IDR">IDR</option>
                                        </select>
                                        
                                    </div>        
                                </div>
                               
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="ssn" class="control-label mr-4">Wallet</label>
                                        <select name="wallet" class="form-control Wallet">
                                            <option value="Main">Main</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="product-key" class="control-label mr-4">Account Balance</label>
                                        <input type="text" name="account_balance" class="form-control Category">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="product-key" class="control-label mr-4">Category</label>
                                        <select name="category" class="form-control Wallet">
                                            <option value="Bonus">Bonus</option>
                                        </select>
                                        
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="product-key" class="control-label mr-4">Reference ID</label>
                                        <input type="text" name="reference_id" class="form-control Reference">
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="product-key" class="control-label mr-4">Adjustment Amount</label>
                                        <input type="text" name="adjustment_amount" class="form-control Adjustment">
                                    </div>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <div class="form-group">
                                        <label for="product-key" class="control-label mr-4">Note</label>
                                        <textarea type="text" name="note" class="form-control Note" rows="2"></textarea>
                                    </div>
                                </div>

                              

                            </div>
                        </div>
                    </div>
                </div>
            </div>
                                            
                                                         
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="SAVE CHANGES">
                    <a class="btn reset btn-primary" style="color: white">Reset</a>
                </div>
                    </form>
                       </div>
            </div>
        </div>
    </div>




@endsection

@section('javascript')
<script>

      $("#member_code").select2();

      $('a.btn.reset').click(function() {  
            $("select.form-control.member_code").val(''); 
            $("input.form-control.Currency").val(''); 
            $("input.form-control.Wallet").val(''); 
            $("input.form-control.Account").val(''); 
            $("input.form-control.Category").val(''); 
            $("input.form-control.Reference").val(''); 
            $("input.form-control.Adjustment").val(''); 
            $("textarea.form-control.Note").val(''); 
      });

     $('a.btn.delete').click(function() {  
            var id = $(this).data('type');   

            swal({
                  title: "Are you sure ?",
                  text: "After you delete it, you won't be able to recover this data !",
                  icon: "warning",
                  buttons: true,
                  dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            url: "{{site_url()}}index.php/Adjustment/delete",
                            method: "POST",
                            data: {id: id},
                            success: function (data) {
                                location.reload();
                            }
                        });
                        swal("Your data has been deleted!", {
                          icon: "success",
                        });
                      } else {
                        swal("Your action is canceled !");
                      }
                   

                
                });
     });

    </script>
@endsection
