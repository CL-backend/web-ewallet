@extends('main.base_main')

@section('title',$title)

@section('content')
	
	<div id="main-content">
	
		 	<div class="block-header">
                <div class="row">
                    <div class="col-lg-5 col-md-8 col-sm-12">
                        <h2>Fund Transfer</h2>
                    </div>            
                    <div class="col-lg-7 col-md-4 col-sm-12 text-right">
                        <ul class="breadcrumb justify-content-end">
                            <li class="breadcrumb-item"><a href="index.html"><i class="icon-home"></i></a></li>                            
                            <li class="breadcrumb-item active">Fund Transfer</li>
                        </ul>
                    </div>
                </div>
            </div>

             <div class="body">
             
		            <div class="clearfix">
		            	<div class="col-sm-12 col-sm-12">
		                    <div class="card">
		                        <div class="header">
		                            <h2> Financial > Transfer > Fund Transfer</h2>
		                        </div>

		                        <div class="body">

		                        	<div class="form-group row">
									  <div class="input-daterange input-group col-sm-8" data-provide="datepicker">
									  	<div class="col-sm-8">
                                        <small class="input-group-addon range-to">Date Range</small>
                                    
                                    	<input type="text"  class="input-sm form-control date-range" name="datefilter" />
                                       </div>
                                     </div>
									 </div>

									<div class="form-group row">
									  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										 
	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Currency</small>
	                                   
	                                    		<select class="form-control form-control-xs" name="end">
	                                    			<option>IDR</option>
	                                    			<option>USD</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Fund Out Status</small>
	                                   
	                                    		<select class="input-sm form-control" name="end">
	                                    			<option>All</option>
	                                    		</select>
	                                        </div>
	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Fund In Status</small>
	                                    	
	                                    		<select class="input-sm form-control" name="end">
	                                    			<option>All</option>
	                                    		</select>
	                                        </div>

	                                     	</div>
									 </div>

									 <div class="form-group row">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-4">
	                                        	<small>Reference ID</small>
	                                    
	                                    		<input type="text" class="input-sm form-control" name="end">
	                                        </div>
	                                        <div class="col-sm-4">
	                                        	<small class="input-group-addon range-to">Member Code</small>
	                                   
	                                    		<input type="text" class="input-sm form-control" name="end">
	                                        </div>
	                                     
	                                     </div>
									 </div>


									 <div class="form-group row float-right">
										  <div class="input-daterange input-group col-sm-12" data-provide="datepicker">
										  	<div class="col-sm-12">
	                                        
	                                   			<button class="btn btn-primary export"><i class="fa fa-download"></i> Export</button>
	                                        	<button class="btn btn-primary search"><i class="fa fa-search"></i> Search</button>
	                                        	
	                                        </div>
	                                      

	                                     </div>
									 </div>

									 <br>
									 <br>
									 <br>

									   <div class="form-group row float-right">
									 	 <div class="col-sm-12">
	                                        	
	                                   			<div class="input-group mb-12">
		                                    		<input type="text" class="input-sm form-control auto-search" placeholder="Search" >
		                                    		
		                                    		<div class="input-group-prepend">
		                                               
		                                                	<button type="button" class="note-btn btn btn-default btn-sm dropdown-toggle" tabindex="-1" data-toggle="dropdown" title="" data-original-title="Table"><i class="note-icon-table"></i> <span class="note-icon-caret"></span></button>
		                                                
		                                            </div>
	                                    		</div>
	                                        </div>
		                               
		                               </div>

		                        </div>

		                       
		               

		                         <div class="col-xs-12 mr-1 ml-1">
				                    <div class="card">
				                   
				                            <div class="table-responsive">
				                                <table class="table table-bordered table-hover dataTable table-custom" id="examples">
				                                   <thead class="thead-dark">
				                                        <tr>
				                                            <th>No</th>
				                                            <th>Reference ID</th>
				                                            <th>Member Code</th>
				                                            <th>Currency</th>
				                                            <th>Source Wallet</th>
				                                            <th>Target Wallet</th>
				                                            <th>Amount</th>
				                                            <th>Transfer Date</th>
				                                            <th>Fund Out Status</th>
				                                                                                    
				                                        </tr>
				                                    </thead>
				                                   
				                                    
				                                </table>
				                            </div>
				                        
				                    </div>
				                </div>

		                    </div>
		              
		                </div>
		           
		
	</div>
@endsection

@section('javascript')
<script type="text/javascript">

	$(function() {
	  $('input[name="datefilter"]').daterangepicker({
	    timePicker: true,
	    startDate: moment().startOf('hour'),
	    endDate: moment().startOf('hour').add(32, 'hour'),
	    locale: {
	      format: 'YYYY-MM-DD'
	    }
	  });
	});

$('#examples').DataTable({
     	
   		// "searching":false,

        "ajax": "{{site_url()}}index.php/transfer/get_pending_transfer",
         "columns": [
            { "data": "id" },
            { "data": "reference_id" },
            { "data": "member_code" },
            { "data": "currency" },
            { "data": "source_wallet" },
            { "data": "target_wallet" },
            { "data": "amount" },
            { "data": "transfer_date" },
            { "data": "fund_out_status",
	            "render": function (data, type, row) {
	 				
			        if (row.fund_out_status  == 0) {
			            return 'Pending'
			        }else {
			        	return false;
			        }
				}

			}
            
        ],
        dom: 'rtip',
        buttons: [{
                extend: 'pdfHtml5',
                orientation: 'potrait',
                pageSize: 'LEGAL',
                title: 'Fund Transfer Data'
            }
        	
        ]
    });

 $(".search").on("click", function (e) {
	   e.preventDefault();
		search();
	   $('#examples').DataTable().draw();
	});

   
   	$(".search-time").change(function() {
	    if(this.checked) {
	   var time = $('.auto-search').val();
	   var spinHandle = loadingOverlay.activate();
            
	  
	   setTimeout(function(){ 
	   loadingOverlay.cancel(spinHandle);
	 search();
	 $('#examples').DataTable().draw();

	        }, (time*1000));
	    }
	});


$(".export").on("click", function() {
    $('#examples').DataTable().button( '.buttons-pdf' ).trigger();
});

function search(){
  $.fn.dataTable.ext.search.push(
		function( settings, data, dataIndex ) {
			console.log(data);
			var date_range = $('.date-range').val();
		   var dates = date_range.split(" - ");
		   var start = dates[0];
		   var end = dates[1];

		    var createdAt = data[7] || 0; // Our date column in the table

		    if ((start == "" || end == "") || (moment(createdAt).isSameOrAfter(start) && moment(createdAt).isSameOrBefore(end))) {
		    	console.log("hasil "+ createdAt+" "+start+" "+end);
		      	return true;
		    }if($('.currency').children("option:selected").val()==data[2]){
		    	return true;
		    }if($('.payment-method').children("option:selected").val()==data[3]){
		    	return true;
		    }if($('.status-withdrawal').children("option:selected").val()==data[8]){
		    	return true;
		    }if($('.payment-method').children("option:selected").val()==data[4]){
		    	return true;
		    }if($('.member-code').val()==data[1]){
		    	return true;
		    }if($('.withdrawal-id').val()==data[0]){
		    	return true;
		    // }if($('.reference-id').val()==){
		    // 	return true;
		    // }if($('.platform').children("option:selected").val()==){
		    // 	return true;
		    // }if($('.fund-in-account').children("option:selected").val()==){
		    // 	return true;
		    // }
		}else{
		    
		    	return false;
                  
            }
            return false;
		}
	);
}



</script>
@endsection