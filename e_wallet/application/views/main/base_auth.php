@php
defined('BASEPATH') OR exit('No direct script access allowed');
@endphp

<!doctype html>
<html lang="en">

<head>
<title>:: {{$title}} :: Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Mplify Bootstrap 4.1.1 Admin Template">
<meta name="author" content="ThemeMakker, design by: ThemeMakker.com">

<link rel="icon" href="favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<!-- MAIN CSS -->
<link rel="stylesheet" href="{{APP_ASSETS}}css/main.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/color_skins.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/ecommerce.css">

</head>

<body class="theme-blue">

@yield('content')

<!-- Javascript -->



@section('javascript')
<script src="{{APP_ASSETS}}bundles/libscripts.bundle.js"></script>    
<script src="{{APP_ASSETS}}bundles/vendorscripts.bundle.js"></script>

<script src="{{APP_ASSETS}}bundles/chartist.bundle.js"></script>
<script src="{{APP_ASSETS}}bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script src="{{APP_ASSETS}}bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script src="{{APP_ASSETS}}vendor/flot-charts/jquery.flot.selection.js"></script>

<script src="{{APP_ASSETS}}bundles/mainscripts.bundle.js"></script>
<script src="{{APP_ASSETS}}js/index.js"></script>
@show

</body>
</html>