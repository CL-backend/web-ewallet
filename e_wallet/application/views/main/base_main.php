@php
defined('BASEPATH') OR exit('No direct script access allowed');
@endphp

<!doctype html>
<html lang="en">

<head>
<title>:: {{$title}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name="description" content="Mplify Bootstrap 4.1.1 Admin Template">
<meta name="author" content="ThemeMakker, design by: ThemeMakker.com">

<link rel="icon" href="{{APP_ASSETS}}images/favicon.ico" type="image/x-icon">
<!-- VENDOR CSS -->
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/animate-css/animate.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/font-awesome.min.css" />
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/chartist/css/chartist.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/sweetalert/sweetalert.css"/>
<!-- MAIN CSS -->
<link rel="stylesheet" href="{{APP_ASSETS}}css/main.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/color_skins.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/dropify/css/dropify.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/toastr/toastr.min.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/buttons.dataTables.min.css" />
<link rel="stylesheet" href="{{APP_ASSETS}}css/daterangepicker.css" />
<link rel="stylesheet" href="{{APP_ASSETS}}vendor/summernote/dist/summernote.css">
<link rel="stylesheet" href="{{APP_ASSETS}}css/select2.css" rel="stylesheet" />
@section('css')

@show

</head>

<body class="theme-blue">



<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img src="{{APP_ASSETS}}images/thumbnail.png" width="48" height="48" alt="Mplify"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay" style="display: none;"></div>

<div id="wrapper">

    <nav class="navbar navbar-fixed-top">
        <div class="container-fluid">

            <div class="navbar-brand">
                <a href="index.html">
                    <img src="{{APP_ASSETS}}images/logo-icon.svg" alt="Mplify Logo" class="img-responsive logo">
                    <span class="name" >{{$title}}</span>
                </a>
            </div>
            
            <div class="navbar-right">
                <ul class="list-unstyled clearfix mb-0">
                    <li>
                        <div class="navbar-btn btn-toggle-show">
                            <button type="button" class="btn-toggle-offcanvas"><i class="lnr lnr-menu fa fa-bars"></i></button>
                        </div>                        
                        <a href="javascript:void(0);" class="btn-toggle-fullwidth btn-toggle-hide"><i class="fa fa-bars"></i></a>
                    </li>
                 


                    <li>
                        <div id="navbar-menu">
                            <ul class="nav navbar-nav">
                                
                                <li class="dropdown">
                                  
                                   
                                   
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                        <i class="icon-bell"></i>
                                        <span class="notification-dot"></span>
                                    </a>
                                    <ul class="dropdown-menu animated bounceIn notifications">
                                        <li class="header"><strong>You have 4 new Notifications</strong></li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-info text-warning"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Campaign <strong>Holiday Sale</strong> is nearly reach budget limit.</p>
                                                        <span class="timestamp">10:00 AM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>                               
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-like text-success"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Your New Campaign <strong>Holiday Sale</strong> is approved.</p>
                                                        <span class="timestamp">11:30 AM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-pie-chart text-info"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Website visits from Twitter is 27% higher than last week.</p>
                                                        <span class="timestamp">04:00 PM Today</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">
                                                <div class="media">
                                                    <div class="media-left">
                                                        <i class="icon-info text-danger"></i>
                                                    </div>
                                                    <div class="media-body">
                                                        <p class="text">Error on website analytics configurations</p>
                                                        <span class="timestamp">Yesterday</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="footer"><a href="javascript:void(0);" class="more">See all notifications</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="icon-flag"></i><span class="notification-dot"></span></a>
                                    <ul class="dropdown-menu animated bounceIn task">
                                        <li class="header"><strong>Project</strong></li>
                                        <li class="body">
                                            <ul class="menu tasks list-unstyled">
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Clockwork Orange <span class="float-right">29%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-turquoise" role="progressbar" aria-valuenow="29" aria-valuemin="0" aria-valuemax="100" style="width: 29%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Blazing Saddles <span class="float-right">78%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-slategray" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 78%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Project Archimedes <span class="float-right">45%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-parpl" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 45%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Eisenhower X <span class="float-right">68%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-coral" role="progressbar" aria-valuenow="68" aria-valuemin="0" aria-valuemax="100" style="width: 68%;"></div>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:void(0);">
                                                        <span class="text-muted">Oreo Admin Templates <span class="float-right">21%</span></span>
                                                        <div class="progress">
                                                            <div class="progress-bar l-amber" role="progressbar" aria-valuenow="21" aria-valuemin="0" aria-valuemax="100" style="width: 21%;"></div>
                                                        </div>
                                                    </a>
                                                </li>                        
                                            </ul>
                                        </li>
                                        <li class="footer"><a href="javascript:void(0);">View All</a></li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown"><i class="fa fa-language"></i></a>
                                    <ul class="dropdown-menu animated flipInX choose_language">                                        
                                        <li><a href="javascript:void(0);">English</a></li>
                                        <li><a href="javascript:void(0);">French</a></li>
                                        <li><a href="javascript:void(0);">Spanish</a></li>
                                        <li><a href="javascript:void(0);">Portuguese</a></li>
                                    </ul>
                                </li>
                              <!--   <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                        
                                           
                                               
                                                <img src="{{get_user_info()->avatar}}" class="rounded" width="30" height="30" alt="">
                                                
                                    </a>
                                    <div class="dropdown-menu animated flipInY user-profile">
                                        <div class="d-flex p-3 align-items-center">
                                            <div class="drop-left m-r-10">
                                                @if(get_user_info()->avatar==="")
                                                <img src="{{APP_ASSETS}}images/ecommerce/no_image.jpg" class="rounded" width="50" height="50" alt="">
                                                @else
                                                <img src="{{get_user_info()->avatar}}" class="rounded" width="50" height="50" alt="">
                                                @endif
                                            </div>
                                            <div class="drop-right">
                                                <h4>{{get_user_info()->first_name}} {{get_user_info()->last_name}}</h4>
                                                <p class="user-name">{{get_user_info()->email}}</p>
                                            </div>
                                        </div>
                                        <div class="m-t-10 p-3 drop-list">
                                            <ul class="list-unstyled">
                                                <li><a href="javascript:void(0)"><i class="icon-lock"></i>{{get_group()}}</a></li>
                                                <li><a href="{{site_url()}}index.php/user"><i class="icon-user"></i>My Profile</a></li>
                                                
                                                <li class="divider"></li>
                                                <li><a href="{{site_url()}}index.php/authentication/logout"><i class="icon-power"></i>Logout</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </li> -->
                               
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div id="leftsidebar" class="sidebar">
        <div class="sidebar-scroll">
            <nav id="leftsidebar-nav" class="sidebar-nav">
                <ul id="main-menu" class="metismenu">
                    <li class="heading">Main</li>
                    <li class="active"><a href="{{site_url()}}index.php/dashboard"><i class="icon-home"></i><span>Dashboard</span></a></li>
                    <li class="heading">User Info</li>
                    <li><a href="{{site_url()}}index.php/user"><i class="icon-user"></i><span>Profile Detail</span></a></li>
                    <li><a href="{{site_url()}}index.php/Admin"><i class="icon-users"></i><span>Create Admin / Staff</span></a></li>

                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="fa fa-dollar"></i><span>FINANCIAL</span></a>
                        <ul>
                            <li><a href="{{site_url()}}index.php/financial/deposit">Deposit</a></li>
                            <li><a href="{{site_url()}}index.php/financial/withdrawal">Withdrawal <span class="badge-danger badge">{{total_withdraw()->total}}</span></a></li>
                            
                        </ul>

                        
                        <ul>
                            <li class="middle">
                            <a href="#uiElements" class="has-arrow"><span>Transfer</span></a>
                                <ul>
                                    <li><a href="{{site_url()}}index.php/transfer/fund_transfer">Fund Transfer</a></li>
                                    <li><a href="{{site_url()}}index.php/transfer/pending_transfer">Pending Fund Transfer</a></li>
                                    <li><a href="{{site_url()}}index.php/financial">Transaction Report</a></li>
                                </ul>
                            </li>

                            <li class="middle">
                                <a href="{{site_url()}}index.php/adjustment" class="has-arrow"><span>Adjustment</span></a>
                                <ul>
                                    <li><a href="{{site_url()}}index.php/adjustment/create_adjustment">Adjustment Management</a></li>
                                    <li><a href="{{site_url()}}index.php/adjustment/adjustment_report">Adjustment Report</a></li>
                                </ul>
                            </li>

                              <li class="middle">
                                    <a href="#uiElements" class="has-arrow"><span>Bank Account</span></a>
                                    <ul>
                                        <li><a href="{{site_url()}}index.php/Bank">Member Bank</a></li>
                                        <li><a href="{{site_url()}}index.php/financial">Withdrawal</a></li>
                                        
                                    </ul>
                                </li>

                        </ul>

                        <ul>
                            <li><a href="{{site_url()}}index.php/financial/report">Financial Reports</a></li>
                        </ul>
                  
                    </li>

                    

                    

                  

                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="fa fa-suitcase"></i><span>Local Bank Method</span></a>
                        <ul>
                            <li><a href="{{site_url()}}index.php/financial">Deposit</a></li>
                            <li><a href="{{site_url()}}index.php/financial">Withdrawal</a></li>
                            
                        </ul>
                    </li>

                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="fa fa-money"></i><span>Payment Group</span></a>
                        <ul>
                            <li><a href="{{site_url()}}index.php/financial">Deposit</a></li>
                            <li><a href="{{site_url()}}index.php/financial">Withdrawal</a></li>
                            
                        </ul>
                    </li>

                    <li class="middle">
                        <a href="#uiElements" class="has-arrow"><i class="icon-settings"></i><span>Payment Settings</span></a>
                        <ul>
                            <li><a href="{{site_url()}}index.php/financial">Deposit</a></li>
                            <li><a href="{{site_url()}}index.php/financial">Withdrawal</a></li>
                            
                        </ul>
                    </li>

                    <li class="middle">
                        <a href="{{site_url()}}index.php/Member"><i class="fa fa-group"></i><span>Member List</span></a>

                    </li>

                    <li class="middle">
                        <a href="{{site_url()}}index.php/Notice"><i class="fa fa-camera"></i><span>Notice</span></a>

                    </li>

                    <li class="middle">
                        <a href="{{site_url()}}index.php/Banner"><i class="fa fa-camera"></i><span>Banner Promotion</span></a>

                    </li>

                     <li class="middle">
                        <a href="{{site_url()}}index.php/Bonus"><i class="fa fa-bold"></i><span>Bonus</span></a>

                    </li>

                     <li class="middle">
                        <a href="{{site_url()}}index.php/Authentication/logout"><i class="icon-logout"></i><span>Logout</span></a>

                    </li>

                
                </ul>
            </nav>
        </div>
    </div>

     

@yield('content')

<!-- Javascript -->

<script type="text/javascript" src="{{APP_ASSETS}}js/jquery.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}bundles/libscripts.bundle.js"></script>    
<script type="text/javascript" src="{{APP_ASSETS}}bundles/vendorscripts.bundle.js"></script>



<script type="text/javascript" src="{{APP_ASSETS}}bundles/knob.bundle.js"></script> <!-- Jquery Knob-->
<script type="text/javascript" src="{{APP_ASSETS}}bundles/flotscripts.bundle.js"></script> <!-- flot charts Plugin Js --> 
<script type="text/javascript" src="{{APP_ASSETS}}vendor/flot-charts/jquery.flot.selection.js"></script>

<script type="text/javascript" src="{{APP_ASSETS}}bundles/mainscripts.bundle.js"></script>
<!-- <script type="text/javascript" src="{{APP_ASSETS}}js/index.js"></script> -->


<script type="text/javascript" src="{{APP_ASSETS}}bundles/morrisscripts.bundle.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/pages/forms/dropify.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}vendor/dropify/js/dropify.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/pages/ui/dialogs.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/sweetalert.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/pages/tables/jquery-datatable.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}bundles/datatablescripts.bundle.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}vendor/toastr/toastr.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/buttons.flash.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/jszip.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/pdfmake.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/vfs_fonts.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/buttons.html5.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/buttons.print.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/jquery.progressTimer.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/loadingOverlay.js"></script>

<script type="text/javascript" src="{{APP_ASSETS}}js/moment.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/daterangepicker.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/select2.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}js/select2.min.js"></script>
<script type="text/javascript" src="{{APP_ASSETS}}plugins/tinymce/js/tinymce/tinymce.min.js"></script>
@section('javascript')

@show

</body>
</html>