<?php
defined('BASEPATH') OR exit('No direct script access allowed');

define("APP_ASSETS", base_url('assets/'));
define('APP_NAME', "E Wallet");

function get_group(){
	$ci  =& get_instance();
	$ci->load->library('aauth');
	$id = $ci->aauth->get_user()->account_type;
	$group_name= $ci->aauth->get_group_name($id);
	return $group_name;
}

function get_user_info(){
	$ci =&get_instance();
	$ci->load->library('aauth');
	
	return $ci->aauth->get_user();
}

function get_balance($id_user)
{
	$ci =& get_instance();
	$query= $ci->db->query("select sum(value) as total_balance from tb_wallet where user_id='$id_user' ");
	if($query->num_rows()>0){
		return $query->row()->total_balance;
	}else{
		return 0;
	}
}

function convertDate ($date) 
    { 
        $sec = strtotime($date); 
        $date = date("M d, Y : H:i", $sec); 
        return $date; 
    } 

function convert_country($id)
{
	$ci =& get_instance();
	$query = $ci->db->query("select nicename,phonecode from country where iso='$id' ");
	return $query->row();
}

function total_withdraw(){
	$ci =& get_instance();
	$query = $ci->db->query("select count(id) as total from tb_withdraw ");
	return $query->row();
}


?>